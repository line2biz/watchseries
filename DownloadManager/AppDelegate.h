//
//  AppDelegate.h
//  DownloadManager
//
//  Created by Denis on 06.02.15.
//  Copyright (c) 2015 ComputerVerse Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <MessageUI/MFMailComposeViewController.h>

#import "KKPasscodeLock.h"
#import "MusicPlayerVC.h"
#import "SNSpecialWindow.h"
#import "WatchseriesWebCache.h"

FOUNDATION_EXTERN NSString * const kApplicationDidEnterBackground;
FOUNDATION_EXTERN NSString * const kApplicationWillEnterForeground;
FOUNDATION_EXTERN NSString * const kSpecialWindowTapNotification;

/* additional system functions */
#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)
#define IDIOM    UI_USER_INTERFACE_IDIOM()
#define IPAD     UIUserInterfaceIdiomPad

#define DBGLog( s, ... ) NSLog( @"\nFile: %@\nFunction: %s\nLine: %zd\nMessage:\n%@\n", [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __PRETTY_FUNCTION__, __LINE__, [NSString stringWithFormat:(s), ##__VA_ARGS__] )

// key to save in user defaults BOOL which tells us to use
// local player or not
FOUNDATION_EXTERN NSString * const kLocalPlayerDefaultUsage;

// key to save in user defaults BOOL screen mode automatic
FOUNDATION_EXTERN NSString * const kScreenModeAutomatic;

FOUNDATION_EXTERN NSString * const kBlockedAdsTotal;
FOUNDATION_EXTERN NSString * const kBlockedAdsCurrent;

@interface AppDelegate : UIResponder <UIApplicationDelegate, KKPasscodeViewControllerDelegate>

@property (strong, nonatomic) SNSpecialWindow *window;

@property (nonatomic, strong) MusicPlayerVC* musicPlayer;

@property (nonatomic, strong) WatchseriesWebCache *webCache;


+ (instancetype)sharedDelegate;

- (UIViewController*)topViewController;

@property (nonatomic, copy) void(^backgroundTransferCompletionHandler)();

@end

