//
//  DMCategories+Category.h
//  DownloadManager
//
//  Created by Denis on 29.12.14.
//  Copyright (c) 2014 Denis. All rights reserved.
//

#import "DMCategories.h"

@interface DMCategories (Category)

+ (void)addNewCategoryWithTitle:(NSString *)title;
+ (instancetype)addAndReturnNewCategoryWithTitle:(NSString *)title;
+ (void)addDefaultCategory;
+ (instancetype)defaultCategory;
+ (void)deleteCategory:(DMCategories *)category;
+ (void)deleteCategories:(NSArray *)categories;

@end
