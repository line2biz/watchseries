//
//  DMCategories+Category.m
//  DownloadManager
//
//  Created by Denis on 29.12.14.
//  Copyright (c) 2014 Denis. All rights reserved.
//

#import "DMCategories+Category.h"
#import "DMManager.h"

@implementation DMCategories (Category)

#pragma mark - Virtual Methods
- (void)awakeFromInsert {
    [super awakeFromInsert];
    
    NSManagedObjectContext *context = [[DMManager sharedManager] managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([self class])];
    NSSortDescriptor *sortDesc = [NSSortDescriptor sortDescriptorWithKey:@"categoryID" ascending:NO];
    fetchRequest.sortDescriptors = @[sortDesc];
    fetchRequest.fetchLimit = 1;
    NSError *error = nil;
    NSArray *results = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"%s Error inserting category: %@", __FUNCTION__, error);
    } else {
        DMCategories *category = [results firstObject];
        self.categoryID = @([category.categoryID integerValue] + 1);
        self.sortOrder = self.categoryID;
    }
    
    //    sortDesc = [NSSortDescriptor sortDescriptorWithKey:@"sortOrder" ascending:NO];
    //    error = nil;
    //    results = [context executeFetchRequest:fetchRequest error:&error];
    //    if (error) {
    //        NSLog(@"%s Error inserting category: %@", __FUNCTION__, error);
    //    } else {
    //        DMCategories *category = [results firstObject];
    //    }
}


#pragma mark - Public class methods
+ (void)addNewCategoryWithTitle:(NSString *)title
{
    NSManagedObjectContext *managedObjectContext = [[DMManager sharedManager] managedObjectContext];
    
    DMCategories *category = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([self class])
                                                           inManagedObjectContext:managedObjectContext];
    category.title = title;
    category.defaultCategory = [NSNumber numberWithBool:NO];
    
    [[DMManager sharedManager] saveContext];
}

+ (instancetype)addAndReturnNewCategoryWithTitle:(NSString *)title
{
    NSManagedObjectContext *managedObjectContext = [[DMManager sharedManager] managedObjectContext];
    
    DMCategories *category = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([self class])
                                                           inManagedObjectContext:managedObjectContext];
    category.title = title;
    category.defaultCategory = [NSNumber numberWithBool:NO];
    
    [[DMManager sharedManager] saveContext];
    
    return category;
}

+ (void)addDefaultCategory
{
    NSManagedObjectContext *managedObjectContext = [[DMManager sharedManager] managedObjectContext];
    
    DMCategories *category = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([self class])
                                                           inManagedObjectContext:managedObjectContext];
    category.title = NSLocalizedString(@"No Category", nil);
    category.defaultCategory = [NSNumber numberWithBool:YES];
    
    [[DMManager sharedManager] saveContext];
}

+ (instancetype)defaultCategory
{
    NSManagedObjectContext *context = [[DMManager sharedManager] managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:NSStringFromClass([self class])
                                   inManagedObjectContext:context]];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(defaultCategory = %@)", [NSNumber numberWithBool:YES]];
    
    [request setPredicate:pred];
    
    NSError *error;
    NSArray *objects = [context executeFetchRequest:request
                                              error:&error];
    if (error) {
        NSLog(@"\nError while fetching default category\n");
        return nil;
    }
    
    DMCategories *category = (DMCategories *)[objects firstObject];
    
    return category;
}

+ (void)deleteCategory:(DMCategories *)category
{
    if (category) {
        NSManagedObjectContext *managedObjectContext = [[DMManager sharedManager] managedObjectContext];
        
        [managedObjectContext deleteObject:category];
        [[DMManager sharedManager] saveContext];
    }
}

+ (void)deleteCategories:(NSArray *)categories
{
    NSManagedObjectContext *managedObjectContext = [[DMManager sharedManager] managedObjectContext];
    for (DMCategories *category in categories) {
        [managedObjectContext deleteObject:category];
    }
    [[DMManager sharedManager] saveContext];
}
@end
