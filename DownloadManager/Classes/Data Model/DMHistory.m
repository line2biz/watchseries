//
//  DMHistory.m
//  DownloadManager
//
//  Created by Denis on 06.02.15.
//  Copyright (c) 2015 ComputerVerse Corporation. All rights reserved.
//

#import "DMHistory.h"


@implementation DMHistory

@dynamic url;
@dynamic title;
@dynamic historyID;

@end
