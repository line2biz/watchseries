//
//  DMBookmark.m
//  DownloadManager
//
//  Created by Denis on 05.03.15.
//  Copyright (c) 2015 ComputerVerse Corporation. All rights reserved.
//

#import "DMBookmark.h"
#import "DMCategories.h"


@implementation DMBookmark

@dynamic sortOrder;
@dynamic title;
@dynamic url;
@dynamic bookmarkID;
@dynamic category;

@end
