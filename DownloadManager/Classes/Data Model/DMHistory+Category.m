//
//  DMHistory+Category.m
//  DownloadManager
//
//  Created by Denis on 06.02.15.
//  Copyright (c) 2015 ComputerVerse Corporation. All rights reserved.
//

#import "DMHistory+Category.h"
#import "DMManager.h"

@implementation DMHistory (Category)

#pragma mark - Virtual Methods
- (void)awakeFromInsert
{
    [super awakeFromInsert];
    
    NSManagedObjectContext *context = [[DMManager sharedManager] managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([self class])];
    NSSortDescriptor *sortDesc = [NSSortDescriptor sortDescriptorWithKey:@"historyID" ascending:NO];
    fetchRequest.sortDescriptors = @[sortDesc];
    fetchRequest.fetchLimit = 1;
    NSError *error = nil;
    NSArray *results = [context executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        NSLog(@"\n\n%s Error inserting history: %@\n", __FUNCTION__, error);
        
    } else {
        DMHistory *history = [results firstObject];
        self.historyID = @([history.historyID integerValue] + 1);
    }
}

#pragma mark - Public Methods
+ (void)addNewHistoryWithURL:(NSString *)url andTitle:(NSString *)title
{
    NSManagedObjectContext *managedObjectContext = [[DMManager sharedManager] managedObjectContext];
    
    DMHistory *history = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([self class])
                                                            inManagedObjectContext:managedObjectContext];
    history.url = url;
    history.title = title;
    
    [[DMManager sharedManager] saveContext];
}

+ (void)removeHistory:(DMHistory *)history
{
    NSManagedObjectContext *context = [[DMManager sharedManager] managedObjectContext];
    
    if (history) {
        [context deleteObject:history];
        [[DMManager sharedManager] saveContext];
    } else {
        NSLog(@"\nThere is no such object to delete!\n");
    }
}

+ (void)removeAllHistory
{
    NSArray *aHistoryObjs = [DMHistory allHistory];
    if (![aHistoryObjs count]) {
        return;
    }
    
    NSManagedObjectContext *managedObjectContext = [[DMManager sharedManager] managedObjectContext];
    
    for (NSManagedObject *managedObject in aHistoryObjs) {
        [managedObjectContext deleteObject:managedObject];
    }
    [[DMManager sharedManager] saveContext];
}

+ (NSArray *)allHistory
{
    NSManagedObjectContext *managedObjectContext = [[DMManager sharedManager] managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([self class])];
    NSSortDescriptor *sortDesc = [NSSortDescriptor sortDescriptorWithKey:@"historyID" ascending:NO];
    fetchRequest.sortDescriptors = @[sortDesc];
    
    NSError *error = nil;
    NSArray *results = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"\n\n%s\nError while fetching history:\n%@\n",__PRETTY_FUNCTION__, error);
    } else {
        return results;
    }
    
    return nil;
}

@end
