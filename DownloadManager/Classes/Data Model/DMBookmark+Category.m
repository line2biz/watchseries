//
//  DMBookmark+Category.m
//  DownloadManager
//
//  Created by Denis on 29.12.14.
//  Copyright (c) 2014 Denis. All rights reserved.
//

#import "DMBookmark+Category.h"
#import "DMManager.h"

@implementation DMBookmark (Category)

- (void)awakeFromInsert
{
    [super awakeFromInsert];
    
    NSManagedObjectContext *context = [[DMManager sharedManager] managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([self class])];
    
    BOOL isIDsupported = NO;
    for (NSAttributeDescription *attrDesc in self.entity.properties) {
        if ([attrDesc.name isEqualToString:@"bookmarkID"]) {
            isIDsupported = YES;
            break;
        }
    }
    NSSortDescriptor *sortDesc;
    if (isIDsupported) {
        sortDesc = [NSSortDescriptor sortDescriptorWithKey:@"bookmarkID" ascending:NO];
        fetchRequest.sortDescriptors = @[sortDesc];
    }
    fetchRequest.fetchLimit = 1;
    NSError *error = nil;
    NSArray *results;
    if (isIDsupported) {
        results = [context executeFetchRequest:fetchRequest error:&error];
        
        if (error) {
            NSLog(@"\n%s\nError inserting boomark: %@\n", __FUNCTION__, error);
        } else {
            DMBookmark *bookmark = [results firstObject];
            self.bookmarkID = @([bookmark.bookmarkID integerValue] + 1);
        }
    }
    
    sortDesc = [NSSortDescriptor sortDescriptorWithKey:@"sortOrder" ascending:NO];
    fetchRequest.sortDescriptors = @[sortDesc];
    error = nil;
    results = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"%s Error fetching bookmark: %@", __FUNCTION__, error);
    } else {
        DMBookmark *bookmark = [results firstObject];
        self.sortOrder = @([bookmark.sortOrder integerValue] + 1);
    }
}

#pragma mark - Public Methods

+ (void)addNewBookmarkWithTitle:(NSString *)title url:(NSString *)url
{
    [DMBookmark addNewBookmarkWithTitle:title url:url category:nil];
}

+ (void)addNewBookmarkWithTitle:(NSString *)title url:(NSString *)url category:(DMCategories *)categorie
{
    NSManagedObjectContext *managedObjectContext = [[DMManager sharedManager] managedObjectContext];
    
    DMBookmark *bookmark = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([self class])
                                                         inManagedObjectContext:managedObjectContext];
    bookmark.title = title;
    bookmark.url = url;
    if (categorie) {
        bookmark.category = categorie;
    }
    
    [[DMManager sharedManager] saveContext];
}

+ (NSArray *)bookmarksWithCategory:(DMCategories *)categorie
{
    NSManagedObjectContext *context = [[DMManager sharedManager] managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:NSStringFromClass([self class])
                                   inManagedObjectContext:context]];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(category = %@)", categorie];
    [request setPredicate:pred];
    
    NSError *error;
    NSArray *objects = [context executeFetchRequest:request
                                              error:&error];
    if (error) {
        NSLog(@"\nError while fetching bookmarks by categorie\n");
        return nil;
    }
    
    return objects;
}

+ (void)deleteBookmark:(DMBookmark *)bookmark
{
    if (bookmark) {
        NSManagedObjectContext *managedObjectContext = [[DMManager sharedManager] managedObjectContext];
        
        [managedObjectContext deleteObject:bookmark];
        [[DMManager sharedManager] saveContext];
    }
}

+ (void)deleteBookmarks:(NSArray *)bookmarks
{
    NSManagedObjectContext *managedObjectContext = [[DMManager sharedManager] managedObjectContext];
    
    for (DMBookmark *bookmark in bookmarks) {
        [managedObjectContext deleteObject:bookmark];
    }
    
    [[DMManager sharedManager] saveContext];
}


@end
