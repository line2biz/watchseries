//
//  DMCategories.m
//  DownloadManager
//
//  Created by Denis on 29.12.14.
//  Copyright (c) 2014 Denis. All rights reserved.
//

#import "DMCategories.h"
#import "DMBookmark.h"


@implementation DMCategories

@dynamic categoryID;
@dynamic sortOrder;
@dynamic title;
@dynamic defaultCategory;
@dynamic bookmarks;

@end
