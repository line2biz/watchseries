//
//  DMHistory+Category.h
//  DownloadManager
//
//  Created by Denis on 06.02.15.
//  Copyright (c) 2015 ComputerVerse Corporation. All rights reserved.
//

#import "DMHistory.h"

@interface DMHistory (Category)

+ (void)addNewHistoryWithURL:(NSString *)url andTitle:(NSString *)title;
+ (void)removeHistory:(DMHistory *)history;
+ (void)removeAllHistory;
+ (NSArray *)allHistory;

@end
