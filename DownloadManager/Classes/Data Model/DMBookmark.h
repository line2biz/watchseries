//
//  DMBookmark.h
//  DownloadManager
//
//  Created by Denis on 05.03.15.
//  Copyright (c) 2015 ComputerVerse Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DMCategories;

@interface DMBookmark : NSManagedObject

@property (nonatomic, retain) NSNumber * sortOrder;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSNumber * bookmarkID;
@property (nonatomic, retain) DMCategories *category;

@end
