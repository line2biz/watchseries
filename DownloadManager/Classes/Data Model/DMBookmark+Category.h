//
//  DMBookmark+Category.h
//  DownloadManager
//
//  Created by Denis on 29.12.14.
//  Copyright (c) 2014 Denis. All rights reserved.
//

#import "DMBookmark.h"

@class DMCategories;

@interface DMBookmark (Category)

+ (void)addNewBookmarkWithTitle:(NSString *)title url:(NSString *)url;
+ (void)addNewBookmarkWithTitle:(NSString *)title url:(NSString *)url category:(DMCategories *)categorie;
+ (NSArray *)bookmarksWithCategory:(DMCategories *)categorie;
+ (void)deleteBookmark:(DMBookmark *)bookmark;
+ (void)deleteBookmarks:(NSArray *)bookmarks;

@end
