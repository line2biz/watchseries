//
//  DMCategories.h
//  DownloadManager
//
//  Created by Denis on 29.12.14.
//  Copyright (c) 2014 Denis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DMBookmark;

@interface DMCategories : NSManagedObject

@property (nonatomic, retain) NSNumber * categoryID;
@property (nonatomic, retain) NSNumber * sortOrder;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSNumber * defaultCategory;
@property (nonatomic, retain) NSSet *bookmarks;
@end

@interface DMCategories (CoreDataGeneratedAccessors)

- (void)addBookmarksObject:(DMBookmark *)value;
- (void)removeBookmarksObject:(DMBookmark *)value;
- (void)addBookmarks:(NSSet *)values;
- (void)removeBookmarks:(NSSet *)values;

@end
