//
//  SNAlertQueueManager.h
//  FakeVPNApp
//
//  Created by Denis on 03.06.15.
//  Copyright (c) 2015 DSSolutions GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "SNAlert.h"

@interface SNAlertQueueManager : NSObject

+ (instancetype)sharedManager;

- (void)addAlertToQueue:(SNAlert *)alert;

@end

