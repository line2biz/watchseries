//
//  SNAllert.m
//  FakeVPNApp
//
//  Created by Denis on 03.06.15.
//  Copyright (c) 2015 DSSolutions GmbH. All rights reserved.
//

#import "SNAlert.h"

@implementation SNAlert

- (instancetype)initWithTitle:(NSString *)title
                      message:(NSString *)message
            cancelButtonTitle:(NSString *)cancelButtonTitle
           confirmButtonTitle:(NSString *)confirmButtonTitle
                   completion:(AlertCompletion)completion
{
    self = [super init];
    if (self) {
        _title = title;
        _message = message;
        _cancelButton = cancelButtonTitle;
        _confirmButton = confirmButtonTitle;
        if (completion) {
            _completionBlock = completion;
        }
    }
    return self;
}

- (instancetype)initWithMessage:(NSString *)message
{
    self = [super init];
    if (self) {
        _title = nil;
        _message = message;
        _cancelButton = NSLocalizedString(@"Dismiss", nil);
        _confirmButton = nil;
    }
    return self;
}

+ (instancetype)alertWithTitle:(NSString *)title
                       message:(NSString *)message
             cancelButtonTitle:(NSString *)cancelButtonTitle
            confirmButtonTitle:(NSString *)confirmButtonTitle
                    completion:(AlertCompletion)completion
{
    return [[SNAlert alloc] initWithTitle:title
                                  message:message
                        cancelButtonTitle:cancelButtonTitle
                       confirmButtonTitle:confirmButtonTitle
                               completion:completion];
}

+ (instancetype)alertWithMessage:(NSString *)message
{
    return [[SNAlert alloc] initWithMessage:message];
}

@end
