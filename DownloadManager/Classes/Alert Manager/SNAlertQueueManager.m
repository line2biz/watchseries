//
//  SNAlertQueueManager.m
//  FakeVPNApp
//
//  Created by Denis on 03.06.15.
//  Copyright (c) 2015 DSSolutions GmbH. All rights reserved.
//

#import "SNAlertQueueManager.h"

#import "SNAlertQueue.h"

@interface SNAlertQueueManager () <UIAlertViewDelegate>

@property (nonatomic, strong) SNAlert *currentAlert;
@property (nonatomic, strong) SNAlertQueue *alertQueue;

@end

@implementation SNAlertQueueManager

#pragma mark - Life Cycle
+ (instancetype)sharedManager
{
    static SNAlertQueueManager *alertManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        alertManager = [[SNAlertQueueManager alloc] init];
    });
    return alertManager;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _alertQueue = [[SNAlertQueue alloc] init];
        _alertQueue.alerts = [NSMutableArray new];
        [self.alertQueue addObserver:self
                          forKeyPath:@"alerts"
                             options:NSKeyValueObservingOptionNew
                             context:nil];
    }
    return self;
}

#pragma mark - Private Methods
- (void)showNextAlert
{
    BOOL proceed = NO;
    if ([self.alertQueue.alerts count]) {
        if (self.currentAlert) {
            if (self.currentAlert.isFinished == YES) {
                self.currentAlert = [self.alertQueue.alerts firstObject];
                self.currentAlert.isShown = YES;
                proceed = YES;
            }
        }
        else {
            self.currentAlert = [self.alertQueue.alerts firstObject];
            self.currentAlert.isShown = YES;
            proceed = YES;
        }
    }
    
    if (!proceed) {
        return;
    }
    
    id alertToShow = [self alertUIObject:self.currentAlert];
    if ([alertToShow isKindOfClass:[UIAlertView class]]) {
        UIAlertView *alertView = alertToShow;
        [alertView show];
    }
    else if ([alertToShow isKindOfClass:[UIAlertController class]]) {
        UIAlertController *alertController = alertToShow;
        
        UIViewController *topController = [self topViewController];
        if (!topController) {
            topController = [AppDelegate sharedDelegate].window.rootViewController;
            if (![[AppDelegate sharedDelegate].window.subviews containsObject:topController.view]) {
                [[AppDelegate sharedDelegate].window addSubview:topController.view];
                [[AppDelegate sharedDelegate].window makeKeyAndVisible];
            }
        }
        
        [topController presentViewController:alertController animated:YES completion:nil];
    }
}

- (id)alertUIObject:(SNAlert *)alert
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:alert.title
                                                        message:alert.message
                                                       delegate:self
                                              cancelButtonTitle:alert.cancelButton
                                              otherButtonTitles:alert.confirmButton, nil];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:alert.title
                                                                                 message:alert.message
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:alert.cancelButton
                                                            style:UIAlertActionStyleCancel
                                                          handler:^(UIAlertAction *action) {
                                                              
                                                              if (alert.completionBlock) {
                                                                  alert.completionBlock(alertView, [alertView cancelButtonIndex]);
                                                              }
                                                              self.currentAlert.isFinished = YES;
                                                              
                                                              NSMutableArray *alerts = [self.alertQueue mutableArrayValueForKey:@"alerts"];
                                                              [alerts removeObject:self.currentAlert];
                                                          }]];
        if (alert.confirmButton) {
            [alertController addAction:[UIAlertAction actionWithTitle:alert.confirmButton
                                                                style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction *action) {
                                                                  
                                                                  if (alert.completionBlock) {
                                                                      alert.completionBlock(alertView, 1);
                                                                  }
                                                                  self.currentAlert.isFinished = YES;
                                                                  
                                                                  NSMutableArray *alerts = [self.alertQueue mutableArrayValueForKey:@"alerts"];
                                                                  [alerts removeObject:self.currentAlert];
                                                              }]];            
        }
        
        return alertController;
    }
    else {
        
        return alertView;
    }
    return nil;
}

#pragma mark Top Controller
- (UIViewController*)topViewController
{
    return [self topViewControllerWithRootViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

- (UIViewController*)topViewControllerWithRootViewController:(UIViewController*)rootViewController
{
    if ([rootViewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController* tabBarController = (UITabBarController*)rootViewController;
        return [self topViewControllerWithRootViewController:tabBarController.selectedViewController];
    } else if ([rootViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController* navigationController = (UINavigationController*)rootViewController;
        return [self topViewControllerWithRootViewController:navigationController.visibleViewController];
    } else if (rootViewController.presentedViewController) {
        UIViewController* presentedViewController = rootViewController.presentedViewController;
        return [self topViewControllerWithRootViewController:presentedViewController];
    } else {
        return rootViewController;
    }
}

#pragma mark - Public Methods
- (void)addAlertToQueue:(SNAlert *)alert
{
    NSMutableArray *alerts = [self.alertQueue mutableArrayValueForKey:@"alerts"];
    [alerts addObject:alert];
}

#pragma mark - Alert View Delegate
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (self.currentAlert.completionBlock) {
        self.currentAlert.completionBlock(alertView, buttonIndex);
    }
    self.currentAlert.isFinished = YES;
    
    NSMutableArray *alerts = [self.alertQueue mutableArrayValueForKey:@"alerts"];
    [alerts removeObject:self.currentAlert];
}

#pragma mark - KVO
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object
                        change:(NSDictionary *)change context:(void *)context {
    if (object != self.alertQueue) {
        return;
    }
    
    [self showNextAlert];
}


@end
