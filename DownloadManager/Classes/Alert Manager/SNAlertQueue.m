//
//  SNAlertQueue.m
//  FakeVPNApp
//
//  Created by Denis on 03.06.15.
//  Copyright (c) 2015 DSSolutions GmbH. All rights reserved.
//

#import "SNAlertQueue.h"

@interface SNAlertQueue ()
{
    NSMutableArray *_alerts;
}
@end

@implementation SNAlertQueue
@synthesize alerts = _alerts;


- (NSArray *)alerts
{
    return [_alerts copy];
}

- (void)setAlerts:(NSArray *)alerts
{
    if ([_alerts isEqualToArray:alerts] == NO)
    {
        _alerts = [alerts mutableCopy];
    }
}

@end
