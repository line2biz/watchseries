//
//  SNAllert.h
//  FakeVPNApp
//
//  Created by Denis on 03.06.15.
//  Copyright (c) 2015 DSSolutions GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SNAlert : NSObject

typedef void(^AlertCompletion)(UIAlertView *alertView, NSUInteger buttonIndex);

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *message;
@property (nonatomic, copy) NSString *cancelButton;
@property (nonatomic, copy) NSString *confirmButton;
@property (nonatomic, assign) BOOL isShown;
@property (nonatomic, assign) BOOL isFinished;
@property (nonatomic, copy) AlertCompletion completionBlock;

- (instancetype)initWithTitle:(NSString *)title
                      message:(NSString *)message
            cancelButtonTitle:(NSString *)cancelButtonTitle
           confirmButtonTitle:(NSString *)confirmButtonTitle
                   completion:(AlertCompletion)completion;

- (instancetype)initWithMessage:(NSString *)message;

+ (instancetype)alertWithTitle:(NSString *)title
                       message:(NSString *)message
             cancelButtonTitle:(NSString *)cancelButtonTitle
            confirmButtonTitle:(NSString *)confirmButtonTitle
                    completion:(AlertCompletion)completion;

+ (instancetype)alertWithMessage:(NSString *)message;

@end
