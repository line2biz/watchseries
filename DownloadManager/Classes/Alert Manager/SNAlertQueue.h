//
//  SNAlertQueue.h
//  FakeVPNApp
//
//  Created by Denis on 03.06.15.
//  Copyright (c) 2015 DSSolutions GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SNAlertQueue : NSObject

@property (nonatomic, strong) NSArray *alerts;

@end
