//
//  FileDownloadInfo.h
//  DownloadManager
//
//  Created by Denis on 03.02.15.
//  Copyright (c) 2015 ComputerVerse Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FileDownloadInfo : NSObject <NSCoding>

@property (nonatomic, strong)   NSString *fileTitle;
@property (nonatomic, strong)   NSString *downloadSource;
@property (nonatomic, strong)   NSURLSessionDownloadTask *downloadTask;
@property (nonatomic, strong)   NSData *taskResumeData;
@property (nonatomic, strong)   NSDate *startDate;
@property (nonatomic, assign)   double downloadProgress;
@property (nonatomic, assign)   BOOL isDownloading;
@property (nonatomic, assign)   BOOL downloadComplete;
@property (nonatomic, assign)   BOOL sizeChecked;
@property (nonatomic, assign)   unsigned long taskIdentifier;
@property (nonatomic, assign)   uint64_t fileSize;
@property (nonatomic, readonly, getter=stringWithFormattedSize) NSString *formattedSize;

-(id)initWithFileTitle:(NSString *)title andDownloadSource:(NSString *)source;

@end
