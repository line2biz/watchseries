//
//  DownloadsViewController.m
//  DownloadManager
//
//  Created by Denis on 03.02.15.
//  Copyright (c) 2015 ComputerVerse Corporation. All rights reserved.
//

#import "DownloadsViewController.h"
#import "FileDownloadInfo.h"
#import "AppDelegate.h"
#import "MediaManager.h"

#import "SNDownloadManager.h"

// Define some constants regarding the tag values of the prototype cell's subviews.
#define CellTitleTagValue               10
#define CellStopButtonTagValue          20
#define CellProgressBarTagValue         30
#define CellSizeTagValue                40
#define CellSpeedTagValue               50
#define CellImageTagValue               60

@interface DownloadsViewController () <SNDownloadManagerDelegate>

@end

@implementation DownloadsViewController

#pragma mark - Life Cycle
- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super initWithCoder:aDecoder])) {
        
        // add observer for start dowload notification
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(download:) name:@"download" object:nil];
        
        [SNDownloadManager sharedManager].delegate = self;
        
        [self.tableView setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = @"Downloads";
    self.view.backgroundColor = [UIColor colorWithRed:0.176f green:0.176f blue:0.176f alpha:1.000f];
    
    // add editing button
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.tableView reloadData];
    
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    [super setEditing:editing animated:animated];
    [self.tableView setEditing:editing animated:animated];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"download" object:nil];
}

#pragma mark - Status Bar
- (BOOL)prefersStatusBarHidden
{
    if (!UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation]) && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        return YES;
    } else {
        return NO;
    }
}

- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation
{
    return UIStatusBarAnimationSlide;
}

#pragma mark - Private Methods
- (void)addDownloadRequestWithURL:(NSURL *)url destPath:(NSString *)destPath
{
    FileDownloadInfo *fdi = [[FileDownloadInfo alloc] initWithFileTitle:[destPath lastPathComponent] andDownloadSource:[url absoluteString]];
    
    [[SNDownloadManager sharedManager] startNewTaskWithFDI:fdi];
    
    NSInteger newIndex = [[SNDownloadManager sharedManager].activeDownloads count] - 1;
    [self.tableView beginUpdates];
    [self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:newIndex inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
    [self.tableView endUpdates];
}


- (void)stopDownloadingAtIndexPath:(NSIndexPath *)indexPath
{
    // Get the row (index) of the cell
    NSInteger cellIndex = indexPath.row;
    
    FileDownloadInfo *fdi = [[[SNDownloadManager sharedManager] activeDownloads] objectAtIndex:cellIndex];
    [[SNDownloadManager sharedManager] stopTaskWithFDI:fdi];
    
    [self.tableView beginUpdates];
    [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.tableView endUpdates];
}

#pragma mark Notification Handlers
- (void)download:(NSNotification*)aNot
{
    NSDictionary* userInfo = aNot.userInfo;
    
    [self addDownloadRequestWithURL:userInfo[@"url"] destPath:userInfo[@"path"]];
}


#pragma mark - Table View Data Source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[SNDownloadManager sharedManager].activeDownloads count];
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    // interface
    if (indexPath.row % 2 == 1) {
        cell.backgroundColor = [UIColor colorWithRed:0.129f green:0.129f blue:0.129f alpha:1.000f];
    } else {
        cell.backgroundColor = [UIColor clearColor];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    // Get the respective FileDownloadInfo object from the arrFileDownloadData array.
    FileDownloadInfo *fdi = [[SNDownloadManager sharedManager].activeDownloads objectAtIndex:indexPath.row];
    
    // Get all cell's subviews.
    UILabel *displayedTitle = (UILabel *)[cell viewWithTag:CellTitleTagValue];
    UIButton *stopButton = (UIButton *)[cell viewWithTag:CellStopButtonTagValue];
    UIProgressView *progressView = (UIProgressView *)[cell viewWithTag:CellProgressBarTagValue];
    UILabel *labelFileSize = (UILabel *)[cell viewWithTag:CellSizeTagValue];
    UILabel *labelSpeed = (UILabel *)[cell viewWithTag:CellSpeedTagValue];
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:CellImageTagValue];
    
    // Set the file title.
    displayedTitle.text = fdi.fileTitle;
    
    // set other text
    labelFileSize.text = @"0.0 B";
    labelSpeed.text = @"0.0 B/s";
    imageView.image = [MediaManager iconWithName:fdi.fileTitle];
    
    // Depending on whether the current file is being downloaded or not, specify the status
    // of the progress bar.
    if (!fdi.isDownloading) {
        // Hide the progress view and disable the stop button.
        progressView.hidden = YES;
        stopButton.enabled = NO;
        
        // Set a flag value depending on the downloadComplete property of the fdi object.
        BOOL hideControls = (fdi.downloadComplete) ? YES : NO;
        stopButton.hidden = hideControls;
        labelSpeed.hidden = hideControls;
        labelFileSize.hidden = hideControls;
        
    } else {
        // Show the progress view and update its progress, enable the stop button.
        progressView.hidden = NO;
        progressView.progress = fdi.downloadProgress;
        
        stopButton.enabled = YES;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

#pragma mark - Table View Delegate
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( editingStyle == UITableViewCellEditingStyleDelete ) {
        [self stopDownloadingAtIndexPath:indexPath];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 52.0f;
}

#pragma mark - Download Manager Delegates
- (void)sharedDownloadManager:(SNDownloadManager *)manager
                 downloadTask:(NSURLSessionDownloadTask *)downloadTask
                  objectIndex:(int)index
                 didWriteData:(int64_t)bytesWritten
            totalBytesWritten:(int64_t)totalBytesWritten
    totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite
{
     FileDownloadInfo *fdi = [[SNDownloadManager sharedManager].activeDownloads objectAtIndex:index];
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        // Get the progress view of the appropriate cell and update its progress.
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
        UIProgressView *progressView = (UIProgressView *)[cell viewWithTag:CellProgressBarTagValue];
        progressView.progress = fdi.downloadProgress;
        UILabel *labelSpeed = (UILabel *)[cell viewWithTag:CellSpeedTagValue];
        UILabel *labelSize = (UILabel *)[cell viewWithTag:CellSizeTagValue];
        
        // calculate speed
        int currentTimeInterval = [[NSDate date] timeIntervalSince1970];
        
        if ( currentTimeInterval == [fdi.startDate timeIntervalSince1970] || totalBytesWritten == 0 ) {
            labelSpeed.text = @"0.00 KB/s";
            
        } else {
            CGFloat fKB = 1024.0;
            int timeInterval = currentTimeInterval - [fdi.startDate timeIntervalSince1970];
            float speed = (float)totalBytesWritten / (float) timeInterval / fKB;
            
            NSString *speedText = [NSString stringWithFormat:@"%.2f", speed];
            NSString *unitText = @"KB/s";
            if ( speed > fKB ) {
                speed = speed / fKB;
                speedText = [NSString stringWithFormat:@"%.2f", speed];
                unitText = @"MB/s";
            }
            [labelSpeed setText:[NSString stringWithFormat:@"%@ %@", speedText, unitText]];
        }
        
        // set file size
        if (fdi.fileSize) {
            labelSize.text = fdi.formattedSize;
        }
        else {
            labelSize.text = NSLocalizedString(@"Unknown", nil);
        }
    }];
}

- (void)sharedDownloadManager:(SNDownloadManager *)manager
                 downloadTask:(NSURLSessionDownloadTask *)downloadTask
                  objectIndex:(int)index
    didFinishDownloadingToURL:(NSURL *)location
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        // remove download cell
        [self.tableView beginUpdates];
        [self.tableView deleteRowsAtIndexPaths:@[
                                                 [NSIndexPath indexPathForRow:index inSection:0]
                                                 ]
                              withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tableView endUpdates];
    }];
}





@end
