//
//  DownloadsViewController.h
//  DownloadManager
//
//  Created by Denis on 03.02.15.
//  Copyright (c) 2015 ComputerVerse Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DownloadsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>//, NSURLSessionDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
