//
//  FileDownloadInfo.m
//  DownloadManager
//
//  Created by Denis on 03.02.15.
//  Copyright (c) 2015 ComputerVerse Corporation. All rights reserved.
//

#import "FileDownloadInfo.h"
#import "SNDownloadManager.h"
#import "SNDeviceSpaceManager.h"

NSString * const kFileTitle             = @"kFileTitle";
NSString * const kDownloadSource        = @"kDownloadSource";
NSString * const kDownloadTask          = @"kDownloadTask";
NSString * const kTaskResumeData        = @"kTaskResumeData";
NSString * const kStartDate             = @"kStartDate";
NSString * const kDownloadProgress      = @"kDownloadProgress";
NSString * const kIsDownloading         = @"kIsDownloading";
NSString * const kDownloadComplete      = @"kDownloadComplete";
NSString * const kTaskIdentifier        = @"kTaskIdentifier";
NSString * const kSizeIdentifier        = @"kSizeIdentifier";
NSString * const kSizeCheckedIdentifier = @"kSizeCheckedIdentifier";

@implementation FileDownloadInfo

- (id)initWithFileTitle:(NSString *)title andDownloadSource:(NSString *)source
{
    self = [super init];
    if (self) {
        self.fileTitle = title;
        self.downloadSource = source;
        self.startDate = [NSDate date];
        self.downloadProgress = 0.0f;
        self.isDownloading = NO;
        self.downloadComplete = NO;
        self.sizeChecked = NO;
        self.taskIdentifier = -1;
        self.fileSize = 0;
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder
{
    [coder encodeObject:_fileTitle forKey:kFileTitle];
    [coder encodeObject:_downloadSource forKey:kDownloadSource];
//    [coder encodeObject:_downloadTask forKey:kDownloadTask];
    [coder encodeObject:_taskResumeData forKey:kTaskResumeData];
    [coder encodeObject:_startDate forKey:kStartDate];
    [coder encodeDouble:_downloadProgress forKey:kDownloadProgress];
    [coder encodeBool:_isDownloading forKey:kIsDownloading];
    [coder encodeBool:_downloadComplete forKey:kDownloadComplete];
    [coder encodeBool:_sizeChecked forKey:kSizeCheckedIdentifier];
    [coder encodeObject:[NSNumber numberWithUnsignedLong:_taskIdentifier] forKey:kTaskIdentifier];
    [coder encodeObject:[NSNumber numberWithLongLong:_fileSize] forKey:kSizeIdentifier];
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super init];
    if (self) {
        _fileTitle = [coder decodeObjectForKey:kFileTitle];
        _downloadSource = [coder decodeObjectForKey:kDownloadSource];
//        _downloadTask = [coder decodeObjectForKey:kDownloadTask];
        _taskResumeData = [coder decodeObjectForKey:kTaskResumeData];
        _startDate = [coder decodeObjectForKey:kStartDate];
        _downloadProgress = [coder decodeDoubleForKey:kDownloadProgress];
        _isDownloading = [coder decodeBoolForKey:kIsDownloading];
        _downloadComplete = [coder decodeBoolForKey:kDownloadComplete];
        _sizeChecked = [coder decodeBoolForKey:kSizeCheckedIdentifier];
        _taskIdentifier = [((NSNumber *)[coder decodeObjectForKey:kTaskIdentifier]) unsignedLongValue];
        _fileSize = [((NSNumber *)[coder decodeObjectForKey:kSizeIdentifier]) unsignedLongLongValue];
    }
    return self;
}

- (NSString *)stringWithFormattedSize
{
    return [SNDeviceSpaceManager formattedSizeFromBytes:self.fileSize];
}

@end
