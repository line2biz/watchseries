//
//  SNDownloadManager.h
//  DownloadManager
//
//  Created by Denis on 28.05.15.
//  Copyright (c) 2015 ComputerVerse Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FileDownloadInfo;

/* Delegate Protocol to inform delegate about downloading objects state */
@protocol SNDownloadManagerDelegate;

/* Manager wich handles file`s download */
@interface SNDownloadManager : NSObject <NSURLSessionDelegate>

#pragma mark Properties
/** Delegate Object weak Reference */
@property (weak, nonatomic) id <SNDownloadManagerDelegate> delegate;
/** Returns an array of active Downloads */
@property (nonatomic, readonly, getter=activeDownloads) NSArray *activeDownloads;

#pragma mark Methods
/** Shared Instance */
+ (instancetype)sharedManager;

/** Start Download Task with file downlod info object */
- (void)startNewTaskWithFDI:(FileDownloadInfo *)fdi;

/** Stop Download Task with file download info object */
- (void)stopTaskWithFDI:(FileDownloadInfo *)fdi;

@end



@protocol SNDownloadManagerDelegate <NSObject>

@optional
/** Delegate informing about download`s progress */
- (void)sharedDownloadManager:(SNDownloadManager *)manager
                 downloadTask:(NSURLSessionDownloadTask *)downloadTask
                  objectIndex:(int)index
                 didWriteData:(int64_t)bytesWritten
            totalBytesWritten:(int64_t)totalBytesWritten
    totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite;

/** Delegate informing about download`s finish event */
- (void)sharedDownloadManager:(SNDownloadManager *)manager
                 downloadTask:(NSURLSessionDownloadTask *)downloadTask
                  objectIndex:(int)index
    didFinishDownloadingToURL:(NSURL *)location;

@end
