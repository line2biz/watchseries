//
//  SNDownloadManager.m
//  DownloadManager
//
//  Created by Denis on 28.05.15.
//  Copyright (c) 2015 ComputerVerse Corporation. All rights reserved.
//

#import "SNDownloadManager.h"
#import "FileDownloadInfo.h"
#import "AppDelegate.h"
#import "SNDeviceSpaceManager.h"

#define DBGLog( s, ... ) NSLog( @"\nFile: %@\nFunction: %s\nLine: %zd\nMessage:\n%@\n", [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __PRETTY_FUNCTION__, __LINE__, [NSString stringWithFormat:(s), ##__VA_ARGS__] )

NSString * const kSavedArray = @"kSavedArray";

@interface SNDownloadManager () <UIAlertViewDelegate>

/** NSURLSession Object */
@property (nonatomic, strong) NSURLSession *session;
/** URL of document`s directory */
@property (nonatomic, strong) NSURL *docDirectoryURL;
/** Mutable Array to hold all active downloads */
@property (nonatomic, strong) NSMutableArray *maDownloads;

@end

@implementation SNDownloadManager

#pragma mark - Life Cycle
+ (instancetype)sharedManager
{
    static SNDownloadManager *downloadManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        downloadManager = [[SNDownloadManager alloc] init];
    });
    return downloadManager;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        // init main buffer
        _maDownloads = [NSMutableArray new];
        
        // register Observers
        [self registerObservers:YES];
        
        // create main Directory
        [self createDocDirectory];
        
        // configure Session
        [self configureSession];
    }
    return self;
}

- (void)dealloc
{
    // unregister Observers
    [self registerObservers:NO];
}

#pragma mark - Observers
/* Registers or unregisters observers for App`s State change */
- (void)registerObservers:(BOOL)bRegister
{
    if (bRegister == YES) {
        // observers for background/foreground app change
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(prepareToBackground)
                                                     name:kApplicationDidEnterBackground object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(prepareToForeground)
                                                     name:kApplicationWillEnterForeground object:nil];
    }
    else {
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }
}

#pragma mark - Private Methods
/* creates session configuration and session it self */
- (void)configureSession
{
    NSURLSessionConfiguration *sessionConfiguration;
    
    // set unique session ID in system
    NSString *sSessionID = @"Watchseries/DownloadManager";
    
    // check if system version is equal or older
    if ([[[UIDevice currentDevice] systemVersion] compare:@"8.0" options:NSNumericSearch] != NSOrderedAscending) {
        sessionConfiguration = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:sSessionID];
    } else {
        sessionConfiguration = [NSURLSessionConfiguration backgroundSessionConfiguration:sSessionID];
    }
    
    // Set max connections per session
    sessionConfiguration.HTTPMaximumConnectionsPerHost = 10;
    
    // create session or get from system if it was created already
    self.session = [NSURLSession sessionWithConfiguration:sessionConfiguration
                                                 delegate:self
                                            delegateQueue:[NSOperationQueue mainQueue]];
}

/* creates Directory to save files in if there is none */
- (void)createDocDirectory
{
    // set URL of main document`s directory
    NSArray *URLs = [[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
    _docDirectoryURL = [URLs objectAtIndex:0];
    _docDirectoryURL = [self.docDirectoryURL URLByAppendingPathComponent:@"Downloads" isDirectory:YES];
    
    // create file manager object
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    // create directory
    if (![fileManager fileExistsAtPath:self.docDirectoryURL.path]) {
        NSError *creationError;
        BOOL bCreationSucceded = [fileManager createDirectoryAtURL:self.docDirectoryURL
                                       withIntermediateDirectories:NO
                                                        attributes:nil
                                                             error:&creationError];
        if (!bCreationSucceded) {
            DBGLog(@"Directory creation failed:\n%@\n", creationError);
        }
    }
}

/* Finds Index of file download info object by task identifier */
- (int)getFileDownloadInfoIndexWithTaskIdentifier:(unsigned long)taskIdentifier
{
    int index = -1;
    for (int i=0; i<[self.maDownloads count]; i++) {
        FileDownloadInfo *fdi = [self.maDownloads objectAtIndex:i];
        if (fdi.taskIdentifier == taskIdentifier) {
            index = i;
            break;
        }
    }
    
    return index;
}

- (NSArray *)activeDownloads
{
    return [self.maDownloads mutableCopy];
}


#pragma mark - Alert
- (void)showAlertWithFDI:(FileDownloadInfo *)fdi
{
    fdi.sizeChecked = YES;
    NSString *sMsg;
    if (fdi.fileSize == 0) {
        sMsg = [NSString stringWithFormat:@"File size of \"%@\" is unknown, free space on disk available %@.", fdi.fileTitle, [SNDeviceSpaceManager formattedSizeFromBytes:[SNDeviceSpaceManager diskSpaceFree]]];
    }
    else {
        uint64_t totalBytesLoading = 0;
        uint64_t freeDiskSpaceBytes = [SNDeviceSpaceManager diskSpaceFree];
        fdi.sizeChecked = YES;
        for (FileDownloadInfo *activeFDI in self.maDownloads) {
            if ([activeFDI isDownloading]) {
                totalBytesLoading += activeFDI.fileSize;
            }
        }
        freeDiskSpaceBytes -= totalBytesLoading;
        if (totalBytesLoading + fdi.fileSize*1.5f >= freeDiskSpaceBytes) {
            sMsg = [NSString stringWithFormat:@"File size of \"%@\" %@, free space on disk available %@.", fdi.fileTitle, [SNDeviceSpaceManager formattedSizeFromBytes:fdi.fileSize], [SNDeviceSpaceManager formattedSizeFromBytes:freeDiskSpaceBytes]];
        }
        else {
            return;
        }
    }
    
    // pause download
    [fdi.downloadTask cancelByProducingResumeData:^(NSData *resumeData) {
        if (resumeData != nil) {
            fdi.taskResumeData = [[NSData alloc] initWithData:resumeData];
        }
        fdi.isDownloading = NO;
        dispatch_async(dispatch_get_main_queue(), ^{
            SNAlert *alert = [SNAlert alertWithTitle:nil
                                             message:sMsg
                                   cancelButtonTitle:NSLocalizedString(@"Continue", nil)
                                  confirmButtonTitle:NSLocalizedString(@"Stop Download", nil)
                                          completion:^(UIAlertView *alertView, NSUInteger buttonIndex) {
                                              if (buttonIndex != alertView.cancelButtonIndex) {
                                                  if (fdi) {
                                                      [self stopTaskWithFDI:fdi];
                                                  }
                                              }
                                              else {
                                                  // Create a new download task, which will use the stored resume data.
                                                  if (fdi.taskResumeData) {
                                                      fdi.downloadTask = [self.session downloadTaskWithResumeData:fdi.taskResumeData];
                                                      [fdi.downloadTask resume];
                                                      
                                                      // Keep the new download task identifier.
                                                      fdi.taskIdentifier = fdi.downloadTask.taskIdentifier;
                                                      fdi.isDownloading = YES;
                                                  }
                                                  else {
                                                      // create a new task from URL
                                                      fdi.downloadTask = [self.session downloadTaskWithURL:[NSURL URLWithString:fdi.downloadSource]];
                                                      // Keep the new task identifier.
                                                      fdi.taskIdentifier = fdi.downloadTask.taskIdentifier;
                                                      // Start the task.
                                                      [fdi.downloadTask resume];
                                                      fdi.isDownloading = YES;
                                                  }
                                              }
                                              if (self.delegate) {
                                                  SEL tableViewSelector = NSSelectorFromString(@"tableView");
                                                  if ([self.delegate respondsToSelector:tableViewSelector]) {
                                                      UITableView *tableView = (UITableView *)[self.delegate performSelector:tableViewSelector];
                                                      [tableView reloadData];
                                                  }
                                              }
                                          }];
            [[SNAlertQueueManager sharedManager] addAlertToQueue:alert];
        });
    }];
}

#pragma mark - Notification Handlers
/* This method prepares class for 
 * background mode of App */
- (void)prepareToBackground
{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self.maDownloads];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:kSavedArray];
}

/* This method prepares class for
 * foreground mode of App */
- (void)prepareToForeground
{
    [self prepareToForegroundWithCompletion:nil];
}

/* This method prepares class for
 * foreground mode of App with completion*/
- (void)prepareToForegroundWithCompletion:(void(^)())FinalBlock
{
    if (!self.session) {
        [self configureSession];
    }
    
    __weak typeof(self) weakSelf = self;
    [self.session getTasksWithCompletionHandler:^(NSArray *dataTasks, NSArray *uploadTasks, NSArray *downloadTasks) {
        typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf) {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                // check if active downloads` amount equals to the saved
                if ([strongSelf.maDownloads count] == [downloadTasks count]) {
                    return;
                }
                
                // restore saved data if any
                NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:kSavedArray];
                if (!data.length || !data) {
                    // cancel all active tasks - we don`t have info to deal with them
                    for (NSURLSessionDownloadTask *downloadTask in downloadTasks) {
                        [downloadTask cancel];
                    }
                    DBGLog(@"All active tasks (%zd) have been deleted: no saved info data.", [downloadTasks count]);
                    return;
                }
                NSArray *arrayOfFDI = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                
                // find active downloads we have info about
                NSMutableArray *maNeededFDI = [NSMutableArray new];
                for (__strong FileDownloadInfo *fdi in arrayOfFDI) {
                    for (NSURLSessionDownloadTask *downloadTask in downloadTasks) {
                        if (fdi.taskIdentifier == downloadTask.taskIdentifier) {
                            fdi.downloadTask = downloadTask;
                            [maNeededFDI addObject:fdi];
                            break;
                        }
                    }
                }
                
                // cancel active downloads `cause we have no info
                if (![maNeededFDI count]) {
                    DBGLog(@"No needed download tasks were found among active and saved.");
                    for (NSURLSessionDownloadTask *downloadTask in downloadTasks) {
                        [downloadTask cancel];
                    }
                    return;
                }
                
                // delete tasks we don`t have info about
                NSMutableSet *allTasksSet = [NSMutableSet setWithArray:downloadTasks];
                NSMutableSet *neededTasksSet = [NSMutableSet setWithArray:maNeededFDI];
                DBGLog(@"%zd active tasks were found.", [neededTasksSet count]);
                [allTasksSet minusSet:neededTasksSet];
                if ([allTasksSet count]) {
                    DBGLog(@"%zd will be deleted as unidentified", [allTasksSet count]);
                    for (NSURLSessionDownloadTask *downloadTask in allTasksSet) {
                        [downloadTask cancel];
                    }
                }
                
                // copy found needed downloads
                strongSelf.maDownloads = [NSMutableArray arrayWithArray:[maNeededFDI mutableCopy]];
                
                // increase a download badge number
                [[NSNotificationCenter defaultCenter] postNotificationName:@"setDownloadBadge"
                                                                    object:nil
                                                                  userInfo:@{@"value": [NSString stringWithFormat:@"%zd", [strongSelf.maDownloads count]]}];
                
                // execute final block
                if ([strongSelf.maDownloads count]) {
                    if (FinalBlock) {
                        FinalBlock();
                    }
                }
            }];
        }
    }];
}

/* Check if File Title isn`t the same as one of the loaded/ing */
- (NSString *)checkFDITitleForUniquety:(FileDownloadInfo *)fdi
{
    // create file manager
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    // normalize file name
    NSString *sTitle = [fdi.fileTitle stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    // get url to manipulate extensions
    NSURL *urlTitle = [NSURL URLWithString:sTitle];
    
    // get contents of main Directory
    NSURL *directoryURL = self.docDirectoryURL;
    NSError *error;
    NSArray *aContents = [fileManager contentsOfDirectoryAtURL:directoryURL
                                    includingPropertiesForKeys:nil
                                                       options:0
                                                         error:&error];
    
    // check if any of contents has the same title
    NSInteger iNameAddition = 0;
    // 1st stage - find alike titles
    NSMutableArray *maAlikeTitles = [NSMutableArray new];
    for (NSURL *url in aContents) {
        if (([[[url URLByDeletingPathExtension] lastPathComponent] rangeOfString:[[urlTitle URLByDeletingPathExtension] lastPathComponent]].location != NSNotFound)) {
            [maAlikeTitles addObject:[[url URLByDeletingPathExtension] lastPathComponent]];
        }
    }
    
    // 2nd stage - find equal title
    
    NSRange(^RangeOfLaps)(NSString *) = ^(NSString *sSearch) {
        NSInteger iF = -1, iS = -1;
        for (NSInteger i=0; i<sSearch.length; i++) {
            unichar a = [sSearch characterAtIndex:i];
            if (a == ')') {
                if (i == sSearch.length-1) {
                    iF = i;
                }
            }
            else if (a == '(') {
                if (i < sSearch.length && i > sSearch.length - 4) {
                    iS = i;
                }
            }
        }
        if (iF >=0 && iS >= 0) {
            NSRange endLapsRange = NSMakeRange(iS, iF - iS + 1);
            return endLapsRange;
        }
        
        return NSMakeRange(0, 1);
    };
    
    NSString *sOurTitle = [[urlTitle URLByDeletingPathExtension] lastPathComponent];
    NSMutableArray *maEqualTitles = [NSMutableArray new];
    for (NSString *sAlikeTitle in maAlikeTitles) {
        NSString *sQuerryTitle;
        NSRange endLapsRange = RangeOfLaps(sAlikeTitle);
        if (NSEqualRanges(NSMakeRange(0, 1), endLapsRange)) {
            sQuerryTitle = sAlikeTitle;
        }
        else {
            sQuerryTitle = [sAlikeTitle stringByReplacingCharactersInRange:endLapsRange withString:@""];
        }
        
        if ([sQuerryTitle isEqualToString:sOurTitle]) {
            [maEqualTitles addObject:sAlikeTitle];
        }
    }
    
    // 3d stage - get unique name addition
    NSMutableArray *maPossibleNumbers = [NSMutableArray arrayWithCapacity:[maEqualTitles count]+1];
    for (NSInteger i=0; i<[maEqualTitles count]+1; i++) {
        [maPossibleNumbers addObject:[NSNumber numberWithInteger:i]];
    }
    NSMutableArray *maExistingNumbers = [NSMutableArray arrayWithCapacity:[maEqualTitles count]];
    for (NSInteger i=0; i<maEqualTitles.count; i++) {
        NSString *sOldTitle = [maEqualTitles objectAtIndex:i];
        NSRange endLapsRange = RangeOfLaps(sOldTitle);
        endLapsRange.location += 1;
        endLapsRange.length = 1;
        NSString *sNumber = [sOldTitle substringWithRange:endLapsRange];
        NSNumber *number = [NSNumber numberWithInteger:[sNumber integerValue]];
        [maExistingNumbers addObject:number];
    }
    
    for (NSNumber *number in maPossibleNumbers) {
        if (![maExistingNumbers containsObject:number]) {
            iNameAddition = [number integerValue];
            break;
        }
    }
    
    // change title if needed
    if (iNameAddition) {
        sTitle = [NSString stringWithFormat:@"%@(%zd).%@", [[urlTitle URLByDeletingPathExtension] lastPathComponent], iNameAddition, [urlTitle pathExtension]];
    }
    sTitle = [sTitle stringByRemovingPercentEncoding];
    
    return sTitle;
}

#pragma mark - Public Methods
/** Start Download Task with file downlod info object */
- (void)startNewTaskWithFDI:(FileDownloadInfo *)fdi
{
    // add it to all
    [self.maDownloads addObject:fdi];
    
    // create a new task
    fdi.downloadTask = [self.session downloadTaskWithURL:[NSURL URLWithString:fdi.downloadSource]];
    
    // Keep the new task identifier.
    fdi.taskIdentifier = fdi.downloadTask.taskIdentifier;
    
    // Start the task.
    [fdi.downloadTask resume];
    
    // Change the isDownloading property value.
    fdi.isDownloading = !fdi.isDownloading;
    
    // increase a download badge number
    [[NSNotificationCenter defaultCenter] postNotificationName:@"setDownloadBadge"
                                                        object:nil
                                                      userInfo:@{@"value": [NSString stringWithFormat:@"%zd", [self.maDownloads count]]}];
}

/** Stop Download Task with file download info object */
- (void)stopTaskWithFDI:(FileDownloadInfo *)fdi
{
    // Cancel the task.
    if (fdi.isDownloading) {
        [fdi.downloadTask cancel];
    }
    
    // Change all related properties.
    fdi.isDownloading = NO;
    fdi.taskIdentifier = -1;
    fdi.downloadProgress = 0.0;
    
    [self.maDownloads removeObject:fdi];
    
    // post finish loading notification
    [[NSNotificationCenter defaultCenter] postNotificationName:@"setDownloadBadge"
                                                        object:nil
                                                      userInfo:@{@"value": [NSString stringWithFormat:@"%zd", self.maDownloads.count]}];
}

#pragma mark - NSURLSession Delegates
- (void)               URLSession:(NSURLSession *)session
                     downloadTask:(NSURLSessionDownloadTask *)downloadTask
                     didWriteData:(int64_t)bytesWritten
                totalBytesWritten:(int64_t)totalBytesWritten
        totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite
{
    
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(sharedDownloadManager:downloadTask:objectIndex:didWriteData:totalBytesWritten:totalBytesExpectedToWrite:)]) {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                // Locate the FileDownloadInfo object among all based on the taskIdentifier property of the task.
                int index = [self getFileDownloadInfoIndexWithTaskIdentifier:downloadTask.taskIdentifier];
                
                if (index >= 0) {
                    FileDownloadInfo *fdi = [self.maDownloads objectAtIndex:index];
                    
                    // Calculate the progress.
                    if (totalBytesExpectedToWrite != NSURLSessionTransferSizeUnknown) {
                        fdi.downloadProgress = (double)totalBytesWritten / (double)totalBytesExpectedToWrite;
                        if (!fdi.fileSize) {
                            fdi.fileSize = totalBytesExpectedToWrite;
                        }
                    }
                    // check if file size is not bigger then systems free space left
                    if (fdi.sizeChecked == NO) {
                        // show alert for user
                        [self showAlertWithFDI:fdi];
                    }
                    [self.delegate sharedDownloadManager:self
                                            downloadTask:downloadTask
                                             objectIndex:index
                                            didWriteData:bytesWritten
                                       totalBytesWritten:totalBytesWritten
                               totalBytesExpectedToWrite:totalBytesExpectedToWrite];
                }
                else {
                    [self prepareToForegroundWithCompletion:^{
                        
                        int index = [self getFileDownloadInfoIndexWithTaskIdentifier:downloadTask.taskIdentifier];
                        FileDownloadInfo *fdi = [self.maDownloads objectAtIndex:index];
                        
                        // Calculate the progress.
                        if (totalBytesExpectedToWrite != NSURLSessionTransferSizeUnknown) {
                            fdi.downloadProgress = (double)totalBytesWritten / (double)totalBytesExpectedToWrite;
                        }
                        [self.delegate sharedDownloadManager:self
                                                downloadTask:downloadTask
                                                 objectIndex:index
                                                didWriteData:bytesWritten
                                           totalBytesWritten:totalBytesWritten
                                   totalBytesExpectedToWrite:totalBytesExpectedToWrite];
                    }];
                }
            }];
        }
    }
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location
{
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(sharedDownloadManager:downloadTask:objectIndex:didFinishDownloadingToURL:)]) {
            
            // finish handler block
            typedef void(^Completion)();
            
            void(^FinalActionsBlock)(FileDownloadInfo *, Completion) = ^(FileDownloadInfo *fdi, Completion blockCompletion) {
                
                // prepare for error
                NSError *error;
                
                // File Managaer
                NSFileManager *fileManager = [NSFileManager defaultManager];
                
                // check if file`s name is unique
                NSString *sFileName = [self checkFDITitleForUniquety:fdi];
                if (sFileName) {
                    fdi.fileTitle = sFileName;
                }
                
                // form final URL
                NSURL *destinationURL = [self.docDirectoryURL URLByAppendingPathComponent:fdi.fileTitle];
                
                // remove file if it exists
                if ([fileManager fileExistsAtPath:[destinationURL path]]) {
                    [fileManager removeItemAtURL:destinationURL error:nil];
                }
                
                // try to copy file from temp URL to final URL
                BOOL success = [fileManager copyItemAtURL:location
                                                    toURL:destinationURL
                                                    error:&error];
                
                if (success) {
                    // Change the flag values of the respective FileDownloadInfo object.
                    
                    fdi.isDownloading = NO;
                    fdi.downloadComplete = YES;
                    
                    // Set the initial value to the taskIdentifier property of the fdi object
                    fdi.taskIdentifier = -1;
                    
                    // In case there is any resume data stored in the fdi object, just make it nil.
                    fdi.taskResumeData = nil;
                    
                } else {
                    DBGLog(@"Unable to copy temp file with name %@.\nError:\n%@\n", fdi.fileTitle, [error localizedDescription]);
                }
                [self.maDownloads removeObject:fdi];
                
                // decrease a download badge number
                [[NSNotificationCenter defaultCenter] postNotificationName:@"setDownloadBadge"
                                                                    object:nil
                                                                  userInfo:@{@"value": [NSString stringWithFormat:@"%zd", [self.maDownloads count]]}];
                if (blockCompletion) {
                    blockCompletion();
                }
            };
            
            // Locate the FileDownloadInfo object among all based on the taskIdentifier property of the task.
            int index = [self getFileDownloadInfoIndexWithTaskIdentifier:downloadTask.taskIdentifier];
            
            if (index >= 0) {
                
                FileDownloadInfo *fdi = [self.maDownloads objectAtIndex:index];
                
                FinalActionsBlock(fdi,^{
                    [self.delegate sharedDownloadManager:self
                                            downloadTask:downloadTask
                                             objectIndex:index
                               didFinishDownloadingToURL:location];
                });
                
            }
            else {
                
                [self prepareToForegroundWithCompletion:^{
                    int index = [self getFileDownloadInfoIndexWithTaskIdentifier:downloadTask.taskIdentifier];
                    FileDownloadInfo *fdi = [self.maDownloads objectAtIndex:index];
                    FinalActionsBlock(fdi,^{
                        [self.delegate sharedDownloadManager:self
                                                downloadTask:downloadTask
                                                 objectIndex:index
                                   didFinishDownloadingToURL:location];
                    });
                }];
            }
        }
    }
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error
{
    if (error != nil) {
        NSLog(@"\n%s\nDownload completed with error:\n%@\n",__FUNCTION__, [error localizedDescription]);
    } else {
        NSLog(@"\n%s\nDownload finished successfully.\n", __FUNCTION__);
    }
}

- (void)URLSessionDidFinishEventsForBackgroundURLSession:(NSURLSession *)session
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    // Check if all download tasks have been finished.
    [self.session getTasksWithCompletionHandler:^(NSArray *dataTasks, NSArray *uploadTasks, NSArray *downloadTasks) {
        if ([downloadTasks count] == 0) {
            if (appDelegate.backgroundTransferCompletionHandler != nil) {
                DBGLog(@"All Downloads are finished");
                // Copy locally the completion handler.
                void(^completionHandler)() = appDelegate.backgroundTransferCompletionHandler;
                
                // Make nil the backgroundTransferCompletionHandler.
                appDelegate.backgroundTransferCompletionHandler = nil;
                
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    // Call the completion handler to tell the system that there are no other background transfers.
                    completionHandler();
                    DBGLog(@"All Downloads are finished");
                    
                    // Show a local notification when all downloads are over.
                    [[UIApplication sharedApplication] cancelAllLocalNotifications];
                    
                    UILocalNotification *localNotif = [[UILocalNotification alloc] init];
                    localNotif.alertBody = NSLocalizedString(@"DownloadFinished", @"");
                    localNotif.alertAction = NSLocalizedString(@"DownloadFinished", @"");
                    localNotif.soundName = UILocalNotificationDefaultSoundName;
                    
                    [[UIApplication sharedApplication] presentLocalNotificationNow:localNotif];
                }];
            }
        }
    }];
}



@end
