//
//  NSDictionary+UrlEncoding.h
//  DownloadManager
//
//  Created by Denis on 04.03.15.
//  Copyright (c) 2015 Denis. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (UrlEncoding)

- (NSString *)urlEncodedString;

@end
