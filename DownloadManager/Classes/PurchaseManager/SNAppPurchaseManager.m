//
//  SNAppPurchaseManager.m
//  DownloadManager
//
//  Created by Denis on 13.02.15.
//  Copyright (c) 2015 Denis. All rights reserved.
//

#import "SNAppPurchaseManager.h"
#import "NSDictionary+UrlEncoding.h"

@interface SNAppPurchaseManager ()

@property (strong, nonatomic) SKProductsRequest *productsRequest;

@end

@implementation SNAppPurchaseManager

#pragma mark - Shared Instance
+ (instancetype)defaultManager
{
    static SNAppPurchaseManager *appPurchaseManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        appPurchaseManager = [[self alloc] init];
    });
    return appPurchaseManager;
}

#pragma mark - Life Cycle
- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

#pragma mark - Private Methods
- (void)startProductsRequestWithObject:(id)object
{
    if ([SKPaymentQueue canMakePayments]) {
        
        // In-App Purchase is enabled on the Device
        
        NSSet *productIdentifiers;
        if ([object isKindOfClass:[NSString class]]) {
            productIdentifiers = [NSSet setWithObject:object];
            
        } else if ([object isKindOfClass:[NSSet class]]) {
            productIdentifiers = (NSSet *)object;
        }
        _productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:productIdentifiers];
        _productsRequest.delegate = self;
        [_productsRequest start];
        
    } else {
        if (self.delegate) {
            [self.delegate purchaseManagerDisplayErrorMessage:NSLocalizedString(@"In-App purchase is disabled for this device.", nil)];
        }
    }
}

- (void)completePaymentTransaction:(SKPaymentTransaction *)paymentTransaction
{
    // Return transaction data. App should provide user with purchased product.
    if (self.delegate) {
        [self.delegate purchaseManagerPurchasedProductWithID:paymentTransaction.payment.productIdentifier];
    }
    
    // After customer has successfully received purchased content,
    // remove the finished transaction from the payment queue.
    [[SKPaymentQueue defaultQueue] finishTransaction: paymentTransaction];
}

- (void)restorePaymentTransaction:(SKPaymentTransaction *)paymentTransaction
{
    // Return transaction data. App should provide user with purchased product.
    if (self.delegate) {
        [self.delegate purchaseManagerRestoredProductWithID:paymentTransaction.payment.productIdentifier];
    }
    
    // After customer has restored purchased content on this device,
    // remove the finished transaction from the payment queue.
    [[SKPaymentQueue defaultQueue] finishTransaction: paymentTransaction];
}

- (void)failedPaymentTransaction:(SKPaymentTransaction *)paymentTransaction
{
    if (paymentTransaction.error.code != SKErrorPaymentCancelled) {
        
        if (paymentTransaction.error.code != SKErrorPaymentCancelled) {
            // A transaction error occurred, so notify user.
            if (self.delegate) {
                [self.delegate purchaseManagerFailedToPurchaseProductWithID:paymentTransaction.payment.productIdentifier error:paymentTransaction.error];
            }            
        }
    }
    
    // Finished transactions should be removed from the payment queue.
    [[SKPaymentQueue defaultQueue] finishTransaction: paymentTransaction];
}

- (void)parseReceiptDataFromDictionary:(NSDictionary *)dictionary
{
    NSArray *aErrorStatusCodes = @[
                                   @(21000),
                                   @(21002),
                                   @(21003),
                                   @(21005),
                                   @(21007),
                                   @(21008)
                                   ];
    NSArray *aErrorReasons = @[
                               @"The App Store could not read the JSON object you provided.",
                               @"The data in the receipt-data property was malformed or missing.",
                               @"The receipt could not be authenticated.",
                               @"The receipt server is not currently available.",
                               @"This receipt is from the test environment, but it was sent to the production environment for verification. Send it to the test environment instead.",
                               @"This receipt is from the production environment, but it was sent to the test environment for verification. Send it to the production environment instead."
                               ];
    
    // check if receipt status is ok
    NSNumber *receiptStatus = [dictionary valueForKey:@"status"];
    if (![receiptStatus  isEqualToNumber: @(0)]) {
        // receipt is not ok, error code must provide needed info
        NSInteger iStatusCodeIdx = -1;
        for (NSInteger i=0; i<aErrorStatusCodes.count; i++) {
            NSNumber *statusCode = [aErrorStatusCodes objectAtIndex:i];
            if ([statusCode isEqualToNumber:receiptStatus]) {
                iStatusCodeIdx = i;
                break;
            }
        }
        if (iStatusCodeIdx >= 0) {
            // error is in list
            NSLog(@"\n%s\nReceipt status error, reason:\n%@\n", __PRETTY_FUNCTION__, [aErrorReasons objectAtIndex:iStatusCodeIdx]);
            
        } else {
            // unknown error
            NSLog(@"\n%s\nReceipt status error, unknown error code: %@\n", __PRETTY_FUNCTION__, receiptStatus);
        }
        
        // Notify user that receipt is bad
        if (self.delegate) {
            [self.delegate purchaseManagerReceiptIsOk:NO];
        }
        return;
    }
    
    if (self.delegate) {
        [self.delegate purchaseManagerReceiptIsOk:YES];
    }
    
    NSLog(@"\n%s\nReceipt was checked. It`s ok. Full info:\n%@\n",__PRETTY_FUNCTION__, dictionary);
}

#pragma mark - Public Methods
- (void)purchaseProduct:(SKProduct *)product
{
    if ([SKPaymentQueue canMakePayments]) {
        
        // In-App Purchase is enabled on the Device
        
        SKPayment * payment = [SKPayment paymentWithProduct:product];
        
        [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
        [[SKPaymentQueue defaultQueue] addPayment:payment];
        
    } else {
        if (self.delegate) {
            [self.delegate purchaseManagerDisplayErrorMessage:NSLocalizedString(@"In-App purchase is disabled for this device.", nil)];
        }
    }
}

- (void)restorePurchase
{
    if ([SKPaymentQueue canMakePayments]) {
        // In-App Purchase is enabled on this device.
        
        [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
        
        // Request to restore previous purchases.
        [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
        
    } else {
        if (self.delegate) {
            [self.delegate purchaseManagerDisplayErrorMessage:NSLocalizedString(@"In-App purchase is disabled for this device.", nil)];
        }
    }
}

- (void)checkReceipt
{
    NSData *dataReceipt = [NSData dataWithContentsOfURL:[[NSBundle mainBundle] appStoreReceiptURL]];
    if (!dataReceipt) {
        NSLog(@"\n%s\nThere is no receipt in main bundle!\n",__PRETTY_FUNCTION__);
        return;
    }
    NSString *receiptBase64 = [dataReceipt base64EncodedStringWithOptions:0];
    
    // Create the JSON object that describes the request
    NSError *error;
    NSDictionary *requestContents = @{@"receipt-data": receiptBase64};
    
    NSData *requestData = [NSJSONSerialization dataWithJSONObject:requestContents
                                                          options:0
                                                            error:&error];
    
    if (!requestData) {
        NSLog(@"\n%s\nRequest data JSON Serialization failed with error:\n%@\n",__PRETTY_FUNCTION__, error);
        return;
    }
    
    NSString *sUrlSandbox = @"https://sandbox.itunes.apple.com/verifyReceipt";
    NSString *sUrlBuy = @"https://buy.itunes.apple.com/verifyReceipt";
    
    // Create a POST request with the receipt data.
    NSURL *storeURL = [NSURL URLWithString:(self.sandboxMode) ? sUrlSandbox : sUrlBuy];
    NSMutableURLRequest *storeRequest = [NSMutableURLRequest requestWithURL:storeURL];
    [storeRequest setHTTPMethod:@"POST"];
    [storeRequest setHTTPBody:requestData];
    
    // Make a connection to the iTunes Store on a background queue.
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:storeRequest
                                       queue:queue
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               if (connectionError) {
                                   NSLog(@"\n%s\nConnection error:\n%@\n",__PRETTY_FUNCTION__, connectionError);
                               } else {
                                   NSError *error;
                                   NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                                   if (!jsonResponse) {
                                       NSLog(@"\n%s\nJSON Serialization for response data failed with error:\n%@\n",__PRETTY_FUNCTION__, error);
                                   } else {
                                       [self parseReceiptDataFromDictionary:jsonResponse];
                                   }
                               }
                           }];
}

- (void)checkReceiptByServerWithAdditionalFields:(NSDictionary *)dictionary
                              primaryResponseKey:(NSString *)primaryKey
                              messageResponseKey:(NSString *)messageKey
{
    // check if server adress was set
    if (!self.serverAdress || ![self.serverAdress length]) {
        NSLog(@"\n%s\nThere is no Server Adress!\n", __PRETTY_FUNCTION__);
        
        return;
    }
    
    // check if primary key was set
    if (![primaryKey length]) {
        NSLog(@"\n%s\nThere is no Primary Key!\n", __PRETTY_FUNCTION__);
        return;
    }
    
    // get Data from receipt
    NSData *dataReceipt = [NSData dataWithContentsOfURL:[[NSBundle mainBundle] appStoreReceiptURL]];
    if (!dataReceipt) {
        NSLog(@"\n%s\nThere is no receipt in main bundle!\n",__PRETTY_FUNCTION__);
        return;
    }
    
    // provide main and additional parametrs
    NSMutableDictionary *mdPostKeysData = [NSMutableDictionary new];
    
    NSString *receiptBase64 = [dataReceipt base64EncodedStringWithOptions:0];
    
    NSString *encodedString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                                    NULL,
                                                                                                    (CFStringRef)receiptBase64,
                                                                                                    NULL,
                                                                                                    (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                                    kCFStringEncodingUTF8 ));
    
    [mdPostKeysData setObject:encodedString forKey:@"receipt-data"]; // default key
    
    // to make difference between test and real users
    [mdPostKeysData setObject:[NSNumber numberWithBool:self.sandboxMode] forKey:@"sandbox"];
    
    if (dictionary) { // if additional parametrs were set
        [mdPostKeysData addEntriesFromDictionary:dictionary];
    }
    
    // create request data
    //    NSError *error;
    //    NSData *requestData = [NSJSONSerialization dataWithJSONObject:mdPostKeysData
    //                                                          options:0
    //                                                            error:&error];
    //    if (!requestData) {
    //        NSLog(@"\n%s\nError:\n%@\n", __PRETTY_FUNCTION__, error);
    //        return;
    //    }
    
    // create POST request to server
    NSURL *serverURL = [NSURL URLWithString:self.serverAdress];
    if (!serverURL) {
        NSLog(@"\n%s\nWrong server adress format!\n", __PRETTY_FUNCTION__);
        return;
    }
    
    NSString *sParams = [mdPostKeysData urlEncodedString];
    
    NSMutableURLRequest *serverRequest = [NSMutableURLRequest requestWithURL:serverURL];
    [serverRequest setHTTPMethod:@"POST"];
    [serverRequest setHTTPBody:[sParams dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES]];
    
    // send background request
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:serverRequest
                                       queue:queue
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               if (connectionError) {
                                   NSLog(@"\n%s\nConnection error:\n%@\n",__PRETTY_FUNCTION__, connectionError);
                               } else {
                                   NSError *error;
                                   NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                                   if (!jsonResponse) {
                                       NSLog(@"\n%s\nJSON Serialization for response data failed with error:\n%@\n",__PRETTY_FUNCTION__, error);
                                   } else {
                                       
                                       // get receipt status
                                       BOOL bReceiptIsOk = NO;
                                       BOOL bPath = ([primaryKey rangeOfString:@"."].location != NSNotFound) ? YES : NO;
                                       if (bPath) {
                                           bReceiptIsOk = [((NSNumber *)[jsonResponse valueForKeyPath:primaryKey]) boolValue];
                                       } else {
                                           bReceiptIsOk = [((NSNumber *)[jsonResponse valueForKey:primaryKey]) boolValue];
                                       }
                                       
                                       // get message if it exists
                                       NSString *sMessage;
                                       if (messageKey && [messageKey length]) {
                                           bPath = ([messageKey rangeOfString:@"."].location != NSNotFound) ? YES : NO;
                                           
                                           if (bPath) {
                                               sMessage = [jsonResponse valueForKeyPath:messageKey];
                                           } else {
                                               sMessage = [jsonResponse valueForKey:messageKey];
                                           }
                                       }
                                       
                                       if (sMessage && [sMessage length]) {
                                           _serverMessage = sMessage;
                                       }
                                       
                                       dispatch_async(dispatch_get_main_queue(), ^{
                                           // notify about request result
                                           if (self.delegate) {
                                               [self.delegate purchaseManagerReceiptIsOk:bReceiptIsOk];
                                           }
                                       });
                                       
                                   }
                               }
                           }];
}

#pragma mark - Property Accessors
- (void)setDelegate:(id<SNAppPurchaseManagerDelegate>)delegate
{
    if (delegate) {
        if ([delegate conformsToProtocol:@protocol(SNAppPurchaseManagerDelegate)]) {
            _delegate = delegate;
        } else {
            NSLog(@"\n%s\nYou`re trying to set wrong delegate object\n", __PRETTY_FUNCTION__);
        }
    } else {
        _delegate = nil;
        NSLog(@"\n%s\nDelegate was nullified. You won`t get any message.\n", __PRETTY_FUNCTION__);
    }
}


- (void)setProductID:(NSString *)productID
{
    if (productID.length) {
        _productID = productID;
        
        // after getting product`s ID we can load it from server
        // if it is available
        [self startProductsRequestWithObject:self.productID];
    }
}

- (void)setProductsIDs:(NSSet *)productsIDs
{
    if ([productsIDs count]) {
        _productsIDs = productsIDs;
        
        // after getting IDs of products we can load them from server
        // if they are available
        [self startProductsRequestWithObject:self.productsIDs];
    }
}

#pragma mark - SKProducts Request Delegate
- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
    NSArray *products = response.products;
    
    if ([products count]) {
        for (SKProduct *product in products) {
            if (self.delegate) {
                [self.delegate purchaseManagerProductsAvailable:products];
            }
            NSLog(@"\n%s\n%@\n", __FUNCTION__, product);
        }
    }
    
    if ([response.invalidProductIdentifiers count]) {
        for (NSString *invalidProductId in response.invalidProductIdentifiers) {
            NSLog(@"\n%s\nInvalid product id: %@\n", __PRETTY_FUNCTION__, invalidProductId);
        }
    }
}

- (void)request:(SKRequest *)request didFailWithError:(NSError *)error
{
    if (error) {
        if (self.delegate) {
            [self.delegate purchaseManagerDisplayErrorMessage:NSLocalizedString(@"Purchase Manager FAILED to load list of products.", nil)];
            
        }
    }
    
    _productsRequest = nil;
}

#pragma mark - SKPayment Transaction Observer Delegate
// The transaction status of the SKPaymentQueue is sent here.
- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    for(SKPaymentTransaction *transaction in transactions) {
        switch (transaction.transactionState) {
                
            case SKPaymentTransactionStatePurchasing:
                // Item is still in the process of being purchased
                break;
                
            case SKPaymentTransactionStatePurchased:
                // Item was successfully purchased!
                
                [self completePaymentTransaction:transaction];
                break;
                
            case SKPaymentTransactionStateRestored:
                // Verified that user has already paid for this item.
                // Ideal for restoring item across all devices of this customer.
                
                [self restorePaymentTransaction:transaction];
                break;
                
            case SKPaymentTransactionStateFailed:
                // Purchase was either cancelled by user or an error occurred.
                
                [self failedPaymentTransaction:transaction];
                break;
            default:
                break;
        }
    }
}

// Called when SKPaymentQueue has finished sending restored transactions.
- (void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue
{
    if ([queue.transactions count] == 0) {
        // Queue does not include any transactions, so either user has not yet made a purchase
        // or the user's prior purchase is unavailable, so notify app (and user) accordingly.
        
        // Release the transaction observer since no prior transactions were found.
        [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
        
        if (self.delegate) {
            [self.delegate purchaseManagerFailedToRestoreProductWithError:nil];
        }
        
    } else {
        // Queue does contain one or more transactions, so return transaction data.
        // App should provide user with purchased product.
        
        for(SKPaymentTransaction *transaction in queue.transactions) {
            if (self.delegate) {
                [self.delegate purchaseManagerRestoredProductWithID:transaction.payment.productIdentifier];
            }
        }
    }
}

// Called if an error occurred while restoring transactions.
- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error
{
    // Restore was cancelled or an error occurred, so notify user.
    
    if (self.delegate) {
        [self.delegate purchaseManagerFailedToRestoreProductWithError:error];
    }
}

@end
