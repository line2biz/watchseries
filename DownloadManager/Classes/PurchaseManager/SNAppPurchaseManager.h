//
//  SNAppPurchaseManager.h
//  DownloadManager
//
//  Created by Denis on 13.02.15.
//  Copyright (c) 2015 Denis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>

@protocol SNAppPurchaseManagerDelegate;

@interface SNAppPurchaseManager : NSObject <SKProductsRequestDelegate, SKPaymentTransactionObserver>

/// delegate object will recieve process notifications
@property (strong, nonatomic) id <SNAppPurchaseManagerDelegate> delegate;

/// start property, process of product obtaining starts when it changes to non nil value
@property (strong, nonatomic) NSString *productID;

/// start property, process of products obtaining starts when it changes to non nil value
@property (strong, nonatomic) NSSet *productsIDs;

/// array of obtained SKProduct Objects, may be nil if there is no product IDs available
@property (strong, nonatomic) NSArray *products;

// must be set before receipt check
/// show if all aperations are in sandbox for test user
@property (assign, nonatomic) BOOL sandboxMode;

// if You will check receipt by server, set this property
@property (strong, nonatomic) NSString *serverAdress;

// properties to describe checkReceipt by server response; nil by default
@property (strong, nonatomic, readonly) NSString *serverMessage;

/// shared instance
+ (instancetype)defaultManager;

/// purchase product
- (void)purchaseProduct:(SKProduct *)product;

/// restore purchase
- (void)restorePurchase;

// after purchasing of product we must check receipt
/// check if receipt is right locally
- (void)checkReceipt;

/// check if receipt is right by server; by default there is one field called "receipt",
/// it contains NSData of the receipt from [NSBundle mainBundle], and another one is
/// "sandboxMode", it contains [NSNumber numberWithBool:] of manager`s mode.
- (void)checkReceiptByServerWithAdditionalFields:(NSDictionary *)dictionary
                              primaryResponseKey:(NSString *)primaryKey
                              messageResponseKey:(NSString *)messageKey;

@end



@protocol SNAppPurchaseManagerDelegate <NSObject>
@required

/// if there is an error, it must be processed
- (void)purchaseManagerDisplayErrorMessage:(NSString *)message;

/// reports about available products
- (void)purchaseManagerProductsAvailable:(NSArray *)productsAvailable;

// payment action results
- (void)purchaseManagerPurchasedProductWithID:(NSString *)productID;

- (void)purchaseManagerRestoredProductWithID:(NSString *)productID;

- (void)purchaseManagerFailedToPurchaseProductWithID:(NSString *)productID error:(NSError *)error;

- (void)purchaseManagerFailedToRestoreProductWithError:(NSError *)error;

// payment receipt`s check result
- (void)purchaseManagerReceiptIsOk:(BOOL)receiptIsOk;

@optional


@end
