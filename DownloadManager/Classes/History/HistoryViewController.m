//
//  HistoryViewController.m
//  DownloadManager
//
//  Created by Nils on 20.10.13.
//  Copyright (c) 2013 ComputerVerse Corporation. All rights reserved.
//

#import "HistoryViewController.h"
#import "DMManager.h"

@implementation HistoryViewController
@synthesize delegate = _delegate;

-(id)init {
    self = [super initWithStyle:UITableViewStylePlain];
    if (self) {
        @try {
            self.history = [NSMutableArray arrayWithArray:[DMHistory allHistory]];
        }
        @catch (NSException *exception) {
            NSLog(@"\n%s\nException:\n%@\n", __FUNCTION__, exception);
        }
        @finally {
            
        }
    }
    return self;
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
    [super setEditing:editing animated:animated];
    
    @try {
        [self.tableView setEditing:editing animated:animated];
        
        if (editing == YES) {
            [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"Delete All" style:UIBarButtonItemStyleDone target:self action:@selector(deleteAll)] animated:YES];
        } else {
            [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(close)] animated:YES];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"\n%s\nException:\n%@\n", __FUNCTION__, exception);
    }
    @finally {
        
    }
}

- (void)orientInferface:(UIInterfaceOrientation)newOrientation duration:(NSTimeInterval)duration {
//    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
//        if (duration == 0.0) {
//            self.automaticallyAdjustsScrollViewInsets = NO;
//        }
//        [self setNeedsStatusBarAppearanceUpdate];
//        [UIView animateWithDuration:duration animations:^{
//            if (UIInterfaceOrientationIsPortrait(newOrientation)) {
//                self.tableView.contentInset = UIEdgeInsetsMake(64, 0, 49, 0);
//                self.tableView.scrollIndicatorInsets = UIEdgeInsetsMake(64, 0, 49, 0);
//            } else {
//                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
//                    self.tableView.contentInset = UIEdgeInsetsMake(32, 0, 49, 0);
//                    self.tableView.scrollIndicatorInsets = UIEdgeInsetsMake(32, 0, 49, 0);
//                } else {
//                    self.tableView.contentInset = UIEdgeInsetsMake(64, 0, 49, 0);
//                    self.tableView.scrollIndicatorInsets = UIEdgeInsetsMake(64, 0, 49, 0);
//                }
//            }
//        }];
//    }
}

- (BOOL)prefersStatusBarHidden {
    if (!UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation]) && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        return YES;
    } else {
        return NO;
    }
}

- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation {
    return UIStatusBarAnimationSlide;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration {
    [self orientInferface:interfaceOrientation duration:duration];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem *backBtnItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(close)];
	self.navigationItem.leftBarButtonItem = backBtnItem;

    self.navigationItem.title = @"History";
    
    self.view.backgroundColor = [UIColor colorWithRed:0.176f green:0.176f blue:0.176f alpha:1.000f];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	
    [self orientInferface:[[UIApplication sharedApplication] statusBarOrientation] duration:0.0];
}

- (void)close {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (self.history.count == 0) {
        return 0;
    } else {
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.history.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
		
		cell.textLabel.textColor = [UIColor whiteColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.detailTextLabel.textColor = [UIColor colorWithRed:0.529f green:0.529f blue:0.529f alpha:1.000f];
    }
    
    if (indexPath.row % 2 == 1) {
        cell.backgroundColor = [UIColor colorWithRed:0.129f green:0.129f blue:0.129f alpha:1.000f];
    } else {
        cell.backgroundColor = [UIColor clearColor];
    }
	
    
    DMHistory *history = [self.history objectAtIndex:indexPath.row];
	
	NSString *titleText = history.title;
	[cell.textLabel setText:titleText];
	
	NSString *urlText = history.url;
	[cell.detailTextLabel setText:urlText];
	
	return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        DMHistory *history = [self.history objectAtIndex:indexPath.row];
		
		[DMHistory removeHistory:history];
		
		[self.history removeObjectAtIndex:indexPath.row];
        
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

- (void)deleteAll {
    if (self.history.count != 0) {
        
        [DMHistory removeAllHistory];
        
        [self.history removeAllObjects];
        
        [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    [self setEditing:NO animated:YES];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	NSString *urlText = ((DMHistory *)[self.history objectAtIndex:indexPath.row]).url;
	
    if ([_delegate respondsToSelector:@selector(loadUrl:)]) {
        [_delegate performSelector:@selector(loadUrl:) withObject:urlText];
    }
    
	[self dismissViewControllerAnimated:YES completion:NULL];
}

@end
