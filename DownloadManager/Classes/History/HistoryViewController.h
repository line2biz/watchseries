//
//  HistoryViewController.h
//  DownloadManager
//
//  Created by Nils on 20.10.13.
//  Copyright (c) 2013 ComputerVerse Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistoryViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSMutableArray* history;

@property (nonatomic, unsafe_unretained) id delegate;

@end
