//
//  MediaManager.h
//  DLManager
//
//  Created by Cai DaRong on 3/7/13.
//  Copyright (c) 2013 Cai DaRong. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum _MEDIATYPE {
	AUDIO = 1,
	VIDEO,
	PDF,
	IMAGE,
    FOLDER
} MEDIATYPE ;

@interface MediaManager : NSObject

+ (id) instance;

+ (MEDIATYPE) typeWithName:(NSString *)fileName;
+ (UIImage *) iconWithName:(NSString *)fileName;

@end
