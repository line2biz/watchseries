//
//  DirectoryManager.h
//  DLManager
//
//  Created by Cai DaRong on 1/29/13.
//  Copyright (c) 2013 Cai DaRong. All rights reserved.
//

#import <Foundation/Foundation.h>

#define DIRECTORY_COUNT	4

typedef enum _DirectoryType {
	DirectoryImage = 0,
	DirectoryVideo,
	DirectoryMusic,
	DirectoryPDF
} DirectoryType ;

typedef enum _FileType {
	FileUnknown = 0,
	FileImage,
	FileVideo,
	FileMusic,
	FilePDF,
} FileType ;

@interface DirectoryManager : NSObject

+ (id)instance;
+ (FileType)fileTypeWithPath:(NSString *)path;

+ (void)createDirectories;

- (NSString *)pathWithDirectoryType:(DirectoryType)type;
+ (NSString *)filePathWithType:(DirectoryType)type fileName:(NSString *)fileName;

- (NSMutableArray *)fileInfoWithType:(DirectoryType)type;

@end
