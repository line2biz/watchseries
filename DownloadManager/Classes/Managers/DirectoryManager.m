//
//  DirectoryManager.m
//  DLManager
//
//  Created by Cai DaRong on 1/29/13.
//  Copyright (c) 2013 Cai DaRong. All rights reserved.
//

#import "DirectoryManager.h"

DirectoryManager *g_directoryManager = nil;

NSString *directoryNames[DIRECTORY_COUNT] = {
	@"Image",
	@"Video",
	@"Music",
	@"PDF"
};

@implementation DirectoryManager

+ (id)instance {
	if (!g_directoryManager)
		g_directoryManager = [[DirectoryManager alloc] init];
	
	return g_directoryManager;
}

+ (void)createDirectories {
    for (int i = 0; i < DIRECTORY_COUNT; i++) {
        NSString *documentPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *directoryPath = [documentPath stringByAppendingPathComponent:directoryNames[i]];
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if ( ![fileManager isExecutableFileAtPath:directoryPath] ) {
            NSError *error;
            [[NSFileManager defaultManager] createDirectoryAtPath:directoryPath withIntermediateDirectories:NO attributes:nil error:&error];
            
            [[NSURL fileURLWithPath:directoryPath] setResourceValue:[NSNumber numberWithBool:YES] forKey:NSURLIsExcludedFromBackupKey error:nil];
        }
    }
}

+ (FileType)fileTypeWithPath:(NSString *)path {
	NSString *extension = path.pathExtension;
	NSDictionary *fileTypeDict = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:FileImage], @"png",
								  [NSNumber numberWithInt:FileImage], @"jpeg",
								  [NSNumber numberWithInt:FileImage], @"jpg",
								  [NSNumber numberWithInt:FileImage], @"bmp",
								  [NSNumber numberWithInt:FileVideo], @"mp4",
								  [NSNumber numberWithInt:FileVideo], @"mov",
								  [NSNumber numberWithInt:FileMusic], @"mp3",
								  [NSNumber numberWithInt:FileMusic], @"wav",
								  [NSNumber numberWithInt:FileMusic], @"m4a",
								  [NSNumber numberWithInt:FilePDF], @"pdf", nil];
	NSNumber *typeValue = [fileTypeDict objectForKey:extension];
	if ( !typeValue )
		return FileUnknown;
    return typeValue.integerValue;
}

- (NSString *)pathWithDirectoryType:(DirectoryType)type {
	NSString *documentPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *directoryPath = [documentPath stringByAppendingPathComponent:directoryNames[type]];
	
	return directoryPath;
}

+ (NSString *)filePathWithType:(DirectoryType)type fileName:(NSString *)fileName {
	NSString *documentPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *directoryPath = [documentPath stringByAppendingPathComponent:directoryNames[type]];
	
	NSString *filePath = [directoryPath stringByAppendingPathComponent:fileName];
	if ( [fileName.pathExtension isEqualToString:@""] )
	{
		if ( type == DirectoryImage )
			filePath = [filePath stringByAppendingPathExtension:@"png"];
		else if ( type == DirectoryMusic )
			filePath = [filePath stringByAppendingPathExtension:@"mp3"];
		else if ( type == DirectoryPDF )
			filePath = [filePath stringByAppendingPathExtension:@"pdf"];
		else if ( type == DirectoryVideo )
			filePath = [filePath stringByAppendingPathExtension:@"mp4"];
	}
	
	return filePath;
}

- (NSMutableArray *)fileInfoWithType:(DirectoryType)type {
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSString *directoryPath = [self pathWithDirectoryType:type];
	NSArray *aryFiles = [fileManager contentsOfDirectoryAtPath:directoryPath error:nil];
	
	NSMutableArray *aryFullPath = [NSMutableArray arrayWithArray:0];
	for (NSString *fileName in aryFiles)
	{
		if ( fileName.pathExtension.length > 0 )
		{
			NSString *filePath = [directoryPath stringByAppendingPathComponent:fileName];
			[aryFullPath addObject:filePath];
		}
	}
	
	return aryFullPath;
}

@end