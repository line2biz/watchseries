//
//  MediaManager.m
//  DLManager
//
//  Created by Cai DaRong on 3/7/13.
//  Copyright (c) 2013 Cai DaRong. All rights reserved.
//

#import "MediaManager.h"

MediaManager *g_mediaManager = nil;

@implementation MediaManager

+ (id) instance
{
	if ( !g_mediaManager )
		g_mediaManager = [[MediaManager alloc] init];
	
	return g_mediaManager;
}

+ (MEDIATYPE) typeWithName:(NSString *)fileName
{
    @try {
        NSString *extension = fileName.pathExtension.lowercaseString;
        static NSMutableDictionary *mimeDict = nil;
        if (mimeDict == nil)
        {
            mimeDict = [[NSMutableDictionary alloc] init];
            
            /// photo
            [mimeDict setObject:[NSNumber numberWithInt:IMAGE] forKey:@"jpeg"];
            [mimeDict setObject:[NSNumber numberWithInt:IMAGE] forKey:@"jpe"];
            [mimeDict setObject:[NSNumber numberWithInt:IMAGE] forKey:@"jpg"];
            [mimeDict setObject:[NSNumber numberWithInt:IMAGE] forKey:@"png"];
            [mimeDict setObject:[NSNumber numberWithInt:IMAGE] forKey:@"bmp"];
            
            /// audio
            [mimeDict setObject:[NSNumber numberWithInt:AUDIO] forKey:@"mp3"];
            [mimeDict setObject:[NSNumber numberWithInt:AUDIO] forKey:@"wav"];
            [mimeDict setObject:[NSNumber numberWithInt:AUDIO] forKey:@"m4a"];
            
            /// video
            [mimeDict setObject:[NSNumber numberWithInt:VIDEO] forKey:@"mov"];
            [mimeDict setObject:[NSNumber numberWithInt:VIDEO] forKey:@"mp4"];
            
            /// pdf
            [mimeDict setObject:[NSNumber numberWithInt:PDF] forKey:@"pdf"];
            
            /// folder
            [mimeDict setObject:[NSNumber numberWithInt:FOLDER] forKey:@""];
        }
        
        MEDIATYPE mediaType = [[mimeDict objectForKey:extension] integerValue];
        
        return mediaType;
    }
    @catch (NSException *exception) {
        NSLog(@"\n%s\nException:\n%@\n", __FUNCTION__, exception);
    }
    @finally {
        
    }
}

+ (id) iconWithName:(NSString *)fileName
{
    @try {
        MEDIATYPE type = [MediaManager typeWithName:fileName];
        
        switch (type)
        {
            case PDF:
                return  [UIImage imageNamed:@"pdfIcon.png"];
            case IMAGE:
                return  [UIImage imageNamed:@"imageIcon.png"];
            case VIDEO:
                return  [UIImage imageNamed:@"tableMovieIcon.png"];
            case AUDIO:
                return [UIImage imageNamed:@"tableMusicIcon.png"];
            case FOLDER:
                return [UIImage imageNamed:@"file.png"];
                
            default:
                break;
        }
        
        return [UIImage imageNamed:@"unknownIcon.png"];
    }
    @catch (NSException *exception) {
        NSLog(@"\n%s\nException:\n%@\n", __FUNCTION__, exception);
    }
    @finally {
        
    }
}

@end
