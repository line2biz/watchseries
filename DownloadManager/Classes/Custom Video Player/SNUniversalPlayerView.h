//
//  SNUniversalPlayerView.h
//  DownloadManager
//
//  Created by Denis on 05.05.15.
//  Copyright (c) 2015 ComputerVerse Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Media;

/* Player State types */
typedef NS_ENUM(NSUInteger, PlayerState) {
    PlayerStateUninited,
    PlayerStateInited,
    PlayerStatePlaying,
    PlayerStatePaused
};


/* Player toolbars State types */
typedef NS_ENUM(NSUInteger, PlayerToolbarsState) {
    PlayerToolbarsStateHidden,
    PlayerToolbarsStateShown
};


/* Player Screen types */
typedef NS_ENUM(NSUInteger, PlayerScreenType) {
    PlayerScreenTypeLocal,
    PlayerScreenTypeAirPlay,
    PlayerScreenTypeChromecast
};


/* Delegate protocol for universal player view */
@protocol UniversalPlayerViewDelegate;



/** Universal player for video, represented by UIView Subclass */
@interface SNUniversalPlayerView : UIView

#pragma mark Properties

/** Media object to play */
@property (nonatomic, strong) Media *mediaObject;

/** Local player elapsed time. */
@property (nonatomic, assign) NSInteger playbackTime;

/** State of player */
@property (nonatomic, assign) PlayerState state;

/** State of player toolbars */
@property (nonatomic, assign) PlayerToolbarsState toolbarsState;

/** Target Screen type */
@property (nonatomic, assign) PlayerScreenType screenType;

/** Delegate object */
@property (nonatomic, weak) id <UniversalPlayerViewDelegate> delegate;

/** Title of cancel button */
@property (nonatomic, strong) NSString *cancelButtonTitle;

/** Chrome Cast Button */
@property (nonatomic, strong) UIButton *castButton;

#pragma mark Methods

/** check if chrome cast is on */
- (BOOL)isChromeCasted;

/** check if airplay is on */
- (BOOL)isAirPlayON;

/** start or stop playing */
- (void)play:(BOOL)play;

/** Chromecast Volume Changed */
- (void)setCastVolume:(CGFloat)castVolume;

@end



@protocol UniversalPlayerViewDelegate <NSObject>
@optional

/** send message to delegate object that player cancel button was tapped */
- (void)universalPlayer:(SNUniversalPlayerView *)playerView didTapCancelButton:(UIButton *)sender;

/** send message to delegate object that player share button was tapped */
- (void)universalPlayer:(SNUniversalPlayerView *)playerView didTapShareButton:(UIButton *)sender;

/** send message to delegate object that player download button was tapped */
- (void)universalPlayer:(SNUniversalPlayerView *)playerView didTapDownloadButton:(UIButton *)sender;

/** send message to delegate object that player export button was tapped */
- (void)universalPlayer:(SNUniversalPlayerView *)playerView didTapExportButton:(UIButton *)sender;

/** send message to delegate object that player back button was tapped */
- (void)universalPlayer:(SNUniversalPlayerView *)playerView didTapBackButton:(UIButton *)sender;

/** send message to delegate object that player forward button was tapped */
- (void)universalPlayer:(SNUniversalPlayerView *)playerView didTapForwardButton:(UIButton *)sender;

/** get permision to show download action */
- (BOOL)downloadIsAvailableForUniversalPlayer;

/** get permision to show export action */
- (BOOL)exportIsAvailableForUniversalPlayer;

/** get permision to activate back/foward actions */
- (BOOL)fileChangeIsAvailableForUniversalPlayer;

@end



