//
//  TestController.m
//  DownloadManager
//
//  Created by Denis on 06.05.15.
//  Copyright (c) 2015 ComputerVerse Corporation. All rights reserved.
//

#import "SNUniversalPlayerController.h"
#import "SNUniversalPlayerView.h"

// chrome cast
#import "Media.h"
#import "ChromecastDeviceController.h"
#import "GCKMediaInformation+LocalMedia.h"

// share options
#import "OpenFacebookPage.h"
#import "BrowserViewController.h"

// export option
#import <AssetsLibrary/AssetsLibrary.h>
#import "MBProgressHUD.h"

@interface SNUniversalPlayerController () <ChromecastControllerDelegate, UIAlertViewDelegate, UIActionSheetDelegate, UniversalPlayerViewDelegate>
{
    __weak ChromecastDeviceController* _chromecastController;
    NSInteger _loadIdx;
}

@property (weak, nonatomic) IBOutlet SNUniversalPlayerView *playerView;
@end

@implementation SNUniversalPlayerController

#pragma mark - Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _loadIdx = 0;
    
    [self.playerView setDelegate:self];
    
    _chromecastController = [ChromecastDeviceController sharedInstance];
    
    // register for chrome cast device volume change notification
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receivedVolumeChangedNotification:)
                                                 name:@"Volume changed"
                                               object:_chromecastController];
    
    // Listen to orientation changes.
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // Assign ourselves as delegate ONLY in viewWillAppear of a view controller.
    _chromecastController.delegate = self;
    
    // Add the cast icon to our nav bar.
    [[ChromecastDeviceController sharedInstance] manageViewController:self icon:YES toolbar:NO];
    
    // register for magic stick notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didTapMagicStick) name:@"ActivityLink2" object:nil];
    
    // setup player view
    if (!_loadIdx) {
        [self.playerView setCancelButtonTitle: NSLocalizedString(@"Cancel", nil)];
        [self.playerView setCastButton:[[ChromecastDeviceController sharedInstance] castButton]];
        [self.playerView setMediaObject:self.mediaToPlay];
        [self.playerView play:YES];
        
        _loadIdx++;
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    // remove obervers
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    // notify delegate
    if ([self.delegate respondsToSelector:@selector(playerControllerWillDissappear)]) {
        [self.delegate playerControllerWillDissappear];
    }
}

#pragma mark - Status Bar
/* Prefer hiding the status bar if we're full screen. */
- (BOOL)prefersStatusBarHidden
{
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (UIInterfaceOrientationIsLandscape(orientation)) {
        return YES;
    }
    return NO;
}

- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation
{
    return UIStatusBarAnimationFade;
}

#pragma mark - Private Methods
- (void)receivedVolumeChangedNotification:(NSNotification *) notification
{
    ChromecastDeviceController *deviceController = (ChromecastDeviceController *) notification.object;
    NSLog(@"Got volume changed notification: %g", deviceController.deviceVolume);
    
    [self.playerView setCastVolume:_chromecastController.deviceVolume];
}

- (NSArray *)listOfAvailableMoviesURLs
{
    // check if we are plying item from internet
    if (!self.sourceSiteURL) {
        // item is local
        // get its URL
        NSURL *itemURL = self.mediaToPlay.URL;
        
        // get url of folder item is saved in
        NSURL *folderURL = [itemURL URLByDeletingLastPathComponent];
        
        // get contents of folder
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        NSError *contentsError = nil;
        NSArray *aContents = [fileManager contentsOfDirectoryAtPath:folderURL.path error:&contentsError];
        if (contentsError) {
            NSLog(@"\n%s\nGetting folder content error:\n%@\n", __PRETTY_FUNCTION__, contentsError);
        }
        else {
            // we`ve got file names
            if ([aContents count]) {
                NSMutableArray *maURLsList = [NSMutableArray arrayWithCapacity:[aContents count]];
                for (NSString *sFileName in aContents) {
                    [maURLsList addObject:[folderURL URLByAppendingPathComponent:sFileName]];
                }
                return maURLsList;
            }
        }
    }
    
    return nil;
}

#pragma mark - Controller Methods
- (void)dismiss
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didTapDownload
{
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"_filename", @"")
                                                        message:nil
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"_cancel", @"")
                                              otherButtonTitles:NSLocalizedString(@"_Free_Download", @""),nil];
    
    alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    alertView.tag = 11;
    [alertView show];
}

- (void)didTapShare
{
    NSArray *activityItems = @[self.sourceSiteURL];
    
    OpenFacebookPage *page1, *page2, *page3;
    
    page1 = [[OpenFacebookPage alloc] init];
    page1.nType = 0;
    page2 = [[OpenFacebookPage alloc] init];
    page2.nType = 1;
    page3 = [[OpenFacebookPage alloc] init];
    page3.nType = 2;
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:@[page1, page2, page3]];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        [activityVC.view setTintColor:[UIColor blackColor]];
    }
    
    [self presentViewController:activityVC animated:YES completion:nil];
}

- (void)didTapMagicStick
{
    NSString *currentURL = [self.sourceSiteURL absoluteString];
    NSString *thenewurl = [NSString stringWithFormat:@"http://convert2mp3.net/c-mp4.php?url=%@", [currentURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    SNVideoPlayerManager * delegate = ((SNVideoPlayerManager *)self.delegate);
    BrowserViewController *masterDelegate = (BrowserViewController *)delegate.delegate;
    [masterDelegate loadUrl:thenewurl];
    [self dismiss];
}

- (void)didTapExportButton
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Export", @"")
                                                             delegate:self
                                                    cancelButtonTitle:NSLocalizedString(@"abbrechen", @"")
                                               destructiveButtonTitle:NSLocalizedString(@"Exporttext", @"")
                                                    otherButtonTitles:nil];
    [actionSheet showFromTabBar:self.tabBarController.tabBar];
}

- (void)didTapPreviousButton
{
    NSArray *aContentsURLs = [self listOfAvailableMoviesURLs];
    NSInteger iCurrentMediaIndex = -1;
    // get current item URL index
    iCurrentMediaIndex = [aContentsURLs indexOfObject:self.mediaToPlay.URL];
    
    // check if we`ve got right URL index
    if (iCurrentMediaIndex > -1) {
        // right item URL index
        NSInteger iItemsTotal = [aContentsURLs count];
        
        // next item URL index
        if (iCurrentMediaIndex == 0) {
            iCurrentMediaIndex = iItemsTotal - 1;
        }
        else {
            iCurrentMediaIndex -= 1;
        }
        
        // new URL of item
        NSURL *newURL = (NSURL *)[aContentsURLs objectAtIndex:iCurrentMediaIndex];
        
        // change media object URL properties
        Media *media = [[Media alloc] init];
        media.title = newURL.lastPathComponent;
        media.subtitle = @"";
        media.mimeType = @"video/mp4";
        media.descrip = @"";
        media.thumbnailURL = nil;
        media.posterURL = nil;
        media.URL = newURL;
        media.tracks = nil;
        
        self.mediaToPlay = media;
        
        // set new media to player view
        [self.playerView setMediaObject:self.mediaToPlay];
    }
    else {
        NSLog(@"\n%s\nWrong index of item URL\nCurrent URL: %@\nList of URLs:\n%@\n", __PRETTY_FUNCTION__, self.mediaToPlay.URL, aContentsURLs);
    }
}

- (void)didTapNextButton
{
    NSArray *aContentsURLs = [self listOfAvailableMoviesURLs];
    NSInteger iCurrentMediaIndex = -1;
    // get current item URL index
    iCurrentMediaIndex = [aContentsURLs indexOfObject:self.mediaToPlay.URL];
    
    // check if we`ve got right URL index
    if (iCurrentMediaIndex > -1) {
        // right item URL index
        NSInteger iItemsTotal = [aContentsURLs count];
        
        // next item URL index
        iCurrentMediaIndex = (iCurrentMediaIndex + 1) % iItemsTotal;
        
        // new URL of item
        NSURL *newURL = (NSURL *)[aContentsURLs objectAtIndex:iCurrentMediaIndex];
        
        // change media object URL properties
        Media *media = [[Media alloc] init];
        media.title = newURL.lastPathComponent;
        media.subtitle = @"";
        media.mimeType = @"video/mp4";
        media.descrip = @"";
        media.thumbnailURL = nil;
        media.posterURL = nil;
        media.URL = newURL;
        media.tracks = nil;
        
        self.mediaToPlay = media;
        
        // set new media to player view
        [self.playerView setMediaObject:self.mediaToPlay];
    }
    else {
        NSLog(@"\n%s\nWrong index of item URL\nCurrent URL: %@\nList of URLs:\n%@\n", __PRETTY_FUNCTION__, self.mediaToPlay.URL, aContentsURLs);
    }
    
}

#pragma mark - Universal Player View Delegate
- (void)universalPlayer:(SNUniversalPlayerView *)playerView didTapCancelButton:(UIButton *)sender
{
    [self dismiss];
}

- (void)universalPlayer:(SNUniversalPlayerView *)playerView didTapShareButton:(UIButton *)sender
{
    [self didTapShare];
}

- (void)universalPlayer:(SNUniversalPlayerView *)playerView didTapDownloadButton:(UIButton *)sender
{
    [self didTapDownload];
}

- (void)universalPlayer:(SNUniversalPlayerView *)playerView didTapExportButton:(UIButton *)sender
{
    [self didTapExportButton];
}

- (void)universalPlayer:(SNUniversalPlayerView *)playerView didTapBackButton:(UIButton *)sender
{
    [self didTapPreviousButton];
}

- (void)universalPlayer:(SNUniversalPlayerView *)playerView didTapForwardButton:(UIButton *)sender
{
    [self didTapNextButton];
}

- (BOOL)exportIsAvailableForUniversalPlayer
{
    return !self.sourceSiteURL;
}

- (BOOL)downloadIsAvailableForUniversalPlayer
{
    return self.showDownload;
}

- (BOOL)fileChangeIsAvailableForUniversalPlayer
{
    return ([self listOfAvailableMoviesURLs].count > 1);
}

#pragma mark - ChromecastControllerDelegate

- (void)didConnectToDevice:(GCKDevice *)device
{
    NSLog(@"\n%s\nChromecast: Did connect to device\n", __PRETTY_FUNCTION__);
}

- (void)didDisconnect
{
    NSLog(@"\n%s\nChromecast: Did disconnect from device\n", __PRETTY_FUNCTION__);
}

- (BOOL)shouldDisplayModalDeviceController
{
    NSLog(@"\n%s\n", __PRETTY_FUNCTION__);
    
    [self.playerView play:NO];
    return YES;
}

#pragma mark - Alert View Delegate
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 11) {
        if (buttonIndex != alertView.cancelButtonIndex) {
            NSString *fileName = [alertView textFieldAtIndex:0].text;
            NSString *filePath = [DirectoryManager filePathWithType:DirectoryVideo fileName:fileName];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"download" object:nil userInfo:@{@"url": self.mediaToPlay.URL, @"path": filePath}];
        }
    }
}

#pragma mark - Action Sheet Delegate
- (void) actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    @try {
        if (buttonIndex == 0) {
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
            [library writeVideoAtPathToSavedPhotosAlbum:[[SNVideoPlayerManager sharedManager] currentURL]
                                        completionBlock:^(NSURL *assetURL, NSError *error) {
                                            [MBProgressHUD hideHUDForView:self.view animated:YES];
                                        }];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"\n%s\nException:\n%@\n", __FUNCTION__, exception);
    }
    @finally {
        
    }
}


@end
