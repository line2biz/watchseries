//
//  TestController.h
//  DownloadManager
//
//  Created by Denis on 06.05.15.
//  Copyright (c) 2015 ComputerVerse Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Media;

@protocol UniversalPlayerControllerDelegate;

@interface SNUniversalPlayerController : UIViewController

/** The media object being played on Chromecast device. Set this before presenting the view. */
@property(strong, nonatomic) Media *mediaToPlay;

/** property to show download option */
@property (assign, nonatomic) BOOL showDownload;

/** URL of site player was called from */
@property (strong, nonatomic) NSURL *sourceSiteURL;

/** Delegate object */
@property (weak, nonatomic) id <UniversalPlayerControllerDelegate> delegate;

@end


@protocol UniversalPlayerControllerDelegate <NSObject>

@optional
- (void)playerControllerWillDissappear;

@end
