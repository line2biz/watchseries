//
//  SNVideoPlayerManager.m
//  DownloadManager
//
//  Created by Denis on 27.03.15.
//  Copyright (c) 2015 ComputerVerse Corporation. All rights reserved.
//

#import "SNVideoPlayerManager.h"

#import "Media.h"
#import "LocalPlayerViewController.h"
#import "PlayerNavController.h"

#import "ChromecastDeviceController.h"
#import "GCKMediaInformation+LocalMedia.h"

#import "SNRemoteServerManager.h"

// new player controller
#import "SNUniversalPlayerController.h"

@interface SNVideoPlayerManager ()<UniversalPlayerControllerDelegate>

@property (strong, nonatomic) Media *lastPlayedMedia;
@property (strong, nonatomic) NSURL *lastSourceURL;
@property (assign, nonatomic) BOOL lastDownloadGranted;

@end

@implementation SNVideoPlayerManager

#pragma mark - Share Instance
+ (instancetype)sharedManager
{
    static SNVideoPlayerManager *VPM = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        VPM = [[SNVideoPlayerManager alloc] init];
    });
    return VPM;
}

#pragma mark - Life Cycle
- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

#pragma mark - Private Methods


#pragma mark - Public Methods
- (void)startLocalPlaybackForVideoWithURL:(NSURL *)videoURL
{
    [self startLocalPlaybackForVideoWithURL:videoURL fromURL:nil];
}


- (void)startLocalPlaybackForVideoWithURL:(NSURL *)videoURL fromURL:(NSURL *)sourceURL
{
    self.playerIsShown = YES;
    if (videoURL == nil) {
        if (self.delegate) {
            if ([self.delegate respondsToSelector:@selector(shouldOpenVideoInLocalPlayer)]) {
                [self.delegate shouldOpenVideoInLocalPlayer];
            }
        }
    } else {
        self.currentURL = videoURL;
        
        // exception check
        BOOL bExceptionSite = NO;
        if (sourceURL) {
            if ([self.delegate respondsToSelector:@selector(forbiddenSitesList)]) {
                NSArray *aSiteExceptions = [self.delegate forbiddenSitesList];
                NSString *sURL = [NSString stringWithFormat:@"%@",sourceURL];
                for (NSString *site in aSiteExceptions) {
                    if ([sURL rangeOfString:site].location != NSNotFound) {
                        bExceptionSite = YES;
                        break;
                    }
                }
            } else {
                bExceptionSite = YES;
            }
        } else {
            bExceptionSite = YES;
        }
        
        // if download option is on
        if (![[NSUserDefaults standardUserDefaults] boolForKey:@"switch7"])
            bExceptionSite = YES;
        
        // create media object
        Media *media = [[Media alloc] init];
        
        media.title = [videoURL lastPathComponent];
        media.subtitle = @"";
        media.mimeType = @"video/mp4";
        media.descrip = @"";
        media.thumbnailURL = nil;
        media.posterURL = nil;
        media.URL = videoURL;
        media.tracks = nil;
        
        // save settings objects
        self.lastPlayedMedia = media;
        self.lastSourceURL = sourceURL;
        self.lastDownloadGranted = !bExceptionSite;
        
        // prepare player controller
        SNUniversalPlayerController *uniPlayerController = [self universalControllerWithMedia:media
                                                                                    sourceURL:sourceURL
                                                                              downloadGranted:!bExceptionSite];
        
        // notify delegate
        if (self.delegate) {
            if ([self.delegate respondsToSelector:@selector(customPlayerControllerWillBeShown)]) {
                [self.delegate customPlayerControllerWillBeShown];
            }
        }
        
        // show controller
        [((UIViewController *)self.delegate) presentViewController:uniPlayerController animated:YES completion:nil];
    }
}

- (void)showPlayerController
{
    if (!self.lastPlayedMedia) {
        return;
    }
    
    // prepare player controller
    SNUniversalPlayerController *uniPlayerController = [self universalControllerWithMedia:self.lastPlayedMedia
                                                                                sourceURL:self.lastSourceURL
                                                                          downloadGranted:self.lastDownloadGranted];
    
    // notify delegate
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(customPlayerControllerWillBeShown)]) {
            [self.delegate customPlayerControllerWillBeShown];
        }
    }
    
    // show controller
    [((UIViewController *)self.delegate) presentViewController:uniPlayerController animated:YES completion:nil];
}

- (SNUniversalPlayerController *)universalControllerWithMedia:(Media *)media sourceURL:(NSURL *)sourceURL downloadGranted:(BOOL)download
{
    SNUniversalPlayerController *uniPlayerController = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:NSStringFromClass([SNUniversalPlayerController class])];
    
    uniPlayerController.mediaToPlay = media;
    uniPlayerController.showDownload = download;
    uniPlayerController.sourceSiteURL = sourceURL;
    uniPlayerController.delegate = self;
    
    return uniPlayerController;
}

#pragma mark - Univarsal Player Controller Delegate
- (void)playerControllerWillDissappear
{
    self.playerIsShown = NO;
    self.currentURL = nil;
}


@end
