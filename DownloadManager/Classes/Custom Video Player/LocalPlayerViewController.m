// Copyright 2014 Google Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#import "AppDelegate.h"
//#import "CastInstructionsViewController.h"
#import "ChromecastDeviceController.h"
#import "GCKMediaInformation+LocalMedia.h"
#import "LocalPlayerViewController.h"

// share options
#import "OpenFacebookPage.h"
#import "BrowserViewController.h"

// export option
#import <AssetsLibrary/AssetsLibrary.h>
#import "MBProgressHUD.h"


@interface LocalPlayerViewController () <ChromecastControllerDelegate, UIAlertViewDelegate, UIActionSheetDelegate>

{
    NSInteger _loadIdx;
}

/* Whether to reset the edges on disappearing. */
@property(nonatomic) BOOL resetEdgesOnDisappear;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTop;

@end

@implementation LocalPlayerViewController

#pragma mark - ViewController lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _loadIdx = 0;
    
    self.view.backgroundColor = [UIColor darkGrayColor];
    [self.navigationItem setHidesBackButton:YES];
    if (self.showDownload == NO) {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", nil)
                                                                                 style:UIBarButtonItemStylePlain
                                                                                target:self
                                                                                action:@selector(dismiss)];
        
    } else {
        
        UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", nil)
                                                                     style:UIBarButtonItemStylePlain
                                                                    target:self
                                                                    action:@selector(dismiss)];
        
        UIBarButtonItem *downloadItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"_Free_Download", nil)
                                                                         style:UIBarButtonItemStylePlain
                                                                        target:self
                                                                        action:@selector(didTapDownload)];
        
        self.navigationItem.leftBarButtonItems = @[
                                                   backItem,
                                                   downloadItem
                                                   ];
    }
    
    if (![self.playerView fullscreen]) {
        CGRect screenBounds = [UIScreen mainScreen].bounds;
        self.constraintTop.constant = screenBounds.size.height / 2 - CGRectGetHeight(self.playerView.frame)/2;
    }
    else {
        self.constraintTop.constant = 0;
    }
    
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
    
  [self.playerView setMedia:self.mediaToPlay];
  _resetEdgesOnDisappear = YES;

  // Listen to orientation changes.
  [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
  [[NSNotificationCenter defaultCenter] addObserver:self
                                           selector:@selector(deviceOrientationDidChange:)
                                               name:UIDeviceOrientationDidChangeNotification
                                             object:nil];
  _playerView.delegate = self;
  [self syncTextToMedia];
  if (self.playerView.fullscreen) {
    [self hideNavigationBar:YES];
  }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didTapMagicStick) name:@"ActivityLink2" object:nil];
    
    
    // start playing
    if (!_loadIdx) {
        _loadIdx++;
        [self.playerView startPlayback];
    }

  // Assign ourselves as delegate ONLY in viewWillAppear of a view controller.
  [ChromecastDeviceController sharedInstance].delegate = self;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    if (_playerView.playingLocally) {
        [_playerView pause];
    }
    if (_resetEdgesOnDisappear) {
        [self setNavigationBarStyle:LPVNavBarDefault];
    }
    
    if (self.delegate) {
        [self.delegate playerControllerWillDissappear];
    }
}


- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];
  [[ChromecastDeviceController sharedInstance] manageViewController:self icon:YES toolbar:YES];
    
    NSMutableArray *maRightBarButtons = [NSMutableArray arrayWithCapacity:2];
    if (self.navigationItem.rightBarButtonItem) {
        [maRightBarButtons addObject:self.navigationItem.rightBarButtonItem];
    }
    if (self.sourceSiteURL) {
        
        UIBarButtonItem *shareItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction
                                                                                   target:self
                                                                                   action:@selector(didTapShare)];
        [maRightBarButtons addObject:shareItem];
    }
    else {
        UIBarButtonItem *exportItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Export", @"")
                                                                       style:UIBarButtonItemStylePlain
                                                                      target:self
                                                                      action:@selector(didTapExportButton)];
        [maRightBarButtons addObject:exportItem];
    }
    self.navigationItem.rightBarButtonItems = maRightBarButtons;
}

#pragma mark - Orientation Change Handler
- (void)deviceOrientationDidChange:(NSNotification *)notification
{
    
    if (![self.playerView fullscreen]) {
        CGRect screenBounds = [UIScreen mainScreen].bounds;
        self.constraintTop.constant = screenBounds.size.height / 2 - CGRectGetHeight(self.playerView.frame)/2 - CGRectGetHeight(self.navigationController.navigationBar.frame) - (([self prefersStatusBarHidden]) ? 0 : 24);
    }
    else {
        self.constraintTop.constant = 0;
    }
    [self.playerView orientationChanged];

  UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
  if (!UIInterfaceOrientationIsLandscape(orientation) || !self.playerView.playingLocally) {
    [self setNavigationBarStyle:LPVNavBarDefault];
  }
}

#pragma mark - Status Bar
/* Prefer hiding the status bar if we're full screen. */
- (BOOL)prefersStatusBarHidden {
  return self.playerView.fullscreen;
}

- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation
{
    return UIStatusBarAnimationFade;
}

#pragma mark - Managing the detail item

- (void)setMediaToPlay:(id)newMediaToPlay {
  if (_mediaToPlay != newMediaToPlay) {
    _mediaToPlay = newMediaToPlay;
    [self syncTextToMedia];
  }
}

- (void)syncTextToMedia {
  self.mediaTitle.text = self.mediaToPlay.title;
  self.mediaSubtitle.text = self.mediaToPlay.subtitle;
  self.mediaDescription.text = self.mediaToPlay.descrip;
}

#pragma mark - LocalPlayerController

/* Signal the requested style for the view. */
- (void)setNavigationBarStyle:(LPVNavBarStyle)style
{
  if (style == LPVNavBarDefault) {
    self.edgesForExtendedLayout = UIRectEdgeAll;
    [self hideNavigationBar:NO];
    [self.navigationController.navigationBar setBackgroundImage:nil
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = nil;
    [[UIApplication sharedApplication] setStatusBarHidden:NO
                                            withAnimation:UIStatusBarAnimationFade];
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    _resetEdgesOnDisappear = NO;
  } else if(style == LPVNavBarTransparent) {
    self.edgesForExtendedLayout = UIRectEdgeNone;
      
      UIGraphicsBeginImageContext(self.navigationController.navigationBar.frame.size);
      
      CGContextRef context = UIGraphicsGetCurrentContext();
      
      [[UIColor clearColor] set];
      
      UIImage *clearImage = UIGraphicsGetImageFromCurrentImageContext();
      
      UIGraphicsEndImageContext();
      
      
    [self.navigationController.navigationBar setBackgroundImage:clearImage
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = clearImage;
    [[UIApplication sharedApplication] setStatusBarHidden:YES
                                            withAnimation:UIStatusBarAnimationFade];
    // Disable the swipe gesture if we're fullscreen.
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    _resetEdgesOnDisappear = YES;
  }
}

/* Request the navigation bar to be hidden or shown. */
- (void)hideNavigationBar:(BOOL)hide
{
  [self.navigationController.navigationBar setHidden:hide];
}

/* Play has been pressed in the LocalPlayerView. */
- (BOOL)continueAfterPlayButtonClicked {
  ChromecastDeviceController *controller = [ChromecastDeviceController sharedInstance];
  if (controller.isConnected) {
    [self castCurrentMedia:0];
    return NO;
  }
  NSTimeInterval pos =
      [controller streamPositionForPreviouslyCastMedia:[self.mediaToPlay.URL absoluteString]];
  if (pos > 0) {
    _playerView.playbackTime = pos;
    // We are playing locally, so don't try and reconnect.
    [controller clearPreviousSession];
  }
  return YES;
}

- (void)castCurrentMedia:(NSTimeInterval)from {
  if (from < 0) {
    from = 0;
  }
  ChromecastDeviceController *controller = [ChromecastDeviceController sharedInstance];
  GCKMediaInformation *media =
      [GCKMediaInformation mediaInformationFromLocalMedia:self.mediaToPlay];
  UIViewController *cvc = [controller castViewControllerForMedia:media
                                                withStartingTime:from];
  [self.navigationController pushViewController:cvc animated:YES];
}

#pragma mark - Controller Methods
- (void)dismiss
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)didTapDownload
{
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"_filename", @"")
                                                        message:nil
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"_cancel", @"")
                                              otherButtonTitles:NSLocalizedString(@"_Free_Download", @""),nil];
    
    alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    alertView.tag = 11;
    [alertView show];
}

- (void)didTapShare
{
    NSArray *activityItems = @[self.sourceSiteURL];
    
    OpenFacebookPage *page1, *page2, *page3;
    
    page1 = [[OpenFacebookPage alloc] init];
    page1.nType = 0;
    page2 = [[OpenFacebookPage alloc] init];
    page2.nType = 1;
    page3 = [[OpenFacebookPage alloc] init];
    page3.nType = 2;
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:@[page1, page2, page3]];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        [activityVC.view setTintColor:[UIColor blackColor]];
    }
    
    [self presentViewController:activityVC animated:YES completion:nil];
}

- (void)didTapMagicStick
{
    NSString *currentURL = [self.sourceSiteURL absoluteString];
    NSString *thenewurl = [NSString stringWithFormat:@"http://convert2mp3.net/c-mp4.php?url=%@", [currentURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    SNVideoPlayerManager * delegate = ((SNVideoPlayerManager *)self.delegate);
    BrowserViewController *masterDelegate = (BrowserViewController *)delegate.delegate;
    [masterDelegate loadUrl:thenewurl];
    [self dismiss];
}

- (void)didTapExportButton
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Export", @"")
                                                             delegate:self
                                                    cancelButtonTitle:NSLocalizedString(@"abbrechen", @"")
                                               destructiveButtonTitle:NSLocalizedString(@"Exporttext", @"")
                                                    otherButtonTitles:nil];
    [actionSheet showFromTabBar:self.tabBarController.tabBar];
}

#pragma mark - ChromecastControllerDelegate

/**
 * Called when connection to the device was established.
 *
 * @param device The device to which the connection was established.
 */
- (void)didConnectToDevice:(GCKDevice *)device {
  if (_playerView.playingLocally) {
    [_playerView pause];
    [self castCurrentMedia:_playerView.playbackTime];
  }

  [_playerView showSplashScreen];
}

/**
 * Called to display the modal device view controller from the cast icon.
 */
- (BOOL)shouldDisplayModalDeviceController {
  [self.playerView pause];
    if (!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad) {
            // If we are likely to have a fullscreen display, don't reset our edges
            // to avoid issues on iOS 7.
            _resetEdgesOnDisappear = NO;
        }        
    }
  return YES;
}

/**
 *  Trigger the icon to appear if a device is discovered. 
 */
- (void)didDiscoverDeviceOnNetwork {
//  if (![CastInstructionsViewController hasSeenInstructions]) {
//    [self hideNavigationBar:NO]; // Display the nav bar for the instructions.
//    _resetEdgesOnDisappear = NO;
//  }
}

#pragma mark - Alert View Delegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 11) {
        if (buttonIndex != alertView.cancelButtonIndex) {
            NSString *fileName = [alertView textFieldAtIndex:0].text;
            NSString *filePath = [DirectoryManager filePathWithType:DirectoryVideo fileName:fileName];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"download" object:nil userInfo:@{@"url": self.mediaToPlay.URL, @"path": filePath}];
        }
    }
}

#pragma mark - Action Sheet Delegate
- (void) actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    @try {
        if (buttonIndex == 0) {
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
            [library writeVideoAtPathToSavedPhotosAlbum:[[SNVideoPlayerManager sharedManager] currentURL]
                                        completionBlock:^(NSURL *assetURL, NSError *error) {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
            }];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"\n%s\nException:\n%@\n", __FUNCTION__, exception);
    }
    @finally {
        
    }
}




@end