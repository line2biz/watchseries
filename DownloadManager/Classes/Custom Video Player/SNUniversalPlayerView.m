//
//  SNUniversalPlayerView.m
//  DownloadManager
//
//  Created by Denis on 05.05.15.
//  Copyright (c) 2015 ComputerVerse Corporation. All rights reserved.
//

#import "SNUniversalPlayerView.h"

// AV frameworks
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
// Chrome cast
#import "Media.h"
#import "ChromecastDeviceController.h"
#import "GCKMediaInformation+LocalMedia.h"
// Remote Server
#import "SNRemoteServerManager.h"

// time to hide toolbar while playing locally
#define kToolbarObserverTime 5;

@interface SNUniversalPlayerView ()

/* AVPlayer used to play locally. */
@property (nonatomic, strong) AVPlayer *moviePlayer;
/* The CALayer on which the video plays. */
@property (nonatomic, strong) AVPlayerLayer *playerLayer;
/* Opaque observer reference for the played duration observer. */
@property (nonatomic, strong) id playerObserver;
/* Time played. */
@property (nonatomic, assign) Float64 duration;
/* Time observer to hide toolbars */
@property (nonatomic, assign) NSInteger timeObserverToolbars;
/* Download availability flag */
@property (nonatomic, assign) BOOL downloadIsAvailable;
/* Export availability flag */
@property (nonatomic, assign) BOOL exportIsAvailable;
/* Back/Forward availability flag */
@property (nonatomic, assign) BOOL fileChangeIsAvailable;
/* Chrome cast playing timer */
@property (nonatomic, strong) NSTimer *updateStreamTimer;
/* Start playing at new media flag */
@property (nonatomic, assign) BOOL isPlayOptionNeeded;
/* Changing progress activity flag */
@property (nonatomic, assign) BOOL isChangingProgressManualy;
/* URL for Chromcast */
@property (nonatomic, strong) NSURL *urlChromCast;

/* View for top toolbar controls */
@property (nonatomic, strong) UIView *topToolbarView;
/* View for bottom toolbar controls */
@property (nonatomic, strong) UIView *bottomToolbarView;
/* Play/Pause button. */
@property (nonatomic, strong) UIButton *buttonPlay;
/* Cancel button. */
@property (nonatomic, strong) UIButton *buttonCancel;
/* Download button */
@property (nonatomic, strong) UIButton *buttonDownload;
/* Share button */
@property (nonatomic, strong) UIButton *buttonShare;
/* Export button */
@property (nonatomic, strong) UIButton *buttonExport;
/* Forward button */
@property (nonatomic, strong) UIButton *buttonForward;
/* Back button */
@property (nonatomic, strong) UIButton *buttonBack;
/* Playback position slider. */
@property (nonatomic, strong) UISlider *sliderPlayback;
/* Label displaying length of video. */
@property (nonatomic, strong) UILabel *labelTotalTime;
/* Label displaying current play time. */
@property (nonatomic, strong) UILabel *labelCurrTime;
/* Play image. */
@property (nonatomic, strong) UIImage *playImage;
/* Pause image. */
@property (nonatomic, strong) UIImage *pauseImage;
/* Loading indicator */
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
/* Airplay button */
@property (nonatomic, strong) MPVolumeView *airPlayView;
/* Volume View */
@property (nonatomic, strong) MPVolumeView *volumeView;
/* Volume Slider */
@property (nonatomic, strong) UISlider *sliderVolume;

/* Label to show screen type for user */
@property (nonatomic, strong) UILabel *labelScreenType;

/* Chrome Cast Device Controller weak reference */
@property (nonatomic, weak) ChromecastDeviceController* chromecastController;

@end

@implementation SNUniversalPlayerView

#pragma mark - Life Cycle
- (instancetype)init
{
    self = [super init];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    self.backgroundColor = [UIColor blackColor];
    
    // download option
    _downloadIsAvailable = YES;
    
    // Chrome cast reference
    _chromecastController = [ChromecastDeviceController sharedInstance];
    
    [self.chromecastController addObserver:self
                                forKeyPath:@"connectionIsOn"
                                   options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                                   context:nil];
    
    // Create Controls
    [self createControls];
    
    // States
    self.state = PlayerStateUninited;
    _screenType = PlayerScreenTypeLocal;
    
    // Audio
    NSError *error = nil;
    BOOL activated = [[AVAudioSession sharedInstance] setActive:YES error:&error];
    if (!activated) {
        NSLog(@"\n%s\nError in audio session activation:\n%@\n",__PRETTY_FUNCTION__, error);
    }
    
    // Play/Pause images.
    self.playImage = [UIImage imageNamed:@"Icon-Play"];
    self.pauseImage = [UIImage imageNamed:@"media_pause"];
}

- (void)dealloc
{
    // remove volume observer
    [[AVAudioSession sharedInstance] removeObserver:self forKeyPath:@"outputVolume"];
    
    // unregister movie observers
    [self registerMovieObservers:NO];
    
    // unregister chromecast state observer
    [self.chromecastController removeObserver:self forKeyPath:@"connectionIsOn"];
}

#pragma mark - Layout Management
- (void)layoutSubviews
{
    // configure toolbars after initialization
    if (self.state != PlayerStateUninited) {
        [self configureToolbars];
    }
    
    // download action availability check
    if ([self.delegate respondsToSelector:@selector(downloadIsAvailableForUniversalPlayer)]) {
        self.downloadIsAvailable = [self.delegate downloadIsAvailableForUniversalPlayer];
    }
    
    // export action availability check
    if ([self.delegate respondsToSelector:@selector(exportIsAvailableForUniversalPlayer)]) {
        self.exportIsAvailable = [self.delegate exportIsAvailableForUniversalPlayer];
    }
    
    // file change availability check
    if ([self.delegate respondsToSelector:@selector(fileChangeIsAvailableForUniversalPlayer)]) {
        self.fileChangeIsAvailable = [self.delegate fileChangeIsAvailableForUniversalPlayer];
    }
    
    // set frame for player layer every time we need to layout
    if (self.playerLayer) {
        [self.playerLayer setFrame:self.bounds];
    }
    
    // invalidate toolbar observer after screen position change
    if (self.screenType == PlayerScreenTypeLocal) {
        [self invalidateToolbarObserver];
    }
    
    
    [super layoutSubviews];
}

#pragma mark - Private Methods
- (void)createControls
{
    // default color
    UIColor *defaultColor = [UIColor blackColor];
    
    // Activity Indicator
    self.activityIndicator = [[UIActivityIndicatorView alloc]
                              initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.activityIndicator.hidesWhenStopped = YES;
    
    // Current time.
    if (!self.labelCurrTime) {
        self.labelCurrTime = [[UILabel alloc] init];
        self.labelCurrTime.clearsContextBeforeDrawing = YES;
        self.labelCurrTime.text = @"00:00";
        [self.labelCurrTime setFont:[UIFont fontWithName:@"Helvetica" size:14.0]];
        [self.labelCurrTime setTextColor:defaultColor];
        self.labelCurrTime.tintColor = [UIColor whiteColor];
        self.labelCurrTime.translatesAutoresizingMaskIntoConstraints = NO;
        self.labelCurrTime.textAlignment = NSTextAlignmentCenter;
    }
    
    // Total time.
    if (!self.labelTotalTime) {
        self.labelTotalTime = [[UILabel alloc] init];
        self.labelTotalTime.clearsContextBeforeDrawing = YES;
        self.labelTotalTime.text = @"00:00";
        [self.labelTotalTime setFont:[UIFont fontWithName:@"Helvetica" size:14.0]];
        [self.labelTotalTime setTextColor:defaultColor];
        self.labelTotalTime.tintColor = [UIColor whiteColor];
        self.labelTotalTime.translatesAutoresizingMaskIntoConstraints = NO;
        self.labelTotalTime.textAlignment = NSTextAlignmentCenter;
    }
    
    // Progress Slider.
    if (!self.sliderPlayback) {
        self.sliderPlayback = [[UISlider alloc] init];
        [self.sliderPlayback addTarget:self
                                action:@selector(onSliderValueChanged:)
                      forControlEvents:UIControlEventValueChanged];
        [self.sliderPlayback addTarget:self
                                action:@selector(onSliderTouchStarted:)
                      forControlEvents:UIControlEventTouchDown];
        [self.sliderPlayback addTarget:self
                                action:@selector(onSliderTouchEnded:)
                      forControlEvents:UIControlEventTouchUpInside];
        [self.sliderPlayback addTarget:self
                                action:@selector(onSliderTouchEnded:)
                      forControlEvents:UIControlEventTouchCancel];
        [self.sliderPlayback addTarget:self
                                action:@selector(onSliderTouchEnded:)
                      forControlEvents:UIControlEventTouchUpOutside];
        [self.sliderPlayback setThumbImage:[UIImage imageNamed:@"Icon-SliderDot"] forState:UIControlStateNormal];
        self.sliderPlayback.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        self.sliderPlayback.minimumValue = 0;
        self.sliderPlayback.minimumTrackTintColor = [UIColor whiteColor];
        self.sliderPlayback.maximumTrackTintColor = defaultColor;
        self.sliderPlayback.translatesAutoresizingMaskIntoConstraints = NO;
    }
    
    // Volume slider.
    if (!self.sliderVolume) {
        self.sliderVolume = [[UISlider alloc] init];
        [self.sliderVolume addTarget:self
                              action:@selector(onVolumeSliderValueChanged:)
                    forControlEvents:UIControlEventValueChanged];
        [self.sliderVolume setThumbImage:[UIImage imageNamed:@"Icon-SliderDot"] forState:UIControlStateNormal];
        self.sliderVolume.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        self.sliderVolume.minimumValue = 0;
        self.sliderVolume.minimumTrackTintColor = [UIColor whiteColor];
        self.sliderVolume.maximumTrackTintColor = defaultColor;
        self.sliderVolume.translatesAutoresizingMaskIntoConstraints = NO;
    }
    
    
    // === create volume view to be able to control device volume
    if (!self.volumeView) {
        self.volumeView = [[MPVolumeView alloc] init];
        [self.volumeView setShowsVolumeSlider:YES];
        [self.volumeView setShowsRouteButton:NO];
        [self.volumeView setFrame:CGRectMake(-200, -200, 40, 40)];
        [self addSubview:self.volumeView];
        
        // KVO for audio volume change
        [[AVAudioSession sharedInstance] addObserver:self
                                          forKeyPath:@"outputVolume"
                                             options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                                             context:nil];
    }
    
    // Play/Pause Button.
    if (!self.buttonPlay) {
        self.buttonPlay = [UIButton buttonWithType:UIButtonTypeSystem];
        [self.buttonPlay setFrame:CGRectMake(0, 0, 40, 40)];
        [self.buttonPlay setImage:self.playImage forState:UIControlStateNormal];
        [self.buttonPlay addTarget:self
                            action:@selector(playButtonClicked:)
                  forControlEvents:UIControlEventTouchUpInside];
        self.buttonPlay.tintColor = defaultColor;
        self.buttonPlay.translatesAutoresizingMaskIntoConstraints = NO;
    }
    
    // Download Button.
    if (!self.buttonDownload) {
        self.buttonDownload = [UIButton buttonWithType:UIButtonTypeSystem];
        [self.buttonDownload setFrame:CGRectMake(0, 0, 40, 40)];
        [self.buttonDownload setImage:[UIImage imageNamed:@"Icon-Download-Black"] forState:UIControlStateNormal];
        [self.buttonDownload addTarget:self
                                action:@selector(downloadButtonClicked:)
                      forControlEvents:UIControlEventTouchUpInside];
        self.buttonDownload.tintColor = defaultColor;
        self.buttonDownload.translatesAutoresizingMaskIntoConstraints = NO;
    }
    
    // Share Button.
    if (!self.buttonShare) {
        self.buttonShare = [UIButton buttonWithType:UIButtonTypeSystem];
        [self.buttonShare setFrame:CGRectMake(0, 0, 40, 40)];
        [self.buttonShare setImage:[UIImage imageNamed:@"Icon-Upload-Black"] forState:UIControlStateNormal];
        [self.buttonShare addTarget:self
                             action:@selector(shareButtonClicked:)
                   forControlEvents:UIControlEventTouchUpInside];
        self.buttonShare.tintColor = defaultColor;
        self.buttonShare.translatesAutoresizingMaskIntoConstraints = NO;
    }
    
    // Export Button.
    if (!self.buttonExport) {
        self.buttonExport = [UIButton buttonWithType:UIButtonTypeSystem];
        [self.buttonExport setFrame:CGRectMake(0, 0, 40, 40)];
        [self.buttonExport setImage:[UIImage imageNamed:@"Icon-Export-Black"] forState:UIControlStateNormal];
        [self.buttonExport addTarget:self
                              action:@selector(exportButtonClicked:)
                    forControlEvents:UIControlEventTouchUpInside];
        self.buttonExport.tintColor = defaultColor;
        self.buttonExport.translatesAutoresizingMaskIntoConstraints = NO;
    }
    
    // Forward Button.
    if (!self.buttonForward) {
        self.buttonForward = [UIButton buttonWithType:UIButtonTypeSystem];
        [self.buttonForward setFrame:CGRectMake(0, 0, 40, 40)];
        [self.buttonForward setImage:[UIImage imageNamed:@"Icon-Right"] forState:UIControlStateNormal];
        [self.buttonForward addTarget:self
                             action:@selector(forwardButtonClicked:)
                   forControlEvents:UIControlEventTouchUpInside];
        self.buttonForward.tintColor = defaultColor;
        self.buttonForward.translatesAutoresizingMaskIntoConstraints = NO;
    }
    
    // Back Button.
    if (!self.buttonBack) {
        self.buttonBack = [UIButton buttonWithType:UIButtonTypeSystem];
        [self.buttonBack setFrame:CGRectMake(0, 0, 40, 40)];
        [self.buttonBack setImage:[UIImage imageNamed:@"Icon-Left"] forState:UIControlStateNormal];
        [self.buttonBack addTarget:self
                               action:@selector(backButtonClicked:)
                     forControlEvents:UIControlEventTouchUpInside];
        self.buttonBack.tintColor = defaultColor;
        self.buttonBack.translatesAutoresizingMaskIntoConstraints = NO;
    }
    
    // Airplay Button.
    if (!self.airPlayView) {
        self.airPlayView = [[MPVolumeView alloc] init];
        [self.airPlayView setShowsVolumeSlider:NO];
        [self.airPlayView setShowsRouteButton:YES];
        [self.airPlayView sizeToFit];
        self.airPlayView.translatesAutoresizingMaskIntoConstraints = NO;
        
        UIImage *imageAirPlayOff, *imageAirPlayOn;
        
        UIImage *(^ImageWithNameAndFrame)(NSString *, CGRect) = ^UIImage *(NSString *sImageName, CGRect frame) {
            
            UIImage *image;
            UIGraphicsBeginImageContext(frame.size);
            
            [[UIImage imageNamed:sImageName] drawInRect:frame];
            image = UIGraphicsGetImageFromCurrentImageContext();
            
            UIGraphicsEndImageContext();
            
            return image;
        };
        
        CGRect buttonFrame = CGRectMake(0, 0, 25.0, 25.0);
        imageAirPlayOn = ImageWithNameAndFrame(@"Icon-AirPlay-White", buttonFrame);
        imageAirPlayOff = ImageWithNameAndFrame(@"Icon-AirPlay-Black", buttonFrame);
        
        [self.airPlayView setRouteButtonImage:imageAirPlayOff forState:UIControlStateNormal];
        [self.airPlayView setRouteButtonImage:imageAirPlayOn forState:UIControlStateSelected];
    }
    
    // Screen type label for user
    if (!self.labelScreenType) {
        self.labelScreenType = [[UILabel alloc] init];
        self.labelScreenType.clearsContextBeforeDrawing = YES;
        [self.labelScreenType setFont:[UIFont fontWithName:@"Helvetica" size:20.0]];
        [self.labelScreenType setTextColor:[UIColor whiteColor]];
        self.labelScreenType.tintColor = [UIColor whiteColor];
        self.labelScreenType.translatesAutoresizingMaskIntoConstraints = NO;
        self.labelScreenType.textAlignment = NSTextAlignmentCenter;
    }
}

- (void)configureToolbars
{
    // configure Activity Indicator
    if (self.activityIndicator) {
        [self.activityIndicator removeFromSuperview];
    }
    [self addSubview:self.activityIndicator];
    [self autolayoutView:self.activityIndicator forSuperview:self withEdgeInsets:UIEdgeInsetsZero atSides:UIEdgeInsetsZero width:0 height:0 centeredX:YES centeredY:YES];
    
    // configure screen type label
    if (self.labelScreenType) {
        [self.labelScreenType removeFromSuperview];
    }
    [self insertSubview:self.labelScreenType belowSubview:self.activityIndicator];
    [self autolayoutView:self.labelScreenType forSuperview:self withEdgeInsets:UIEdgeInsetsZero atSides:UIEdgeInsetsMake(0, 1, 0, 1) width:0.0 height:30.0f centeredX:NO centeredY:YES];
    
    // configure bars
    if (self.topToolbarView) {
        [self.topToolbarView removeConstraints:self.topToolbarView.constraints];
        [self.topToolbarView removeFromSuperview];
        self.topToolbarView = nil;
    }
    if (self.bottomToolbarView) {
        [self.bottomToolbarView removeConstraints:self.bottomToolbarView.constraints];
        [self.bottomToolbarView removeFromSuperview];
        self.bottomToolbarView = nil;
    }
    
    
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    CGFloat fTopBarHeight = (!UIInterfaceOrientationIsLandscape(orientation)) ? 64.0f : (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) ? 40.0f : 64.0f;
    CGFloat fBottomBarHeight = (!UIInterfaceOrientationIsLandscape(orientation)) ? 80 : 40.0f;
    
    // create toolbar views with opaque background, constraints
    
    // background color for toolbars
    UIColor *bkgColor = [UIColor colorWithRed:125/255.0 green:125/255.0 blue:125/255.0 alpha:0.9f];
    
    // top toolbar
    
    self.topToolbarView = [[UIView alloc] init];
    self.topToolbarView.backgroundColor = bkgColor;
    // === set frame
    self.topToolbarView.frame = CGRectMake(0, 0, CGRectGetWidth(self.bounds), fTopBarHeight);
    // === add to superview
    [self addSubview:self.topToolbarView];
    // === add constraints
    [self autolayoutView:self.topToolbarView forSuperview:self withEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 0) atSides:UIEdgeInsetsMake(1, 1, 0, 1) width:0 height:fTopBarHeight centeredX:NO centeredY:NO];
    // === set tint color
    [self.topToolbarView setTintColor:[UIColor blackColor]];
    
    // configure controls
    [self configureTopControlsWithToolbar:self.topToolbarView];
    
    // bottom toolbar
    
    self.bottomToolbarView = [[UIView alloc] init];
    self.bottomToolbarView.backgroundColor = bkgColor;
    // === set frame
    self.bottomToolbarView.frame = CGRectMake(0, 0, CGRectGetWidth(self.bounds), CGRectGetHeight(self.bounds) - fBottomBarHeight);
    // === add to superview
    [self addSubview:self.bottomToolbarView];
    // === add constraints
    [self autolayoutView:self.bottomToolbarView forSuperview:self withEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 0) atSides:UIEdgeInsetsMake(0, 1, 1, 1) width:0 height:fBottomBarHeight centeredX:NO centeredY:NO];
    
    // configure controls
    [self configureBottomControlsWithToolbar:self.bottomToolbarView];
    
    // change toolbars state
    self.toolbarsState = PlayerToolbarsStateShown;
}

- (void)configureTopControlsWithToolbar:(UIView *)toolbarView
{
    // cancel button, current time label, progress slider, time left label, google cast button
    
    // Cancel button.
    if (!self.buttonCancel) {
        self.buttonCancel = [[UIButton alloc] init];
        [self.buttonCancel setFrame:CGRectMake(0, 0, 40, 40)];
        [self.buttonCancel setTitle:self.cancelButtonTitle forState:UIControlStateNormal];
        [self.buttonCancel addTarget:self
                              action:@selector(cancelButtonClicked:)
                    forControlEvents:UIControlEventTouchUpInside];
        self.buttonCancel.tintColor = [UIColor blackColor];
        [self.buttonCancel setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.buttonCancel sizeToFit];
        self.buttonCancel.translatesAutoresizingMaskIntoConstraints = NO;
    }
    
    // Chrome Cast Button.
    self.castButton.translatesAutoresizingMaskIntoConstraints = NO;
    
    // Add subviews.
    [toolbarView addSubview:self.buttonCancel];
    [toolbarView addSubview:self.labelCurrTime];
    [toolbarView addSubview:self.sliderPlayback];
    [toolbarView addSubview:self.labelTotalTime];
    [toolbarView addSubview:self.castButton];
    
    // Layout.
    NSString *hlayout = [NSString stringWithFormat:@"|-[cancelButton(==%f)]-5-[currTime(==60)]-5-[slider(>=60)]-5-[totalTime(==currTime)]-5-[chromeCastButton(==40)]-|", CGRectGetWidth(self.buttonCancel.frame)];
    
    NSString *vlayout;
    if (NSFoundationVersionNumber <= NSFoundationVersionNumber_iOS_7_1) {
        vlayout = @"V:|-[cancelButton(==40)]";
    }
    else {
        UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
        vlayout = [NSString stringWithFormat:@"V:|-%zd-[cancelButton(==40)]", (!UIInterfaceOrientationIsLandscape(orientation)) ? 20 : 0 ];
    }
    
    NSDictionary *viewsDictionary = @{ @"slider" : self.sliderPlayback,
                                       @"currTime" : self.labelCurrTime,
                                       @"totalTime" :  self.labelTotalTime,
                                       @"cancelButton" : self.buttonCancel,
                                       @"chromeCastButton" : self.castButton
                                       };
    [toolbarView addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:hlayout
                                                                         options:NSLayoutFormatAlignAllCenterY
                                                                         metrics:nil
                                                                           views:viewsDictionary]];
    
    [toolbarView addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:vlayout
                                                                         options:0
                                                                         metrics:nil
                                                                           views:viewsDictionary]];
}

- (void)configureBottomControlsWithToolbar:(UIView *)toolbarView
{
    // volume slider, back button, play/pause button, forward button, download button, share button, export button, airplay button
    
    // Add subviews.
    [toolbarView addSubview:self.sliderVolume];
    [toolbarView addSubview:self.buttonBack];
    [toolbarView addSubview:self.buttonPlay];
    [toolbarView addSubview:self.buttonForward];
    [toolbarView addSubview:self.buttonDownload];
    [toolbarView addSubview:self.buttonShare];
    [toolbarView addSubview:self.buttonExport];
    [toolbarView addSubview:self.airPlayView];
    
    // Layout.
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (UIInterfaceOrientationIsLandscape(orientation)) {
        
        // Landscape.
        NSString *hLayout = @"|-10-[slider(>=60)]-20-[buttonBack(==40)]-0-[buttonPlay(==buttonBack)]-0-[buttonForward(==buttonBack)]-5-[buttonShare(==40)]-0-[buttonDownload(==buttonShare)]-0-[buttonExport(==buttonShare)]-0-[airPlay(==40)]";
        
        NSString *vLayout = @"V:|[slider(==40)]";
        
        NSDictionary *dictViews = @{
                                    @"slider" : self.sliderVolume,
                                    @"buttonPlay" : self.buttonPlay,
                                    @"buttonDownload" :  self.buttonDownload,
                                    @"buttonShare" : self.buttonShare,
                                    @"buttonExport" : self.buttonExport,
                                    @"buttonBack" : self.buttonBack,
                                    @"buttonForward" : self.buttonForward,
                                    @"airPlay" : self.airPlayView
                                    };
        [self autolayoutView:self.buttonDownload forSuperview:toolbarView withEdgeInsets:UIEdgeInsetsZero atSides:UIEdgeInsetsZero width:0.0 height:40.0 centeredX:NO centeredY:NO];
        [self autolayoutView:self.buttonPlay forSuperview:toolbarView withEdgeInsets:UIEdgeInsetsZero atSides:UIEdgeInsetsZero width:0.0 height:40.0 centeredX:YES centeredY:NO];
        
        [toolbarView addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:hLayout
                                                                             options:NSLayoutFormatAlignAllCenterY
                                                                             metrics:nil
                                                                               views:dictViews]];
        
        [toolbarView addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:vLayout
                                                                             options:0
                                                                             metrics:nil
                                                                               views:dictViews]];
        
    }
    else {
        
        // Portrait.
        [self autolayoutView:self.buttonDownload forSuperview:toolbarView withEdgeInsets:UIEdgeInsetsZero atSides:UIEdgeInsetsZero width:0.0 height:40.0 centeredX:NO centeredY:NO];
        [self autolayoutView:self.buttonBack forSuperview:toolbarView withEdgeInsets:UIEdgeInsetsZero atSides:UIEdgeInsetsZero width:0.0 height:40.0 centeredX:NO centeredY:NO];
        [self autolayoutView:self.buttonPlay forSuperview:toolbarView withEdgeInsets:UIEdgeInsetsZero atSides:UIEdgeInsetsZero width:0.0 height:40.0 centeredX:YES centeredY:NO];
        [self autolayoutView:self.buttonForward forSuperview:toolbarView withEdgeInsets:UIEdgeInsetsZero atSides:UIEdgeInsetsZero width:0.0 height:40.0 centeredX:NO centeredY:NO];
        [self autolayoutView:self.buttonShare forSuperview:toolbarView withEdgeInsets:UIEdgeInsetsZero atSides:UIEdgeInsetsZero width:0.0 height:40.0 centeredX:NO centeredY:NO];
        [self autolayoutView:self.buttonExport forSuperview:toolbarView withEdgeInsets:UIEdgeInsetsZero atSides:UIEdgeInsetsZero width:0.0 height:40.0 centeredX:NO centeredY:NO];
        
        NSString *h1Layout = @"[buttonDownload(==40)]-2-[buttonShare(==buttonDownload)]-2-[buttonBack(==40)]-0-[buttonPlay(==buttonBack)]-0-[buttonForward(==buttonBack)]-2-[buttonExport(==buttonShare)]";
        NSString *v1Layout = @"V:|[buttonDownload(==40)]";
        NSDictionary *dictTop = @{
                                  @"buttonPlay" : self.buttonPlay,
                                  @"buttonDownload" :  self.buttonDownload,
                                  @"buttonShare" : self.buttonShare,
                                  @"buttonExport" : self.buttonExport,
                                  @"buttonBack" : self.buttonBack,
                                  @"buttonForward" : self.buttonForward
                                  };
        
        NSString *h2Layout = @"|-10-[slider(>=60)]-10-[airPlay(==40)]-10-|";
        NSString *v2Layout = @"V:|-40-[slider(==40)]";
        NSDictionary *dictBottom = @{
                                     @"slider" : self.sliderVolume,
                                     @"airPlay" : self.airPlayView
                                     };
        
        [self autolayoutView:self.airPlayView forSuperview:self withEdgeInsets:UIEdgeInsetsZero atSides:UIEdgeInsetsZero width:0.0 height:40.0 centeredX:NO centeredY:NO];
        
        [toolbarView addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:h1Layout
                                                                             options:NSLayoutFormatAlignAllTop
                                                                             metrics:nil
                                                                               views:dictTop]];
        [toolbarView addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:v1Layout
                                                                             options:0
                                                                             metrics:nil
                                                                               views:dictTop]];
        
        [toolbarView addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:h2Layout
                                                                             options:NSLayoutFormatAlignAllBottom
                                                                             metrics:nil
                                                                               views:dictBottom]];
        [toolbarView addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:v2Layout
                                                                             options:0
                                                                             metrics:nil
                                                                               views:dictBottom]];
        
    }
}

- (void)configureControls
{
    // configure controls for specific player state
    switch (self.state) {
        case PlayerStateUninited:
        {
            [self.castButton setEnabled:NO];
            if (![self.activityIndicator isAnimating]) {
                [self.activityIndicator startAnimating];
            }
        }
            break;
        case PlayerStateInited:
        {
            [self.castButton setEnabled:YES];
            [self.buttonPlay setImage:self.playImage forState:UIControlStateNormal];
            [self.sliderPlayback setValue:0.0f];
            [self.sliderPlayback setEnabled:NO];
            [self.sliderVolume setEnabled:NO];
            self.labelCurrTime.text = @"00:00";
            self.labelTotalTime.text = @"00:00";
            self.toolbarsState = PlayerToolbarsStateShown;
            if (![self.activityIndicator isAnimating]) {
                [self.activityIndicator startAnimating];
            }
        }
            break;
        case PlayerStatePaused:
        {
            [self.buttonPlay setImage:self.playImage forState:UIControlStateNormal];
            [self.sliderPlayback setEnabled:YES];
            [self.sliderVolume setEnabled:YES];
            self.toolbarsState = PlayerToolbarsStateShown;
        }
            break;
        case PlayerStatePlaying:
        {
            [self.buttonPlay setImage:self.pauseImage forState:UIControlStateNormal];
            [self.sliderPlayback setEnabled:YES];
            [self.sliderVolume setEnabled:YES];
            self.toolbarsState = PlayerToolbarsStateShown;
        }
            break;
            
        default:
            break;
    }
    
    // configure controls for specific player screen state
    switch (self.screenType) {
        case PlayerScreenTypeLocal:
        {
            [self.sliderVolume setEnabled:YES];
            [self.labelScreenType setHidden:YES];
        }
            break;
        case PlayerScreenTypeAirPlay:
        {
            [self.sliderVolume setEnabled:NO];
            
            self.labelScreenType.text = NSLocalizedString(@"Air Play is ON", nil);
            [self.labelScreenType setHidden:NO];
            
            self.toolbarsState = PlayerToolbarsStateShown;
        }
            break;
        case PlayerScreenTypeChromecast:
        {
            [self.sliderVolume setEnabled:YES];
            
            self.labelScreenType.text = [NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"Chrome Casting to", nil), _chromecastController.deviceName];
            [self.labelScreenType setHidden:NO];
            
            self.toolbarsState = PlayerToolbarsStateShown;
        }
            break;
            
        default:
            break;
    }
}

- (UIImage*)imageForSliderThumb
{
    CGFloat fRadius = 120.0f;
    CGRect frame = CGRectMake(0, 0, fRadius*2, fRadius*2);
    UIBezierPath *gradientPath = [UIBezierPath bezierPathWithOvalInRect:frame];
    
    UIImage *resultImage = nil;
    UIGraphicsBeginImageContext(frame.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGPoint myStartPoint = CGPointMake(fRadius, fRadius);
    CGPoint myEndPoint = CGPointMake(fRadius, fRadius);
    
    CGContextAddPath(context, gradientPath.CGPath);
    CGContextClosePath(context);
    CGContextClip(context);
    
    CGGradientRef myGradient;
    CGColorSpaceRef myColorspace;
    size_t num_locations = 3;
    CGFloat locations[3] = { 0.0, 0.35, 1.0 };
    CGFloat components[12] =  { 1.0, 1.0, 1.0, 1.0,   // Start color
        1.0, 1.0, 1.0, 0.75,   // Mid Color
        1.0, 1.0, 1.0, 0.0 }; // End color
    
    myColorspace = CGColorSpaceCreateDeviceRGB();
    myGradient = CGGradientCreateWithColorComponents (myColorspace, components,
                                                      locations, num_locations);
    
    CGContextDrawRadialGradient (context, myGradient, myStartPoint, 0.0, myEndPoint, fRadius, 0);
    CFRelease(myGradient);
    CFRelease(myColorspace);
    
    resultImage = UIGraphicsGetImageFromCurrentImageContext();
    
    CGContextRelease(context);
    
    UIGraphicsEndImageContext();
    
    CGFloat fNewRadius = 20.0f;
    CGRect finalRect = CGRectMake(0, 0, fNewRadius*2, fNewRadius*2);
    UIGraphicsBeginImageContext(finalRect.size);
    [resultImage drawInRect:CGRectMake(0, 0, finalRect.size.width, finalRect.size.height)];
    resultImage = [UIGraphicsGetImageFromCurrentImageContext() resizableImageWithCapInsets:UIEdgeInsetsMake(fNewRadius, fNewRadius, fNewRadius-1,fNewRadius-1)];
    UIGraphicsEndImageContext();
    
    return resultImage;
}

#pragma mark Toolbars Actions
- (void)invalidateToolbarObserver
{
    _timeObserverToolbars = kToolbarObserverTime;
}

- (void)changeToolbarStateByTime
{
    if (self.state == PlayerStatePlaying) {
        if (self.screenType == PlayerScreenTypeLocal) {
            if (_timeObserverToolbars > 0) {
                _timeObserverToolbars -= 1;
            }
            else if (_timeObserverToolbars == 0) {
                self.toolbarsState = PlayerToolbarsStateHidden;
                _timeObserverToolbars = -1;
            }
        }
    }
}

- (void)animateToolbarsStateChange
{
    CGFloat fTargetAlpha = 0.0f;
    BOOL bHide = YES;
    if (self.toolbarsState == PlayerToolbarsStateShown) {
        fTargetAlpha = 1.0f;
        bHide = NO;
        self.topToolbarView.hidden = bHide;
        self.bottomToolbarView.hidden = bHide;
    }
    [UIView animateWithDuration:1.0f
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^(void) {
                         self.topToolbarView.alpha = fTargetAlpha;
                         self.bottomToolbarView.alpha = fTargetAlpha;
                     }
                     completion:^(BOOL completed) {
                         if (completed) {
                             if (bHide) {
                                 self.topToolbarView.hidden = bHide;
                                 self.bottomToolbarView.hidden = bHide;
                             }
                         }
                     }];
}

#pragma mark Observers
- (void)registerMovieObservers:(BOOL)bRegister
{
    if (bRegister == YES) {
        // register observers
        __weak typeof(self) weakSelf = self;
        // opaque observer for movie player
        self.playerObserver = [self.moviePlayer addPeriodicTimeObserverForInterval:CMTimeMake(1, 1)
                                                                             queue:NULL
                                                                        usingBlock:^(CMTime time) {
                                                                            typeof(weakSelf) strongSelf = weakSelf;
                                                                            if (strongSelf) {
                                                                                if (!strongSelf.isChangingProgressManualy) {
                                                                                    [strongSelf updateLocalTime:time];
                                                                                    [strongSelf changeToolbarStateByTime];
                                                                                    
                                                                                    if ([strongSelf.activityIndicator isAnimating]) {
                                                                                        [strongSelf.activityIndicator stopAnimating];
                                                                                    }
                                                                                }
                                                                            }
                                                                        }];
        
        // on movie finish observer
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(movieDidFinish)
                                                     name:AVPlayerItemDidPlayToEndTimeNotification
                                                   object:self.moviePlayer.currentItem];
        // if data buffer is empty observer
        [self.moviePlayer.currentItem addObserver:self
                                       forKeyPath:@"playbackBufferEmpty"
                                          options:NSKeyValueObservingOptionNew
                                          context:nil];
        // if player can play further and data amount is enough observer
        [self.moviePlayer.currentItem addObserver:self
                                       forKeyPath:@"playbackLikelyToKeepUp"
                                          options:NSKeyValueObservingOptionNew
                                          context:nil];
        // if player status change observer
        [self.moviePlayer.currentItem addObserver:self
                                       forKeyPath:@"status"
                                          options:NSKeyValueObservingOptionNew
                                          context:nil];
        // air play status changed
        [self.moviePlayer addObserver:self
                           forKeyPath:@"externalPlaybackActive"
                              options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                              context:nil];
    }
    else {
        // unregister observers
        if (self.moviePlayer && self.playerObserver) {
            [self.moviePlayer removeTimeObserver:self.playerObserver];
            self.playerObserver = nil;
        }
        if (self.moviePlayer) {
            if ([self.moviePlayer observationInfo]) {
                [self.moviePlayer removeObserver:self forKeyPath:@"externalPlaybackActive"];
            }
            if ([self.moviePlayer.currentItem observationInfo]) {
                if (self.moviePlayer.currentItem) {
                    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                                    name:AVPlayerItemDidPlayToEndTimeNotification
                                                                  object:self.moviePlayer.currentItem];
                    [self.moviePlayer.currentItem removeObserver:self forKeyPath:@"playbackBufferEmpty"];
                    [self.moviePlayer.currentItem removeObserver:self forKeyPath:@"playbackLikelyToKeepUp"];
                    [self.moviePlayer.currentItem removeObserver:self forKeyPath:@"status"];
                }
            }
        }
    }
}

#pragma mark Screens Management
- (void)manageScreens
{
    // screen type was changed, reset players
    
    if (self.screenType == PlayerScreenTypeLocal || self.screenType == PlayerScreenTypeAirPlay) {
        // player is on device or AirPlay device
        
        if ([self.chromecastController isConnected]) {
            // switch off chrome casting if it is connected
            [_chromecastController disconnectFromDevice];
        }
        if (self.updateStreamTimer) {
            // remove chrome cast stream timer
            [self.updateStreamTimer invalidate];
            self.updateStreamTimer = nil;
        }
        
        // if player is nil previous state was chrome cast
        if (!self.moviePlayer) {
            [self playerInit];
            [self registerMovieObservers:YES];
            if (self.moviePlayer.currentItem && !CMTIME_IS_INDEFINITE(self.moviePlayer.currentItem.duration)) {
                [self prepareForMovieStart];
            }
        }
        else {
            // start play
            [self localPlay:YES];
        }
    }
    else if (self.screenType == PlayerScreenTypeChromecast) {
                
        // remove local player
        [self clearMovie];
        
        // create cast info
        Media *chromeCastMedia = [[Media alloc] init];
        NSURL *urlChromCast = self.urlChromCast;
        chromeCastMedia.title = self.mediaObject.title;
        chromeCastMedia.subtitle = @"";
        chromeCastMedia.mimeType = @"video/mp4";
        chromeCastMedia.descrip = @"";
        chromeCastMedia.thumbnailURL = nil;
        chromeCastMedia.posterURL = nil;
        chromeCastMedia.URL = urlChromCast;
        chromeCastMedia.tracks = nil;
        
        GCKMediaInformation *mediaInfo = [GCKMediaInformation mediaInformationFromLocalMedia:chromeCastMedia];
        
        // init or resume chrome cast
        if ([self.chromecastController isPlayingMedia] && [mediaInfo isEqual:[self.chromecastController mediaInformation]]) {
            // chrome cast is already set with this media and playing it
            [self updateInterfaceFromCast:nil];
            
        }
        else {
            // init chrome cast with new media
            [_chromecastController loadMedia:mediaInfo
                                   startTime:_playbackTime
                                    autoPlay:YES];
            
        }
        // Start the timer
        if (self.updateStreamTimer) {
            [self.updateStreamTimer invalidate];
            self.updateStreamTimer = nil;
        }
        
        // set chrome cast stream timer for interface updates
        self.updateStreamTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                                  target:self
                                                                selector:@selector(updateInterfaceFromCast:)
                                                                userInfo:nil
                                                                 repeats:YES];
    }
}

#pragma mark Local Player Management
- (void)playerInit
{
    if (!self.moviePlayer) {
        self.moviePlayer = [AVPlayer playerWithURL:self.mediaObject.URL];
        _playerLayer = [AVPlayerLayer playerLayerWithPlayer:self.moviePlayer];
        [_playerLayer setFrame:self.bounds];
        [_playerLayer setBackgroundColor:[[UIColor blackColor] CGColor]];
        [self.layer insertSublayer:_playerLayer atIndex:0];
    }
}

- (void)prepareForMovieStart
{
    if (CMTIME_IS_INDEFINITE(self.moviePlayer.currentItem.duration)) {
        // Loading has failed, try it again.
        __weak typeof(self) weakSelf = self;
        [self clearMovieWithCompletion:^{
            typeof(weakSelf) strongSelf = weakSelf;
            if (strongSelf) {
                [strongSelf playerInit];
                [strongSelf registerMovieObservers:YES];
            }
        }];
        return;
    }
    
    if (!self.duration) {
        self.sliderPlayback.minimumValue = 0;
        CGFloat fSeconds = CMTimeGetSeconds(self.moviePlayer.currentItem.duration);
        self.duration = fSeconds;
        self.sliderPlayback.enabled = YES;
        [self.activityIndicator stopAnimating];
        
        int totalTime = roundf(CMTimeGetSeconds(self.moviePlayer.currentItem.duration));
        int timeLeft = totalTime - (int)self.playbackTime;
        self.labelTotalTime.text = [NSString stringWithFormat:@"-%@",[self formattedTimeFromSeconds:timeLeft]];
    }
    // Jump to last playback time if we have one.
    [self localSyncToLastPlayback];
}

- (void)movieDidFinish
{
    [self movieDidFinishManualy:NO];
}

- (void)movieDidFinishManualy:(BOOL)manualy
{
    self.state = PlayerStateInited;
    self.duration = 0;
    self.playbackTime = 0;
    [self clearMovie];
    [self.moviePlayer seekToTime:CMTimeMake(0, 1)];
    [self configureControls];
    
    if (!manualy) {
        if (_fileChangeIsAvailable == YES) {
            self.isPlayOptionNeeded = YES;
            [self forwardButtonClicked:nil];            
        }
    }
}

- (void)clearMovie
{
    [self clearMovieWithCompletion:nil];
}

- (void)clearMovieWithCompletion:(void(^)())completion
{
    if (self.moviePlayer) {
        [self registerMovieObservers:NO];
        
        if (self.moviePlayer.externalPlaybackActive) {
            [self.moviePlayer pause];
            self.moviePlayer.allowsExternalPlayback = NO;
        }
        
        [self.playerLayer removeFromSuperlayer];
        self.playerLayer = nil;
        self.moviePlayer = nil;
    }
    
    if (completion) {
        completion();
    }
}

- (void)updateLocalTime:(CMTime)time
{
    if (self.moviePlayer.currentItem.status != AVPlayerItemStatusReadyToPlay) {
        return;
    }
    if (self.labelCurrTime) {
        
        
        NSInteger iTime = roundf(CMTimeGetSeconds(time));
        if (self.playbackTime > iTime && self.playbackTime - iTime >= 10) {
            [self.moviePlayer seekToTime:CMTimeMake(self.playbackTime, 1)];
            sleep(1);
        }
        else {
            
            self.playbackTime = iTime;
            self.sliderPlayback.value = CMTimeGetSeconds(time)/ self.duration;
        }
        
        
        self.labelCurrTime.text = [self formattedTimeFromSeconds:(int)iTime];
        
        int totalTime = roundf(CMTimeGetSeconds(self.moviePlayer.currentItem.duration));
        int timeLeft = totalTime - (int)self.playbackTime;
        self.labelTotalTime.text = [NSString stringWithFormat:@"-%@",[self formattedTimeFromSeconds:timeLeft]];
    }
}

/**
 *  Update the play state to the last stored playback time.
 */
- (void)localSyncToLastPlayback {
    if (self.duration) {
        if (_playbackTime > 0 && _playbackTime < (self.duration - 20)) {
            [self.moviePlayer seekToTime:CMTimeMake(_playbackTime, 1)];
            if (self.moviePlayer.status != AVPlayerStatusReadyToPlay) {
                [self.activityIndicator startAnimating];
            }
        }
        [self.moviePlayer play];
        self.state = PlayerStatePlaying;
    }
}


- (void)localPlay:(BOOL)play
{
    if (self.isPlayOptionNeeded) {
        self.isPlayOptionNeeded = NO;
    }
    
    if (self.state == PlayerStateInited) {
        
        if (self.screenType != PlayerScreenTypeChromecast) {
            
            if (!self.moviePlayer) {
                [self playerInit];
                [self registerMovieObservers:YES];
                [self prepareForMovieStart];
                return;
            }
        }
    }
    
    // start/stop playing
    if (self.screenType == PlayerScreenTypeLocal || self.screenType == PlayerScreenTypeAirPlay) {
        if (play) {
            [self.moviePlayer play];
        }
        else {
            [self.moviePlayer pause];
        }
    }
    else if (self.screenType == PlayerScreenTypeChromecast) {
        if (play) {
            if ([_chromecastController isPaused]) {
                [_chromecastController pauseCastMedia:NO];
            }
        } else {
            if (![_chromecastController isPaused]) {
                [_chromecastController pauseCastMedia:YES];
            }
        }
    }
    
    // change player state
    if (play) {
        self.state = PlayerStatePlaying;
    }
    else {
        self.state = PlayerStatePaused;
    }
    [self configureControls];
}

#pragma mark Air Play isOn check
- (BOOL)isAudioSessionUsingAirplayOutputRoute
{
    AVAudioSession* audioSession = [AVAudioSession sharedInstance];
    AVAudioSessionRouteDescription* currentRoute = audioSession.currentRoute;
    if (currentRoute) {
        if ([currentRoute.outputs count]) {
            for (AVAudioSessionPortDescription* outputPort in currentRoute.outputs){
                if ([outputPort.portType isEqualToString:AVAudioSessionPortAirPlay])
                    return true;
            }
        }
    }
    
    return false;
}

#pragma mark Chromecast Player Management
- (void)updateInterfaceFromCast:(NSTimer*)timer {
    [_chromecastController updateStatsFromDevice];
    
    if (_chromecastController.playerState != GCKMediaPlayerStateBuffering) {
        [self.activityIndicator stopAnimating];
    } else {
        [self.activityIndicator startAnimating];
    }
    
    if ([self isAudioSessionUsingAirplayOutputRoute]) {
        self.screenType = PlayerScreenTypeAirPlay;
        return;
    }
    
    if (_chromecastController.streamDuration > 0) {
        _playbackTime = roundf(_chromecastController.streamPosition);
        self.labelCurrTime.text = [self formattedTimeFromSeconds:(int)_playbackTime];
        
        if (!self.duration) {
            self.duration = [_chromecastController streamDuration];
        }
        
        int totalTime = roundf(_chromecastController.streamDuration);
        int timeLeft = totalTime - (int)_playbackTime;
        if (self.playbackTime < 2) {
            self.playbackTime = _chromecastController.streamDuration;
        }
        self.labelTotalTime.text = [NSString stringWithFormat:@"-%@",[self formattedTimeFromSeconds:timeLeft]];
        
        [self.sliderPlayback setValue:_chromecastController.streamPosition/self.duration
                             animated:YES];
    }
    [self updateState];
}

- (void)updateState
{
    if (_chromecastController.playerState == GCKMediaPlayerStatePaused ||
        _chromecastController.playerState == GCKMediaPlayerStateIdle) {
        
        self.state = PlayerStatePaused;
    }
    else if (_chromecastController.playerState == GCKMediaPlayerStatePlaying ||
               _chromecastController.playerState == GCKMediaPlayerStateBuffering) {
        
        self.state = PlayerStatePlaying;
    }
}

#pragma mark Formatters
- (NSString *)formattedTimeFromSeconds:(int)seconds
{
    int hours = seconds / (60 * 60);
    seconds %= (60 * 60);
    
    int minutes = seconds / 60;
    seconds %= 60;
    
    if (hours > 0) {
        return [NSString stringWithFormat:@"%d:%02d:%02d", hours, minutes, seconds];
    } else {
        return [NSString stringWithFormat:@"%d:%02d", minutes, seconds];
    }
}

#pragma mark Autolayout Management
- (void)autolayoutView:(UIView *)view forSuperview:(UIView *)superView withEdgeInsets:(UIEdgeInsets)edgeInsets atSides:(UIEdgeInsets)sides width:(CGFloat)width height:(CGFloat)height centeredX:(BOOL)centeredX centeredY:(BOOL)centeredY
{
    NSLayoutConstraint* (^AppendConstraint)(UIView *, NSLayoutAttribute, UIView *, CGFloat) = ^(UIView *target, NSLayoutAttribute attribute, UIView *superTarget, CGFloat fConstant) {
        return [NSLayoutConstraint constraintWithItem:target
                                            attribute:attribute
                                            relatedBy:NSLayoutRelationEqual
                                               toItem:superTarget
                                            attribute:attribute
                                           multiplier:1
                                             constant:fConstant];
    };
    
    NSMutableArray *maConstraints = [NSMutableArray new];
    
    if (width) {
        [maConstraints addObject:AppendConstraint(view, NSLayoutAttributeWidth, nil, width)];
    }
    
    if (height) {
        [maConstraints addObject:AppendConstraint(view, NSLayoutAttributeHeight, nil, height)];
    }
    
    if (centeredX) {
        [maConstraints addObject:AppendConstraint(view, NSLayoutAttributeCenterX, superView, 0.0f)];
    }
    
    if (centeredY) {
        [maConstraints addObject:AppendConstraint(view, NSLayoutAttributeCenterY, superView, 0.0f)];
    }
    
    if (!UIEdgeInsetsEqualToEdgeInsets(sides, UIEdgeInsetsZero)) {
        if (!centeredX) {
            if (sides.left) {
                [maConstraints addObject:AppendConstraint(view, NSLayoutAttributeLeading, superView, edgeInsets.left)];
            }
            if (sides.right) {
                [maConstraints addObject:AppendConstraint(view, NSLayoutAttributeTrailing, superView, edgeInsets.right)];
            }
        }
        
        if (!centeredY) {
            if (sides.top) {
                [maConstraints addObject:AppendConstraint(view, NSLayoutAttributeTop, superView, edgeInsets.top)];
            }
            if (sides.bottom) {
                [maConstraints addObject:AppendConstraint(view, NSLayoutAttributeBottom, superView, edgeInsets.bottom)];
            }
        }
    }
    
    view.translatesAutoresizingMaskIntoConstraints = NO;
    
    [superView addConstraints:[maConstraints copy]];
}

#pragma mark Outlet Methods
- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    // show toolbars
    _timeObserverToolbars = kToolbarObserverTime;
    self.toolbarsState = PlayerToolbarsStateShown;
    
    return [super hitTest:point withEvent:event];
}

- (void)cancelButtonClicked:(UIButton *)sender
{
    [self clearMovieWithCompletion:^{
        [self movieDidFinishManualy:YES];
        if ([self.delegate respondsToSelector:@selector(universalPlayer:didTapCancelButton:)]) {
            [self.delegate universalPlayer:self didTapCancelButton:sender];
        }
    }];
}

- (void)onSliderValueChanged:(UISlider *)sender
{
    _playbackTime = roundf(sender.value);
    if (self.screenType == PlayerScreenTypeChromecast) {
        self.labelCurrTime.text = [self formattedTimeFromSeconds:(int)_playbackTime];
    }
    else {
        CMTime newTime = CMTimeMakeWithSeconds(self.sliderPlayback.value * self.duration, 1);
        self.labelCurrTime.text = [self formattedTimeFromSeconds:(int)_playbackTime];
        [self.moviePlayer seekToTime:newTime];
        [self invalidateToolbarObserver];
    }
}

- (void)onSliderTouchStarted:(UISlider *)sender
{
    if (self.screenType == PlayerScreenTypeChromecast) {
        [self.activityIndicator startAnimating];
    }
    else {
        self.isChangingProgressManualy = YES;
        [self localPlay:NO];
        [self.moviePlayer setRate:0.f];
        [self.activityIndicator startAnimating];
        [self invalidateToolbarObserver];
    }
}

- (void)onSliderTouchEnded:(UISlider *)sender
{
    _playbackTime = roundf(sender.value);
    if (self.screenType == PlayerScreenTypeChromecast) {
        self.labelCurrTime.text = [self formattedTimeFromSeconds:(int)_playbackTime];
        [self.activityIndicator stopAnimating];
        [_chromecastController setPlaybackTime:(NSTimeInterval)_playbackTime];
    }
    else {
        self.isChangingProgressManualy = NO;
        [self.activityIndicator stopAnimating];
        CMTime newTime = CMTimeMakeWithSeconds(self.sliderPlayback.value * self.duration, 1);
        [self.moviePlayer seekToTime:newTime];
        [self.moviePlayer setRate:1.0f];
//        if (self.state == PlayerStatePaused) {
//            [self localPlay:NO];
//        }
//        else if (self.state == PlayerStatePlaying) {
//        }
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self localPlay:YES];
        });
        
        [self invalidateToolbarObserver];
        [self animateToolbarsStateChange];
    }
}

- (void)onVolumeSliderValueChanged:(UISlider *)sender
{
    if (self.screenType == PlayerScreenTypeChromecast) {
        _chromecastController.deviceVolume = sender.value;
    }
    else if (self.screenType == PlayerScreenTypeAirPlay) {
        // can`t be handled
    }
    else if (self.screenType == PlayerScreenTypeLocal) {
        
        if (self.volumeView) {
            for (UIView *subview in self.volumeView.subviews) {
                if ([subview isKindOfClass:[UISlider class]]) {
                    UISlider *slider = (UISlider *)subview;
                    [slider setValue:sender.value animated:YES];
                    
                    break;
                }
            }
        }
        
        [self invalidateToolbarObserver];
    }
}

- (void)playButtonClicked:(UIButton *)sender
{
    switch (self.state) {
        case PlayerStateInited:
        {
            [self localPlay:YES];
        }
            break;
        case PlayerStatePaused:
        {
            [self localPlay:YES];
        }
            break;
        case PlayerStatePlaying:
        {
            [self localPlay:NO];
        }
            break;
            
        default:
            break;
    }
    [self invalidateToolbarObserver];
}

- (void)downloadButtonClicked:(UIButton *)sender
{
    [self invalidateToolbarObserver];
    
    if ([self.delegate respondsToSelector:@selector(universalPlayer:didTapDownloadButton:)]) {
        [self.delegate universalPlayer:self didTapDownloadButton:sender];
    }
}

- (void)shareButtonClicked:(UIButton *)sender
{
    [self invalidateToolbarObserver];
    
    if ([self.delegate respondsToSelector:@selector(universalPlayer:didTapShareButton:)]) {
        [self.delegate universalPlayer:self didTapShareButton:sender];
    }
}

- (void)exportButtonClicked:(UIButton *)sender
{
    [self invalidateToolbarObserver];
    
    if ([self.delegate respondsToSelector:@selector(universalPlayer:didTapExportButton:)]) {
        [self.delegate universalPlayer:self didTapExportButton:sender];
    }
}

- (void)backButtonClicked:(UIButton *)sender
{
    [self invalidateToolbarObserver];
    
    if ([self.delegate respondsToSelector:@selector(universalPlayer:didTapBackButton:)]) {
        [self.delegate universalPlayer:self didTapBackButton:sender];
    }
}

- (void)forwardButtonClicked:(UIButton *)sender
{
    [self invalidateToolbarObserver];
    
    if ([self.delegate respondsToSelector:@selector(universalPlayer:didTapForwardButton:)]) {
        [self.delegate universalPlayer:self didTapForwardButton:sender];
    }
}

#pragma mark - Property Accessors
- (void)setMediaObject:(Media *)mediaObject
{
    if (_mediaObject == mediaObject) {
        return;
    }
    if (self.moviePlayer) {
        [self movieDidFinishManualy:YES];
        self.isPlayOptionNeeded = YES;
    }
    _mediaObject = mediaObject;
    self.state = PlayerStateInited;
    _playbackTime = 0;
    if (self.screenType == PlayerScreenTypeChromecast) {
        _screenType = PlayerScreenTypeLocal;
    }
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:self.mediaObject.URL.path]) {
        self.urlChromCast = [[SNRemoteServerManager sharedInstance] remoteURLForFileAtURL:self.mediaObject.URL];
    }
    else {
        self.urlChromCast = self.mediaObject.URL;
    }
    
    [self configureToolbars];
    
    if ([self isAudioSessionUsingAirplayOutputRoute]) {
        self.screenType = PlayerScreenTypeAirPlay;
        
        if ([_chromecastController isConnected]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil
                                                                message:NSLocalizedString(@"Air Play is ON, Chrome cast will be disconnected.", nil)
                                                               delegate:nil
                                                      cancelButtonTitle:NSLocalizedString(@"Dismiss", nil)
                                                      otherButtonTitles:nil, nil];
            [alertView show];
        }
    }
    else if ([_chromecastController isConnected]) {
        self.screenType = PlayerScreenTypeChromecast;
    }
    else {
        self.screenType = PlayerScreenTypeLocal;
    }
    if (self.isPlayOptionNeeded) {
        [self localPlay:YES];
    }    
}

- (void)setState:(PlayerState)state
{
    if (_state == state) {
        return;
    }
    _state = state;
    if (state == PlayerStateInited) {
        self.toolbarsState = PlayerToolbarsStateShown;
    }
    [self configureControls];
}

- (void)setScreenType:(PlayerScreenType)screenType
{
    if (_screenType == screenType) {
        return;
    }
    _screenType = screenType;
    
    [self manageScreens];
    [self configureControls];
}

- (void)setToolbarsState:(PlayerToolbarsState)toolbarsState
{
    if (_toolbarsState == toolbarsState) {
        return;
    }
    _toolbarsState = toolbarsState;
    [self animateToolbarsStateChange];
}

- (void)setDownloadIsAvailable:(BOOL)downloadIsAvailable
{
    if (_downloadIsAvailable == downloadIsAvailable) {
        return;
    }
    _downloadIsAvailable = downloadIsAvailable;
    self.buttonDownload.enabled = downloadIsAvailable;
}

- (void)setExportIsAvailable:(BOOL)exportIsAvailable
{
    _exportIsAvailable = exportIsAvailable;
    self.buttonExport.enabled = exportIsAvailable;
    self.buttonShare.enabled = !exportIsAvailable;
}

- (void)setFileChangeIsAvailable:(BOOL)fileChangeIsAvailable
{
    _fileChangeIsAvailable = fileChangeIsAvailable;
    self.buttonBack.enabled = self.buttonForward.enabled = fileChangeIsAvailable;
}

#pragma mark - Public Methods
- (BOOL)isChromeCasted
{
    return self.screenType == PlayerScreenTypeChromecast;
}

- (BOOL)isAirPlayON
{
    return self.screenType == PlayerScreenTypeAirPlay;
}

- (void)play:(BOOL)play
{
    [self localPlay:play];
}

- (void)setCastVolume:(CGFloat)castVolume
{
    if (self.screenType == PlayerScreenTypeChromecast) {
        if (castVolume != self.sliderVolume.value) {
            [self.sliderVolume setValue:castVolume];
            _chromecastController.deviceVolume = castVolume;
        }
    }
}

#pragma mark - Key Value Observing
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object
                        change:(NSDictionary *)change context:(void *)context {
    if ((!self.moviePlayer.currentItem || object != self.moviePlayer.currentItem) && (object != [AVAudioSession sharedInstance]) &&
        (object != self.moviePlayer) && (object != self.chromecastController)) {
        return;
    }
    
    if ([keyPath isEqualToString:@"playbackLikelyToKeepUp"]) {
        [self.activityIndicator stopAnimating];
    }
    else if ([keyPath isEqualToString:@"playbackBufferEmpty"]) {
        [self.activityIndicator startAnimating];
    }
    else if ([keyPath isEqualToString:@"status"]) {
        if (self.moviePlayer.status == AVPlayerStatusReadyToPlay) {
            [self prepareForMovieStart];
        }
    }
    else if ([keyPath isEqualToString:@"outputVolume"]) {
        // device volume changed
        self.sliderVolume.value = [(NSNumber *)change[@"new"] floatValue];
        
        if (self.screenType == PlayerScreenTypeChromecast) {
            _chromecastController.deviceVolume = self.sliderVolume.value;
        }
    }
    else if ([keyPath isEqualToString:@"externalPlaybackActive"]) {
        // air play status has changed
        if (self.state == PlayerStateUninited) {
            return;
        }
        BOOL bAirPlay = [(NSNumber *)change[@"new"] boolValue];
        if (bAirPlay) {
            self.screenType = PlayerScreenTypeAirPlay;
        }
        else {
            self.screenType = PlayerScreenTypeLocal;
        }
    }
    else if ([keyPath isEqualToString:@"connectionIsOn"]) {
        // chrome cast connection status changed
        if (self.state == PlayerStateUninited) {
            return;
        }
        if (self.screenType == PlayerScreenTypeAirPlay) {
            // no need to change screen to local if air play
            return;
        }
        BOOL bIsOn = [(NSNumber *)change[@"new"] boolValue];
        PlayerScreenType newScreenType = (bIsOn) ? PlayerScreenTypeChromecast : PlayerScreenTypeLocal;
        if (self.screenType != newScreenType) {
            if (bIsOn) {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    self.screenType = newScreenType;
                });
            }
            else {
                self.screenType = newScreenType;
            }
        }
    }
}


@end


@interface UISlider (thumb)

@end

@implementation UISlider (thumb)

- (CGRect)trackRectForBounds:(CGRect)bounds
{
    CGFloat fx = bounds.origin.x + 5;
    CGFloat fHeight = 2;
    CGFloat fy = CGRectGetMidY(bounds) - fHeight/2;
    CGFloat fWidth = bounds.size.width - 10;
    
    return CGRectMake(fx, fy, fWidth, fHeight);
}

- (CGRect)thumbRectForBounds:(CGRect)bounds trackRect:(CGRect)rect value:(float)value
{
    CGFloat fThumbRadius = 40.0f;
    CGFloat fX = floorf(rect.origin.x - fThumbRadius/2 + (rect.size.width * value));
    CGFloat fY = floorf(rect.origin.y + rect.size.height/2 - fThumbRadius/2);
    
    return CGRectMake(fX, fY, fThumbRadius, fThumbRadius);
}

@end
