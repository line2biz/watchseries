//
//  SNVideoPlayerManager.h
//  DownloadManager
//
//  Created by Denis on 27.03.15.
//  Copyright (c) 2015 ComputerVerse Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

/** Delegate Protocol for clas that represents video playback from webView. */
@protocol PlayerManagerDelegate <NSObject>
@optional

/** method which will hide default player controller */
- (IBAction)back;

/** delegate notification method to open video in local player */
- (void)shouldOpenVideoInLocalPlayer;

/** List of sites which are forbidden to download video from */
- (NSArray *)forbiddenSitesList;

/** Inform delegate that video player is going to be shown */
- (void)customPlayerControllerWillBeShown;

@end

/** Class which provides support for local and remote video playback */
@interface SNVideoPlayerManager : NSObject

@property (strong, nonatomic) id <PlayerManagerDelegate> delegate;

@property (strong, nonatomic) NSURL *currentURL;

@property (assign, nonatomic) BOOL playerIsShown;

/** singleton class shared instance */
+ (instancetype)sharedManager;

/** method which start video playback in custom local player
 * @param videoURL  NSURL adress of video file, if nil delegate will execute back method, else will show player controller
 */
- (void)startLocalPlaybackForVideoWithURL:(NSURL *)videoURL;

/** additional method which start video playback in custom local player and check if it should be downloadable
 * @param videoURL  NSURL adress of video file, if nil delegate will execute back method, else will show player controller
 * @param sourceURL NSURL adress of site where the video was started
 */
- (void)startLocalPlaybackForVideoWithURL:(NSURL *)videoURL fromURL:(NSURL *)sourceURL;

/** shows player controller if it there is chromecast active media */
- (void)showPlayerController;

@end
