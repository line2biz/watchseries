//
//  PlayerNavController.m
//  DownloadManager
//
//  Created by Denis on 01.04.15.
//  Copyright (c) 2015 ComputerVerse Corporation. All rights reserved.
//

#import "PlayerNavController.h"

@interface PlayerNavController ()

@end

@implementation PlayerNavController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Rotations
- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return  UIInterfaceOrientationMaskAllButUpsideDown;
}

//- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
//{
//    return  UIInterfaceOrientationPortrait;
//}

@end
