//
//  DirectoryViewController.m
//  DownloadManager
//
//  Created by Denis on 03.02.15.
//  Copyright (c) 2015 ComputerVerse Corporation. All rights reserved.
//

#import "DirectoryViewController.h"
#import "MediaManager.h"
#import "ChromecastDeviceController.h"

#define CellImageTag 10
#define CellTitleTag 20
#define AlertViewCreateFolderTag 10
#define AlertViewRenameTag 20

@interface DirectoryViewController () <DirectoryControllerChangeProtocol, UIAlertViewDelegate, PlayerManagerDelegate, ChromecastControllerDelegate>
{
    NSInteger _loadIdx;
}
@property (strong, nonatomic) NSArray *aContentsOfDirectory;
@property (strong, nonatomic) NSString *selectedPath;

@end

@implementation DirectoryViewController

#pragma mark - Life Cycle

+ (instancetype)DirectoryViewControllerWithPath:(NSURL *)urlPath
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    DirectoryViewController *directoryViewController = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([DirectoryViewController class])];
    if (urlPath) {
        directoryViewController.urlPath = urlPath;
    } else {
        directoryViewController.urlPath = [DirectoryViewController defaultDocumentsPath];
    }
    
    return directoryViewController;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _loadIdx = 0;
    
    self.view.backgroundColor = [UIColor colorWithRed:0.176f green:0.176f blue:0.176f alpha:1.000f];
    
    if ([self.urlPath isEqual:[DirectoryViewController defaultDocumentsPath]]) {
        self.navigationItem.title = @"Media-Files";
    } else {
        self.navigationItem.title = [self.urlPath lastPathComponent];
    }
    
    // add toolbar buttons
    NSMutableArray *maToolbarItems = [NSMutableArray new];
    
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    if ([self.urlPath isEqual:[DirectoryViewController defaultDocumentsPath]]) {
        UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithTitle:@"New Folder" style:UIBarButtonItemStylePlain target:self action:@selector(createNewFolder)];
        [maToolbarItems addObject:addButton];
        [maToolbarItems addObject:flexSpace];
    }
    
    UIBarButtonItem *deleteButton = [[UIBarButtonItem alloc] initWithTitle:@"Delete" style:UIBarButtonItemStylePlain target:self action:@selector(delete)];
    [maToolbarItems addObject:deleteButton];
    [maToolbarItems addObject:flexSpace];
    
    UIBarButtonItem *renameButton = [[UIBarButtonItem alloc] initWithTitle:@"Rename" style:UIBarButtonItemStylePlain target:self action:@selector(rename)];
    [maToolbarItems addObject:renameButton];
    
    if (![self.urlPath isEqual:[DirectoryViewController defaultDocumentsPath]]) {
        UIBarButtonItem *changeDirButton = [[UIBarButtonItem alloc] initWithTitle:@"Change Directory" style:UIBarButtonItemStylePlain target:self action:@selector(changeFolder)];
        [maToolbarItems addObject:flexSpace];
        [maToolbarItems addObject:changeDirButton];
    }
    
    
    [self setToolbarItems:(NSArray*)maToolbarItems];
    
    // load data source
    [self loadContentsOfDirectory];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationItem.rightBarButtonItems = nil;
    self.navigationItem.rightBarButtonItem = nil;
    
    self.navigationController.toolbarHidden = ![self.tableView isEditing];
    
    if (!_loadIdx) {
        _loadIdx++;
    } else {
        [self loadContentsOfDirectory];
    }
    
    // add editing button
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    // crome cast delegate
    [ChromecastDeviceController sharedInstance].delegate = self;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // crome cast button
    [[ChromecastDeviceController sharedInstance] manageViewController:self icon:YES toolbar:NO];
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    [super setEditing:editing animated:animated];
    [self.tableView setEditing:editing animated:animated];
    if (editing) {
        
        self.navigationController.toolbarHidden = NO;
        
    } else {
        
        self.navigationController.toolbarHidden = YES;
        [self.tableView reloadData];
        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Status Bar
- (BOOL)prefersStatusBarHidden
{
    if (!UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation]) && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        return YES;
    } else {
        return NO;
    }
}

- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation
{
    return UIStatusBarAnimationSlide;
}

#pragma mark - Private Methods
- (void)loadContentsOfDirectory
{
    NSError *error;
    self.aContentsOfDirectory = [[NSFileManager defaultManager] contentsOfDirectoryAtURL:self.urlPath
                                                              includingPropertiesForKeys:[NSArray array]
                                                                                 options:0
                                                                                   error:&error];
    if (error) {
        NSLog(@"\n%s\nError while getting contents of URL:\n%@\n", __FUNCTION__, error);
    } else {
        NSMutableArray *maClearedContent = [NSMutableArray new];
        for (NSURL *url in self.aContentsOfDirectory) {
            BOOL isSqlite = ([[url lastPathComponent] rangeOfString:@"sqlite"].location != NSNotFound);
            BOOL isDiskCache = ([[url lastPathComponent] rangeOfString:@"diskCache"].location != NSNotFound);
            BOOL isSecureDir = [[url lastPathComponent] isEqualToString:@"TempWatchSeriesDirectory"];
            if (!isSqlite && !isDiskCache && !isSecureDir) {
                [maClearedContent addObject:url];
            }
        }
        self.aContentsOfDirectory = [NSArray arrayWithArray:(NSArray *)maClearedContent];
        [self.tableView reloadData];
    }
    
}

#pragma mark Toolbar Methods
- (void)delete
{
    if ([self.tableView indexPathsForSelectedRows].count) {
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSArray *aIndexPaths = [self.tableView indexPathsForSelectedRows];
        
        for (NSIndexPath *indexPath in aIndexPaths) {
            NSURL *fileURL = [self.aContentsOfDirectory objectAtIndex:indexPath.row];
            
            if ([((NSString*)([[fileURL pathComponents] lastObject])) isEqualToString:@"Downloads"]) {
                // default downloads directory
                [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
            } else {
                NSError *error;
                BOOL bRemoved = [fileManager removeItemAtURL:fileURL error:&error];
                if (!bRemoved) {
                    NSLog(@"\n%s\nError while removing object at URL:\n%@\nError:\n%@\n", __FUNCTION__, fileURL, error);
                    [self showAlertWithError:error];
                }
            }
        }
        [self loadContentsOfDirectory];
    }
}

- (void)rename
{
    NSArray *aIndexPaths = [self.tableView indexPathsForSelectedRows];
    if ([aIndexPaths count]) {
        
        if ([aIndexPaths count] > 1) {
            NSString *sMsg = NSLocalizedString(@"Can`t rename several items equaly", nil);
            
            [self showMessage:sMsg];
            
        } else {
            NSString *sMsg = NSLocalizedString(@"Input new Title", nil);
            
            [self showQuerryAlertViewWithTitle:nil message:sMsg tag:AlertViewRenameTag];
        }
    }
}

- (void)createNewFolder
{
    NSString *sMsg = NSLocalizedString(@"Input Title of new Folder", nil);
    
    [self showQuerryAlertViewWithTitle:nil message:sMsg tag:AlertViewCreateFolderTag];
}

- (void)changeFolder
{
    if ([self.tableView indexPathsForSelectedRows].count) {
        NSLog(@"\n%s\n", __FUNCTION__);
        
        DirectoryViewController *dirViewController = [DirectoryViewController DirectoryViewControllerWithPath:[DirectoryViewController defaultDocumentsPath]];
        dirViewController.changeDelegate = self;
        [self.navigationController pushViewController:dirViewController animated:YES];
    }
}

#pragma mark Message User
- (void)showMessage:(NSString *)sMsg
{
    NSString *sCancel = NSLocalizedString(@"Ok", nil);
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil
                                                        message:sMsg
                                                       delegate:nil
                                              cancelButtonTitle:sCancel
                                              otherButtonTitles:nil, nil];
    [alertView show];
}

#pragma mark Query Alert View
- (void)showQuerryAlertViewWithTitle:(NSString *)sTitle
                             message:(NSString *)sMsg
                                 tag:(NSInteger)tag
{
    NSString *sCancel = NSLocalizedString(@"Cancel", nil);
    NSString *sOk = NSLocalizedString(@"Ok", nil);
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:sTitle
                                                        message:sMsg
                                                       delegate:self
                                              cancelButtonTitle:sCancel
                                              otherButtonTitles:sOk, nil];
    alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    alertView.tag = tag;
    [alertView show];
}

#pragma mark Error Alert View
- (void)showAlertWithError:(NSError *)error
{
    NSString *sTitle = NSLocalizedString(@"Error", nil);
    NSString *sMsg = [NSString stringWithFormat:@"%@", error.localizedDescription];
    NSString *sCancel = NSLocalizedString(@"Ok", nil);
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:sTitle
                                                        message:sMsg
                                                       delegate:nil
                                              cancelButtonTitle:sCancel
                                              otherButtonTitles:nil, nil];
    [alertView show];
}

#pragma mark - Public Methods
+ (NSURL *)defaultDocumentsPath
{
    NSArray *URLs = [[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
    return [URLs firstObject];
}

#pragma mark - Directory Controller Protocol
- (void)DirectoryController:(DirectoryViewController *)directoryController newFileFolderPath:(NSURL *)newFolderPath
{
    [directoryController.navigationController popViewControllerAnimated:YES];
    
    self.navigationController.toolbarHidden = ![self.tableView isEditing];
    
    NSArray *aIndexPaths = [self.tableView indexPathsForSelectedRows];
    if (![aIndexPaths count]) {
        return;
    }
    NSURL *oldUrl = [self.aContentsOfDirectory objectAtIndex:((NSIndexPath*)[aIndexPaths firstObject]).row];
    NSURL *oldDirUrl = [oldUrl URLByDeletingLastPathComponent];
    
    if (![oldDirUrl isEqual:newFolderPath]) {
        NSFileManager *fileManager = [NSFileManager defaultManager];
        for (NSIndexPath *indexPath in aIndexPaths) {
            NSURL *fileUrl = [self.aContentsOfDirectory objectAtIndex:indexPath.row];
            NSString *sFileName = [fileUrl lastPathComponent];
            NSURL *newFileUrl = [newFolderPath URLByAppendingPathComponent:sFileName];
            
            NSError *error;
            BOOL bFileMoved = [fileManager moveItemAtURL:fileUrl toURL:newFileUrl error:&error];
            if (!bFileMoved) {
                NSLog(@"\n%s\nError while moving object at URL:\n%@\nError:\n%@\n", __FUNCTION__, fileUrl, error);
                [self showAlertWithError:error];
            }
        }
        [self loadContentsOfDirectory];
    }
}

#pragma mark - Table View Data Source
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.urlPath.path isEqualToString:[DirectoryViewController defaultDocumentsPath].path]) {
        if ([self.tableView isEditing]) {
            NSURL *urlForIndexPath = [self.aContentsOfDirectory objectAtIndex:indexPath.row];
            NSString *sName = [urlForIndexPath lastPathComponent];
            if ([sName isEqualToString:@"Downloads"]) {
                return NO;
            }
        }
    }
    return YES;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.aContentsOfDirectory count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    if (indexPath.row % 2 == 1) {
        cell.backgroundColor = [UIColor colorWithRed:0.129f green:0.129f blue:0.129f alpha:1.000f];
    } else {
        cell.backgroundColor = [UIColor clearColor];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    
    NSURL *urlOfContent = [self.aContentsOfDirectory objectAtIndex:indexPath.row];
    
    UILabel *labelTitle = (UILabel *)[cell viewWithTag:CellTitleTag];
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:CellImageTag];
    
    NSString *sTitle;
    if ([self.urlPath isEqual:[DirectoryViewController defaultDocumentsPath]]) {
        // directories list
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSError *error;
        NSInteger iFilesAmount = [[fileManager contentsOfDirectoryAtURL:urlOfContent
                                             includingPropertiesForKeys:nil
                                                                options:0
                                                                  error:&error] count];
        if (error) {
            sTitle = [urlOfContent lastPathComponent];
        } else {
            sTitle = [NSString stringWithFormat:@"%@ (%zd)", [urlOfContent lastPathComponent], iFilesAmount];
        }
    } else {
        // files list
        sTitle = [urlOfContent lastPathComponent];
    }
    labelTitle.text = sTitle;
    labelTitle.textColor = [UIColor whiteColor];
    imageView.image = [MediaManager iconWithName:labelTitle.text];
    
    return cell;
}

#pragma mark - Table View Delegate
- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.urlPath.path isEqualToString:[DirectoryViewController defaultDocumentsPath].path]) {
        if ([self.tableView isEditing]) {
            NSURL *urlForIndexPath = [self.aContentsOfDirectory objectAtIndex:indexPath.row];
            NSString *sName = [urlForIndexPath lastPathComponent];
            if ([sName isEqualToString:@"Downloads"]) {
                return nil;
            }
        }
    }
    return indexPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    UILabel *titleLabel = (UILabel *)[cell viewWithTag:CellTitleTag];
    if ([cell isSelected]) {
        titleLabel.textColor = [UIColor blackColor];
    } else {
        titleLabel.textColor = [UIColor whiteColor];
    }
    
    if (![self.urlPath isEqual:[DirectoryViewController defaultDocumentsPath]]) {
        // action with file
        if (tableView.isEditing) {
            // table view is editing
            
        } else {
            
            NSURL *fileURL = [self.aContentsOfDirectory objectAtIndex:indexPath.row];
            
            NSString *path = [fileURL path];
            self.selectedPath = path;
            MEDIATYPE fileType = [MediaManager typeWithName:path];
            if (fileType == AUDIO) {
                AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
                if (!delegate.musicPlayer) {
                    delegate.musicPlayer = [MusicPlayerVC instanceWithPath:path];
                } else {
                    if (![delegate.musicPlayer.itemPath isEqualToString:path]) {
                        [delegate.musicPlayer loadNewItem:path];
                    }
                }
                
                [self.navigationController pushViewController:delegate.musicPlayer animated:YES];
            } else if (fileType == IMAGE) {
                // Create & present browser
                MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
                // Set options
                browser.displayActionButton = YES; // Show action button to allow sharing, copying, etc (defaults to YES)
                browser.displayNavArrows = NO; // Whether to display left and right nav arrows on toolbar (defaults to NO)
                browser.zoomPhotosToFill = YES; // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
                
                [self.navigationController pushViewController:browser animated:YES];
            } else if (fileType == PDF) {
                ReaderDocument *document = [ReaderDocument withDocumentFilePath:self.selectedPath password:@""];
                
                if (document != nil) // Must have a valid ReaderDocument object in order to proceed with things
                {
                    ReaderViewController *readerViewController = [[ReaderViewController alloc] initWithReaderDocument:document];
                    
                    readerViewController.delegate = self; // Set the ReaderViewController delegate to self
                    
                    [self presentViewController:readerViewController animated:YES completion:NULL];
                }
                
            } else {
                
                // this is VIDEO
                
                [[SNVideoPlayerManager sharedManager] setDelegate:self];
                [[SNVideoPlayerManager sharedManager] startLocalPlaybackForVideoWithURL:fileURL];
                
//                FilePreviewViewController *previewController = [[FilePreviewViewController alloc] initWithNibName:@"FilePreviewViewController" bundle:[NSBundle mainBundle]];
//                [self.navigationController pushViewController:previewController animated:YES];
//                [previewController setFilePath:path];
            }
            
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
        }
        
    } else {
        // action with folder
        if (tableView.isEditing) {
            // table view is editing
        } else {
            if (self.changeDelegate) {
                // if we are selecting new folder for file
                [self.changeDelegate DirectoryController:self newFileFolderPath:[self.aContentsOfDirectory objectAtIndex:indexPath.row]];
            } else {
                DirectoryViewController *dirViewController = [DirectoryViewController DirectoryViewControllerWithPath:[self.aContentsOfDirectory objectAtIndex:indexPath.row]];
                [self.navigationController pushViewController:dirViewController animated:YES];
                [tableView deselectRowAtIndexPath:indexPath animated:YES];
            }
        }
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    UILabel *titleLabel = (UILabel *)[cell viewWithTag:CellTitleTag];
    if ([cell isSelected]) {
        titleLabel.textColor = [UIColor blackColor];
    } else {
        titleLabel.textColor = [UIColor whiteColor];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 53.0f;
}

#pragma mark - UIAlert View Delegate
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == AlertViewCreateFolderTag) {
        // create new folder
        if (buttonIndex != alertView.cancelButtonIndex) {
            NSFileManager *fileManager = [NSFileManager defaultManager];
            NSString *sFolderName = [alertView textFieldAtIndex:0].text;
            NSURL *folderPath = [[DirectoryViewController defaultDocumentsPath] URLByAppendingPathComponent:sFolderName isDirectory:YES];
            NSError *error;
            BOOL bDirectoryCreated = [fileManager createDirectoryAtURL:folderPath withIntermediateDirectories:NO attributes:0 error:&error];
            if (!bDirectoryCreated) {
                NSLog(@"\n%s\nError while creating directory at URL:\n%@\nError:\n%@\n", __FUNCTION__, folderPath, error);
                
                [self showAlertWithError:error];
            } else {
                [self loadContentsOfDirectory];
            }
        }
        
    } else if (alertView.tag == AlertViewRenameTag) {
        // rename item
        if (buttonIndex != alertView.cancelButtonIndex) {
            NSString *sNewName = [alertView textFieldAtIndex:0].text;
            if (sNewName.length <= 0) {
                NSString *sMsg = NSLocalizedString(@"New name must have at least 1 character", nil);
                
                [self showMessage:sMsg];
                
                return;
            }
            NSFileManager *fileManager = [NSFileManager defaultManager];
            
            NSArray *aIndexPaths = [self.tableView indexPathsForSelectedRows];
            
            for (NSIndexPath *indexPath in aIndexPaths) {
                NSURL *fileURL = [self.aContentsOfDirectory objectAtIndex:indexPath.row];
                if ([((NSString*)([[fileURL pathComponents] lastObject])) isEqualToString:@"Downloads"]) {
                    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
                } else {
                    NSError *error;
                    NSString *sPathExtension = [fileURL pathExtension];
                    NSURL *newUrl = [fileURL URLByDeletingLastPathComponent];
                    if (sPathExtension && ![sPathExtension isEqual:[NSNull null]] && ![sPathExtension isEqualToString:@""]) {
                        // file
                        newUrl = [[newUrl URLByAppendingPathComponent:sNewName] URLByAppendingPathExtension:sPathExtension];
                    } else {
                        // directory
                        newUrl = [newUrl URLByAppendingPathComponent:sNewName isDirectory:YES];
                    }
                    BOOL bRenamed = [fileManager moveItemAtURL:fileURL toURL:newUrl error:&error];
                    if (!bRenamed) {
                        NSLog(@"\n%s\nError while renaming object at URL:\n%@\nError:\n%@\n", __FUNCTION__, fileURL, error);
                        [self showAlertWithError:error];
                    }
                }
            }
            [self loadContentsOfDirectory];
        }
    }
}

#pragma mark - Chrome Cast Controller Delegate
- (void)didDiscoverDeviceOnNetwork
{
    if (!self.navigationItem.rightBarButtonItems) {
        [[ChromecastDeviceController sharedInstance] manageViewController:self icon:YES toolbar:NO];
    }
}

#pragma mark - Outsource Delegates
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return 1;
}

- (MWPhoto *)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index == 0) {
        return [MWPhoto photoWithImage:[UIImage imageWithContentsOfFile:self.selectedPath]];
    }
    return nil;
}

- (void)dismissReaderViewController:(ReaderViewController *)viewController {
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}


@end
