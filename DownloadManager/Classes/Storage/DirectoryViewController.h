//
//  DirectoryViewController.h
//  DownloadManager
//
//  Created by Denis on 03.02.15.
//  Copyright (c) 2015 ComputerVerse Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MWPhotoBrowser.h"
#import "ReaderViewController.h"
#import "MusicPlayerVC.h"
#import "FilePreviewViewController.h"
#import "AppDelegate.h"

@protocol DirectoryControllerChangeProtocol;

@interface DirectoryViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, MWPhotoBrowserDelegate, ReaderViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSURL *urlPath;
@property (strong, nonatomic) id <DirectoryControllerChangeProtocol> changeDelegate;

+ (instancetype)DirectoryViewControllerWithPath:(NSURL *)urlPath;
+ (NSURL *)defaultDocumentsPath;

@end

@protocol DirectoryControllerChangeProtocol <NSObject>

@required
- (void)DirectoryController:(DirectoryViewController *)directoryController newFileFolderPath:(NSURL *)newFolderPath;

@end