//
//  SNDefaults.h
//  DownloadManager
//
//  Created by Denis on 08.04.15.
//  Copyright (c) 2015 dssolutions. All rights reserved.
//

#import <Foundation/Foundation.h>

/** Class created for more convenient NSUserDefaults saving and getting */
@interface SNDefaults : NSObject

/** saves object for key in NSUserDefaults
 * @param object - object to save
 * @param key - key value to associate first param with
 */
+ (void)saveObject:(id)object forKey:(NSString *)key;

/** saves BOOL value for key in NSUserDefaults
 * @param boolValue - BOOL value to save
 * @param key - key value to associate first param with
 */
+ (void)saveBool:(BOOL)boolValue forKey:(NSString *)key;

/** saves value for key in NSUserDefaults
 * @param value - value to save
 * @param key - key value to associate first param with
 */
+ (void)saveValue:(NSValue *)value forKey:(NSString *)key;

/** get object for key*/
+ (id)objectForKey:(NSString *)key;

/** get BOOL for key */
+ (BOOL)boolForKey:(NSString *)key;

/** get NSValue for key */
+ (NSValue *)valueForKey:(NSString *)key;

@end
