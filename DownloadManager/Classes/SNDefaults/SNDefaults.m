//
//  SNDefaults.m
//  DownloadManager
//
//  Created by Denis on 08.04.15.
//  Copyright (c) 2015 dssolutions. All rights reserved.
//

#import "SNDefaults.h"

@implementation SNDefaults

#pragma mark - Save
/** saves object for key in NSUserDefaults
 * @param object - object to save
 * @param key - key value to associate first param with
 */
+ (void)saveObject:(id)object forKey:(NSString *)key
{
    [[NSUserDefaults standardUserDefaults] setObject:object forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

/** saves BOOL value for key in NSUserDefaults
 * @param boolValue - BOOL value to save
 * @param key - key value to associate first param with
 */
+ (void)saveBool:(BOOL)boolValue forKey:(NSString *)key
{
    [[NSUserDefaults standardUserDefaults] setBool:boolValue forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

/** saves value for key in NSUserDefaults
 * @param value - value to save
 * @param key - key value to associate first param with
 */
+ (void)saveValue:(NSValue *)value forKey:(NSString *)key
{
    [[NSUserDefaults standardUserDefaults] setValue:value forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


#pragma mark - Get
/** get object for key*/
+ (id)objectForKey:(NSString *)key
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:key];
}

/** get BOOL for key */
+ (BOOL)boolForKey:(NSString *)key
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:key];
}

/** get NSValue for key */
+ (NSValue *)valueForKey:(NSString *)key
{
    return [[NSUserDefaults standardUserDefaults] valueForKey:key];
}

@end
