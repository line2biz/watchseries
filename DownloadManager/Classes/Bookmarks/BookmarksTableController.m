//
//  BookmarksTableController.m
//  TestAddBlock
//
//  Created by Denis on 11.02.15.
//  Copyright (c) 2015 Denis. All rights reserved.
//

#import "BookmarksTableController.h"

#import "DMManager.h"
#import "AppDelegate.h"

// alert view tags
#define kAlertViewNewCategoryTag    10
#define kAlertViewDeleteObjectsTag  20
#define kAlertViewRenameObjectsTag  30
#define kAlertViewChangeCategoryTag 40
#define kAlertViewRemoveCategoryTag 50

// object types
#define kObjectTypeUndefined    -1
#define kObjectTypeCategory     100
#define kObjectTypeBookmark     200
#define kObjectTypeCombined     300

@interface BookmarksTableController () <BookmarksTableControllerChangeCategoryProtocol, UIAlertViewDelegate>
{
    NSInteger _loadIdx;
}

@property (strong, nonatomic) NSMutableArray *maObjects;

@end

@implementation BookmarksTableController

#pragma mark - Life Cycle

+ (instancetype)bookmarksControllerWithCategory:(DMCategories *)category
{
    UIStoryboard *storyboard = [[AppDelegate sharedDelegate].window.rootViewController storyboard];
    BookmarksTableController *bookmarksTableController = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([BookmarksTableController class])];
    
    if (category) {
        bookmarksTableController.category = category;
    }
    
    return bookmarksTableController;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _loadIdx = 0;
    
     self.clearsSelectionOnViewWillAppear = NO;
    
     self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    if (!self.managedObjectContext) {
        self.managedObjectContext = [[DMManager sharedManager] managedObjectContext];
    }
    
    if (!self.category && !self.changeCategoryDelegate) {
        UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel", nil)
                                                                          style:UIBarButtonItemStylePlain
                                                                         target:self
                                                                         action:@selector(closeController)];
        self.navigationItem.leftBarButtonItem = leftBarButton;
    }
    
    [self createToolbarItems];
    
    [self loadObjects];
}

- (void)viewWillAppear:(BOOL)animated
{
    if (!_loadIdx) {
        _loadIdx++;
    } else {
        [self loadObjects];
    }
    
    self.navigationController.toolbarHidden = ![self.tableView isEditing];
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    [super setEditing:editing animated:animated];
    if (editing) {
        
        self.navigationController.toolbarHidden = NO;
        
    } else {
        
        self.navigationController.toolbarHidden = YES;
    }
}

#pragma mark - Private Methods
- (void)closeController
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)createToolbarItems
{
    NSMutableArray *maToolbarItems = [NSMutableArray new];
    
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    if (!self.category) {
        UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Add Category", nil) style:UIBarButtonItemStylePlain target:self action:@selector(addNewCategory)];
        [maToolbarItems addObject:addButton];
        [maToolbarItems addObject:flexSpace];
    } else {
        UIBarButtonItem *removeCatButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Remove Category", nil) style:UIBarButtonItemStylePlain target:self action:@selector(removeCategoryAction)];
        [maToolbarItems addObject:removeCatButton];
        [maToolbarItems addObject:flexSpace];
    }
    
    UIBarButtonItem *deleteButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Delete", nil) style:UIBarButtonItemStylePlain target:self action:@selector(deleteObjects)];
    [maToolbarItems addObject:deleteButton];
    [maToolbarItems addObject:flexSpace];
    
    UIBarButtonItem *renameButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Rename", nil) style:UIBarButtonItemStylePlain target:self action:@selector(renameObjects)];
    [maToolbarItems addObject:renameButton];
    
    if (!self.changeCategoryDelegate) {
        UIBarButtonItem *changeCategoryButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Change category", nil) style:UIBarButtonItemStylePlain target:self action:@selector(changeCategory)];
        [maToolbarItems addObject:flexSpace];
        [maToolbarItems addObject:changeCategoryButton];
    }
    
    
    [self setToolbarItems:(NSArray*)maToolbarItems];
}

- (void)loadObjects
{
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sortOrder" ascending:YES];
    NSArray *sortDescriptors = @[sortDescriptor];
    NSFetchRequest *fetchRequest;
    NSArray *aCategories;
    NSArray *aBookmarks;
    NSError *error;
    
    // load categories
    if (!self.category) {
        fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([DMCategories class])];
        [fetchRequest setSortDescriptors:sortDescriptors];
        
        aCategories = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
        if (![aCategories count]) {
            if (error) {
                NSLog(@"\n%s\nError while fetching Categories:\n%@\n", __PRETTY_FUNCTION__, error);
            }
        }
    }
    
    // load bookmarks
    if (!self.changeCategoryDelegate) {
        error = nil;
        fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([DMBookmark class])];
        [fetchRequest setSortDescriptors:sortDescriptors];
        [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"(category = %@)", self.category]];
        
        aBookmarks = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
        if (![aBookmarks count]) {
            if (error) {
                NSLog(@"\n%s\nError while fetching Bookmarks:\n%@\n", __PRETTY_FUNCTION__, error);
            }
        }
    }
    
    if (self.maObjects) {
        self.maObjects = nil;
    }
    self.maObjects = [NSMutableArray new];
    
    // unite arrays
    if ([aCategories count]) {
        [self.maObjects addObjectsFromArray:aCategories];
    }
    if ([aBookmarks count]) {
        [self.maObjects addObjectsFromArray:aBookmarks];
    }
    
    [self.tableView reloadData];
}

#pragma mark Ordering objects

- (void)reorderBookmarksInCategory:(DMCategories *)category completition:(void(^)())completition
{
    NSManagedObjectContext *context = self.managedObjectContext;
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:NSStringFromClass([DMBookmark class])
                                   inManagedObjectContext:context]];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(category = %@)", category];
    NSSortDescriptor *sortDesc = [NSSortDescriptor sortDescriptorWithKey:@"sortOrder" ascending:YES];
    
    [request setPredicate:pred];
    [request setSortDescriptors:@[sortDesc]];
    
    NSError *error;
    NSArray *objects = [context executeFetchRequest:request
                                              error:&error];
    if (error) {
        NSLog(@"\n%s\nError while fetching bookmarks by categorie\n", __PRETTY_FUNCTION__);
    } else {
        for (NSInteger i=0; i < [objects count]; i++) {
            DMBookmark *bookmark = (DMBookmark *)[objects objectAtIndex:i];
            
            bookmark.sortOrder = @(i + 1);
        }
    }
    
    if (completition) {
        completition();
    }
}

- (void)reorderBookmarksIndexesInListWithCompletition:(void(^)())completition
{
    NSMutableArray *maBookmarks = [NSMutableArray new];
    for (id object in self.maObjects) {
        if ([self typeOfObject:object] == kObjectTypeBookmark) {
            [maBookmarks addObject:object];
        }
    }
    
    for (NSInteger i=0; i < [maBookmarks count]; i++) {
        DMBookmark *bookmark = (DMBookmark*)[maBookmarks objectAtIndex:i];
        bookmark.sortOrder = @(i + 1);
    }
    
    if (completition) {
        completition();
    }
}

#pragma mark Actions with objects
- (NSInteger)typeOfObject:(id)object
{
    if ([object isKindOfClass:[DMCategories class]]) {
        return kObjectTypeCategory;
    } else if ([object isKindOfClass:[DMBookmark class]]) {
        return kObjectTypeBookmark;
    } else {
        return kObjectTypeUndefined;
    }
}

- (NSInteger)typeOfObjects:(NSArray *)arrayOfObjects
{
    BOOL categoriesPresent = NO;
    BOOL bookmarksPresent = NO;
    for (id object in arrayOfObjects) {
        if ([self typeOfObject:object] == kObjectTypeCategory) {
            if (!categoriesPresent) {
                categoriesPresent = YES;
            }
        } else if ([self typeOfObject:object] == kObjectTypeBookmark) {
            if (!bookmarksPresent) {
                bookmarksPresent = YES;
            }
        } else {
            return kObjectTypeUndefined;
        }
    }
    
    if (categoriesPresent && bookmarksPresent) {
        return kObjectTypeCombined;
    } else if (categoriesPresent) {
        return kObjectTypeCategory;
    } else if (bookmarksPresent) {
        return kObjectTypeBookmark;
    } else {
        return kObjectTypeUndefined;
    }
}

- (void)addNewCategory
{
    [self.tableView beginUpdates];
    [self showRequestAlertViewWithTag:kAlertViewNewCategoryTag
                                title:nil
                              message:NSLocalizedString(@"Input title", nil)
                            textField:YES
                         cancelButton:NSLocalizedString(@"Cancel", nil)
                        confirmButton:NSLocalizedString(@"OK", nil)];
}

- (void)deleteObjects
{
    NSArray *aIndexPaths = [self.tableView indexPathsForSelectedRows];
    if (aIndexPaths.count) {
        
        NSMutableArray *maSelectedObjs = [NSMutableArray arrayWithCapacity:aIndexPaths.count];
        for (NSIndexPath *indexPath in aIndexPaths) {
            [maSelectedObjs addObject:[self.maObjects objectAtIndex:indexPath.row]];
        }
        
        NSInteger iObjectsType = [self typeOfObjects:maSelectedObjs];
        NSMutableString *msFinalRequest = [NSMutableString new];
        
        switch (iObjectsType) {
            case kObjectTypeBookmark:
            {
                [msFinalRequest appendString:NSLocalizedString(@"Are You shure you want to delete ", nil)];
                [msFinalRequest appendString:(aIndexPaths.count > 1) ? NSLocalizedString(@"those Bookmarks?", nil) : NSLocalizedString(@"this Bookmark?", nil)];
            }
                break;
            case kObjectTypeCategory:
            {
                [msFinalRequest appendString:NSLocalizedString(@"Are You shure? This action will delete all bookmarks of ", nil)];
                [msFinalRequest appendString:(aIndexPaths.count > 1) ? NSLocalizedString(@"those Categories!", nil) : NSLocalizedString(@"this Category!", nil)];
            }
                break;
            case kObjectTypeCombined:
            {
                [msFinalRequest appendString:NSLocalizedString(@"Are You shure? This action will delete all bookmarks of ", nil)];
                [msFinalRequest appendString:(aIndexPaths.count > 1) ? NSLocalizedString(@"those Categories!", nil) : NSLocalizedString(@"this Category!", nil)];
            }
                break;
            case kObjectTypeUndefined:
            {
                return;
            }
                break;
                
            default:
                break;
        }
        
        [self.tableView beginUpdates];
        
        NSString *sCancel = NSLocalizedString(@"Cancel", nil);
        NSString *sOK = NSLocalizedString(@"OK", nil);
        
        [self showRequestAlertViewWithTag:kAlertViewDeleteObjectsTag
                                    title:nil
                                  message:msFinalRequest
                                textField:NO
                             cancelButton:sCancel
                            confirmButton:sOK];
    }
}

- (void)renameObjects
{
    NSArray *aIndexPaths = [self.tableView indexPathsForSelectedRows];
    if (aIndexPaths.count) {
        
        [self.tableView beginUpdates];
        
        NSString *sMsg = NSLocalizedString(@"Input new title", nil);
        NSString *sCancel = NSLocalizedString(@"Cancel", nil);
        NSString *sOK = NSLocalizedString(@"OK", nil);
        [self showRequestAlertViewWithTag:kAlertViewRenameObjectsTag
                                    title:nil
                                  message:sMsg
                                textField:YES
                             cancelButton:sCancel
                            confirmButton:sOK];
    }
}

- (void)changeCategory
{
    NSArray *aIndexPaths = [self.tableView indexPathsForSelectedRows];
    if (aIndexPaths.count) {
        NSMutableArray *maSelectedObjs = [NSMutableArray arrayWithCapacity:aIndexPaths.count];
        for (NSIndexPath *indexPath in aIndexPaths) {
            [maSelectedObjs addObject:[self.maObjects objectAtIndex:indexPath.row]];
        }
        
        NSInteger iObjectsType = [self typeOfObjects:maSelectedObjs];
        
        if (iObjectsType == kObjectTypeBookmark) {
            
            NSString *sMsg = NSLocalizedString(@"Are You shure You wish to change category for selected Bookmarks?", nil);
            NSString *sCancel = NSLocalizedString(@"Cancel", nil);
            NSString *sOK = NSLocalizedString(@"OK", nil);
            
            [self showRequestAlertViewWithTag:kAlertViewChangeCategoryTag
                                        title:nil
                                      message:sMsg
                                    textField:NO
                                 cancelButton:sCancel
                                confirmButton:sOK];
        } else {
            for (NSIndexPath *indexPath in aIndexPaths) {
                [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
            }
            NSString *sAlertMessage = NSLocalizedString(@"You can change Category only for Bookmark", nil);
            [self showAlertViewWithErrorMessage:sAlertMessage];
        }
    }
}

- (void)removeCategoryAction
{
    NSArray *aIndexPaths = [self.tableView indexPathsForSelectedRows];
    if (aIndexPaths.count) {
        
        [self.tableView beginUpdates];
        
        NSString *sMsg = NSLocalizedString(@"Are You shure You wish to remove category for selected Bookmarks?", nil);
        NSString *sCancel = NSLocalizedString(@"Cancel", nil);
        NSString *sOK = NSLocalizedString(@"OK", nil);
        
        [self showRequestAlertViewWithTag:kAlertViewRemoveCategoryTag
                                    title:nil
                                  message:sMsg
                                textField:NO
                             cancelButton:sCancel
                            confirmButton:sOK];
    }
}

#pragma mark Alerts methods
- (void)showRequestAlertViewWithTag:(NSInteger)iTag
                              title:(NSString *)sTitle
                            message:(NSString *)sMessage
                          textField:(BOOL)hasTextField
                       cancelButton:(NSString *)sCancel
                      confirmButton:(NSString *)sOk
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:sTitle
                                                        message:sMessage
                                                       delegate:self
                                              cancelButtonTitle:sCancel
                                              otherButtonTitles:sOk, nil];
    alertView.tag = iTag;
    if (hasTextField == YES) {
        alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    }
    [alertView show];
}

- (void)showAlertViewWithErrorMessage:(NSString *)sMessage
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil
                                                        message:sMessage
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                              otherButtonTitles:nil, nil];
    [alertView show];
}

#pragma mark - Change Category Protocol
- (void)bookmarkTableController:(BookmarksTableController *)bookmarkController didSelectCategory:(DMCategories *)category
{
    DMCategories *oldCat;
    NSArray *aIndexPaths = [self.tableView indexPathsForSelectedRows];
    for (NSIndexPath *indexPath in aIndexPaths) {
        id object = [self.maObjects objectAtIndex:indexPath.row];
        
        DMBookmark *bookmark = (DMBookmark *)object;
        if (!oldCat) {
            oldCat = bookmark.category;
        }
        bookmark.category = category;
        bookmark.sortOrder = @([category.bookmarks count]);
    }
    
    // reconfigure sort indexes in old and new categories
    __weak typeof(self) weakSelf = self;
    [self reorderBookmarksInCategory:oldCat completition:^{
        typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf) {
            [strongSelf reorderBookmarksInCategory:category completition:^{
                
                [[DMManager sharedManager] saveContext];
                
                [strongSelf loadObjects];
            }];
        }
    }];
    
    [bookmarkController.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.maObjects count];
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    id object = [self.maObjects objectAtIndex:indexPath.row];
    NSInteger iObjectType = [self typeOfObject:object];
    
    if (iObjectType == kObjectTypeBookmark) {
        DMBookmark *bookmark = (DMBookmark *)object;
        
        cell.textLabel.text = bookmark.title;
        cell.detailTextLabel.text = bookmark.url;
        
    } else if (iObjectType == kObjectTypeCategory) {
        DMCategories *category = (DMCategories *)object;
        
        NSString *sTitle = [NSString stringWithFormat:@"%@ (%zd)", category.title, [category.bookmarks count]];
        cell.textLabel.text = sTitle;
        cell.detailTextLabel.text = @"";
    } else {
        cell.textLabel.text = @"Wrong Object";
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

#pragma mark - Table View Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (![tableView isEditing]) {
        id object = [self.maObjects objectAtIndex:indexPath.row];
        NSInteger iObjectType = [self typeOfObject:object];
        
        if (iObjectType == kObjectTypeBookmark) {
            [self.delegate loadBookmark:((DMBookmark *)object).url];
            [self closeController];
            
        } else if (iObjectType == kObjectTypeCategory) {
            
            if (!self.changeCategoryDelegate) {
                BookmarksTableController *controller = [BookmarksTableController bookmarksControllerWithCategory:(DMCategories *)object];
                controller.delegate = self.delegate;
                [self.navigationController pushViewController:controller animated:YES];
            } else {
                [self.changeCategoryDelegate bookmarkTableController:self didSelectCategory:(DMCategories *)object];
            }
        }
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [self deleteObjects];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    NSInteger iLastCategoryIdx = 0;
    for (id object in self.maObjects) {
        if ([self typeOfObject:object] == kObjectTypeCategory) {
            iLastCategoryIdx++;
        } else {
            break;
        }
    }
    
    NSInteger iPrevIndex = fromIndexPath.row;
    NSInteger iNextIndex = toIndexPath.row;
    
    BOOL bFromTopToBottom = (iPrevIndex < iNextIndex) ? YES : NO;
    
    // configure next index
    if (iPrevIndex < iLastCategoryIdx) {
        // category is being moved
        // max next idx must be last category idx
        iNextIndex = (iNextIndex < iLastCategoryIdx) ? iNextIndex : (iLastCategoryIdx - 1);
    } else {
        // bookmark is being moved
        // min next idx must be last category idx + 1
        iNextIndex = (iNextIndex >= iLastCategoryIdx) ? iNextIndex : iLastCategoryIdx;
    }
    
    // indexes for enumerators
    NSInteger iStartIdx, iEndIndex, iMod;
    if (bFromTopToBottom) {
        iStartIdx = iPrevIndex + 1;
        iEndIndex = iNextIndex + 1;
        iMod = -1;
    } else {
        iStartIdx = iNextIndex;
        iEndIndex = iPrevIndex;
        iMod = 1;
    }
    
    if (iPrevIndex < iLastCategoryIdx) {
        // other categories
        for (NSInteger i=iStartIdx; i < iEndIndex; i++) {
            DMCategories *category = (DMCategories *)[self.maObjects objectAtIndex:i];
            category.sortOrder = [NSNumber numberWithInteger:[category.sortOrder integerValue] + iMod];
        }
        
        // current category
        DMCategories *category = (DMCategories *)[self.maObjects objectAtIndex:iPrevIndex];
        category.sortOrder = [NSNumber numberWithInteger:iNextIndex+1];
        
    } else {
        // other bookmarks
        for (NSInteger i=iStartIdx; i < iEndIndex; i++) {
            DMBookmark *bookmark = (DMBookmark *)[self.maObjects objectAtIndex:i];
            bookmark.sortOrder = [NSNumber numberWithInteger:[bookmark.sortOrder integerValue] + iMod];
        }
        
        // current bookmark
        DMBookmark *bookmark = (DMBookmark *)[self.maObjects objectAtIndex:iPrevIndex];
        bookmark.sortOrder = [NSNumber numberWithInteger:iNextIndex-iLastCategoryIdx+1];
    }
    
    // save objects at DB
    [[DMManager sharedManager] saveContext];
    
    // save changes at main array
    id object = [self.maObjects objectAtIndex:fromIndexPath.row];
    [self.maObjects removeObject:object];
    [self.maObjects insertObject:object atIndex:iNextIndex];
    
    // move Row at needed location
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.tableView moveRowAtIndexPath:toIndexPath toIndexPath:[NSIndexPath indexPathForRow:iNextIndex inSection:0]];
    });
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEditing]) {
        return YES;
    }
    return NO;
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

#pragma mark - Alert View Delegate
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.cancelButtonIndex != buttonIndex) {
        switch (alertView.tag) {
            case kAlertViewChangeCategoryTag:
            {
                BookmarksTableController *bookmarkController = [BookmarksTableController bookmarksControllerWithCategory:nil];
                bookmarkController.changeCategoryDelegate = self;
                [self.navigationController pushViewController:bookmarkController animated:YES];
            }
                break;
            case kAlertViewNewCategoryTag:
            {
                NSString *sTitle = [alertView textFieldAtIndex:0].text;
                if (sTitle.length) {
                    DMCategories *newCategory = [DMCategories addAndReturnNewCategoryWithTitle:sTitle];
                    
                    NSInteger iLastCategoryIdx = 0;
                    for (id object in self.maObjects) {
                        if ([self typeOfObject:object] == kObjectTypeCategory) {
                            iLastCategoryIdx++;
                        } else {
                            break;
                        }
                    }
                    
                    [self.maObjects insertObject:newCategory atIndex:iLastCategoryIdx];
                    [self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:iLastCategoryIdx inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
                }
            }
                break;
            case kAlertViewDeleteObjectsTag:
            {
                NSArray *aIndexPaths = [self.tableView indexPathsForSelectedRows];
                NSMutableIndexSet *mIndexSet = [NSMutableIndexSet new];
                NSMutableArray *maSelectedCategories = [NSMutableArray new];
                NSMutableArray *maSelectedBookmarks = [NSMutableArray new];
                DMCategories *oldCategory;
                for (NSIndexPath *indexPath in aIndexPaths) {
                    id object = [self.maObjects objectAtIndex:indexPath.row];
                    NSInteger iObjectType = [self typeOfObject:object];
                    
                    if (iObjectType == kObjectTypeBookmark) {
                        if (!oldCategory) {
                            oldCategory = ((DMBookmark*)object).category;
                        }
                        [maSelectedBookmarks addObject:object];
                        
                    } else if (iObjectType == kObjectTypeCategory) {
                        [maSelectedCategories addObject:object];
                    }
                    
                    [mIndexSet addIndex:indexPath.row];
                }
                if ([maSelectedCategories count]) {
                    [DMCategories deleteCategories:(NSArray *)maSelectedCategories];
                }
                if ([maSelectedBookmarks count]) {
                    [DMBookmark deleteBookmarks:(NSArray *)maSelectedBookmarks];
                }
                
                [self.maObjects removeObjectsAtIndexes:(NSIndexSet *)mIndexSet];
                if ([maSelectedBookmarks count]) {
                    [self reorderBookmarksIndexesInListWithCompletition:nil];
                }
                
                [self.tableView deleteRowsAtIndexPaths:aIndexPaths withRowAnimation:UITableViewRowAnimationFade];
            }
                break;
            case kAlertViewRenameObjectsTag:
            {
                NSString *sTitle = [alertView textFieldAtIndex:0].text;
                if (sTitle.length) {
                    NSArray *aIndexPaths = [self.tableView indexPathsForSelectedRows];
                    for (NSIndexPath *indexPath in aIndexPaths) {
                        id object = [self.maObjects objectAtIndex:indexPath.row];
                        NSInteger iObjectType = [self typeOfObject:object];
                        
                        if (iObjectType == kObjectTypeBookmark) {
                            DMBookmark *bookmark = (DMBookmark *)object;
                            bookmark.title = sTitle;
                        } else if (iObjectType == kObjectTypeCategory) {
                            DMCategories *category = (DMCategories *)object;
                            category.title = sTitle;
                        }
                        [self configureCell:[self.tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
                    }
                    [[DMManager sharedManager] saveContext];
                }
            }
                break;
            case kAlertViewRemoveCategoryTag:
            {
                NSArray *aIndexPaths = [self.tableView indexPathsForSelectedRows];
                NSMutableIndexSet *mIndexSet = [NSMutableIndexSet new];
                
                NSInteger iMaxSortOrder = 1000;
                NSManagedObjectContext *context = self.managedObjectContext;
                
                NSFetchRequest *request = [[NSFetchRequest alloc] init];
                [request setEntity:[NSEntityDescription entityForName:NSStringFromClass([DMBookmark class])
                                               inManagedObjectContext:context]];
                DMCategories *category = nil;
                NSPredicate *pred = [NSPredicate predicateWithFormat:@"(category = %@)", category];
                [request setPredicate:pred];
                NSError *error;
                NSArray *objects = [context executeFetchRequest:request
                                                          error:&error];
                if (error) {
                    NSLog(@"\n%s\nError while fetching bookmarks by categorie\n", __PRETTY_FUNCTION__);
                } else {
                    iMaxSortOrder = [objects count];
                }
                
                for (NSInteger i=0; i < [aIndexPaths count]; i++) {
                    NSIndexPath *indexPath = (NSIndexPath *)[aIndexPaths objectAtIndex:i];
                    DMBookmark *bookmark = (DMBookmark *)[self.maObjects objectAtIndex:indexPath.row];
                    bookmark.category = nil;
                    bookmark.sortOrder = @(iMaxSortOrder + i);
                    [mIndexSet addIndex:indexPath.row];
                }
                
                [[DMManager sharedManager] saveContext];
                
                [self.maObjects removeObjectsAtIndexes:(NSIndexSet *)mIndexSet];
                
                [self.tableView deleteRowsAtIndexPaths:aIndexPaths withRowAnimation:UITableViewRowAnimationFade];
            }
                break;
                
            default:
                break;
        }
    }
    
    if (alertView.tag != kAlertViewChangeCategoryTag) {
        [self.tableView endUpdates];
    }
}

@end
