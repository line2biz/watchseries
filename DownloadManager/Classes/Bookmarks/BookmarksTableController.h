//
//  BookmarksTableController.h
//  TestAddBlock
//
//  Created by Denis on 11.02.15.
//  Copyright (c) 2015 Denis. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DMCategories;

@protocol BookmarksTableControllerChangeCategoryProtocol;
@protocol BookmarksTableControllerLoadProtocol;

@interface BookmarksTableController : UITableViewController

@property (strong, nonatomic) DMCategories *category;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) id<BookmarksTableControllerChangeCategoryProtocol> changeCategoryDelegate;
@property (strong, nonatomic) id<BookmarksTableControllerLoadProtocol> delegate;

+ (instancetype)bookmarksControllerWithCategory:(DMCategories *)category;

@end

@protocol BookmarksTableControllerChangeCategoryProtocol <NSObject>

@required
- (void)bookmarkTableController:(BookmarksTableController *)bookmarkController didSelectCategory:(DMCategories *)category;

@end

@protocol BookmarksTableControllerLoadProtocol <NSObject>

@required
- (void)loadBookmark:(NSString*)urlString;

@end