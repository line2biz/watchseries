//
//  FilePreviewViewController.h
//  DLManager
//
//  Created by Cai DaRong on 1/30/13.
//  Copyright (c) 2013 Cai DaRong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

@interface FilePreviewViewController : UIViewController <UIActionSheetDelegate>

@property (nonatomic, retain) NSString *filePath;

+ (FilePreviewViewController *) instance;

@property (nonatomic, strong) MPMoviePlayerController *myPlayer;

- (void) setFilePath:(NSString *)filePath;

@end
