//
//  PlayerVC.m
//  DownloadManager
//
//  Created by Cai DaRong on 6/3/13.
//  Copyright (c) 2013 Cai DaRong. All rights reserved.
//

#import <MediaPlayer/MediaPlayer.h>
#import "MusicPlayerVC.h"
#import "DirectoryManager.h"
#import "MediaManager.h"

@implementation MusicPlayerVC

@synthesize itemPath;
@synthesize player;

+ (MusicPlayerVC *) instanceWithPath:(NSString *)path {
	return [[MusicPlayerVC alloc] initWithPath:path];
}

- (BOOL)prefersStatusBarHidden {
    if (!UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation]) && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        return YES;
    } else {
        return NO;
    }
}

- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation {
    return UIStatusBarAnimationSlide;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration {
    [self setNeedsStatusBarAppearanceUpdate];
}


- (void)beginInterruption {
    [self pause];
}

- (void)endInterruptionWithFlags:(NSUInteger)flags {
    [self playBtnClicked];
}

- (id) initWithPath:(NSString *)path {
	if ( self = [super initWithNibName:@"MusicPlayerVC" bundle:nil] ) {
		self.itemPath = path;
	}
	
	return self;
}

- (void)loadNewItem:(NSString*)path
{
    @try {
        self.itemPath = path;
        [m_lblName setText:self.itemPath.lastPathComponent];
        [self playAudioWithPath:self.itemPath];
    }
    @catch (NSException *exception) {
        NSLog(@"\n%s\nException:\n%@\n", __FUNCTION__, exception);
    }
    @finally {
        
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[AVAudioSession sharedInstance] setDelegate:self];
	
    self.view.backgroundColor = [UIColor colorWithRed:0.176f green:0.176f blue:0.176f alpha:1.000f];
    
	[self.navigationItem setTitle:@"Music Player"];
	
//	DirectoryManager *directoryManager = [DirectoryManager instance];
    
    // code added at update
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (self.itemPath) {
        NSURL *fileUrl = [NSURL URLWithString:self.itemPath];
        NSURL *directoryUrl = [fileUrl URLByDeletingLastPathComponent];
        NSError *error;
        NSArray *aUrlsOfFiles = [fileManager contentsOfDirectoryAtURL:directoryUrl
                                           includingPropertiesForKeys:nil
                                                              options:0
                                                                error:&error];
        NSMutableArray *maPathsOfAudioFiles = [NSMutableArray new];
        for (NSURL *url in aUrlsOfFiles) {
            MEDIATYPE mediaType = [MediaManager typeWithName:[url path]];
            if (mediaType == AUDIO) {
                [maPathsOfAudioFiles addObject:[url path]];
            }
        }
        
        m_aryAudioFiles = [NSArray arrayWithArray:(NSArray *)maPathsOfAudioFiles];
    }
    
//	m_aryAudioFiles = [directoryManager fileInfoWithType:DirectoryMusic];
	
	[m_lblName setText:self.itemPath.lastPathComponent];
    
    MPVolumeView *myVolumeView = [[MPVolumeView alloc] initWithFrame:CGRectMake(15, self.view.frame.size.height - 105, self.view.frame.size.width - 30, 23)];
    myVolumeView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    
    [self.view addSubview:myVolumeView];
	
	[self playBtnClicked];
}

- (void) viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
    
    [self setNeedsStatusBarAppearanceUpdate];
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(musicPlayer:) name:MPMusicPlayerControllerNowPlayingItemDidChangeNotification object:nil];
}

- (void) viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - Base Proc
- (void) playAudioWithPath:(NSString *)path
{
    @try {
        if ( ![self.itemPath isEqualToString:path] )
            self.itemPath = path;
        
        [m_lblName setText:self.itemPath.lastPathComponent];
        
        [self stopAudio];
        
        if ([MPNowPlayingInfoCenter class])  {
            
            NSDictionary *currentlyPlayingTrackInfo = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:m_lblName, nil] forKeys:[NSArray arrayWithObjects:MPMediaItemPropertyTitle, nil]];
            [MPNowPlayingInfoCenter defaultCenter].nowPlayingInfo = currentlyPlayingTrackInfo;
        }
        
        [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
        [self becomeFirstResponder];
        
        NSURL *itemURL = [NSURL fileURLWithPath:self.itemPath];
        self.player = [[AVAudioPlayer alloc] initWithContentsOfURL:itemURL error:nil];
        self.player.delegate = self;
        self.player.numberOfLoops = 0;
        self.player.volume = 1.0;
        
        m_curDurationInSec = self.player.duration;
        
        [m_btnPlay setSelected:YES];
        
        [self startSeekTimer];
        
        [self.player play];
    }
    @catch (NSException *exception) {
        NSLog(@"\n%s\nException:\n%@\n", __FUNCTION__, exception);
    }
    @finally {
        
    }
}

- (void) stopAudio
{
    @try {
        if ( self.player )
        {
            if ( [self.player isPlaying] )
                [self.player stop];
            
            self.player = nil;
        }
        
        if ([MPNowPlayingInfoCenter class])  {
            [MPNowPlayingInfoCenter defaultCenter].nowPlayingInfo = nil;
        }
        [[UIApplication sharedApplication] endReceivingRemoteControlEvents];
        [self resignFirstResponder];
    }
    @catch (NSException *exception) {
        NSLog(@"\n%s\nException:\n%@\n", __FUNCTION__, exception);
    }
    @finally {
        
    }
}

- (void) pause
{
    @try {
        [[UIApplication sharedApplication] endReceivingRemoteControlEvents];
        [self resignFirstResponder];
        if ( self.player && [self.player isPlaying] )
        {
            [self.player pause];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"\n%s\nException:\n%@\n", __FUNCTION__, exception);
    }
    @finally {
        
    }
}

- (NSString *) prevMedia
{
	BOOL bSuffle = m_btnShuffle.isSelected;
	int curIndex = 0;
	for (NSString *path in m_aryAudioFiles)
	{
		if ( [self.itemPath isEqualToString:path] )
		{
			curIndex = [m_aryAudioFiles indexOfObject:path];
			break;
		}
	}
	
	int newIndex = (curIndex == 0) ? m_aryAudioFiles.count-1 : curIndex-1;
	if ( bSuffle )
		newIndex = random() % m_aryAudioFiles.count;
	
	return [m_aryAudioFiles objectAtIndex:newIndex];
}


- (NSString *) nextMedia
{
	BOOL bSuffle = m_btnShuffle.isSelected;
	int curIndex = 0;
	for (NSString *path in m_aryAudioFiles)
	{
		if ( [self.itemPath isEqualToString:path] )
		{
			curIndex = [m_aryAudioFiles indexOfObject:path];
			break;
		}
	}
	
	int newIndex = (curIndex >= m_aryAudioFiles.count-1) ? 0 : curIndex+1;
	if ( bSuffle )
		newIndex = random() % m_aryAudioFiles.count;
	
	return [m_aryAudioFiles objectAtIndex:newIndex];
}

- (void) startSeekTimer
{
	[self endSeekTimer];

	m_seekUpdateTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(seekbarUpdate:) userInfo:nil repeats:YES];
}

- (void) endSeekTimer
{
	if ( m_seekUpdateTimer )
	{
		[m_seekUpdateTimer invalidate];
		m_seekUpdateTimer = nil;
	}
}

- (void)remoteControlReceivedWithEvent:(UIEvent *)receivedEvent {
    
    if (receivedEvent.type == UIEventTypeRemoteControl) {
        
        switch (receivedEvent.subtype) {
                
            case UIEventSubtypeRemoteControlPlay:
                [self playBtnClicked];
                break;
                
            case UIEventSubtypeRemoteControlPause:
                [self playBtnClicked];
                break;
                
            case UIEventSubtypeRemoteControlPreviousTrack:
                [self prevBtnClicked];
                break;
                
            case UIEventSubtypeRemoteControlNextTrack:
                [self nextBtnClicked];
                break;
                
            default:
                break;
        }
    }
}


- (IBAction) playBtnClicked
{
	if ( !m_btnPlay.isSelected )
	{
		if ( !self.player )
			[self playAudioWithPath:self.itemPath];
		else
		{
            [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
            [self becomeFirstResponder];
			[self startSeekTimer];
			
			[self.player play];
		}
		
		[m_btnPlay setSelected:YES];
	}
	else
	{
		if ( [self.player isPlaying] )
		{
			[self pause];
			
			[m_btnPlay setSelected:NO];
		}
	}
}

- (IBAction) prevBtnClicked
{
	[self playAudioWithPath:[self prevMedia]];
}

- (IBAction) nextBtnClicked
{
	[self playAudioWithPath:[self nextMedia]];
}

- (IBAction) volumeSliderChanged
{
	if ( self.player )
		[self.player setVolume:m_sldVolume.value];
}

- (IBAction) repeatBtnClicked
{
	[m_btnRepeat setSelected:!m_btnRepeat.isSelected];
}

- (IBAction) shuffleBtnClicked
{
	[m_btnShuffle setSelected:!m_btnShuffle.isSelected];
}

- (IBAction) seekSliderUpdate
{
	if ( [self.player isPlaying] )
	{
		[self.player setCurrentTime:m_curDurationInSec * m_sldSeek.value];
	}
}

#pragma mark -
#pragma mark - Timer Proc
- (void) seekbarUpdate:(NSTimer *)timer
{
	int playedTime = self.player.currentTime;
	
	int mins = playedTime / 60;
	int secs = playedTime - mins * 60;
	
	[m_lblPlayedTime setText:[NSString stringWithFormat:@"%02d:%02d", mins, secs]];
	
	float remainedTime = MAX(m_curDurationInSec - playedTime, 0);
	
	mins = remainedTime / 60;
	secs = remainedTime - mins * 60;
	
	[m_lblRemainedTime setText:[NSString stringWithFormat:@"-%02d:%02d", mins, secs]];
	
	[m_sldSeek setValue:(float)playedTime/(float)m_curDurationInSec];
}

#pragma mark -
#pragma mark - NSNotification

#pragma mark -
#pragma mark - AVAudioPlayerDelegate
- (void) audioPlayerBeginInterruption:(AVAudioPlayer *)player
{
	[m_btnPlay setSelected:YES];
	
	[self startSeekTimer];
}

- (void) audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
	[m_btnPlay setSelected:NO];
	
	if ( m_btnRepeat.isSelected )
	{
		[self playAudioWithPath:self.nextMedia];
	}
	else
	{
		[self endSeekTimer];
	}
}

@end
