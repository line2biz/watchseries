//
//  FilePreviewViewController.m
//  DLManager
//
//  Created by Cai DaRong on 1/30/13.
//  Copyright (c) 2013 Cai DaRong. All rights reserved.
//

#import "FilePreviewViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "MBProgressHUD.h"
#import "DirectoryManager.h"

@implementation FilePreviewViewController

@synthesize filePath;

+ (FilePreviewViewController *) instance {
	return [[FilePreviewViewController alloc] initWithNibName:@"FilePreviewViewController" bundle:[NSBundle mainBundle]];
}

- (void)orientInferface:(UIInterfaceOrientation)newOrientation duration:(NSTimeInterval)duration {
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        if (duration == 0.0) {
            self.automaticallyAdjustsScrollViewInsets = NO;
        }
        [self setNeedsStatusBarAppearanceUpdate];
        [UIView animateWithDuration:duration animations:^{
            if (UIInterfaceOrientationIsPortrait(newOrientation)) {
                self.myPlayer.view.frame = CGRectMake(0, 64, self.myPlayer.view.frame.size.width, self.view.frame.size.height - 64 - 49);
            } else {
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                    self.myPlayer.view.frame = CGRectMake(0, 32, self.myPlayer.view.frame.size.width, self.view.frame.size.height - 32 - 49);
                } else {
                    self.myPlayer.view.frame = CGRectMake(0, 64, self.myPlayer.view.frame.size.width, self.view.frame.size.height - 64 - 49);
                }
            }
        }];
    }
}

- (BOOL)prefersStatusBarHidden {
    if (!UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation]) && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        return YES;
    } else {
        return NO;
    }
}

- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation {
    return UIStatusBarAnimationSlide;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration {
    [self orientInferface:interfaceOrientation duration:duration];
}

- (void) viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithRed:0.176f green:0.176f blue:0.176f alpha:1.000f];
    
    UIBarButtonItem *exportButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(actionBtnClicked:)];
    self.navigationItem.rightBarButtonItem = exportButton;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self orientInferface:[[UIApplication sharedApplication] statusBarOrientation] duration:0.0];
}

- (void) setFilePath:(NSString *)value
{
    @try {
        filePath = value;
        
        self.myPlayer = [[MPMoviePlayerController alloc] initWithContentURL:[NSURL fileURLWithPath:filePath]];
        [self.myPlayer prepareToPlay];
        self.myPlayer.shouldAutoplay = NO;
        self.myPlayer.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        self.myPlayer.view.frame = CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height - 64 - 49);
        [self.view addSubview:self.myPlayer.view];
    }
    @catch (NSException *exception) {
        NSLog(@"\n%s\nException:\n%@\n", __FUNCTION__, exception);
    }
    @finally {
        
    }
}

// NSLocalizedString(@"_cancel", @"") Export Export to Camera Roll"

- (void) actionBtnClicked:(id)sender {
	UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Export", @"")
														 delegate:self
												cancelButtonTitle:NSLocalizedString(@"abbrechen", @"")
										   destructiveButtonTitle:NSLocalizedString(@"Exporttext", @"")
												otherButtonTitles:nil];
    [actionSheet showFromTabBar:self.tabBarController.tabBar];
}

#pragma mark - UIActionSheetDelegate Methods

- (void) actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    @try {
        if (buttonIndex == 0) {
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
            [library writeVideoAtPathToSavedPhotosAlbum:[NSURL fileURLWithPath:filePath] completionBlock:^(NSURL *assetURL, NSError *error) {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
            }];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"\n%s\nException:\n%@\n", __FUNCTION__, exception);
    }
    @finally {
        
    }
}

@end
