//
//  PlayerVC.h
//  DownloadManager
//
//  Created by Cai DaRong on 6/3/13.
//  Copyright (c) 2013 Cai DaRong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>

@interface MusicPlayerVC : UIViewController <AVAudioPlayerDelegate>
{
	IBOutlet UILabel *m_lblName;
	IBOutlet UISlider *m_sldVolume;
	IBOutlet UISlider *m_sldSeek;
	IBOutlet UIImageView *m_ivThumb;
	
	IBOutlet UILabel *m_lblPlayedTime;
	IBOutlet UILabel *m_lblRemainedTime;
	
	IBOutlet UIButton *m_btnPlay;
	IBOutlet UIButton *m_btnRepeat;
	IBOutlet UIButton *m_btnShuffle;
	
	NSArray *m_aryAudioFiles;
	
	NSTimer *m_seekUpdateTimer;
	int m_curDurationInSec;
}

@property (nonatomic, retain) NSString *itemPath;
@property (nonatomic, retain) AVAudioPlayer *player;

+ (MusicPlayerVC *) instanceWithPath:(NSString *)path;
- (void)loadNewItem:(NSString*)path;

- (void) stopAudio;

@end
