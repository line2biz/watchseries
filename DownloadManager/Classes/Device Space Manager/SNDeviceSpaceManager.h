//
//  SNDeviceSpaceManager.h
//  DownloadManager
//
//  Created by Denis on 05.06.15.
//  Copyright (c) 2015 ComputerVerse Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SNDeviceSpaceManager : NSObject

+ (uint64_t)diskSpaceTotal;

+ (uint64_t)diskSpaceFree;

+ (uint64_t)sizeOfObjectAtURL:(NSURL *)URL;

+ (NSString *)formattedSizeFromBytes:(uint64_t)bytes;

@end
