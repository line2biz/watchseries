//
//  SNDeviceSpaceManager.m
//  DownloadManager
//
//  Created by Denis on 05.06.15.
//  Copyright (c) 2015 ComputerVerse Corporation. All rights reserved.
//

#import "SNDeviceSpaceManager.h"

@implementation SNDeviceSpaceManager

#pragma mark Disk space
- (uint64_t)freeDiskspace
{
    uint64_t totalFreeSpace = 0;
    
    __autoreleasing NSError *error = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSDictionary *dictionary = [[NSFileManager defaultManager] attributesOfFileSystemForPath:[paths lastObject] error: &error];
    
    if (dictionary) {
        NSNumber *freeFileSystemSizeInBytes = [dictionary objectForKey:NSFileSystemFreeSize];
        totalFreeSpace = [freeFileSystemSizeInBytes unsignedLongLongValue];
    } else {
        DBGLog(@"Error Obtaining System Memory Info: Domain = %@, Code = %zd", [error domain], [error code]);
        return 0;
    }
    
    return totalFreeSpace;
}

+ (uint64_t)diskSpaceTotal
{
    uint64_t totalSpace = 0;
    __autoreleasing NSError *error = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSDictionary *dictionary = [[NSFileManager defaultManager] attributesOfFileSystemForPath:[paths lastObject] error: &error];
    if (dictionary) {
        NSNumber *totalFileSystemSizeInBytes = [dictionary objectForKey:NSFileSystemSize];
        totalSpace = [totalFileSystemSizeInBytes unsignedLongLongValue];
    } else {
        DBGLog(@"Error Obtaining System Memory Info: Domain = %@, Code = %zd", [error domain], [error code]);
        return 0;
    }
    
    return totalSpace;
}

+ (uint64_t)diskSpaceFree
{
    uint64_t totalFreeSpace = 0;
    __autoreleasing NSError *error = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSDictionary *dictionary = [[NSFileManager defaultManager] attributesOfFileSystemForPath:[paths lastObject] error: &error];
    
    if (dictionary) {
        NSNumber *freeFileSystemSizeInBytes = [dictionary objectForKey:NSFileSystemFreeSize];
        totalFreeSpace = [freeFileSystemSizeInBytes unsignedLongLongValue];
    } else {
        DBGLog(@"Error Obtaining System Memory Info: Domain = %@, Code = %zd", [error domain], [error code]);
        return 0;
    }
    
    return totalFreeSpace;
}

+ (uint64_t)sizeOfObjectAtURL:(NSURL *)URL
{
    uint64_t objectSize = 0;
    
    __autoreleasing NSError *error = nil;
    NSDictionary *dictionary = [[NSFileManager defaultManager] attributesOfItemAtPath:[URL path] error:&error];
    
    if (dictionary) {
        
        NSString *sFileType = [dictionary objectForKey:NSFileType];
        if ([sFileType isEqualToString:NSFileTypeDirectory]) {
            NSArray *contents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[URL path] error:nil];
            NSEnumerator *contentsEnumurator = [contents objectEnumerator];
            
            NSString *file;
            while (file = [contentsEnumurator nextObject]) {
                objectSize += [SNDeviceSpaceManager sizeOfObjectAtURL:[URL URLByAppendingPathComponent:file]];
            }
        }
        else {
            NSNumber *freeFileSystemSizeInBytes = [dictionary objectForKey:NSFileSize];
            objectSize = [freeFileSystemSizeInBytes unsignedLongLongValue];
        }
        
    } else {
        DBGLog(@"Error Obtaining File Size Info: Domain = %@, Code = %zd", [error domain], [error code]);
        return 0;
    }
    
    return objectSize;
}

+ (NSString *)formattedSizeFromBytes:(uint64_t)bytes
{
    uint64_t KB_size = 1024;
    uint64_t MB_size = KB_size * KB_size;
    uint64_t GB_size = MB_size * KB_size;
    CGFloat fResult = 0.0f;
    NSString *sResult;
    if (bytes > GB_size) {
        fResult = [self ratioBetweenFirst:bytes andSecond:GB_size];
        sResult = @"GB";
    }
    else if (bytes > MB_size) {
        fResult = [self ratioBetweenFirst:bytes andSecond:MB_size];
        sResult = @"MB";
    }
    else if (bytes > KB_size) {
        fResult = [self ratioBetweenFirst:bytes andSecond:KB_size];
        sResult = @"KB";
    }
    else {
        fResult = (CGFloat)bytes;
        sResult = @"B";
    }
    
    return [NSString stringWithFormat:@"%.2f %@", fResult, sResult];
}

+ (CGFloat)ratioBetweenFirst:(uint64_t)value andSecond:(uint64_t)size
{
    return value / size + (double)(value % size) / (double)size;
}

@end
