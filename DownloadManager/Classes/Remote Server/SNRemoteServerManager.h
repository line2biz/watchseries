//
//  SNRemoteServerManager.h
//  DownloadManager
//
//  Created by Denis on 17.04.15.
//  Copyright (c) 2015 ComputerVerse Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SNRemoteServerManager : NSObject

/** Shared instance */
+ (instancetype)sharedInstance;

/** Start server */
- (void)startServer;

/** Stop server */
- (void)stopServer;

/** Get URL for local file on remote server */
- (NSURL *)remoteURLForFileAtURL:(NSURL *)fileURL;

- (NSString *)getIPAddress:(BOOL)preferIPv4;

@end
