//
//  SNRemoteServerManager.m
//  DownloadManager
//
//  Created by Denis on 17.04.15.
//  Copyright (c) 2015 ComputerVerse Corporation. All rights reserved.
//

#import "SNRemoteServerManager.h"

#import "RoutingHTTPServer.h"

#include <ifaddrs.h>
#include <arpa/inet.h>
#include <net/if.h>

#define IOS_CELLULAR    @"pdp_ip0"
#define IOS_WIFI        @"en0"
#define IOS_VPN         @"utun0"
#define IP_ADDR_IPv4    @"ipv4"
#define IP_ADDR_IPv6    @"ipv6"

@interface SNRemoteServerManager ()

@property (strong, nonatomic) RoutingHTTPServer *httpServer;
@property (assign, nonatomic) NSInteger portNumber;
@property (strong, nonatomic) NSString *filePath;
@property (strong, nonatomic) NSString *sRoute;

@end

@implementation SNRemoteServerManager

#pragma mark - Shared Instance
+ (instancetype)sharedInstance
{
    static SNRemoteServerManager *object = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        object = [[SNRemoteServerManager alloc] init];
    });
    return object;
}

#pragma mark - Life Cycle
- (instancetype)init
{
    self = [super init];
    if (self) {
        [self configureManager];
    }
    return self;
}

#pragma mark - Private Methods
- (void)configureManager
{
    _sRoute = @"movieLoad.mp4";
    _httpServer = [[RoutingHTTPServer alloc] init];
    
    self.portNumber = 7999;
    
    [_httpServer setPort:self.portNumber];                               // TODO: make sure this port isn't already in use
    
    // file manager
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    // documents directory URL
    NSArray *URLs = [fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
    NSURL *urlDocuments = [URLs firstObject];
    
    [_httpServer setDocumentRoot:[urlDocuments path]];
    
    [_httpServer handleMethod:@"GET" withPath:[NSString stringWithFormat:@"/%@", _sRoute] target:self selector:@selector(handleLoadRequest:withResponse:)];
}

- (void)handleLoadRequest:(RouteRequest *)request withResponse:(RouteResponse *)response
{
    [response respondWithFile:self.filePath async:NO];
}

- (NSDictionary *)getIPAddresses
{
    NSMutableDictionary *addresses = [NSMutableDictionary dictionaryWithCapacity:8];
    
    // retrieve the current interfaces - returns 0 on success
    struct ifaddrs *interfaces;
    if(!getifaddrs(&interfaces)) {
        // Loop through linked list of interfaces
        struct ifaddrs *interface;
        for(interface=interfaces; interface; interface=interface->ifa_next) {
            if(!(interface->ifa_flags & IFF_UP) /* || (interface->ifa_flags & IFF_LOOPBACK) */ ) {
                continue; // deeply nested code harder to read
            }
            const struct sockaddr_in *addr = (const struct sockaddr_in*)interface->ifa_addr;
            char addrBuf[ MAX(INET_ADDRSTRLEN, INET6_ADDRSTRLEN) ];
            if(addr && (addr->sin_family==AF_INET || addr->sin_family==AF_INET6)) {
                NSString *name = [NSString stringWithUTF8String:interface->ifa_name];
                NSString *type;
                if(addr->sin_family == AF_INET) {
                    if(inet_ntop(AF_INET, &addr->sin_addr, addrBuf, INET_ADDRSTRLEN)) {
                        type = IP_ADDR_IPv4;
                    }
                } else {
                    const struct sockaddr_in6 *addr6 = (const struct sockaddr_in6*)interface->ifa_addr;
                    if(inet_ntop(AF_INET6, &addr6->sin6_addr, addrBuf, INET6_ADDRSTRLEN)) {
                        type = IP_ADDR_IPv6;
                    }
                }
                if(type) {
                    NSString *key = [NSString stringWithFormat:@"%@/%@", name, type];
                    addresses[key] = [NSString stringWithUTF8String:addrBuf];
                }
            }
        }
        // Free memory
        freeifaddrs(interfaces);
    }
    return [addresses count] ? addresses : nil;
}

#pragma mark - Public Methods
- (void)startServer
{
    [_httpServer start:NULL];
}

- (void)stopServer
{
    [_httpServer stop];
}

- (NSURL *)remoteURLForFileAtURL:(NSURL *)fileURL
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:fileURL.path]) { // local url
        return nil;
    }
    _filePath = [fileURL path];
    
    NSString *sIPAddress = [self getIPAddress:YES];
    if (!sIPAddress) {
        NSLog(@"\n%s\nDidn`t get IP\n", __PRETTY_FUNCTION__);
        return nil;
    }
    
    NSURL *remoteURL;
    NSString *sRemoteURL = [NSString stringWithFormat:@"http://%@:%zd/%@", sIPAddress, self.portNumber, self.sRoute];
    remoteURL = [NSURL URLWithString:sRemoteURL];
    
    return remoteURL;
}

- (NSString *)getIPAddress:(BOOL)preferIPv4
{
    NSArray *searchArray = preferIPv4 ?
    @[ IOS_VPN @"/" IP_ADDR_IPv4, IOS_VPN @"/" IP_ADDR_IPv6, IOS_WIFI @"/" IP_ADDR_IPv4, IOS_WIFI @"/" IP_ADDR_IPv6, IOS_CELLULAR @"/" IP_ADDR_IPv4, IOS_CELLULAR @"/" IP_ADDR_IPv6 ] :
    @[ IOS_VPN @"/" IP_ADDR_IPv6, IOS_VPN @"/" IP_ADDR_IPv4, IOS_WIFI @"/" IP_ADDR_IPv6, IOS_WIFI @"/" IP_ADDR_IPv4, IOS_CELLULAR @"/" IP_ADDR_IPv6, IOS_CELLULAR @"/" IP_ADDR_IPv4 ] ;
    
    NSDictionary *addresses = [self getIPAddresses];
    
    __block NSString *address;
    [searchArray enumerateObjectsUsingBlock:^(NSString *key, NSUInteger idx, BOOL *stop) {
         address = addresses[key];
         if(address) *stop = YES;
     }];
    return address ? address : nil;
}



@end
