//
//  WebView.h
//  DownloadManager
//
//  Created by Nils on 19.10.13.
//  Copyright (c) 2013 ComputerVerse Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol WebViewDelegate <NSObject>

- (void)webviewProgressMade:(NSNumber*)progress;
- (void)webviewDidFailWithError:(NSString*)error;
- (void)webviewDidFinishLoad;
- (void)webviewDidStartLoad;
- (void)webviewTitleUpdate;
- (void)webviewUrlUpdate;

@end

@interface WebViewLoad : UIWebView <NSURLConnectionDataDelegate, UIWebViewDelegate>

@property (nonatomic, unsafe_unretained) id <WebViewDelegate> webViewDelegate;

@property (nonatomic, strong) NSString* title;

- (BOOL)validateUrlString:(NSString*)urlString;

- (void)loadUrlorGoogle:(NSString*)urlString;
- (void)loadUrl:(NSString*)urlString withPost:(NSString*)postData;

- (CGSize)windowSize;
- (CGPoint)scrollOffset;

@end
