//
//  SNSpecialWindow.h
//  DownloadManager
//
//  Created by Denis on 27.03.15.
//  Copyright (c) 2015 ComputerVerse Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SNSpecialWindow : UIWindow
{
    CGPoint    tapLocation;
    NSTimer    *contextualMenuTimer;
}

@end
