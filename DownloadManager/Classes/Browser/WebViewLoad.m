//
//  WebView.m
//  DownloadManager
//
//  Created by Nils on 19.10.13.
//  Copyright (c) 2013 ComputerVerse Corporation. All rights reserved.
//

#import "WebViewLoad.h"
#import "OptionViewController.h"

#import "SNAdManager.h"
#import "SNIPAddress.h"

// IP check
#include <netdb.h>
#include <arpa/inet.h>

@implementation WebViewLoad
@synthesize webViewDelegate = _webViewDelegate;

#pragma mark - Life Cycle
- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.delegate = self;
    }
    return self;
}

#pragma mark - Private Methods
- (void)stopLoadingOfWebView {
    
}



- (BOOL)validateUrlString:(NSString*)urlString
{
    @try {
        NSString* urlRegex = @"(?i)\\b(?:[a-z][\\w\\-]+://(?:\\S+?(?::\\S+?)?\\@)?)?(?:(?:(?<!:/|\\.)(?:(?:[a-z0-9\\-]+\\.)+[a-z]{2,4}(?![a-z]))|(?<=://)/))(?:(?:[^\\s()<>]+|\\((?:[^\\s()<>]+|(?:\\([^\\s()<>]*\\)))*\\))*)(?<![\\s`!()\\[\\]{};:'\".,<>?«»“”‘’])";
        
        NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegex];
        BOOL bEvaluated = [urlTest evaluateWithObject:urlString];
        return bEvaluated;
    }
    @catch (NSException *exception) {
        NSLog(@"\n%s\nException:\n%@\n", __FUNCTION__, exception);
    }
    @finally {
        
    }
}


#pragma mark - Public Methods
- (void)loadUrl:(NSString*)urlString withPost:(NSString*)postData
{
    NSURL* url;
    
    if ([self validateUrlString:urlString] == NO) {
        self.title = urlString;
        urlString = [NSString stringWithFormat:@"http://www.google.com/search?q=%@", [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        url = [NSURL URLWithString:urlString];
        self.tag = 13;
    } else {
        if([urlString rangeOfString:@"http://" options:NSCaseInsensitiveSearch].location == NSNotFound && [urlString rangeOfString:@"https://" options:NSCaseInsensitiveSearch].location == NSNotFound) {
            urlString = [NSString stringWithFormat:@"http://%@", urlString];
        }
        
        url = [NSURL URLWithString:urlString];
        self.title = url.host;
    }
    
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url];
    
    if (![postData isEqualToString:@""]) {
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:[postData dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    
    [self loadRequest:request];
    
    if (_webViewDelegate) {
        if ([_webViewDelegate respondsToSelector:@selector(webviewTitleUpdate)]) {
            [_webViewDelegate performSelector:@selector(webviewTitleUpdate)];
        }
        
        if ([_webViewDelegate respondsToSelector:@selector(webviewUrlUpdate)]) {
            [_webViewDelegate performSelector:@selector(webviewUrlUpdate)];
        }
    }
}

- (void)loadUrlorGoogle:(NSString*)urlString {
    [self loadUrl:urlString withPost:@""];
}

- (CGSize)windowSize
{
    CGSize size;
    size.width = [[self stringByEvaluatingJavaScriptFromString:@"window.innerWidth"] integerValue];
    size.height = [[self stringByEvaluatingJavaScriptFromString:@"window.innerHeight"] integerValue];
    return size;
}

- (CGPoint)scrollOffset
{
    CGPoint pt;
    pt.x = [[self stringByEvaluatingJavaScriptFromString:@"window.pageXOffset"] integerValue];
    pt.y = [[self stringByEvaluatingJavaScriptFromString:@"window.pageYOffset"] integerValue];
    return pt;
}

#pragma mark - Web View Delegates
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    BOOL test2 = [[NSUserDefaults standardUserDefaults] boolForKey:@"switch2"];
    if(test2 == YES) {
        
        BOOL blockURL = NO;
        NSString *urlString = [request.URL description];
        
        // NEW VERSION OF FORBIDDEN SITES DETECTION
        
        NSInteger iTotalBlockedAds = [[NSUserDefaults standardUserDefaults] integerForKey:kBlockedAdsTotal];
        NSInteger iCurrentlyBlockedAds = [[NSUserDefaults standardUserDefaults] integerForKey:kBlockedAdsCurrent];
        
        if (SYSTEM_VERSION_LESS_THAN(@"8.0")) { // this check must be on verions prior to 8.0
            NSInteger iForbiddenSitesAmount = [[SNAdManager sharedManager] amountOfForbiddenSitesInURLPath:urlString];
            if (iForbiddenSitesAmount > 0) {
                
                blockURL = YES;
                
                iTotalBlockedAds += iForbiddenSitesAmount;
                iCurrentlyBlockedAds += iForbiddenSitesAmount;
                
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setInteger:iTotalBlockedAds forKey:kBlockedAdsTotal];
                [defaults setInteger:iCurrentlyBlockedAds forKey:kBlockedAdsCurrent];
                [defaults synchronize];
            }
            
            if (blockURL) {
                return NO;
            }
        }
        
        NSURL *URLAddress = request.URL;
        
        NSArray *aIPs = [SNIPAddress IPsFromURL:URLAddress];
        if (!aIPs || ![aIPs count]) {
            return NO;
        }
        else {
            if ([[SNAdManager sharedManager] isForbiddenIPs:aIPs]) {
                
                iTotalBlockedAds += 1;
                iCurrentlyBlockedAds += 1;
                
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setInteger:iTotalBlockedAds forKey:kBlockedAdsTotal];
                [defaults setInteger:iCurrentlyBlockedAds forKey:kBlockedAdsCurrent];
                [defaults synchronize];
                
                DBGLog(@"Forbidden IP found:\nURL: %@", request.URL);
                return NO;
            }
        }
    }
    
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    @try {
        if (webView.tag == 13) {
            webView.tag = 0;
        } else {
            self.title = [webView.request.URL host];
        }
        if (_webViewDelegate) {
            if ([_webViewDelegate respondsToSelector:@selector(webviewDidStartLoad)]) {
                [_webViewDelegate performSelector:@selector(webviewDidStartLoad)];
            }
        }
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    }
    @catch (NSException *exception) {
        NSLog(@"\n%s\nException:\n%@\n", __FUNCTION__, exception);
    }
    @finally {
        
    }
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    @try {
        NSString* siteTitle = [self stringByEvaluatingJavaScriptFromString:@"document.title"];
        
        if (siteTitle && ![siteTitle isEqualToString:@""]) {
            self.title = siteTitle;
        }
        if (_webViewDelegate) {
            if ([_webViewDelegate respondsToSelector:@selector(webviewTitleUpdate)]) {
                [_webViewDelegate performSelector:@selector(webviewTitleUpdate)];
            }
            
            if ([_webViewDelegate respondsToSelector:@selector(webviewUrlUpdate)]) {
                [_webViewDelegate performSelector:@selector(webviewUrlUpdate)];
            }
            
            if ([_webViewDelegate respondsToSelector:@selector(webviewDidFinishLoad)]) {
                [_webViewDelegate performSelector:@selector(webviewDidFinishLoad)];
            }
        }
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    }
    @catch (NSException *exception) {
        NSLog(@"\n%s\nException:\n%@\n", __FUNCTION__, exception);
    }
    @finally {
        
    }
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    @try {
        if (_webViewDelegate) {
            if ([_webViewDelegate respondsToSelector:@selector(webviewDidFailWithError:)]) {
                [_webViewDelegate performSelector:@selector(webviewDidFailWithError:) withObject:[error localizedDescription]];
            }
        }
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    }
    @catch (NSException *exception) {
        NSLog(@"\n%s\nException:\n%@\n", __FUNCTION__, exception);
    }
    @finally {
        
    }
}

@end
