//
//  BrowserViewController.m
//  BrowserViewController
//
//  Created by Nils Weidl on 18.10.13.
//  Copyright (c) 2013 ComputerVerse Corporation. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>

#import "BrowserViewController.h"

#import "HistoryViewController.h"

#import "AppDelegate.h"
#import "DLIDEKeyboardView.h"
#import "OpenFacebookPage.h"

#import "SNVideoPlayerManager.h"

NSString * const kAutomaticScreenMode   = @"kAutomaticScreenMode";
NSString * const kFullScreenMode        = @"kFullScreenMode";
NSString * const kShowInLocalPlayer     = @"Show In Local Player";

// Alert Views Tag Keys
#define kShowInLocalPlayerAlertViewTag 1000

@interface BrowserViewController () <UIScrollViewDelegate, PlayerManagerDelegate, ChromecastControllerDelegate>
{
    BOOL _screenModeAutomatic;
    BOOL _fullScreenModeEnabled;
    __weak IBOutlet NSLayoutConstraint *_constraintToolBarHeight;
    __weak IBOutlet NSLayoutConstraint *_constraintToolBarBottom;
    NSInteger _loadIdx;
    NSArray *_aSiteExceptions;
    
    // custom video player
    NSURL *_videoURLForLocalPlayer, *_videoSourceURL;
    
    // YES if convertion to .mp4 was made by magicStick
    BOOL _convert2mp3Download;
}

@property (weak, nonatomic) IBOutlet UIButton *buttonScreenMode;

@end

@implementation BrowserViewController


#pragma mark - Life Cycle
- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _loadIdx = 0;
    
    // orientation changed - manual way
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(orientationChanged:)
                                                     name:UIDeviceOrientationDidChangeNotification
                                                   object:nil];
    }
    
    // start settings
    [self controllerCreation];
    
    // web view setting
    self.webview.backgroundColor = [UIColor whiteColor];
    self.webview.scrollView.backgroundColor = [UIColor colorWithRed:0.176f green:0.176f blue:0.176f alpha:1.000f];
    self.webview.opaque = NO;
    self.webview.mediaPlaybackAllowsAirPlay = YES;
    self.webview.mediaPlaybackRequiresUserAction = YES;
    self.webview.webViewDelegate = self;
    
    // go start url-address
    [self home];
    
    // register for notifications
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(link1) name:@"ActivityLink1" object:nil];
    
    
    // long tap gesture recognition init
    UILongPressGestureRecognizer *gesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longTap:)];
    gesture.delegate = self;
    [self.webview.scrollView addGestureRecognizer:gesture];
    
    // set delegate for video player manager
    [[SNVideoPlayerManager sharedManager] setDelegate:self];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemTimeJumpedNotification object:nil];
    
    if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"UIMoviePlayerControllerDidEnterFullscreenNotification" object:nil];
    }
}

- (void)controllerCreation
{
    self.view.backgroundColor = [UIColor colorWithRed:0.176f green:0.176f blue:0.176f alpha:1.000f];
    
    [self createArrayWithSiteExceptions];
    
    // navigation bar part
    
    self.textFieldView.layer.cornerRadius = 5.0f;
    self.textFieldView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    self.textFieldView.clipsToBounds = YES;
    
    self.textField = [[UITextField alloc] initWithFrame:CGRectMake(25, 5, self.textFieldView.frame.size.width - 50, self.textFieldView.frame.size.height - 8)];
    self.textField.backgroundColor = [UIColor clearColor];
    self.textField.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.textField.textColor = [UIColor whiteColor];
    self.textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    self.textField.delegate = self;
    self.textField.textAlignment = NSTextAlignmentCenter;
    self.textField.keyboardAppearance = UIKeyboardAppearanceDark;
    self.textField.keyboardType = UIKeyboardTypeWebSearch;
    self.textField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    
    [self.textFieldView addSubview:self.textField];
    
    self.clearButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.clearButton setTitle:@"x" forState:UIControlStateNormal];
    [self.clearButton setTitleColor:[UIColor colorWithRed:0.529f green:0.529f blue:0.529f alpha:1.000f] forState:UIControlStateNormal];
    [self.clearButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [self.clearButton.titleLabel setFont:[UIFont boldSystemFontOfSize:22.0]];
    self.clearButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    self.clearButton.frame = CGRectMake(self.textFieldView.frame.size.width - 24, 4, 20, 22);
    self.clearButton.hidden = YES;
    self.clearButton.alpha = 0.0;
    [self.clearButton addTarget:self action:@selector(clearUrlField) forControlEvents:UIControlEventTouchUpInside];
    
    [self.textFieldView addSubview:self.clearButton];
    
    self.reloadButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.reloadButton setImage:[UIImage imageNamed:@"Refresh"] forState:UIControlStateNormal];
    [self.reloadButton setImage:[UIImage imageNamed:@"RefreshHighlighted"] forState:UIControlStateHighlighted];
    self.reloadButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    self.reloadButton.frame = CGRectMake(self.textFieldView.frame.size.width - 24, 4, 20, 22);
    self.reloadButton.hidden = NO;
    [self.reloadButton addTarget:self action:@selector(reloadUrl) forControlEvents:UIControlEventTouchUpInside];
    
    [self.textFieldView addSubview:self.reloadButton];
    
    self.cancelLoadButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.cancelLoadButton setTitle:@"x" forState:UIControlStateNormal];
    [self.cancelLoadButton setTitleColor:[UIColor colorWithRed:0.529f green:0.529f blue:0.529f alpha:1.000f] forState:UIControlStateNormal];
    [self.cancelLoadButton.titleLabel setFont:[UIFont boldSystemFontOfSize:22.0]];
    [self.cancelLoadButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    self.cancelLoadButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    self.cancelLoadButton.frame = CGRectMake(self.textFieldView.frame.size.width - 24, 4, 20, 22);
    self.cancelLoadButton.hidden = YES;
    [self.cancelLoadButton addTarget:self action:@selector(cancelLoad) forControlEvents:UIControlEventTouchUpInside];
    
    [self.textFieldView addSubview:self.cancelLoadButton];
    
    self.progressView = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleBar];
    self.progressView.frame = CGRectMake(0, 61, self.view.frame.size.width, 1);
    self.progressView.progress = 0.0;
    self.progressView.hidden = YES;
    self.progressView.alpha = 0.0;
    self.progressView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth;
    self.progressView.tintColor = [UIColor whiteColor];
    
    [self.textFieldView addSubview:self.progressView];
    
    // screen mode button
    [self.buttonScreenMode setBackgroundColor:[UIColor clearColor]];
    [self.buttonScreenMode addTarget:self action:@selector(didTapFullScreen:) forControlEvents:UIControlEventTouchUpInside];
    
    UILongPressGestureRecognizer *longGestureRecog = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longGestureAutoScreenHandler:)];
    longGestureRecog.delegate = self;
    longGestureRecog.minimumPressDuration = 0.5f;
    [self.buttonScreenMode addGestureRecognizer:longGestureRecog];
    
    self.webview.scrollView.delegate = self;
    
    // full screen mode swiping from edges
    UIScreenEdgePanGestureRecognizer *edgePanGestureLeft = [[UIScreenEdgePanGestureRecognizer alloc] initWithTarget:self action:@selector(handleEdgePanGesture:)];
    edgePanGestureLeft.delegate = self;
    edgePanGestureLeft.maximumNumberOfTouches = 1;
    edgePanGestureLeft.edges = UIRectEdgeLeft;
    [self.view addGestureRecognizer:edgePanGestureLeft];
    
    UIScreenEdgePanGestureRecognizer *edgePanGestureRight = [[UIScreenEdgePanGestureRecognizer alloc] initWithTarget:self action:@selector(handleEdgePanGesture:)];
    edgePanGestureRight.delegate = self;
    edgePanGestureRight.maximumNumberOfTouches = 1;
    edgePanGestureRight.edges = UIRectEdgeRight;
    [self.view addGestureRecognizer:edgePanGestureRight];
    
    
}

- (void)createArrayWithSiteExceptions
{
    NSArray *aSiteExceptions = @[
                                 @"megatv.to",
                                 @"vimeo.com",
                                 @"youtube.com",
                                 @"dailymotion.com",
                                 @"akamaihd.net",
                                 @"ard.de",
                                 @"dmax.de",
                                 @"mdr.de",
                                 @"mtv.de",
                                 @"viva.de",
                                 @"myvideo.de",
                                 @"n-tv.de",
                                 @"zdf.de",
                                 @"mixcloud.com",
                                 @"soundcloud.com"
                                 ];
    _aSiteExceptions = [NSArray arrayWithArray:aSiteExceptions];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self orientInferface:[[UIApplication sharedApplication] statusBarOrientation] duration:0.0];
    
    [self registerSpecWindowNotifications];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(link2) name:@"ActivityLink2" object:nil];
    
    // crome cast button
    [ChromecastDeviceController sharedInstance].delegate = (id<ChromecastControllerDelegate>)self;
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(audioDidStart:) name:AVPlayerItemTimeJumpedNotification object:nil];
    
    if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(videoDidStart:) name:@"UIMoviePlayerControllerDidEnterFullscreenNotification" object:nil];
    }
    
    // set automatic mode at start
    BOOL bAutomaticScreenModeWasEnabled = [[NSUserDefaults standardUserDefaults] boolForKey:kScreenModeAutomatic];
    if (bAutomaticScreenModeWasEnabled == YES) {
        _screenModeAutomatic = YES;
    } else {
        _screenModeAutomatic = NO;
    }
    
    // set textfield width
    
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.rightBarButtonItem = nil;
    
    [[ChromecastDeviceController sharedInstance] manageViewController:self icon:YES toolbar:NO];
    
    CGRect newFrame = self.navigationItem.titleView.frame;
    newFrame.size.width = CGRectGetWidth(self.view.bounds);
    
    self.navigationItem.titleView.frame = newFrame;
}

- (void) viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemTimeJumpedNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ActivityLink2" object:nil];
    
    if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"UIMoviePlayerControllerDidEnterFullscreenNotification" object:nil];
    }
    
    [self unregisterSpecWindowNotifications];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    NSLog(@"\n%s\nMemory warning\n",__FUNCTION__);
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
}

#pragma mark - State Preservation and Restoration
- (void)encodeRestorableStateWithCoder:(NSCoder *)coder
{
    [super encodeRestorableStateWithCoder:coder];
    
}

- (void)decodeRestorableStateWithCoder:(NSCoder *)coder
{
    [super decodeRestorableStateWithCoder:coder];
}

#pragma mark - Orientation
- (void)orientInferface:(UIInterfaceOrientation)newOrientation duration:(NSTimeInterval)duration
{
//    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
//        if (duration == 0.0) {
//            self.automaticallyAdjustsScrollViewInsets = NO;
//        }
//        [self setNeedsStatusBarAppearanceUpdate];
//        
//        if (self.textField.isEditing && !UIInterfaceOrientationIsPortrait(newOrientation)) {
//            [self.textField resignFirstResponder];
//        }
//        if (UIInterfaceOrientationIsPortrait(newOrientation) && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
////            self.navigationBar.hidden = NO;
////            [self.navigationController setNavigationBarHidden:NO animated:YES];
//        }
//    }
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
    if (_fullScreenModeEnabled) {
        [self.tabBarController resetHiddenStatus];
    }
}

- (void)willTransitionToTraitCollection:(UITraitCollection *)newCollection withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [super willTransitionToTraitCollection:newCollection withTransitionCoordinator:coordinator];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    if (_fullScreenModeEnabled) {
        [self.tabBarController resetHiddenStatus];
        _constraintToolBarHeight.constant = 0;
    }
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration
{
    [self orientInferface:interfaceOrientation duration:duration];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    
    if (_fullScreenModeEnabled) {
        [self.tabBarController setTabBarHidden:YES animated:NO];
        _constraintToolBarHeight.constant = 0;
    }
}

// in iOS 8 and greater method didRotateFromInterfaceOrientation: is Deprecated
// this is manual way
- (void)orientationChanged:(NSNotification *)notification
{
    if (_fullScreenModeEnabled) {
        [self.tabBarController setTabBarHidden:YES animated:NO];
        _constraintToolBarHeight.constant = 0;
    }
}

#pragma mark - Special Window Notification
- (void)registerSpecWindowNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(specialWindowDidSendTapNotification:) name:kSpecialWindowTapNotification object:nil];
}

- (void)unregisterSpecWindowNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSpecialWindowTapNotification object:nil];
}

#pragma mark - Status Bar

- (BOOL)prefersStatusBarHidden {
    if (!UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation]) && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        return YES;
    } else {
        return NO;
    }
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation
{
    return UIStatusBarAnimationSlide;
}


#pragma mark - WebView Delegate

- (void)webviewUrlUpdate
{
    if (self.textField.editing) {
//        self.textField.text = self.webview.request.URL.absoluteString;
    }
}

- (void)webviewTitleUpdate {
    @try {
        if (!self.textField.editing) {
            self.textField.text = self.webview.title;
        }
    }
    @catch (NSException *exception) {
        NSLog(@"\n%s\nException:\n%@\n", __FUNCTION__, exception);
    }
    @finally {
        
    }
}

- (void)webviewDidStartLoad
{
    @try {
        self.shareItem.enabled = NO;
        self.reloadButton.hidden = YES;
        self.cancelLoadButton.hidden = NO;
        [self.progressView setProgress:0.0 animated:NO];
        [self.progressView setHidden:NO];
        [UIView animateWithDuration:0.2 animations:^{
            self.progressView.alpha = 1.0;
        }];
    }
    @catch (NSException *exception) {
        NSLog(@"\n%s\nException:\n%@\n", __FUNCTION__, exception);
    }
    @finally {
        
    }
}

- (void)webviewDidFinishLoad
{
    @try {
        self.shareItem.enabled = YES;
        [self.backItem setEnabled:self.webview.canGoBack];
        [self.forwardItem setEnabled:self.webview.canGoForward];
        
        if (!self.textField.editing) {
            self.reloadButton.hidden = NO;
            self.cancelLoadButton.hidden = YES;
        }
        
        [UIView animateWithDuration:0.2 animations:^{
            self.progressView.alpha = 0.0;
        } completion:^(BOOL finished) {
            self.progressView.hidden = YES;
        }];
        [self.progressView setProgress:0.0 animated:NO];
        
        [self checkContainsFile:self.webview.request.URL];
        
        NSURL *newURL = self.webview.request.URL;
        [DMHistory addNewHistoryWithURL:[newURL description] andTitle:self.webview.title];
        
        
        // check if convertion was made to .mp4
        if (_convert2mp3Download == NO) {
            BOOL bConvertSite = ([[self.webview.request.URL absoluteString] rangeOfString:@"convert2mp3.net"].location != NSNotFound);
            BOOL bFormatted = ([[self.webview.request.URL absoluteString] rangeOfString:@"format=mp4"].location != NSNotFound);
            
            if (bConvertSite && bFormatted) {
                
                _convert2mp3Download = YES;
            }
        } else {
            if (!([[self.webview.request.URL absoluteString] rangeOfString:@"convert2mp3.net"].location != NSNotFound)) {
                _convert2mp3Download = NO;
            }
        }
    }
    @catch (NSException *exception) {
        NSLog(@"\n%s\nException:\n%@\n", __FUNCTION__, exception);
    }
    @finally {
        
    }
}

- (void)webviewDidFailWithError:(NSString *)error
{
    @try {
        [self.backItem setEnabled:self.webview.canGoBack];
        [self.forwardItem setEnabled:self.webview.canGoForward];
        
        if (!self.textField.editing) {
            self.reloadButton.hidden = NO;
            self.cancelLoadButton.hidden = YES;
        }
        
        [UIView animateWithDuration:0.2 animations:^{
            self.progressView.alpha = 0.0;
        } completion:^(BOOL finished) {
            self.progressView.hidden = YES;
        }];
        [self.progressView setProgress:0.0 animated:NO];
    }
    @catch (NSException *exception) {
        NSLog(@"\n%s\nException:\n%@\n", __FUNCTION__, exception);
    }
    @finally {
        
    }
}

- (void)webviewProgressMade:(NSNumber *)progress
{
    @try {
        NSLog(@"%f", progress.floatValue);
        [self.progressView setProgress:progress.floatValue animated:NO];
    }
    @catch (NSException *exception) {
        NSLog(@"\n%s\nException:\n%@\n", __FUNCTION__, exception);
    }
    @finally {
        
    }
}

#pragma mark - Textfield Delegate

- (void)reloadUrl
{
    @try {
        [self.webview reload];
    }
    @catch (NSException *exception) {
        NSLog(@"\n%s\nException:\n%@\n", __FUNCTION__, exception);
    }
    @finally {
        
    }
}

- (void)cancelLoad
{
    @try {
        if (self.webview.isLoading) {
            [self.webview stopLoading];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"\n%s\nException:\n%@\n", __FUNCTION__, exception);
    }
    @finally {
        
    }
}

- (void)clearUrlField
{
    @try {
        if ([self.textField isEditing] == YES) {
            self.textField.text = @"";
        }
    }
    @catch (NSException *exception) {
        NSLog(@"\n%s\nException:\n%@\n", __FUNCTION__, exception);
    }
    @finally {
        
    }
}

- (void)resignKeyboard
{
    @try {
        [self.textField resignFirstResponder];
    }
    @catch (NSException *exception) {
        NSLog(@"\n%s\nException:\n%@\n", __FUNCTION__, exception);
    }
    @finally {
        
    }
}

// UnlockedLevels
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    @try {
        BOOL kk = [[NSUserDefaults standardUserDefaults] boolForKey:@"gekauft"];
        if(kk == YES) {
            
            
            [DLIDEKeyboardView attachToTextView:textField];
            
            
        } else {
            
            BOOL kk1 = [[NSUserDefaults standardUserDefaults] boolForKey:@"adblocktrial"];
            if(kk1 == NO) {
                
                
                BOOL kk = [[NSUserDefaults standardUserDefaults] boolForKey:@"UnlockedLevels"];
                if(kk == YES) {
                    
                    
                    [DLIDEKeyboardView attachToTextView:textField];
                    
                    
                }
            }
            
        }
        
//        self.textField.text = self.webview.request.URL.absoluteString;
        self.textField.text = [self.webview stringByEvaluatingJavaScriptFromString:@"window.location.href"];
        
        self.clearButton.hidden = NO;
        self.cancelLoadButton.hidden = YES;
        self.reloadButton.hidden = YES;
        
        self.cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.cancelButton setTitle:NSLocalizedString(@"_cancel", @"") forState:UIControlStateNormal];
        [self.cancelButton setTitleColor:[UIColor colorWithRed:0.529f green:0.529f blue:0.529f alpha:1.000f] forState:UIControlStateHighlighted];
        self.cancelButton.titleLabel.adjustsFontSizeToFitWidth = YES;
        [self.cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.cancelButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
        self.cancelButton.frame = CGRectMake(self.navigationController.navigationBar.frame.size.width, self.navigationController.navigationBar.frame.size.height - 33, 90, 22);
        
        [self.cancelButton addTarget:self action:@selector(resignKeyboard) forControlEvents:UIControlEventTouchUpInside];
        
        //    [self.navigationController.navigationBar addSubview:self.cancelButton];
        
        
        [UIView animateWithDuration:0.3 animations:^{
            
            CGRect newFrame = self.navigationItem.titleView.frame;
            newFrame.size.width = 0.25f * CGRectGetWidth(self.view.bounds);
            self.cancelButton.frame = newFrame;
            self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.cancelButton];
            
            textField.textAlignment = NSTextAlignmentLeft;
            self.clearButton.alpha = 1.0;
        }];
    }
    @catch (NSException *exception) {
        NSLog(@"\n%s\nException:\n%@\n", __FUNCTION__, exception);
    }
    @finally {
        
    }
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    self.textField.text = self.webview.title;
    
    self.clearButton.hidden = YES;
    if (self.webview.isLoading) {
        self.cancelLoadButton.hidden = NO;
    } else {
        self.reloadButton.hidden = NO;
    }
    
    [UIView animateWithDuration:0.3 animations:^{
        
        CGRect newFrame = self.navigationItem.titleView.frame;
        newFrame.size.width = CGRectGetWidth(self.view.bounds);
        
        textField.textAlignment = NSTextAlignmentCenter;
        
        self.navigationItem.titleView.frame = newFrame;

        self.navigationItem.rightBarButtonItem = nil;
        
        [[ChromecastDeviceController sharedInstance] manageViewController:self icon:YES toolbar:NO];
    }];
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
//    [self.webview loadUrlorGoogle:textField.text];
    [self loadUrl:textField.text];
    [textField resignFirstResponder];
    
    return YES;
}

- (void)link1
{
    @try {
        if (self.webview.title) {
            
            NSString *title = self.webview.title;
            NSString *url = self.webview.request.URL.absoluteString;
            [DMBookmark addNewBookmarkWithTitle:title url:url];
            
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"bookmark", @"") message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [alertView show];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"\n%s\nException:\n%@\n", __FUNCTION__, exception);
    }
    @finally {
        
    }
}

- (void)link2
{
    @try {
//        if (self.webview.isLoading == NO) {
//        }
        NSString *currentURL = [self.webview stringByEvaluatingJavaScriptFromString:@"window.location.href"];
        NSString *thenewurl = [NSString stringWithFormat:@"http://convert2mp3.net/c-mp4.php?url=%@", [currentURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        
        [self loadUrl:thenewurl];
    }
    @catch (NSException *exception) {
        NSLog(@"\n%s\nException:\n%@\n", __FUNCTION__, exception);
    }
    @finally {
        
    }
}



- (void) checkContainsFile:(NSURL *)url
{
    @try {
        NSString *filePath = url.description;
        if ( [filePath rangeOfString:@".jpg"].location != NSNotFound ||
            [filePath rangeOfString:@".png"].location != NSNotFound ||
            [filePath rangeOfString:@".bmp"].location != NSNotFound ) {
            [self showDownloadActionSheetWithURL:url withType:DirectoryImage];
        } else if ( [filePath rangeOfString:@".mid"].location != NSNotFound ||
                   [filePath rangeOfString:@".wav"].location != NSNotFound ||
                   [filePath rangeOfString:@".mp3"].location != NSNotFound ) {
            [self showDownloadActionSheetWithURL:url withType:DirectoryMusic];
        } else if ( [filePath rangeOfString:@".mp4"].location != NSNotFound ||
                   [filePath rangeOfString:@".3gp"].location != NSNotFound ||
                   [filePath rangeOfString:@".3gp"].location != NSNotFound ) {
            [self showDownloadActionSheetWithURL:url withType:DirectoryVideo];
        } else if ( [filePath rangeOfString:@".pdf"].location != NSNotFound ) {
            [self showDownloadActionSheetWithURL:url withType:DirectoryPDF];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"\n%s\nException:\n%@\n", __FUNCTION__, exception);
    }
    @finally {
        
    }
}

- (void)loadUrl:(NSString*)urlString
{
    [self.webview loadUrlorGoogle:urlString];
}

- (void)loadBookmark:(NSString*)urlString
{
    [self loadUrl:urlString];
}

#pragma mark - Screen Mode Methods
- (void)didTapFullScreen:(UIButton *)sender
{
    if (!_screenModeAutomatic) {
        if (_fullScreenModeEnabled) {
            [self fullScreenEnable:NO];
            [sender setBackgroundImage:[UIImage imageNamed:@"Icon-FullScreen.png"] forState:UIControlStateNormal];
        } else {
            [self fullScreenEnable:YES];
            [sender setBackgroundImage:[UIImage imageNamed:@"Icon-NormalScreen.png"] forState:UIControlStateNormal];
        }
    }
}

- (void)fullScreenEnable:(BOOL)enable
{
    [[NSUserDefaults standardUserDefaults] setBool:enable forKey:kFullScreenMode];
    // perform action
    if (enable) {
        if (!_fullScreenModeEnabled) {
            _fullScreenModeEnabled = YES;
            [self.navigationController setNavigationBarHidden:YES animated:YES];
            
            _constraintToolBarHeight.constant = 0;
            [self.tabBarController setTabBarHidden:YES];
        }
    } else {
        if (_fullScreenModeEnabled) {
            _fullScreenModeEnabled = NO;
            [self.navigationController setNavigationBarHidden:NO animated:YES];
            
            _constraintToolBarHeight.constant = 44.0f;
            
            [self.tabBarController setTabBarHidden:NO];
        }
    }
}

#pragma mark - Toolbar Actions

- (IBAction)back
{
	if(self.webview.canGoBack)
		[self.webview goBack];
}

- (IBAction)forward
{
	if(self.webview.canGoForward)
		[self.webview goForward];
}

- (IBAction)bookmark
{
    BookmarksTableController *controller = [BookmarksTableController bookmarksControllerWithCategory:nil];
    
    UINavigationController* navController = [[UINavigationController alloc] initWithRootViewController:controller];
    navController.modalPresentationStyle = UIModalPresentationFormSheet;
    
    controller.delegate = self;
    [self presentViewController:navController animated:YES completion:nil];
}

- (IBAction)history
{
    HistoryViewController* historyController = [[HistoryViewController alloc] init];
    historyController.delegate = self;
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:historyController];
	[self presentViewController:nav animated:YES completion:NULL];
}

- (IBAction)home
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *thenewurl = [defaults objectForKey:@"thenewurl"];
    
    if ([thenewurl length] > 0) {
        [self loadUrl:thenewurl];
//        [self.webview loadUrlorGoogle:thenewurl];
    } else {
        [self loadUrl:NSLocalizedString(@"homepage", @"")];
//        [self.webview loadUrlorGoogle:NSLocalizedString(@"homepage", @"")];
    }
}

- (IBAction)info
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"_appname", @"") message:nil delegate:self cancelButtonTitle:NSLocalizedString(@"ok", @"")  otherButtonTitles:nil];
    
    [alert addButtonWithTitle:NSLocalizedString(@"bedienung", @"")];
    [alert addButtonWithTitle:NSLocalizedString(@"werbungmelden", @"")];
    [alert addButtonWithTitle:NSLocalizedString(@"ladefreundeein", @"")];
    
    BOOL kk = [[NSUserDefaults standardUserDefaults] boolForKey:@"gekauft"];
    if(kk == NO) {
        NSLog(@"keine pro");

                    BOOL kk5 = [[NSUserDefaults standardUserDefaults] boolForKey:@"adblocktrial"];
                    if(kk5 == NO)   {
                        NSLog(@"keine abgelaufene trial");
        
                                        BOOL kk = [[NSUserDefaults standardUserDefaults] boolForKey:@"UnlockedLevels"];
                                        if(kk == NO) {
                                            
                                            NSLog(@"keine UnlockedLevels");

                                                        [alert addButtonWithTitle:NSLocalizedString(@"bewerten", @"")];
                                                    }
                                    }
        
                            else  {
                                NSLog(@"eine trial");

                                
                                    }
        
                 }
    
    [alert addButtonWithTitle:NSLocalizedString(@"musthave", @"")];
    [alert addButtonWithTitle:NSLocalizedString(@"facebook", @"")];
    [alert show];
}

- (IBAction)share
{
    @try {
        if (self.webview.request.URL) {
            NSArray *activityItems = @[self.webview.request.URL];
            
            OpenFacebookPage *page1, *page2, *page3;
            
            page1 = [[OpenFacebookPage alloc] init];
            page1.nType = 0;
            page2 = [[OpenFacebookPage alloc] init];
            page2.nType = 1;
            page3 = [[OpenFacebookPage alloc] init];
            page3.nType = 2;
            
            UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:@[page1, page2, page3]];
            
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
                [activityVC.view setTintColor:[UIColor blackColor]];                
            }
            
            [self presentViewController:activityVC animated:YES completion:nil];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"\n%s\nException:\n%@\n", __FUNCTION__, exception);
    }
    @finally {
        
    }
}

- (void) showDownloadActionSheetWithURL:(NSURL *)url withType:(DirectoryType)type
{
    @try {
        NSUserDefaults *appSettings = [NSUserDefaults standardUserDefaults];
        if (![appSettings boolForKey:@"switch7"])
            return;
        
        if ( [m_selectedURL.absoluteString isEqualToString:url.absoluteString] && m_directoryType == type )
            return;
        
        m_selectedURL = url;
        m_directoryType = type;
        
        UIActionSheet* actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Document_Link_Found", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"_cancel", @"") destructiveButtonTitle:NSLocalizedString(@"_Free_Download", @"") otherButtonTitles: nil, nil];
        
        if (type == DirectoryVideo && !([[url absoluteString] rangeOfString:@"convert2mp3.net"].location != NSNotFound)) {
            [actionSheet addButtonWithTitle:kShowInLocalPlayer];
        }
        
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            [actionSheet showInView:self.view];
        } else {
//            [actionSheet showFromRect:self.view.frame inView:self.view animated:YES];
            
            [actionSheet showFromTabBar:self.tabBarController.tabBar];
            //        [actionSheet showFromTabBar:self.tabBarController.tabBar];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"\n%s\nException:\n%@\n", __FUNCTION__, exception);
    }
    @finally {
        
    }
}



#pragma mark - Video Player Manager Delegate
- (void)shouldOpenVideoInLocalPlayer
{
    NSURL *urlCheck = _videoSourceURL;
    _videoSourceURL = nil;
    [self back];
    if ([[urlCheck absoluteString] rangeOfString:@"youtube.com"].location != NSNotFound) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self loadUrl:[urlCheck absoluteString]];
        });
    }
    if (IDIOM == IPAD) {
        [[SNVideoPlayerManager sharedManager] startLocalPlaybackForVideoWithURL:_videoURLForLocalPlayer fromURL:urlCheck];
    }
    else {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [[SNVideoPlayerManager sharedManager] startLocalPlaybackForVideoWithURL:_videoURLForLocalPlayer fromURL:urlCheck];
        });        
    }
}

- (NSArray *)forbiddenSitesList
{
    return _aSiteExceptions;
}

- (void)customPlayerControllerWillBeShown
{
    // reset tabBar hidden status to avoid collision
    if (_fullScreenModeEnabled) {
        [self fullScreenEnable:NO];
    }
}

#pragma mark - Chrome Cast Controller Delegate
- (void)didDiscoverDeviceOnNetwork
{
    if (!self.navigationItem.rightBarButtonItem) {
        [[ChromecastDeviceController sharedInstance] manageViewController:self icon:YES toolbar:NO];
    }
}

#pragma mark - UIActionSheetDelegate

- (void) actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if ( buttonIndex == 0 ) {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"_filename", @"") message:nil delegate:self cancelButtonTitle:NSLocalizedString(@"_cancel", @"") otherButtonTitles:NSLocalizedString(@"_Free_Download", @"")
                            , nil];
        alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
        alertView.tag = 11;
        [alertView show];
        
    } else if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:kShowInLocalPlayer]) {
        
        NSLog(@"\n\n%s\nBack and SHOW: %@\n", __PRETTY_FUNCTION__, _videoURLForLocalPlayer);
        
        [[SNVideoPlayerManager sharedManager] setDelegate:self];
        [[SNVideoPlayerManager sharedManager] startLocalPlaybackForVideoWithURL:nil fromURL:self.webview.request.URL];
        
    }
    else {
        m_selectedURL = nil;
    }
}

#pragma mark - Scroll View Delegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if (_screenModeAutomatic == YES) {
        if (![scrollView isKindOfClass:[UICollectionView class]]) {
            CGFloat yVelocity = [scrollView.panGestureRecognizer velocityInView:self.webview].y;
            if (yVelocity > 0) {
                // dragging down
                if (_fullScreenModeEnabled) {
                    [self fullScreenEnable:NO];
                }
            } else if (yVelocity < 0){
                // dragging up
                if (!_fullScreenModeEnabled) {
                    [self fullScreenEnable:YES];
                }
            }
        }
    }
}

#pragma mark - UIAlertViewDelegate

- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
	NSString *buttonTitle = [alertView buttonTitleAtIndex:buttonIndex];
	if (alertView.tag == 11) {
		if ([buttonTitle isEqualToString:NSLocalizedString(@"_Free_Download", nil)]) {
			NSString *fileName = [alertView textFieldAtIndex:0].text;
			NSString *filePath = [DirectoryManager filePathWithType:m_directoryType fileName:fileName];
			
            [[NSNotificationCenter defaultCenter] postNotificationName:@"download" object:nil userInfo:@{@"url": m_selectedURL, @"path": filePath}];
		}
        m_selectedURL = nil;
	}
    
    if (alertView.tag == kShowInLocalPlayerAlertViewTag) {
        if (buttonIndex != alertView.cancelButtonIndex) {
            [[SNVideoPlayerManager sharedManager] setDelegate:self];
            [[SNVideoPlayerManager sharedManager] startLocalPlaybackForVideoWithURL:nil fromURL:_videoSourceURL];
        }
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
   if([title isEqualToString:NSLocalizedString(@"bedienung", @"")]) {
        
        NSString *thenewurl = [NSString stringWithFormat:NSLocalizedString(@"homepage", @"")];
        
        if ([thenewurl length] > 0) {
            [self loadUrl:NSLocalizedString(@"homepage", @"")];
        }
       
   } else if ([title isEqualToString:NSLocalizedString(@"werbungmelden", @"")]) {
       
       NSString* body = [NSString stringWithFormat:NSLocalizedString(@"_emailbody", nil),  [[UIDevice currentDevice] model],[[UIDevice currentDevice] systemVersion], [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];
       BOOL canSendMail = [self sendMailSubject:NSLocalizedString(@"_feedback", nil) messageBody:body html:NO to:[NSArray arrayWithObject:@"iosvideodownloader@gmail.com"]];
       if (canSendMail == NO) {
           [self displayError:NSLocalizedString(@"_deviceCantSendMails", nil)];
       }
       
       
   }
   else if ([title isEqualToString:NSLocalizedString(@"facebook", @"")]) {
   
    NSURL *fanPageURL = [NSURL URLWithString:@"fb://profile/1414465062105060"];
    
    if (![[UIApplication sharedApplication] openURL: fanPageURL]) {
        //fanPageURL failed to open.  Open the website in Safari instead
        NSURL *webURL = [NSURL URLWithString:@"https://www.facebook.com/pages/iPhone-Video-Downloader-with-Adblock-for-iphone-and-ipad/1414465062105060"];
        [[UIApplication sharedApplication] openURL: webURL];
        }
    }
    
    
   else if ([title isEqualToString:NSLocalizedString(@"ladefreundeein", @"")]) {
       
       if ([MFMailComposeViewController canSendMail])
       {
           MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
           
           mailer.mailComposeDelegate = self;
           
           [mailer setSubject:NSLocalizedString(@"werbemail", @"")];
           
           NSArray *toRecipients = [NSArray arrayWithObjects:@"", @"", nil];
           [mailer setToRecipients:toRecipients];
           
           // UIImage *myImage = [UIImage imageNamed:@"iPhone-icon-w144.png"];
           //    NSData *imageData = UIImagePNGRepresentation(myImage);
           //    [mailer addAttachmentData:imageData mimeType:@"image/png" fileName:@"mobiletutsImage"];
           
           NSString *emailBody = NSLocalizedString(@"werbemailtext", @"");
           [mailer setMessageBody:emailBody isHTML:YES];
           mailer.modalPresentationStyle = UIModalPresentationPageSheet;
//           [self presentModalViewController:mailer animated:YES];
           [self presentViewController:mailer animated:YES completion:nil];
           
       }
       else
       {
           UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"_error1", @"")                                                            message:NSLocalizedString(@"_deviceCantSendMails", @"")                                                           delegate:nil
                                                 cancelButtonTitle:NSLocalizedString(@"ok", @"")
                                                 otherButtonTitles: nil];
           [alert show];
       }
       
       
       
       
   }
   
   
   
   else if([title isEqualToString:NSLocalizedString(@"bewerten", @"")]) {
        
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"_ratetounlock", @"")
                                                        message:NSLocalizedString(@"_ratetounlocktext", @"")
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"_cancel", @"")
                                              otherButtonTitles:nil];
        
        
        [alert addButtonWithTitle:NSLocalizedString(@"jetzt_bewerten", @"")];
        
        
        [alert show];
        
        
        
        
        
        }
    
    
    else if([title isEqualToString:NSLocalizedString(@"jetzt_bewerten", @"")]) {
        
        NSString *deviceType = [UIDevice currentDevice].model;
        
        if([deviceType isEqualToString:@"iPad"]) {
            // it's an iPad
            NSLog(@"Es ist ein ipad");
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:NSLocalizedString(@"ipad_app_id", @"")]];
            // private vault
            
            
            
        } else {        // it's an iPhone
            NSLog(@"Es ist kein ipad");
            
            
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:NSLocalizedString(@"eigene_app_id", @"")]];
            // // Data Counter
        }

        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"UnlockedLevels"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        

    }
    
    else if([title isEqualToString:NSLocalizedString(@"musthave", @"")]) {
       
       UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"musthave", @"") message:nil delegate:self cancelButtonTitle:NSLocalizedString(@"ok", @"") otherButtonTitles:nil];
       
       [alert addButtonWithTitle:NSLocalizedString(@"_DataCounter", @"")];
       [alert addButtonWithTitle:NSLocalizedString(@"_AdBlock", @"")];
       [alert addButtonWithTitle:NSLocalizedString(@"_private_vault", @"")];
       
       [alert show];
   } else if([title isEqualToString:NSLocalizedString(@"_DataCounter", @"")]) {
       
       
       NSString *deviceType = [UIDevice currentDevice].model;
       
       if([deviceType isEqualToString:@"iPad"]) {
           // it's an iPad
           NSLog(@"Es ist ein ipad");
           [[UIApplication sharedApplication] openURL:[NSURL URLWithString:NSLocalizedString(@"data_counter_app_id_ipad", @"")]];
           // private vault
           
           
           
       } else {        // it's an iPhone
           NSLog(@"Es ist kein ipad");
           
           
           
           [[UIApplication sharedApplication] openURL:[NSURL URLWithString:NSLocalizedString(@"data_counter_app_id", @"")]];
           // // Data Counter
       }
       
      
       
       
       
       
       
       
   } else if([title isEqualToString:NSLocalizedString(@"_AdBlock", @"")]) {
       
       [[UIApplication sharedApplication] openURL:[NSURL URLWithString:NSLocalizedString(@"adblocker_app_id", @"")]];
       
       
   } else if([title isEqualToString:NSLocalizedString(@"_private_vault", @"")]) {
       
       NSString *deviceType = [UIDevice currentDevice].model;
       
       if([deviceType isEqualToString:@"iPad"]) {
           // it's an iPad
           NSLog(@"Es ist ein ipad");
           [[UIApplication sharedApplication] openURL:[NSURL URLWithString:NSLocalizedString(@"photovault_app_id_ipad", @"")]];
           // private vault
           
           
           
       } else {        // it's an iPhone
           NSLog(@"Es ist kein ipad");
           
           
           
           [[UIApplication sharedApplication] openURL:[NSURL URLWithString:NSLocalizedString(@"photovault_app_id", @"")]];
           // private vault
       }

       
    
       
       
   }
}

- (void)displayError:(NSString*)message
{
	UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"_error1", nil) message:message delegate:nil cancelButtonTitle:NSLocalizedString(@"_ok", nil) otherButtonTitles:nil];
	[alertView show];
}

- (void)displayMessage:(NSString*)message
{
	UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message:message delegate:nil cancelButtonTitle:NSLocalizedString(@"_ok", nil) otherButtonTitles:nil];
	[alertView show];
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
	[self dismissViewControllerAnimated:YES completion:NULL];
	
	if (error) {
		[self displayError:[error localizedDescription]];
	}
}

- (BOOL)sendMailSubject:(NSString*)subject messageBody:(NSString*)message html:(BOOL)htmlMessage to:(NSArray*)receiver
{
	if ([MFMailComposeViewController canSendMail]) {
		MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
		mailViewController.mailComposeDelegate = self;
		[mailViewController setSubject:subject];
		[mailViewController setMessageBody:message isHTML:htmlMessage];
		[mailViewController setToRecipients:receiver];
		
		[self presentViewController:mailViewController animated:YES completion:^{}];
		return YES;
	} else {
		return NO;
	}
}

#pragma mark - UIScreenEdgePanGesture Recognizer
- (void)handleEdgePanGesture:(UIScreenEdgePanGestureRecognizer *)sender
{
    if (_fullScreenModeEnabled == YES) {
        switch (sender.state) {
            case UIGestureRecognizerStateEnded:
            {
                if ([sender velocityInView:sender.view].x > 0) {
                    
                    [self back];
                    
                } else {
                    
                    [self forward];
                }
            }
                break;
                
            default:
                break;
        }
    }
}

#pragma mark - UIGestureDelegate

- (BOOL) gestureRecognizer:(UIGestureRecognizer*) gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
	if ([gestureRecognizer isKindOfClass:[UILongPressGestureRecognizer class]])
        return YES;
    
    return NO;
}

- (void) longTap:(UIGestureRecognizer *)gesture
{
    @try {
        CGPoint touchPos = [gesture locationInView:gesture.view];
        
        NSString *jsToType = [NSString stringWithFormat:@"document.elementFromPoint(%f,%f).nodeName", touchPos.x, touchPos.y];
        NSString *nodeName = [self.webview stringByEvaluatingJavaScriptFromString:jsToType].uppercaseString;
        if ([nodeName isEqualToString:@"IMG"]) {
            
            NSString *jsToLink = [NSString stringWithFormat:@"document.elementFromPoint(%f,%f).src", touchPos.x, touchPos.y];
            NSString *src = [self.webview stringByEvaluatingJavaScriptFromString:jsToLink];
            if (!src || [src isEqualToString:@""])
                return;
            
            [self showDownloadActionSheetWithURL:[NSURL URLWithString:src] withType:DirectoryImage];
        } else if ([nodeName isEqualToString:@"VIDEO"]) {
            
            NSString *jsToLink = [NSString stringWithFormat:@"var d = document.elementFromPoint(%f, %f); var elems = d.getElementsByTagName('source'); elems[0].src", touchPos.x, touchPos.y];
            NSString *src = [self.webview stringByEvaluatingJavaScriptFromString:jsToLink];
            NSLog(@"%@", src);
            if ( !src || [src isEqualToString:@""] )
                return;
            
            [self showDownloadActionSheetWithURL:[NSURL URLWithString:src] withType:DirectoryVideo];
        } else if ( [nodeName isEqualToString:@"EMBED"] ) {
            NSString *jsToLink = [NSString stringWithFormat:@"var d = document.elementFromPoint(%f, %f); var elems = d.getElementsByTagName('source'); elems[0].src", touchPos.x, touchPos.y];
            NSString *src = [self.webview stringByEvaluatingJavaScriptFromString:jsToLink];
            if ( !src || [src isEqualToString:@""] )
                return;
            
            [self showDownloadActionSheetWithURL:[NSURL URLWithString:src] withType:DirectoryMusic];
        } else if ( [nodeName isEqualToString:@"OBJECT"] ) {
            NSString *jsToLink = [NSString stringWithFormat:@"document.elementFromPoint(%f, %f).data", touchPos.x, touchPos.y];
            NSString *src = [self.webview stringByEvaluatingJavaScriptFromString:jsToLink];
            if ( !src || [src isEqualToString:@""] )
                return;
            
            if ( [src rangeOfString:@".pdf"].location == NSNotFound ) {
                [self showDownloadActionSheetWithURL:[NSURL URLWithString:src] withType:DirectoryPDF];
            }
        }
    }
    @catch (NSException *exception) {
        NSLog(@"\n%s\nException:\n%@\n", __FUNCTION__, exception);
    }
    @finally {
        
    }
}

- (void)longGestureAutoScreenHandler:(UILongPressGestureRecognizer *)sender
{
    switch (sender.state) {
        case UIGestureRecognizerStateBegan:
        {
            _screenModeAutomatic = !_screenModeAutomatic;
            
            [[NSUserDefaults standardUserDefaults] setBool:_screenModeAutomatic forKey:kAutomaticScreenMode];
            
            UIImage *bkgImage = (_screenModeAutomatic) ? [UIImage imageNamed:@"Icon-AutomaticScreen.png"] :
            (_fullScreenModeEnabled) ? [UIImage imageNamed:@"Icon-NormalScreen.png"] : [UIImage imageNamed:@"Icon-FullScreen"];
            
            NSString *sMsg = (_screenModeAutomatic) ? NSLocalizedString(@"Screen mode switched to automatic", nil) : NSLocalizedString(@"Screen mode switched to manual", nil);
            [self.buttonScreenMode setBackgroundImage:bkgImage forState:UIControlStateNormal];
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil
                                                                message:sMsg
                                                               delegate:nil
                                                      cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                      otherButtonTitles:nil, nil];
            alertView.tintColor = [UIColor blackColor];
            [alertView show];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - Notification Handlers
#pragma mark Content Tap Notifications
- (void)specialWindowDidSendTapNotification:(NSNotification *)notification
{
    [self contextualMenuAction:notification];
}

- (void)contextualMenuAction:(NSNotification*)notification
{
    
    CGPoint pt;
    NSDictionary *coord = [notification object];
    pt.x = [[coord objectForKey:@"x"] floatValue];
    pt.y = [[coord objectForKey:@"y"] floatValue];
    
    // check if tap is realy on web view not full screen button
    CGPoint checkPoint = [self.buttonScreenMode convertPoint:pt fromView:self.view.window];
    if (CGRectContainsPoint(self.buttonScreenMode.bounds, checkPoint)) {
        return;
    }
    
    // convert point from window to view coordinate system
    pt = [self.webview convertPoint:pt fromView:self.view.window];
    
    // convert point from view to HTML coordinate system
    CGSize viewSize = [self.webview frame].size;
    CGSize windowSize = [self.webview windowSize];
    
    CGFloat f = windowSize.width / viewSize.width;
    pt.x = pt.x * f;
    pt.y = pt.y * f;
    
    [self openContextualMenuAt:pt forWebView:self.webview];
}

- (void)openContextualMenuAt:(CGPoint)pt forWebView:(UIWebView *)webView
{
    // Load the JavaScript code from the Resources and inject it into the web page
    NSString *path = [[NSBundle mainBundle] pathForResource:@"JSTools" ofType:@"js"];
    NSString *jsCode = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    [webView stringByEvaluatingJavaScriptFromString:jsCode];
    
    // get the Tags at the touch location
    NSString *tags = [webView stringByEvaluatingJavaScriptFromString:
                      [NSString stringWithFormat:@"MyAppGetHTMLElementsAtPoint(%zd,%zd);",(NSInteger)pt.x,(NSInteger)pt.y]];
    
    NSString *tagsHREF = [webView stringByEvaluatingJavaScriptFromString:
                          [NSString stringWithFormat:@"MyAppGetLinkHREFAtPoint(%zd,%zd);",(NSInteger)pt.x,(NSInteger)pt.y]];
    
//    NSString *tagsSRC = [webView stringByEvaluatingJavaScriptFromString:
//                         [NSString stringWithFormat:@"MyAppGetLinkSRCAtPoint(%zd,%zd);",(NSInteger)pt.x,(NSInteger)pt.y]];
    
    
    // convert2mp3.net site downloadability
    if ([webView.request.URL.absoluteString rangeOfString:@"convert2mp3.net"].location != NSNotFound) {
        if (tagsHREF.length) {
            
            if ([tagsHREF rangeOfString:@"download.php"].location != NSNotFound) {
                if (_convert2mp3Download) {
                    [self showDownloadActionSheetWithURL:[NSURL URLWithString:tagsHREF] withType:DirectoryVideo];
                }
            }
        }
    }
    
    NSRange videoStringRange = [tags rangeOfString:@"VIDEO"];
    if (videoStringRange.location != NSNotFound) {
        
//        NSLog(@"\n\n%s\nBack and SHOW: %@\n", __PRETTY_FUNCTION__, tagsSRC);
//        
//        
//        [SNVideoPlayerManager sharedManager].delegate = self;
//        _videoURLForLocalPlayer = [NSURL URLWithString:tagsSRC];
//        [[SNVideoPlayerManager sharedManager] startLocalPlaybackForVideoWithURL:nil fromURL:self.webview.request.URL];
    }
}

#pragma mark Audio/Video Notifications
- (void)audioDidStart:(NSNotification *)notification
{
    @try {
        
        if ([[SNVideoPlayerManager sharedManager] playerIsShown]) {
            return;
        }
        
        if ([[[AppDelegate sharedDelegate] topViewController] isKindOfClass:NSClassFromString(@"SNUniversalPlayerController")]) {
            return;
        }
        
        AVPlayerItem *playerItem = (AVPlayerItem *)[notification object];
        if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
            AVAsset *asset = playerItem.asset;
            for (AVAssetTrack *assetTrack in asset.tracks) {
                if (assetTrack.mediaType == AVMediaTypeVideo || [assetTrack.mediaType isEqualToString:@"vide"]) {
                    return;
                }
            }
        }
        if (playerItem.status != AVPlayerItemStatusUnknown) {
            //        NSLog(@"\n%s\nEvents:\n%@\n", __FUNCTION__, playerItem.accessLog.events);
            //
            //        for (AVPlayerItemAccessLogEvent *event in playerItem.accessLog.events) {
            //            NSLog(@"\nEvent:%@\nDuration: %f", event, event.durationWatched);
            //        }
            //
            return;
        }
        
        AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        if (delegate.musicPlayer) {
            [delegate.musicPlayer stopAudio];
        }
        NSString *curURLText = self.webview.request.URL.absoluteString;
        
        // check if site is in exception list
        BOOL bExceptionSite = NO;
        for (NSString *site in _aSiteExceptions) {
            if ([curURLText rangeOfString:site].location != NSNotFound) {
                bExceptionSite = YES;
                break;
            }
        }
        
        AVURLAsset *asset = (AVURLAsset *)playerItem.asset;
        
        NSString *urlString = asset.URL.description;
        NSString *extension = urlString.pathExtension;
        
        if ([_videoURLForLocalPlayer isEqual:asset.URL]) {
            return;
        }
        
        _videoURLForLocalPlayer = nil;
        if (asset.URL) {
            if ( [extension isEqualToString:@"mp3"]
                || [extension isEqualToString:@"wav"] ) {
                if (bExceptionSite) {
                    return;
                }
                [self showDownloadActionSheetWithURL:asset.URL withType:DirectoryMusic];
            } else {
                CGFloat fDuration = CMTimeGetSeconds(asset.duration);
                
                _videoURLForLocalPlayer = [asset URL];
                _videoSourceURL = [NSURL URLWithString:[self.webview stringByEvaluatingJavaScriptFromString:@"window.location.href"]];
                
                if (![[NSUserDefaults standardUserDefaults] boolForKey:kLocalPlayerDefaultUsage]) {
                    if (bExceptionSite) {
                        return;
                    }
                    [self showDownloadActionSheetWithURL:_videoURLForLocalPlayer withType:DirectoryVideo];
                } else {
                    if (fDuration < 60) {
                        NSLog(@"\n%s\nCan be an ad\n", __PRETTY_FUNCTION__);
                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Advertisement?", nil)
                                                                            message:NSLocalizedString(@"Show in local player?", nil)
                                                                           delegate:self
                                                                  cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                                                  otherButtonTitles:NSLocalizedString(@"Yes", nil), nil];
                        alertView.tag = kShowInLocalPlayerAlertViewTag;
                        [alertView show];
                        return;
                    }
                    [[SNVideoPlayerManager sharedManager] setDelegate:self];
                    [[SNVideoPlayerManager sharedManager] startLocalPlaybackForVideoWithURL:nil fromURL:_videoSourceURL];
                }
            }
            
            
        }
    }
    @catch (NSException *exception) {
        NSLog(@"\n%s\nException:\n%@\n", __FUNCTION__, exception);
    }
    @finally {
        
    }
}

- (void)videoDidStart:(NSNotification *)notification
{
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (delegate.musicPlayer) {
        [delegate.musicPlayer stopAudio];
    }
	
	NSString *curURLText = self.webview.request.URL.absoluteString;
    
    // check if site is in exception list
    BOOL bExceptionSite = NO;
    for (NSString *site in _aSiteExceptions) {
        if ([curURLText rangeOfString:site].location != NSNotFound) {
            bExceptionSite = YES;
            break;
        }
    }
    
	NSString *url = [self.webview stringByEvaluatingJavaScriptFromString:@"document.getElementsByTagName('video')[0].getAttribute('src')"];
	if ( url.length <= 0 )
		url = [self.webview stringByEvaluatingJavaScriptFromString:@"document.getElementsByTagName('embed')[0].getAttribute('src')"];
    
    NSMutableString *msUrl = [NSMutableString new];
    [msUrl appendFormat:@"%@://%@",self.webview.request.URL.scheme, self.webview.request.URL.host];
    [msUrl appendString:url];
    
    
    
    
    
	if(msUrl.length > 0) {
        _videoURLForLocalPlayer = [NSURL URLWithString:msUrl];
        
        AVPlayerItem *playerItem = [[AVPlayerItem alloc] initWithURL:_videoURLForLocalPlayer];
        AVURLAsset *asset = (AVURLAsset *)playerItem.asset;
        
        CGFloat fDuration = CMTimeGetSeconds(asset.duration);
        
        if (![[NSUserDefaults standardUserDefaults] boolForKey:kLocalPlayerDefaultUsage]) {
            if (bExceptionSite) {
                return;
            }
            [self showDownloadActionSheetWithURL:_videoURLForLocalPlayer withType:DirectoryVideo];
        } else {
            _videoSourceURL = self.webview.request.URL;
            if (fDuration < 45) {
                NSLog(@"\n%s\nCan be an ad\n", __PRETTY_FUNCTION__);
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Advertisement?", nil)
                                                                    message:NSLocalizedString(@"Show in local player?", nil)
                                                                   delegate:self
                                                          cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                                          otherButtonTitles:NSLocalizedString(@"Yes", nil), nil];
                alertView.tag = kShowInLocalPlayerAlertViewTag;
                [alertView show];
                return;
            }
            [[SNVideoPlayerManager sharedManager] setDelegate:self];
            [[SNVideoPlayerManager sharedManager] startLocalPlaybackForVideoWithURL:nil fromURL:_videoSourceURL];
        }
    }
}

@end

#pragma mark - Tab Bar Controller Hide Function

@implementation UITabBarController (HideTabBar)

- (void)setTabBarHidden:(BOOL)hidden
{
    [self setTabBarHidden:hidden animated:YES];
}

- (void)resetHiddenStatus
{
    if (self.tabBar.hidden) {
        [self setTabBarHidden:NO animated:NO];
    }
}

- (void)setTabBarHidden:(BOOL)hidden animated:(BOOL)animated
{
    @try {
        if (self.tabBar.hidden == hidden)
            return;
        
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        BOOL isLandscape = UIDeviceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation);
        float height;
        
        if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
            height = isLandscape ? screenSize.width : screenSize.height;
        } else {
            height = screenSize.height;
        }
        
        if (!hidden) {
            height -= CGRectGetHeight(self.tabBar.frame);
            self.tabBar.hidden = hidden;
        }
        
        void (^animations)();
        
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") && SYSTEM_VERSION_LESS_THAN(@"8.0")) {
            UIView *view = self.selectedViewController.view;
            
            animations = ^{
                
                CGRect frame = self.tabBar.frame;
                frame.origin.y = height;
                self.tabBar.frame = frame;
                
                frame = view.frame;
                CGFloat frameHeight = CGRectGetHeight(self.tabBar.frame);
                frame.size.height += (hidden ? 1.0f : -1.0f) * frameHeight;
                view.frame = frame;
            };
        } else if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            UIView *view = self.selectedViewController.view;
            
            animations = ^{
                
                CGRect frame = self.tabBar.frame;
                frame.origin.y = height;
                self.tabBar.frame = frame;
                
                frame = view.frame;
                CGFloat frameHeight = CGRectGetHeight(self.tabBar.frame);
                frame.size.height += (hidden ? 1.0f : -1.0f) * frameHeight;
                view.frame = frame;
                
            };
        } else {
            animations = ^{
                for (UIView *subview in self.view.subviews) {
                    CGRect frame = subview.frame;
                    
                    if (subview == self.tabBar) {
                        frame.origin.y = height;
                        
                    } else {
                        frame.size.height = height;
                    }
                    
                    subview.frame = frame;
                }
            };
        }
        
        if (animated) {
            
            [UIView animateWithDuration:0.25f animations:animations completion:^(BOOL finished) {
                self.tabBar.hidden = hidden;
            }];
            
        } else {
            animations();
            self.tabBar.hidden = hidden;
        }        
    }
    @catch (NSException *exception) {
        NSLog(@"\n%s\nException:\n%@\n", __FUNCTION__, exception);
    }
    @finally {
        
    }
}

@end
