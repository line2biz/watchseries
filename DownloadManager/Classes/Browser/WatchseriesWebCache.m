//
//  WatchseriesCache.m
//  DownloadManager
//
//  Created by Denis on 10.06.15.
//  Copyright (c) 2015 ComputerVerse Corporation. All rights reserved.
//

#import "WatchseriesWebCache.h"

#import "SNAdManager.h"

@implementation WatchseriesWebCache

- (NSCachedURLResponse*)checkResponseForRequest:(NSURLRequest *)request
{
    BOOL test2 = [[NSUserDefaults standardUserDefaults] boolForKey:@"switch2"];
    if(test2 == YES) {
        
        BOOL blockURL = NO;
        NSString *urlString = [request.URL description];
        
        NSInteger iTotalBlockedAds = [[NSUserDefaults standardUserDefaults] integerForKey:kBlockedAdsTotal];
        NSInteger iCurrentlyBlockedAds = [[NSUserDefaults standardUserDefaults] integerForKey:kBlockedAdsCurrent];
        
        NSInteger iForbiddenSitesAmount = [[SNAdManager sharedManager] amountOfForbiddenSitesInURLPath:urlString];
        if (iForbiddenSitesAmount > 0) {
            
            blockURL = YES;
            
            iTotalBlockedAds += iForbiddenSitesAmount;
            iCurrentlyBlockedAds += iForbiddenSitesAmount;
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setInteger:iTotalBlockedAds forKey:kBlockedAdsTotal];
            [defaults setInteger:iCurrentlyBlockedAds forKey:kBlockedAdsCurrent];
            [defaults synchronize];
        }
        
        if (blockURL) {
            NSURLResponse *response = [[NSURLResponse alloc] initWithURL:[request URL]
                                                                MIMEType:@"text/plain"
                                                   expectedContentLength:1
                                                        textEncodingName:nil];
            
            NSCachedURLResponse *cachedResponse = [[NSCachedURLResponse alloc] initWithResponse:response
                                                                                           data:[NSData dataWithBytes:" " length:1]];
            
            [super storeCachedResponse:cachedResponse forRequest:request];
        }
    }
    
    return [super cachedResponseForRequest:request];
}

- (NSCachedURLResponse*)cachedResponseForRequest:(NSURLRequest*)request
{
    return [self checkResponseForRequest:request];
}


- (void)getCachedResponseForDataTask:(NSURLSessionDataTask *)dataTask completionHandler:(void (^) (NSCachedURLResponse *cachedResponse))completionHandler
{
    [super getCachedResponseForDataTask:dataTask completionHandler:completionHandler];    
}

//- (void)storeCachedResponse:(NSCachedURLResponse *)cachedResponse forDataTask:(NSURLSessionDataTask *)dataTask
//{
//    DBGLog(@"res");
//}
//
//- (void)removeCachedResponseForDataTask:(NSURLSessionDataTask *)dataTask
//{
//    DBGLog(@"res");
//}







@end
