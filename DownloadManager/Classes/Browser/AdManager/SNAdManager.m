//
//  SNAdManager.m
//  DownloadManager
//
//  Created by Denis on 08.04.15.
//  Copyright (c) 2015 dssolutions. All rights reserved.
//

#import "SNAdManager.h"

#import "RNEncryptor.h"
#import "RNDecryptor.h"

#import "CommonCryptor.h"

#import "SNIPAddress.h"

#define kAdsNeedsToUpdate @"http://adm.vpnforios.com/api.php?action=get_last_browser_filter_update_time"
#define kAdsSourceURL @"http://adm.vpnforios.com/api.php?action=get_browser_filter_urls&crypt_key=zaqwsx"

NSString * const kPassword = @"PASSWORD";

// user defaults last update time stamp key
NSString * const kLastUpdateTimeStamp = @"kLastUpdateTimeStamp";

// key to the name of default secure dir
NSString * const kAdSecureDir = @"TempWatchSeriesDirectory";
NSString * const kAdSecureFileName = @"TempFile";
NSString * const kAdIPSecureFileName = @"TempFile(1)";

@interface SNAdManager ()

@property (assign, nonatomic) long long int newTimestamp;

@end

@implementation SNAdManager

#pragma mark - Shared Instance
/** Shared instance */
+ (instancetype)sharedManager
{
    static SNAdManager *object = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        object = [[SNAdManager alloc] init];
    });
    return object;
}

#pragma mark - Private Methods
- (void)saveIPsToFileFromString:(NSString *)IPStrings atDirURL:(NSURL *)dirURL
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    // file name
    NSString *sFileName = kAdIPSecureFileName;
    
    NSURL *fileURL = [dirURL URLByAppendingPathComponent:sFileName];
    if ([fileManager fileExistsAtPath:fileURL.path]) {
        NSError *errorRemoval = nil;
        BOOL itemRemoved = [fileManager removeItemAtURL:fileURL error:&errorRemoval];
        if (!itemRemoved) {
            DBGLog(@"IP-File Removal failed because of an error:\n%@\n", errorRemoval);
            return;
        }
    }
    
    // create Data
    NSData *data = [IPStrings dataUsingEncoding:NSUTF8StringEncoding];
    
    NSError *encryptionError = nil;
    NSData *encryptedData = [RNEncryptor encryptData:data
                                        withSettings:kRNCryptorAES256Settings
                                            password:kPassword
                                               error:&encryptionError];
    // check encryption error
    if (encryptionError) {
        DBGLog(@"IP-Data Encryption failed because of an error:\n%@\n", encryptionError);
        return;
    }
    
    // write data to file
    BOOL fileWritten = [encryptedData writeToURL:fileURL atomically:NO];
    if (!fileWritten) {
        DBGLog(@"IP-File wasn`t written");
    }
}

#pragma mark - Public Methods
/** checks if update of Ads list is needed */
- (void)needsToUpdateAdsListWithCompletion:(void(^)(BOOL updateNeeded))completion
{
    // create URL
    NSURL *requestURL = [NSURL URLWithString:kAdsNeedsToUpdate];
    
    // create URL Request
    NSURLRequest *request = [NSURLRequest requestWithURL:requestURL];
    
    // Send Request Asynchronously
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               // check if there is no connection error
                               if (connectionError) {
                                   DBGLog(@"Connection error:\n%@\n", connectionError);
                                   return;
                               }
                               
                               // try to serialize data with JSON
                               NSError *errorJSON = nil;
                               id resultObject = [NSJSONSerialization JSONObjectWithData:data
                                                                                 options:NSJSONReadingAllowFragments
                                                                                   error:&errorJSON];
                               // if it is not JSON
                               if (errorJSON) {
                                   
                                   // print string from data
                                   NSString *sData = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                   if (sData.length) {
                                       DBGLog(@"Not a JSON. String data:\n%@", sData);
                                   }
                                   
                                   return;
                               }
                               
                               // everything is OK
                               if ([resultObject isKindOfClass:[NSArray class]]) {
                                   // it is array
                                   NSArray *aResult = (NSArray *)resultObject;
                                   
                                   DBGLog(@"There is an array instead of dictionary:\n%@\n", aResult);
                               }
                               else if ([resultObject isKindOfClass:[NSDictionary class]]) {
                                   // it is dictionary
                                   NSDictionary *dictResult = (NSDictionary *)resultObject;
                                   
                                   long long int serverTimeStamp = [((NSNumber *)[dictResult valueForKeyPath:@"response.last_update"]) longLongValue];
                                   long long int oldTimeStamp = [((NSNumber *)[SNDefaults objectForKey:kLastUpdateTimeStamp]) longLongValue];
                                   if (!oldTimeStamp) {
                                       self.newTimestamp = serverTimeStamp;
                                       if (completion) {
                                           completion(YES);
                                       }
                                   }
                                   else {
                                       if (serverTimeStamp > oldTimeStamp) {
                                           self.newTimestamp = serverTimeStamp;
                                           if (completion) {
                                               completion(YES);
                                           }
                                       }
                                       else {
                                           if (completion) {
                                               completion(NO);
                                           }
                                       }
                                   }
                               }
                               else {
                                   DBGLog(@"Object class is:\n%@\n%@\n", NSStringFromClass([resultObject class]), resultObject);
                               }
                           }];
}

/** download sites list with completion */
- (void)downloadAdsListWithCompletion:(void(^)(NSError *error, NSArray *aSites))completion
{
    // create URL
    NSURL *requestURL = [NSURL URLWithString:kAdsSourceURL];
    
    // create URL Request
    NSURLRequest *request = [NSURLRequest requestWithURL:requestURL];
    
    // Send Request Asynchronously
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               
                               // check if there is no connection error
                               NSError *requestError = nil;
                               if (connectionError) {
                                   requestError = connectionError;
                                   if (completion) {
                                       completion(requestError, nil);
                                   }
                                   return;
                               }
                               
                               // check if there is data in response
                               if (!data) {
                                   if (connectionError) {
                                       requestError = [self errorInMethod:NSStringFromSelector(@selector(downloadAdsListWithCompletion:))
                                                                 withDesc:@"Data is nil"];
                                       if (completion) {
                                           completion(requestError, nil);
                                       }
                                       return;
                                   }
                               }
                               
                               // try to serialize data with JSON
                               NSError *errorJSON = nil;
                               id resultObject = [NSJSONSerialization JSONObjectWithData:data
                                                                                  options:NSJSONReadingAllowFragments
                                                                                    error:&errorJSON];
                               // if it is not JSON
                               if (errorJSON) {
                                   requestError = errorJSON;
                                   if (completion) {
                                       completion(requestError, nil);
                                   }
                                   
                                   // print string from data
                                   NSString *sData = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                   if (sData.length) {
                                       DBGLog(@"String data:\n%@", sData);
                                   }
                                   
                                   return;
                               }
                               
                               // everything is OK
                               [SNDefaults saveObject:[NSNumber numberWithLongLong:self.newTimestamp] forKey:kLastUpdateTimeStamp];
                               
                               NSArray *aResult;
                               if ([resultObject isKindOfClass:[NSArray class]]) {
                                   // it is array
                                   DBGLog(@"Object class is:\n%@\n%@\n", NSStringFromClass([resultObject class]), resultObject);
                                   return;
                               }
                               else if ([resultObject isKindOfClass:[NSDictionary class]]) {
                                   // it is dictionary
                                   NSDictionary *dictResult = (NSDictionary *)resultObject;
                                   
                                   NSString *sData = dictResult[@"data"];
                                   
                                   NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:sData options:0];
                                   
                                   NSError *errorJSON = nil;
                                   id resultObject2 = [NSJSONSerialization JSONObjectWithData:decodedData
                                                                                      options:NSJSONReadingAllowFragments
                                                                                        error:&errorJSON];
                                   // if it is not JSON
                                   if (errorJSON) {
                                       requestError = errorJSON;
                                       if (completion) {
                                           completion(requestError, nil);
                                       }
                                       
                                       // print string from data
                                       NSString *sData = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                       if (sData.length) {
                                           DBGLog(@"String data:\n%@", sData);
                                       }
                                       
                                       return;
                                   }
                                   
                                   // data successfully decrypted
                                   dictResult = (NSDictionary *)resultObject2;
                                   aResult = [dictResult valueForKeyPath:@"response.urls"];
                               }
                               else {
                                   DBGLog(@"Object class is:\n%@\n%@\n", NSStringFromClass([resultObject class]), resultObject);
                               }
                               
                               if (completion) {
                                   completion(requestError, aResult);
                               }
                           }];
}

/** save new data to work with */
- (void)saveRawDataToSecureContainerFromArray:(NSArray *)array
                               withCompletion:(void(^)(NSError *error))completion
{
    if ([array count]) {
        
        // create base string
        NSMutableString *msBase = [NSMutableString new];
        NSMutableString *msIps = [NSMutableString new];
        for (NSString *sURL in array) {
            if ([SNIPAddress isIP:sURL]) { // item is an IP
                if (sURL != [array lastObject]) {
                    [msIps appendFormat:@"%@\n", sURL];
                } else {
                    [msIps appendString:sURL];
                }
                continue;
            }
            // item is for DNS filter
            if (sURL != [array lastObject]) {
                [msBase appendFormat:@"%@\n", sURL];
            } else {
                [msBase appendString:sURL];
            }
        }
        
        // file manager
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        // documents directory URL
        NSArray *aURLs = [fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
        
        NSURL *documentsURL = [aURLs firstObject];
        
        // special directory
        NSString *sDirName = kAdSecureDir;
        
        NSURL *dirURL = [documentsURL URLByAppendingPathComponent:sDirName isDirectory:YES];
        if (![fileManager fileExistsAtPath:dirURL.path]) {
            NSError *errorDirCreation = nil;
            BOOL creationSucceded = [fileManager createDirectoryAtURL:dirURL
                                          withIntermediateDirectories:NO
                                                           attributes:nil
                                                                error:&errorDirCreation];
            if (!creationSucceded) {
                DBGLog(@"Directory creation failed because of an error:\n%@\n", errorDirCreation);
                if (completion) {
                    completion(errorDirCreation);
                }
                return;
            }
        }
        
        [self saveIPsToFileFromString:msIps atDirURL:dirURL];
        
        // file name
        NSString *sFileName = kAdSecureFileName;
        
        NSURL *fileURL = [dirURL URLByAppendingPathComponent:sFileName];
        if ([fileManager fileExistsAtPath:fileURL.path]) {
            NSError *errorRemoval = nil;
            BOOL itemRemoved = [fileManager removeItemAtURL:fileURL error:&errorRemoval];
            if (!itemRemoved) {
                DBGLog(@"File Removal failed because of an error:\n%@\n", errorRemoval);
                if (completion) {
                    completion(errorRemoval);
                }
                return;
            }
        }
        
        // create Data
        NSData *data = [msBase dataUsingEncoding:NSUTF8StringEncoding];
        
        NSError *encryptionError = nil;
        NSData *encryptedData = [RNEncryptor encryptData:data
                                            withSettings:kRNCryptorAES256Settings
                                                password:kPassword
                                                   error:&encryptionError];
        // check encryption error
        if (encryptionError) {
            DBGLog(@"Data Encryption failed because of an error:\n%@\n", encryptionError);
            if (completion) {
                completion(encryptionError);
            }
            return;
        }
        
        // write data to file
        BOOL fileWritten = [encryptedData writeToURL:fileURL atomically:NO];
        if (!fileWritten) {
            DBGLog(@"File wasn`t written");
        }
        else {
            if (completion) {
                completion(nil);
            }
        }        
    }
}


- (NSInteger)amountOfForbiddenSitesInURLPath:(NSString *)site
{
    NSMutableArray *maFoundSites = [NSMutableArray new];
    
    NSArray *aSitesList = [self adSitesList];
    for (NSString *line in aSitesList) {
        NSRange rangeOfLine = [site rangeOfString:line];
        if (rangeOfLine.location != NSNotFound) {
            [maFoundSites addObject:line];
        }
    }
    if (maFoundSites.count) {
        NSLog(@"\n\n%s\nFound forbidden sites at path:\n%@:\n%@\n", __PRETTY_FUNCTION__, site, maFoundSites);
    }
    
    return maFoundSites.count;
}

- (BOOL)isForbiddenIPs:(NSArray *)IPAddresses
{
    // file manager
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    // documents directory URL
    NSArray *aURLs = [fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
    
    NSURL *documentsURL = [aURLs firstObject];
    
    // special directory
    NSString *sDirName = kAdSecureDir;
    // file name
    NSString *sFileName = kAdIPSecureFileName;
    
    NSURL *fileURL = [[documentsURL URLByAppendingPathComponent:sDirName isDirectory:YES] URLByAppendingPathComponent:sFileName];
    if ([fileManager fileExistsAtPath:fileURL.path]) {
        
        NSData *data = [NSData dataWithContentsOfURL:fileURL];
        
        NSError *decryptionError = nil;
        NSData *decryptedData = [RNDecryptor decryptData:data
                                            withPassword:kPassword
                                                   error:&decryptionError];
        if (decryptionError) {
            DBGLog(@"IP-Decryption failed because of an error:\n%@\n", decryptionError);
        }
        
        NSString *sBase = [[NSString alloc] initWithData:decryptedData encoding:NSUTF8StringEncoding];
        NSArray *aAds = [sBase componentsSeparatedByString:@"\n"];
        
        NSArray *aForbiddenIPs = @[
                                   @"5.150.195.167", // adrotator.se
                                   @"5.150.195.169",
                                   @"80.252.188.229",
                                   @"80.252.188.228",
                                   @"108.168.157.83", // rhtag.com
                                   @"173.192.117.128", // clkdeals.com
                                   @"216.58.209.162",// google
                                   @"216.58.209.161",
                                   @"216.58.209.194",
                                   @"216.58.209.193",
                                   @"23.229.191.67",
                                   @"54.225.130.6",// fetch-web.revmob
                                   @"23.23.110.236",
                                   @"81.218.31.168", // creative.xtendmedia.com
                                   @"130.211.160.72", // s.m2pub.com
                                   @"130.211.142.217",
                                   @"23.251.146.66",
                                   @"130.211.162.218",
                                   @"130.211.182.66",
                                   @"130.211.114.199",
                                   @"130.211.161.111",
                                   @"130.211.153.252",
                                   @"130.211.116.29",
                                   @"130.211.142.156"
                                   ];
        NSMutableArray *maAds = [NSMutableArray arrayWithCapacity:[aAds count]+[aForbiddenIPs count]];
        [maAds addObjectsFromArray:aAds];
        [maAds addObjectsFromArray:aForbiddenIPs];
        
        for (SNIPAddress *IPAddress in IPAddresses) {
            for (NSString *sIP in maAds) {
                SNIPAddress *checkIPAddress = [[SNIPAddress alloc] initWithStringIP:sIP];
                if (checkIPAddress) {
                    if ([checkIPAddress isEqual:IPAddress]) {
                        return YES;
                    }
                }
            }
        }
    }
    
    return NO;
}

#pragma mark - Error formation
- (NSError *)errorInMethod:(NSString *)method withDesc:(NSString *)description
{
    NSError *error = [NSError errorWithDomain:NSStringFromClass([self class])
                                         code:-1
                                     userInfo:@{
                                                @"point" : method,
                                                @"description" : description
                                                }];
    
    return error;
}

#pragma mark - Ads List
/** List of Ad sites
 * @result - List of Ad sites if they were saved, else nil
 */
- (NSArray *)adSitesList
{
    // file manager
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    // documents directory URL
    NSArray *aURLs = [fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
    
    NSURL *documentsURL = [aURLs firstObject];
    
    // special directory
    NSString *sDirName = kAdSecureDir;
    // file name
    NSString *sFileName = kAdSecureFileName;
    
    NSURL *fileURL = [[documentsURL URLByAppendingPathComponent:sDirName isDirectory:YES] URLByAppendingPathComponent:sFileName];
    if ([fileManager fileExistsAtPath:fileURL.path]) {
        
        NSData *data = [NSData dataWithContentsOfURL:fileURL];
        
        NSError *decryptionError = nil;
        NSData *decryptedData = [RNDecryptor decryptData:data
                                            withPassword:kPassword
                                                   error:&decryptionError];
        if (decryptionError) {
            DBGLog(@"Decryption failed because of an error:\n%@\n", decryptionError);
        }
        
        NSString *sBase = [[NSString alloc] initWithData:decryptedData encoding:NSUTF8StringEncoding];
        NSArray *aAds = [sBase componentsSeparatedByString:@"\n"];
        
        return aAds;
    }
    
    return @[];
}

@end
