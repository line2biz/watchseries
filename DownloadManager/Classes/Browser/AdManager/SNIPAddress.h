//
//  SNIPAddress.h
//  DownloadManager
//
//  Created by Denis on 19.05.15.
//  Copyright (c) 2015 Denis Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SNIPAddress : NSObject

@property (nonatomic, assign) NSUInteger p1;
@property (nonatomic, assign) NSUInteger p2;
@property (nonatomic, assign) NSUInteger p3;
@property (nonatomic, assign) NSUInteger p4;

/** Init object from string IP representation. */
- (instancetype)initWithStringIP:(NSString *)stringIP;

/** Init object from const char* IP representation. */
- (instancetype)initWithChar:(const char *)charIP;

/** Get String value of IP. */
- (NSString *)stringValue;

/** Get IP Array from URL. */
+ (NSArray *)IPsFromURL:(NSURL *)URLAddress;

/** If p4 is the only which differs, then addresses are similar. */
- (BOOL)isSimilarTo:(SNIPAddress *)IPAddress;

/** check if string is an IP */
+ (BOOL)isIP:(NSString *)stringIP;

@end
