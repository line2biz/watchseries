//
//  SNIPAddress.m
//  DownloadManager
//
//  Created by Denis on 19.05.15.
//  Copyright (c) 2015 Denis Inc. All rights reserved.
//

#import "SNIPAddress.h"

#include <netdb.h>
#include <arpa/inet.h>

@implementation SNIPAddress

#pragma mark - Life Cycle
/** Init object from string IP representation. */
- (instancetype)initWithStringIP:(NSString *)stringIP
{
    self = [super init];
    if (self) {
        if (![SNIPAddress isIP:stringIP]) {
            return nil;
        }
        [self commonInitWithString:stringIP];
    }
    return self;
}

/** Init object from const char* IP representation. */
- (instancetype)initWithChar:(const char *)charIP
{
    self = [super init];
    if (self) {
        NSString *stringIP = [NSString stringWithFormat:@"%s", charIP];
        if (![SNIPAddress isIP:stringIP]) {
            return nil;
        }
        [self commonInitWithString:stringIP];
    }
    return self;
}

- (void)commonInitWithString:(NSString *)stringIP
{
    NSArray *aComponents = [stringIP componentsSeparatedByString:@"."];
    
    _p1 = [self integerFromString:[aComponents objectAtIndex:0]];
    _p2 = [self integerFromString:[aComponents objectAtIndex:1]];
    _p3 = [self integerFromString:[aComponents objectAtIndex:2]];
    _p4 = [self integerFromString:[aComponents objectAtIndex:3]];
}

#pragma mark - Property Accessors
- (NSString *)description
{
    return [self stringValue];
}

- (BOOL)isEqual:(id)object
{
    if (![object isKindOfClass:[self class]] && ![object isKindOfClass:[NSString class]]) {
        return NO;
    }
    
    SNIPAddress *IPAddress;
    
    if ([object isKindOfClass:[NSString class]]) {
        IPAddress = [[SNIPAddress alloc] initWithStringIP:object];
    }
    else {
        IPAddress = (SNIPAddress *)object;
    }
    
    if (self.p1 != IPAddress.p1 || self.p2 != IPAddress.p2 || self.p3 != IPAddress.p3 || self.p4 != IPAddress.p4) {
        return NO;
    }
    return YES;
}

#pragma mark - Private Methods
- (NSUInteger)integerFromString:(NSString *)intStr
{
    return (NSUInteger)[intStr integerValue];
}

#pragma mark - Public Methods
- (NSString *)stringValue
{
    NSString *sValue = [NSString stringWithFormat:@"%zd.%zd.%zd.%zd", self.p1, self.p2, self.p3, self.p4];
    
    return sValue;
}

+ (NSArray *)IPsFromURL:(NSURL *)URLAddress
{
    NSString *sHost = [URLAddress host];
    if (!sHost) {
        return nil;
    }
    
    const char *newAddress = [sHost UTF8String];
    NSMutableArray *maIPs = [NSMutableArray new];
    
    struct hostent *host_entry = gethostbyname(newAddress);
    char *buff = nil;
    
    if (host_entry == NULL) {
        return nil;
    }
    
    for (NSInteger i=0; i < 100; i++) {
        char **pointer = host_entry->h_addr_list;
        char *nextP = pointer[i];
        if (!nextP) {
            break;
        }
        char *testBuff = inet_ntoa(*((struct in_addr *)nextP));
        if (i==0) {
            buff = inet_ntoa(*((struct in_addr *)host_entry->h_addr_list[i]));
        }
        
        SNIPAddress *IPAddress = [[SNIPAddress alloc] initWithChar:testBuff];
        if (![maIPs containsObject:IPAddress]) {
            [maIPs addObject:IPAddress];
        }
    }
    
    
    if ([maIPs count]) {
        return [maIPs mutableCopy];
    }
    
    return nil;
}

- (BOOL)isSimilarTo:(SNIPAddress *)IPAddress
{
    if (self.p1 != IPAddress.p1 || self.p2 != IPAddress.p2 || self.p3 != IPAddress.p3) {
        return NO;
    }
    return YES;
}

+ (BOOL)isIP:(NSString *)stringIP
{
    NSRange range = [stringIP rangeOfString:@"."];
    if (range.location == NSNotFound) {
        return NO;
    }
    
    NSArray *aComponents = [stringIP componentsSeparatedByString:@"."];
    if ([aComponents count] != 4) {
        return NO;
    }
    
    for (NSString *sPart in aComponents) {
        if ([sPart rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location != NSNotFound) {
            return NO;
        }
    }
    
    return YES;
}

@end
