//
//  SNAdManager.h
//  DownloadManager
//
//  Created by Denis on 08.04.15.
//  Copyright (c) 2015 dssolutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SNIPAddress;

/** This class manages the list of sites with Ads */
@interface SNAdManager : NSObject

/** Shared instance */
+ (instancetype)sharedManager;

/** List of Ad sites
 * @result - List of Ad sites if they were saved, else nil
 */
- (NSArray *)adSitesList;

/** Check if this site address contains an Ad. */
- (NSInteger)amountOfForbiddenSitesInURLPath:(NSString *)site;

/** Check if IP of responding server is forbidden (ad server) */
- (BOOL)isForbiddenIPs:(NSArray *)IPAddresses;

/** download sites list with completion */
- (void)downloadAdsListWithCompletion:(void(^)(NSError *error, NSArray *aSites))completion;

/** checks if update of Ads list is needed */
- (void)needsToUpdateAdsListWithCompletion:(void(^)(BOOL updateNeeded))completion;

/** save new data to work with */
- (void)saveRawDataToSecureContainerFromArray:(NSArray *)array
                               withCompletion:(void(^)(NSError *error))completion;

@end
