//
//  SNSpecialWindow.m
//  DownloadManager
//
//  Created by Denis on 27.03.15.
//  Copyright (c) 2015 ComputerVerse Corporation. All rights reserved.
//

#import "SNSpecialWindow.h"

@implementation SNSpecialWindow

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)tapAndHoldAction:(NSTimer*)timer
{
    contextualMenuTimer = nil;
    NSDictionary *coord = [NSDictionary dictionaryWithObjectsAndKeys:
                           [NSNumber numberWithFloat:tapLocation.x],@"x",
                           [NSNumber numberWithFloat:tapLocation.y],@"y",nil];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kSpecialWindowTapNotification object:coord];
}

- (void)sendEvent:(UIEvent *)event
{
    NSSet *touches = [event touchesForWindow:self];
    
    [super sendEvent:event];    // Call super to make sure the event is processed as usual
    
    if ([touches count] == 1) { // We're only interested in one-finger events
        UITouch *touch = [touches anyObject];
        
        UITouchPhase touchPhase = [touch phase];
        switch (touchPhase) {
            case UITouchPhaseBegan:  // A finger touched the screen
                tapLocation = [touch locationInView:self];
                [contextualMenuTimer invalidate];
                contextualMenuTimer = [NSTimer scheduledTimerWithTimeInterval:0.0
                                                                       target:self
                                                                     selector:@selector(tapAndHoldAction:)
                                                                     userInfo:nil
                                                                      repeats:NO];
                break;
                
            case UITouchPhaseEnded:
            case UITouchPhaseMoved:
            case UITouchPhaseCancelled:
                [contextualMenuTimer invalidate];
                contextualMenuTimer = nil;
                break;
            default:
                break;
        }
    } else {                    // Multiple fingers are touching the screen
        [contextualMenuTimer invalidate];
        contextualMenuTimer = nil;
    }
}

@end
