//
//  BrowserViewController.h
//  BrowserViewController
//
//  Created by Nils Weidl on 18.10.13.
//  Copyright (c) 2013 ComputerVerse Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>

#import "WebViewLoad.h"
#import "DirectoryManager.h"

#import "BookmarksTableController.h"

@interface BrowserViewController : UIViewController <UITextFieldDelegate, UIWebViewDelegate, UIGestureRecognizerDelegate, UIActionSheetDelegate, UIAlertViewDelegate, MFMailComposeViewControllerDelegate, WebViewDelegate, BookmarksTableControllerLoadProtocol> {
    
	NSURL *m_selectedURL;
	DirectoryType m_directoryType;
}

@property (nonatomic, weak) IBOutlet WebViewLoad *webview;

@property (nonatomic, strong) UIProgressView* progressView;
@property (nonatomic, weak) IBOutlet UIView* textFieldView;
@property (nonatomic, strong) UIButton* clearButton;
@property (nonatomic, strong) UITextField* textField;

@property (nonatomic, strong) UIButton* reloadButton;
@property (nonatomic, strong) UIButton* cancelLoadButton;
@property (nonatomic, strong) UIButton* cancelButton;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *homeItem;
@property (nonatomic, weak) IBOutlet UIBarButtonItem* backItem;
@property (nonatomic, weak) IBOutlet UIBarButtonItem* forwardItem;
@property (nonatomic, weak) IBOutlet UIBarButtonItem* shareItem;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *bookmarksItem;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *historyItem;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *infoItem;

@property (nonatomic, weak) IBOutlet UIToolbar* toolbar;

- (void)loadUrl:(NSString*)url;
- (void)loadBookmark:(NSString*)urlString;
- (IBAction)back;

@end


@interface UITabBarController (Additions)
- (void)resetHiddenStatus;
- (void)setTabBarHidden:(BOOL)hidden;
- (void)setTabBarHidden:(BOOL)hidden animated:(BOOL)animated;
@end
