//
//  SettingsBrightnessTableCell.m
//  DownloadManager
//
//  Created by Denis on 21.04.15.
//  Copyright (c) 2015 ComputerVerse Corporation. All rights reserved.
//

#import "SettingsBrightnessTableCell.h"

@interface SettingsBrightnessTableCell ()

@property (weak, nonatomic) IBOutlet UILabel *lableTitle;
@property (weak, nonatomic) IBOutlet UISlider *sliderBrightness;

@end

@implementation SettingsBrightnessTableCell

#pragma mark - Life Cycle
- (void)awakeFromNib
{
    // Initialization code
    self.lableTitle.text = NSLocalizedString(@"Brightness", nil);
    
    self.sliderBrightness.value = [[UIScreen mainScreen] brightness];
    
}

#pragma mark - Outlet Methods
- (IBAction)sliderValueChanged:(UISlider *)sender
{
    [[UIScreen mainScreen] setBrightness: [sender value]];
    
    [[NSUserDefaults standardUserDefaults] setFloat:sender.value forKey:@"helligkeit"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - Property Accessors
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
