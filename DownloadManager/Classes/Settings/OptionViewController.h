//
//  Created by Nils Weidl on 18.10.13.
//  Copyright (c) 2013 ComputerVerse Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

NSInteger counter;
NSInteger counter2;

@interface OptionViewController : UIViewController <UITextFieldDelegate> {
   
    IBOutlet UILabel *count;
    IBOutlet UITextField *mytextfield;
    IBOutlet UIScrollView *scrollview;
    IBOutlet UIView *contentview;
    IBOutlet UISwitch *mySwitch;
    IBOutlet UISwitch *mySwitch2;
    IBOutlet UISwitch *mySwitch6;
    IBOutlet UISwitch *mySwitch7;
    IBOutlet UILabel *version;
    IBOutlet UILabel *count2;
    IBOutlet UILabel *label;
    IBOutlet UIButton *cookiebutton;
    IBOutlet UISlider *slider;
    IBOutlet UISwitch *soundschalter;
    
    IBOutlet UIButton *logo;
    IBOutlet UILabel *Soundtext;
    IBOutlet UILabel *Downlaoder;
    IBOutlet UILabel *AdBlock;
    IBOutlet UILabel *Homepage;    
    IBOutlet UILabel *PressReturn;    
    IBOutlet UILabel *Passcode;
    IBOutlet UILabel *Useragent;
    IBOutlet UILabel *Cookies;    
    IBOutlet UILabel *Brightness;
    IBOutlet UILabel *adsanzeige;
    
    
    IBOutlet UIButton *iap;
    IBOutlet UIButton *restore;
    IBOutlet UIButton *sperre;
    IBOutlet UIButton *sperre2;
    IBOutlet UIButton *buyButton;
    BOOL isPurchased;

    CGPoint svos;
}

@property (nonatomic, retain) IBOutlet UISlider *slider;
-(IBAction)deletecache;

-(IBAction)sendtext:(id)sender;
-(IBAction)removekeyboard:(id)sender;
-(IBAction)clearcookie;
-(IBAction)sliderValueChanged:(id) sender;

-(IBAction)facebook:(id)sender;
-(IBAction)twitter:(id)sender;

-(IBAction)purchaseProduct;
-(IBAction)restorePurchase;
-(IBAction)reset:(id) sender;
-(IBAction)reset2:(id) sender;
-(IBAction)kauf:(id) sender;

-(void)onRoff;
-(void)soundswitcher;
-(void)onRoff2;
-(void)onRoff6;
- (IBAction)onRoff7:(id)sender;
@end
