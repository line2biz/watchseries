//
//  SettingsSwitchTableCell.h
//  DownloadManager
//
//  Created by Denis on 21.04.15.
//  Copyright (c) 2015 ComputerVerse Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^SwitchCompletion)();

@interface SettingsSwitchTableCell : UITableViewCell

@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) UIImage *icon;
@property (strong, nonatomic) NSString *stateIdentifier;
@property (strong, nonatomic) SwitchCompletion completionBlock;

@end
