//
//  SettingsAdsTableCell.m
//  DownloadManager
//
//  Created by Denis on 21.04.15.
//  Copyright (c) 2015 ComputerVerse Corporation. All rights reserved.
//

#import "SettingsAdsTableCell.h"

@interface SettingsAdsTableCell ()

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelTotal;
@property (weak, nonatomic) IBOutlet UILabel *labelCurrent;

@end

@implementation SettingsAdsTableCell

#pragma mark - Life Cycle
- (void)awakeFromNib {
    // Initialization code
}

#pragma mark - Property Accessors
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setSTitle:(NSString *)sTitle
{
    _sTitle = sTitle;
    self.labelTitle.text = sTitle;
}

- (void)setITotalBlocked:(NSInteger)iTotalBlocked
{
    _iTotalBlocked = iTotalBlocked;
    
    self.labelTotal.text = [NSString stringWithFormat:NSLocalizedString(@"gesamt", nil), iTotalBlocked];
}

- (void)setICurrentlyBlocked:(NSInteger)iCurrentlyBlocked
{
    _iCurrentlyBlocked = iCurrentlyBlocked;
    
    self.labelCurrent.text = [NSString stringWithFormat:NSLocalizedString(@"aktuell", nil), iCurrentlyBlocked];
}

@end
