//
//  SettingsEditTableCell.m
//  DownloadManager
//
//  Created by Denis on 21.04.15.
//  Copyright (c) 2015 ComputerVerse Corporation. All rights reserved.
//

#import "SettingsEditTableCell.h"

#import "DLIDEKeyboardView.h"

@interface SettingsEditTableCell () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imageViewIcon;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UILabel *labelHelp;

@end

@implementation SettingsEditTableCell

#pragma mark - Life Cycle
- (void)awakeFromNib {
    // Initialization code
    
    NSString *sNewUrl = [[NSUserDefaults standardUserDefaults] objectForKey:@"thenewurl"];
    if (sNewUrl.length) {
        self.textField.text = sNewUrl;
    }
    
    self.textField.delegate = self;
}

#pragma mark - Text Field Delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [DLIDEKeyboardView attachToTextView:textField];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [_textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:textField.text forKey:@"thenewurl"];
    [defaults synchronize];
}

#pragma mark - Property Accessors
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setIcon:(UIImage *)icon
{
    _icon = icon;
    self.imageViewIcon.image = icon;
}

- (void)setTitle:(NSString *)title
{
    _title = title;
    
    self.labelTitle.text = title;
}

- (void)setPlaceholderText:(NSString *)placeholderText
{
    _placeholderText = placeholderText;
    
    self.textField.placeholder = placeholderText;
}

- (void)setHelpText:(NSString *)helpText
{
    _helpText = helpText;
    
    self.labelHelp.text = helpText;
}


@end
