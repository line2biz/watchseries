//
//  SettingTableController.m
//  DownloadManager
//
//  Created by Denis on 21.04.15.
//  Copyright (c) 2015 ComputerVerse Corporation. All rights reserved.
//

#import "SettingTableController.h"

// cells classes
#import "SettingsAdsTableCell.h"
#import "SettingsBrightnessTableCell.h"
#import "SettingsEditTableCell.h"
#import "SettingsKeyTableCell.h"
#import "SettingsPurchaseTableCell.h"
#import "SettingsSwitchTableCell.h"

// pass code
#import "KKPasscodeSettingsViewController.h"

// keychain
#import "KeychainItemWrapper.h"

// In-App Purchase
#import "SNAppPurchaseManager.h"

#import "SNDeviceSpaceManager.h"

// REPLACE THIS VALUE WITH YOUR OWN IN-APP PURCHASE PRODUCT ID.
#define SUB_PRODUCT_ID @"com.netzsuche.iphonevideodownloader.pro"

// cell types
typedef NS_ENUM(NSUInteger, SettingsCellType) {
    SettingsCellTypeAds = 1,
    SettingsCellTypeBrightness,
    SettingsCellTypeEdit,
    SettingsCellTypeKey,
    SettingsCellTypePurchase,
    SettingsCellTypeSwitch
};

// cells block types
typedef NS_ENUM(NSUInteger, CellBlockType) {
    CellBlockTypeNone,
    CellBlockTypeBottom, //sperre
    CellBlockTypeAll // sperre2
};

@interface SettingTableController () <SettingsKeyCellDelegate, UIAlertViewDelegate, SNAppPurchaseManagerDelegate>
{
    BOOL _productWasPurchased, _productWasRestored;
    NSString *_purchaseButtonTitle;
    
    // it becomes non nil after purchaseManagerProductsAvailable: delegate method
    SKProduct *_defaultProduct;
    
    UIButton *_buttonBlock;
}

@property (strong, nonatomic) NSArray *aDataSource;
@property (weak, nonatomic) IBOutlet UILabel *labelOffSite;
@property (weak, nonatomic) IBOutlet UILabel *labelAppVersion;
@property (weak, nonatomic) IBOutlet UILabel *labelDiskSpaceStatus;
@property (assign, nonatomic) CellBlockType blockType;

@end

@implementation SettingTableController

#pragma mark - Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationItem setTitle:NSLocalizedString(@"Settings", nil)];
    
    _purchaseButtonTitle = @"Connecting..";
    
    self.labelOffSite.text = @"iPhoneVideoDownloader.com";
    self.labelAppVersion.text = [NSString stringWithFormat:NSLocalizedString(@"_version", nil), [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];
    
    
    self.view.backgroundColor = [UIColor colorWithRed:0.176f green:0.176f blue:0.176f alpha:1.000f];
    
    [self createDataSource];
    
    [self blockCellsWithBlockType:CellBlockTypeBottom];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    uint64_t totalSpace = [SNDeviceSpaceManager diskSpaceTotal];
    uint64_t freeSpace = [SNDeviceSpaceManager diskSpaceFree];
    
    NSArray *URLs = [[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
    NSURL *docDirURL = [URLs firstObject];
    
    NSError *error = nil;
    NSArray *aContentsOfDirectory = [[NSFileManager defaultManager] contentsOfDirectoryAtURL:docDirURL
                                                                  includingPropertiesForKeys:[NSArray array]
                                                                                     options:0
                                                                                       error:&error];
    NSMutableArray *maClearedContent = [NSMutableArray new];
    for (NSURL *url in aContentsOfDirectory) {
        BOOL isSqlite = ([[url lastPathComponent] rangeOfString:@"sqlite"].location != NSNotFound);
        BOOL isDiskCache = ([[url lastPathComponent] rangeOfString:@"diskCache"].location != NSNotFound);
        BOOL isSecureDir = [[url lastPathComponent] isEqualToString:@"TempWatchSeriesDirectory"];
        if (isSqlite || isDiskCache || isSecureDir) {
            [maClearedContent addObject:url];
        }
    }
    aContentsOfDirectory = [NSArray arrayWithArray:(NSArray *)maClearedContent];
    
    uint64_t hiddenSize = 0;
    for (NSURL *url in aContentsOfDirectory) {
        hiddenSize += [SNDeviceSpaceManager sizeOfObjectAtURL:url];
    }
    
    
//    NSURL *downloadsDirURL = [docDirURL URLByAppendingPathComponent:@"Downloads" isDirectory:YES];
    
    uint64_t filesSize = [SNDeviceSpaceManager sizeOfObjectAtURL:docDirURL] - hiddenSize;
    
    NSString *sTotalSpace = [SNDeviceSpaceManager formattedSizeFromBytes:totalSpace];
    NSString *sFreeSpace = [SNDeviceSpaceManager formattedSizeFromBytes:freeSpace];
    NSString *sFileSize = [SNDeviceSpaceManager formattedSizeFromBytes:filesSize];
    
    self.labelDiskSpaceStatus.text = [NSString stringWithFormat:@"%@: %@\n%@: %@\n%@: %@", NSLocalizedString(@"Total Space", nil), sTotalSpace, NSLocalizedString(@"Media Files", nil), sFileSize, NSLocalizedString(@"Free Space", nil), sFreeSpace];
    
    // Listen to orientation changes.
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(deviceOrientationDidChange:)
                                                 name:UIDeviceOrientationDidChangeNotification
                                               object:nil];
    
    [self createDataSource];
    
    BOOL applicationIsPurchased = NO;
    applicationIsPurchased = [[NSUserDefaults standardUserDefaults] boolForKey:@"gekauft"];
    
    if (!applicationIsPurchased) {
        [[SNAppPurchaseManager defaultManager] setDelegate:self];
        [[SNAppPurchaseManager defaultManager] setSandboxMode:NO]; // should be NO if it`s not the test
        [[SNAppPurchaseManager defaultManager] setServerAdress:@"http://data.iosappmedia.com/api.php?action=validate_receipt"];
        [[SNAppPurchaseManager defaultManager] setProductID:SUB_PRODUCT_ID];
    }
    
    BOOL inapp = [[NSUserDefaults standardUserDefaults] boolForKey:@"gekauft"];
    if(inapp == YES) {
        NSLog(@"Pro gekauft!");
        self.blockType = CellBlockTypeNone;
    }
    else {
        
        NSLog(@"gekauft nein");
        
        
        KeychainItemWrapper *wrapper = [[KeychainItemWrapper alloc] initWithIdentifier:NSLocalizedString(@"iphonevideodownloader", @"") accessGroup:nil];
        NSString *key = [wrapper objectForKey:(__bridge id)(kSecValueData)];
        
        BOOL adblocktrial = [[NSUserDefaults standardUserDefaults] boolForKey:@"adblocktrial"];
        if(adblocktrial == YES || [key isEqualToString:@"YES"]) {
            NSLog(@"TRIAL-WERT GESETZT");
            
            self.blockType = CellBlockTypeAll;
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"iap_Hinweis", @"")
                                                            message:NSLocalizedString(@"iap_Hinweis_Text", @"")
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"Ok", @"")
                                                  otherButtonTitles:nil];
            
            [alert show];
        }
        
        else {
            
            NSLog(@"TRIAL-WERT Nicht gesetzt");
            
            BOOL inapp = [[NSUserDefaults standardUserDefaults] boolForKey:@"UnlockedLevels"];
            if(inapp == YES) {
                NSLog(@"UnlockedLevels: jab");
                
                self.blockType = CellBlockTypeNone;
            }
            
            else {
                NSLog(@"UnlockedLevels: nop");
                
                self.blockType = CellBlockTypeBottom;
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"iap_Hinweis", @"")
                                                                message:NSLocalizedString(@"iap_Hinweis_Text", @"")
                                                               delegate:self
                                                      cancelButtonTitle:NSLocalizedString(@"Ok", @"")
                                                      otherButtonTitles:nil];
                
                [alert show];
                
            }
        }
    }
}

#pragma mark - Status Bar
- (BOOL)prefersStatusBarHidden
{
    if (!UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation]) && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        return YES;
    } else {
        return NO;
    }
}

- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation
{
    return UIStatusBarAnimationSlide;
}

#pragma mark - Private Methods
- (void)createDataSource
{
    SettingsCellType firstType;
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"gekauft"]) {
        firstType = SettingsCellTypeSwitch;
    } else {
        firstType = SettingsCellTypePurchase;
    }
    
    NSArray *aTypesOfCells = @[
                               @(firstType),
                               @(SettingsCellTypeSwitch),
                               @(SettingsCellTypeSwitch),
                               @(SettingsCellTypeAds),
                               @(SettingsCellTypeEdit),
                               @(SettingsCellTypeKey),
                               @(SettingsCellTypeSwitch),
                               @(SettingsCellTypeSwitch),
                               @(SettingsCellTypeSwitch),
                               @(SettingsCellTypeSwitch),
                               @(SettingsCellTypeBrightness)
                               ];
    
    self.aDataSource = aTypesOfCells;
}

- (NSString *)cellIDforType:(SettingsCellType)type
{
    NSString *sCellID = nil;
    switch (type) {
        case SettingsCellTypeAds:
            sCellID = @"AdsCell";
            break;
        case SettingsCellTypeBrightness:
            sCellID = @"BrightnessCell";
            break;
        case SettingsCellTypeEdit:
            sCellID = @"EditCell";
            break;
        case SettingsCellTypeKey:
            sCellID = @"KeyCell";
            break;
        case SettingsCellTypePurchase:
            sCellID = @"PurchaseCell";
            break;
        case SettingsCellTypeSwitch:
            sCellID = @"SwitchCell";
            break;
            
        default:
            break;
    }
    return sCellID;
}

- (CGFloat)cellHeightForType:(SettingsCellType)type
{
    CGFloat fHeight = 44.0f;
    switch (type) {
        case SettingsCellTypeAds:
            fHeight = 66.0f;
            break;
        case SettingsCellTypeBrightness:
            fHeight = 50.0f;
            break;
        case SettingsCellTypeEdit:
            fHeight = 120.0f;
            break;
        case SettingsCellTypeKey:
            fHeight = 50.0f;
            break;
        case SettingsCellTypePurchase:
            fHeight = 50.0f;
            break;
        case SettingsCellTypeSwitch:
            fHeight = 44.0f;
            break;
            
        default:
            break;
    }
    return fHeight;
}

- (UITableViewCell *)cellAtIndexPath:(NSIndexPath *)indexPath forType:(SettingsCellType)type
{
    UITableViewCell *cell;
    UITableView *tableView = self.tableView;
    
    if (type == SettingsCellTypeSwitch) { // all switch cells are here
        
        SettingsSwitchTableCell *switchCell = [tableView dequeueReusableCellWithIdentifier:[self cellIDforType:SettingsCellTypeSwitch]
                                                                              forIndexPath:indexPath];
        
        switch (indexPath.row) {
            case 0: // start sound
            {
                switchCell.title = NSLocalizedString(@"Sound", nil);
                switchCell.icon = [UIImage imageNamed:@"alarmios7.png"];
                switchCell.stateIdentifier = @"startsound";
                switchCell.completionBlock = nil;
            }
                break;
            case 1: // downloader
            {
                switchCell.title = NSLocalizedString(@"Downloader", nil);
                switchCell.icon = [UIImage imageNamed:@"downloaderios7.png"];
                switchCell.stateIdentifier = @"switch7";
                switchCell.completionBlock = nil;
            }
                break;
            case 2: // adblock
            {
                switchCell.title = NSLocalizedString(@"AdBlock", nil);
                switchCell.icon = [UIImage imageNamed:@"adblockios7.png"];
                switchCell.stateIdentifier = @"switch2";
                switchCell.completionBlock = nil;
            }
                break;
            case 6: // user agent
            {
                switchCell.title = NSLocalizedString(@"Useragent", nil);
                switchCell.icon = [UIImage imageNamed:@"monitorios7.png"];
                switchCell.stateIdentifier = @"switch";
                switchCell.completionBlock = ^(){
                    UIAlertView *alertclose2 = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"aenderung", @"")
                                                                          message:Nil
                                                                         delegate:self
                                                                cancelButtonTitle:NSLocalizedString(@"Ok", @"")
                                                                otherButtonTitles:nil];
                    
                    [alertclose2 show];
                    
                    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"switch"]) {
                        
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"PC_Hinweis", @"")
                                                                        message:NSLocalizedString(@"PC_Hinweis_Text", @"")
                                                                       delegate:self
                                                              cancelButtonTitle:NSLocalizedString(@"Ok", @"")
                                                              otherButtonTitles:nil];
                        
                        [alert show];
                    }
                    
                };
            }
                break;
            case 7: // delete cookies on close
            {
                switchCell.title = NSLocalizedString(@"Cookies", nil);
                switchCell.icon = [UIImage imageNamed:@"achtungios7.png"];
                switchCell.stateIdentifier = @"switch6";
                switchCell.completionBlock = ^(){
                    UIAlertView *alertclose2 = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"aenderung", @"")
                                                                          message:Nil
                                                                         delegate:self
                                                                cancelButtonTitle:NSLocalizedString(@"Ok", @"")
                                                                otherButtonTitles:nil];
                    
                    [alertclose2 show];
                };
            }
                break;
            case 8: // auto screen mode
            {
                switchCell.title = NSLocalizedString(@"Auto screen mode", nil);
                switchCell.icon = [UIImage imageNamed:@"Icon-AutoScreen"];
                switchCell.stateIdentifier = kScreenModeAutomatic;
                switchCell.completionBlock = nil;
            }
                break;
            case 9: // local player for default
            {
                switchCell.title = NSLocalizedString(@"Use local player", nil);
                switchCell.icon = [UIImage imageNamed:@"Icon-CustomPlayer"];
                switchCell.stateIdentifier = kLocalPlayerDefaultUsage;
                switchCell.completionBlock = nil;
            }
                break;
                
                
            default:
                break;
        }
        
        cell = switchCell;
    }
    else {
        switch (type) {
            case SettingsCellTypeAds: // blocked ads
            {
                SettingsAdsTableCell *adCell = [tableView dequeueReusableCellWithIdentifier:[self cellIDforType:SettingsCellTypeAds]
                                                                               forIndexPath:indexPath];
                adCell.sTitle = [NSString stringWithFormat:@"%@", NSLocalizedString(@"adsanzeige", nil)];
                adCell.iTotalBlocked = [[NSUserDefaults standardUserDefaults] integerForKey:kBlockedAdsTotal];
                adCell.iCurrentlyBlocked = [[NSUserDefaults standardUserDefaults] integerForKey:kBlockedAdsCurrent];
                
                cell = adCell;
            }
                break;
            case SettingsCellTypeBrightness: // brightness
            {
                SettingsBrightnessTableCell *brightnessCell = [tableView dequeueReusableCellWithIdentifier:[self cellIDforType:SettingsCellTypeBrightness]
                                                                                              forIndexPath:indexPath];
                
                cell = brightnessCell;
            }
                break;
            case SettingsCellTypeEdit: // home page
            {
                SettingsEditTableCell *editCell = [tableView dequeueReusableCellWithIdentifier:[self cellIDforType:SettingsCellTypeEdit]
                                                                                  forIndexPath:indexPath];
                editCell.title = NSLocalizedString(@"Homepage", nil);
                editCell.icon = [UIImage imageNamed:@"homeios7.png"];
                editCell.placeholderText = NSLocalizedString(@"Input start URL", nil);
                editCell.helpText = NSLocalizedString(@"PressReturn", nil);
                
                cell = editCell;
            }
                break;
            case SettingsCellTypeKey: // secure
            {
                SettingsKeyTableCell *keyCell = [tableView dequeueReusableCellWithIdentifier:[self cellIDforType:SettingsCellTypeKey]
                                                                                forIndexPath:indexPath];
                
                UIImage *icon = [UIImage imageNamed:@"schlossios7.png"];
                [keyCell configureWithIcon:icon
                                     title:NSLocalizedString(@"Passcode", nil)
                                  delegate:self
                                  selector:@selector(didTapKeyButton:)];
                
                cell = keyCell;
            }
                break;
            case SettingsCellTypePurchase: // purchase
            {
                SettingsPurchaseTableCell *purchaseCell = [tableView dequeueReusableCellWithIdentifier:[self cellIDforType:SettingsCellTypePurchase]
                                                                                          forIndexPath:indexPath];
                purchaseCell.purchaseTitle = _purchaseButtonTitle;
                purchaseCell.restoreTitle = @"Restore";
                purchaseCell.delegate = self;
                purchaseCell.purchaseSelector = @selector(didtapPurchaseButton:);
                purchaseCell.restoreSelector = @selector(didTapRestoreButton:);
                
                cell = purchaseCell;
            }
                break;
                
            default:
                break;
        }
    }
    
    return cell;
}

- (void)blockCellsWithBlockType:(CellBlockType)type
{
    if (_buttonBlock) {
        [_buttonBlock removeFromSuperview];
    }
    
    
    
    CGFloat fWidth = CGRectGetWidth(self.view.bounds);
    CGFloat fStartY = CGRectGetHeight(self.tableView.tableHeaderView.bounds);
    CGFloat fHeight;
    
    CGFloat(^HeightFromStartEndAndSource)(NSInteger, NSInteger, NSArray *) = ^(NSInteger startIdx, NSInteger endIdx, NSArray *aSource) {
        CGFloat fRes = 0.0f;
        for (NSInteger i=startIdx; i < endIdx+1; i++) {
            SettingsCellType type = [[aSource objectAtIndex:i] integerValue];
            fRes += [self cellHeightForType:type];
        }
        return fRes;
    };
    
    switch (type) {
        case CellBlockTypeBottom:
        {
            fStartY += HeightFromStartEndAndSource(0,3,self.aDataSource);
            fHeight = HeightFromStartEndAndSource(4, [self.aDataSource count] - 1, self.aDataSource);
        }
            break;
        case CellBlockTypeAll:
        {
            fStartY += HeightFromStartEndAndSource(1,1,self.aDataSource);
            fHeight = HeightFromStartEndAndSource(1, [self.aDataSource count] - 1, self.aDataSource);
        }
            break;
        case CellBlockTypeNone:
        {
            return;
        }
            break;
        default:
            break;
    }
    
    _buttonBlock = [[UIButton alloc] initWithFrame:CGRectMake(0, fStartY, fWidth, fHeight)];
    [_buttonBlock setImage:[UIImage imageNamed:@"schloss-en.png"] forState:UIControlStateNormal];
    [_buttonBlock setBackgroundColor:[UIColor darkGrayColor]];
    [_buttonBlock setTintColor:[UIColor whiteColor]];
    [_buttonBlock setAlpha:0.4f];
    
    [self.tableView addSubview:_buttonBlock];
}

- (void)deviceOrientationDidChange:(NSNotification *)notification
{
    [self blockCellsWithBlockType:self.blockType];
}

#pragma mark Purchase Cell Methods
- (void)didtapPurchaseButton:(UIButton *)sender
{
    BOOL kk = [[NSUserDefaults standardUserDefaults] boolForKey:@"gekauft"];
    if(kk == NO) {
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:@"/Library/MobileSubstrate/DynamicLibraries/iap.dylib"]) {
            
            NSLog(@"iAP Cracker GEFUNDEN!!!!!!!!!!!!!!!!!!!!!!");
            
            sleep(2);
            
            UIAlertView *failedAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"purchasestopped2", @"")
                                                                  message:NSLocalizedString(@"purchasestoppedtext2", @"")
                                                                 delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", @"") otherButtonTitles:nil];
            [failedAlert show];
        }
        else if ([[NSFileManager defaultManager] fileExistsAtPath:@"/Applications/IAPFree.app"]) {
            
            NSLog(@"iAP Free GEFUNDEN!!!!!!!!!!!!!!!!!!!!!!");
            
            sleep(2);
            
            UIAlertView *failedAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"purchasestopped2", @"")
                                                                  message:NSLocalizedString(@"purchasestoppedtext2", @"")
                                                                 delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", @"") otherButtonTitles:nil];
            [failedAlert show];
        }
        else if ([[NSFileManager defaultManager] fileExistsAtPath:@"/Library/MobileSubstrate/DynamicLibraries/LocalIAPStore.dylib"]) {
            
            NSLog(@"LocallAPStore GEFUNDEN!!!!!!!!!!!!!!!!!!!!!!");
            
            sleep(2);
            
            UIAlertView *failedAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"purchasestopped2", @"")
                                                                  message:NSLocalizedString(@"purchasestoppedtext2", @"")
                                                                 delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", @"") otherButtonTitles:nil];
            [failedAlert show];
        }
        else {   // do something useful
            NSLog(@"keiner der drei iAP Cracker gefunden. guter junge! :)");
            
            // new code
            if (_defaultProduct) {
                [[SNAppPurchaseManager defaultManager] purchaseProduct:_defaultProduct];
            } else {
                [self showAlertViewWithErrorMessage:NSLocalizedString(@"Error must have occur. Purchase is not available.", nil)];
            }
        }
    }
}

- (void)didTapRestoreButton:(UIButton *)sender
{
    BOOL kk = [[NSUserDefaults standardUserDefaults] boolForKey:@"gekauft"];
    if(kk == YES) {
        
    }
    else {
        if ([[NSFileManager defaultManager] fileExistsAtPath:@"/Library/MobileSubstrate/DynamicLibraries/iap.dylib"]) {
            
            NSLog(@"iAP Cracker GEFUNDEN!!!!!!!!!!!!!!!!!!!!!!");
            
            sleep(2);
            UIAlertView *restoreAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"restoreissue", @"") message:NSLocalizedString(@"restoreissuetext", @"") delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", @"") otherButtonTitles:nil];
            [restoreAlert show];
        }
        else if ([[NSFileManager defaultManager] fileExistsAtPath:@"/Applications/IAPFree.app"]) {
            
            NSLog(@"iAP Free GEFUNDEN!!!!!!!!!!!!!!!!!!!!!!");
            
            sleep(2);
            UIAlertView *restoreAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"restoreissue", @"") message:NSLocalizedString(@"restoreissuetext", @"") delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", @"") otherButtonTitles:nil];
            [restoreAlert show];
        }
        else if ([[NSFileManager defaultManager] fileExistsAtPath:@"/Library/MobileSubstrate/DynamicLibraries/LocalIAPStore.dylib"]) {
            
            NSLog(@"LocallAPStore GEFUNDEN!!!!!!!!!!!!!!!!!!!!!!");
            
            
            sleep(2);
            UIAlertView *restoreAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"restoreissue", @"") message:NSLocalizedString(@"restoreissuetext", @"") delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", @"") otherButtonTitles:nil];
            [restoreAlert show];
            
        }
        else {
            // do something useful
            NSLog(@"keiner der drei iAP Cracker gefunden. guter junge! :)");
            
            // new code
            if (_defaultProduct) {
                [[SNAppPurchaseManager defaultManager] restorePurchase];
            }
            else {
                sleep(2);
                UIAlertView *restoreAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"restoreissue", @"") message:NSLocalizedString(@"restoreissuetext", @"") delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", @"") otherButtonTitles:nil];
                [restoreAlert show];
            }
        }
    }
}

#pragma mark Alert View Methods
- (void)showRequestAlertViewWithTag:(NSInteger)iTag
                              title:(NSString *)sTitle
                            message:(NSString *)sMessage
                          textField:(BOOL)hasTextField
                       cancelButton:(NSString *)sCancel
                      confirmButton:(NSString *)sOk
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:sTitle
                                                        message:sMessage
                                                       delegate:self
                                              cancelButtonTitle:sCancel
                                              otherButtonTitles:sOk, nil];
    alertView.tag = iTag;
    if (hasTextField == YES) {
        alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    }
    [alertView show];
}

- (void)showAlertViewWithErrorMessage:(NSString *)sMessage
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil
                                                        message:sMessage
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                              otherButtonTitles:nil, nil];
    [alertView show];
}

#pragma mark - Outlet Methods
- (IBAction)didTapLogo:(UIButton *)sender
{
    NSString *urlString = NSLocalizedString(@"homepage", nil);
    if([urlString rangeOfString:@"http://" options:NSCaseInsensitiveSearch].location == NSNotFound && [urlString rangeOfString:@"https://" options:NSCaseInsensitiveSearch].location == NSNotFound) {
        urlString = [NSString stringWithFormat:@"http://%@", urlString];
    }
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
}

- (IBAction)didTapFB:(UIButton *)sender
{
    NSURL *fanPageURL = [NSURL URLWithString:@"fb://profile/1414465062105060"];
    
    if (![[UIApplication sharedApplication] openURL: fanPageURL]) {
        //fanPageURL failed to open.  Open the website in Safari instead
        NSURL *webURL = [NSURL URLWithString:@"https://www.facebook.com/pages/iPhone-Video-Downloader-with-Adblock-for-iphone-and-ipad/1414465062105060"];
        [[UIApplication sharedApplication] openURL: webURL];
    }
}

- (IBAction)didTapTwitter:(UIButton *)sender
{
    NSURL *twitterURL = [NSURL URLWithString:@"twitter:///user?screen_name=iOSTubeDownload"];
    
    if (![[UIApplication sharedApplication] openURL: twitterURL]) {
        //twitterURL failed to open.  Open the website in Safari instead
        NSURL *webURL = [NSURL URLWithString:@"http://twitter.com/iOSTubeDownload"];
        [[UIApplication sharedApplication] openURL: webURL];
    }
}

- (IBAction)didTapdeleteCookies:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"cookiesloeschen", @"")
                                                    message:NSLocalizedString(@"cookiesloeschentext", @"")
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"abbrechen", @"")
                                          otherButtonTitles:nil];
    [alert addButtonWithTitle:NSLocalizedString(@"ja", @"")];
    
    [alert show];
}

- (IBAction)didTapEmptyCache:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"cacheloeschen", @"")
                                                    message:NSLocalizedString(@"cacheloeschentext", @"")
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"abbrechen", @"")
                                          otherButtonTitles:nil];
    [alert addButtonWithTitle:NSLocalizedString(@"jetztloeschen", @"")];
    
    [alert show];
}

#pragma mark Test Option Methods
- (IBAction)didTapTestBuy:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setBool:YES  forKey:@"gekauft"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self createDataSource];
    
    self.blockType = CellBlockTypeNone;
}
- (IBAction)didTapTestReset:(id)sender
{
    NSLog(@"reset!!!!!");
    
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:NSLocalizedString(@"timer", @"")];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"switch2"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"switch7"];
    [[NSUserDefaults standardUserDefaults] setBool:NO  forKey:@"UnlockedLevels"];
    [[NSUserDefaults standardUserDefaults] setBool:NO  forKey:@"gekauft"];
    [[NSUserDefaults standardUserDefaults] setBool:NO  forKey:@"adblocktrial"];
    [[NSUserDefaults standardUserDefaults] setBool:YES  forKey:@"FirstLaunch11"];
    [[NSUserDefaults standardUserDefaults] setBool:YES  forKey:@"FirstLaunch"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    NSLog(@"Cache gelöscht");
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:Nil forKey:@"thenewurl"];
    [defaults synchronize];
    
    KeychainItemWrapper *wrapper = [[KeychainItemWrapper alloc] initWithIdentifier:NSLocalizedString(@"iphonevideodownloader", @"") accessGroup:nil];
    
    NSString *key = [wrapper objectForKey:(__bridge id)(kSecValueData)];
    
    BOOL inapp = [[NSUserDefaults standardUserDefaults] boolForKey:@"adblocktrial"];
    if(inapp == NO || [key isEqualToString:@"NO"])
    {
        [wrapper setObject:@"NO" forKey:(__bridge id)(kSecValueData)];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    [self createDataSource];
    
    self.blockType = CellBlockTypeBottom;
}
- (IBAction)didTapTestNoPro:(id)sender
{
    NSLog(@"reset2!!!!!");
    [[NSUserDefaults standardUserDefaults] setBool:NO  forKey:@"gekauft"];
    [[NSUserDefaults standardUserDefaults] setBool:YES  forKey:@"adblocktrial"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self createDataSource];
    
    self.blockType = CellBlockTypeAll;
}

#pragma mark - Property Accessors
- (void)setADataSource:(NSArray *)aDataSource
{
    _aDataSource = [NSArray arrayWithArray:aDataSource];
    [self.tableView reloadData];
}

- (void)setBlockType:(CellBlockType)blockType
{
    _blockType = blockType;
    
    [self blockCellsWithBlockType:_blockType];
}

#pragma mark - Table View Data Source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.aDataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SettingsCellType type = [[self.aDataSource objectAtIndex:indexPath.row] integerValue];
    UITableViewCell *cell = [self cellAtIndexPath:indexPath forType:type];
    
    return cell;
}


#pragma mark - Table View Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SettingsCellType type = [[self.aDataSource objectAtIndex:indexPath.row] integerValue];
    
    return [self cellHeightForType:type];
}

#pragma mark - Settings Key Cell Delegate
- (void)didTapKeyButton:(UIButton *)sender
{
    KKPasscodeSettingsViewController* vc = [[KKPasscodeSettingsViewController alloc] initWithStyle:UITableViewStyleGrouped];
    
    UINavigationController* navController = [[UINavigationController alloc] initWithRootViewController:vc];
    
    [self presentViewController:navController animated:YES completion:nil];
}

#pragma mark - Alert View Delegate
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if([title isEqualToString:NSLocalizedString(@"ja", @"")])
    {
        //Clear All Cookies
        for(NSHTTPCookie *cookie in [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies]) {
            
            //if([[cookie domain] isEqualToString:someNSStringUrlDomain]) {
            
            [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:cookie];
            
        }
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"cookiesgeloeschen", @"")
                                                        message:@""
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"ok", @"")
                                              otherButtonTitles:nil];
        [alert show];
    }
    
    if([title isEqualToString:NSLocalizedString(@"jetztloeschen", @"")])
    {
        
        // Flush all cached data
        [[NSURLCache sharedURLCache] removeAllCachedResponses];
        NSLog(@"Cache gelöscht");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"cachegeloescht", @"")
                                                        message:@""
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"ok", @"")
                                              otherButtonTitles:nil];
        [alert show];
    }
}

#pragma mark - Purchase Manager Delegate
/// if there is an error, it must be processed
- (void)purchaseManagerDisplayErrorMessage:(NSString *)message
{
    [self showAlertViewWithErrorMessage:message];
}

/// reports about available products
- (void)purchaseManagerProductsAvailable:(NSArray *)productsAvailable
{
    // here we get the list of products available for purchase
    if ([productsAvailable count]) {
        _defaultProduct = (SKProduct *)[productsAvailable firstObject]; // we have only one product in this situation
        
        NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
        [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        [numberFormatter setLocale:_defaultProduct.priceLocale];
        NSString *formattedPrice = [numberFormatter stringFromNumber:_defaultProduct.price];
        _purchaseButtonTitle = [NSLocalizedString(@"upgradetopro", @"") stringByAppendingString:formattedPrice];
    
    } else {
        // Product is NOT available in the App Store, so notify user.
        _purchaseButtonTitle = NSLocalizedString(@"nichtverfuegbar", nil);
    }
    
    // change purchase cell title
    NSInteger purchaseCellIdx = -1;
    for (NSNumber *number in self.aDataSource) {
        SettingsCellType type = [number integerValue];
        if (type == SettingsCellTypePurchase) {
            purchaseCellIdx = [self.aDataSource indexOfObject:number];
            break;
        }
    }
    NSIndexPath *purchaseCellIndexPath = [NSIndexPath indexPathForRow:purchaseCellIdx inSection:0];
    [self.tableView reloadRowsAtIndexPaths:@[purchaseCellIndexPath] withRowAnimation:UITableViewRowAnimationFade];
}

// payment action results
- (void)purchaseManagerPurchasedProductWithID:(NSString *)productID
{
    _productWasPurchased = YES;
    _productWasRestored = NO;
    [[SNAppPurchaseManager defaultManager] checkReceiptByServerWithAdditionalFields:nil
                                                                 primaryResponseKey:@"valid"
                                                                 messageResponseKey:@"message"];
}

- (void)purchaseManagerRestoredProductWithID:(NSString *)productID
{
    _productWasPurchased = NO;
    _productWasRestored = YES;
    [[SNAppPurchaseManager defaultManager] checkReceiptByServerWithAdditionalFields:nil
                                                                 primaryResponseKey:@"valid"
                                                                 messageResponseKey:@"message"];
}

- (void)purchaseManagerFailedToPurchaseProductWithID:(NSString *)productID error:(NSError *)error
{
    UIAlertView *failedAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"purchasestopped", @"")
                                                          message:NSLocalizedString(@"purchasestoppedtext", @"")
                                                         delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", @"") otherButtonTitles:nil, nil];
    [failedAlert show];
}

- (void)purchaseManagerFailedToRestoreProductWithError:(NSError *)error
{
    if (!error) {
        UIAlertView *restoreAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"restoreissue", @"") message:NSLocalizedString(@"restoreissuetext", @"") delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", @"") otherButtonTitles:nil,nil];
        [restoreAlert show];
    } else {
        
        // if restoration canceled or an error has occur
        [self showAlertViewWithErrorMessage:[NSString stringWithFormat:@"%@\n%@", NSLocalizedString(@"Restoration Failed", nil), error.description]];
    }
}

// payment receipt`s check result
- (void)purchaseManagerReceiptIsOk:(BOOL)receiptIsOk
{
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"gekauft"]) {
        return;
    }
    
    if (receiptIsOk) {
        NSString *alertMessage;
        [[NSUserDefaults standardUserDefaults] setBool:YES  forKey:@"gekauft"];
        [[NSUserDefaults standardUserDefaults] setBool:YES  forKey:@"startsound"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSURLCache sharedURLCache] removeAllCachedResponses];
        NSLog(@"Cache gelöscht");
        
        [self createDataSource];
        self.blockType = CellBlockTypeNone;
        
        if (_productWasRestored) {
            // This was a Restore request.
            alertMessage = NSLocalizedString(@"textrestore", @"");
            
        } else if (_productWasPurchased) {
            // This was a Purchase request.
            alertMessage = NSLocalizedString(@"textpurchase", @"");
        }
        
        UIAlertView *updatedAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"vielendank", @"") message:alertMessage delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", @"") otherButtonTitles:nil];
        [updatedAlert show];
    }
}

@end
