//
//  SettingsPurchaseTableCell.h
//  DownloadManager
//
//  Created by Denis on 21.04.15.
//  Copyright (c) 2015 ComputerVerse Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsPurchaseTableCell : UITableViewCell

@property (strong, nonatomic) NSString *purchaseTitle;
@property (strong, nonatomic) NSString *restoreTitle;
@property (strong, nonatomic) id <NSObject> delegate;
@property (nonatomic) SEL purchaseSelector;
@property (nonatomic) SEL restoreSelector;

@end