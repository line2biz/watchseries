//
//  SettingsEditTableCell.h
//  DownloadManager
//
//  Created by Denis on 21.04.15.
//  Copyright (c) 2015 ComputerVerse Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsEditTableCell : UITableViewCell

@property (strong, nonatomic) UIImage *icon;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *placeholderText;
@property (strong, nonatomic) NSString *helpText;

@end
