//
//  SettingsSwitchTableCell.m
//  DownloadManager
//
//  Created by Denis on 21.04.15.
//  Copyright (c) 2015 ComputerVerse Corporation. All rights reserved.
//

#import "SettingsSwitchTableCell.h"

@interface SettingsSwitchTableCell ()

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewIcon;
@property (weak, nonatomic) IBOutlet UISwitch *switchObject;

@end

@implementation SettingsSwitchTableCell

#pragma mark - Life Cycle
- (void)awakeFromNib {
    // Initialization code
}

#pragma mark - Outlet Methods
- (IBAction)onSwitchChange:(UISwitch *)sender
{
    if (self.stateIdentifier.length) {
        [[NSUserDefaults standardUserDefaults] setBool:sender.isOn forKey:self.stateIdentifier];
        
        if (self.completionBlock) {
            self.completionBlock();
        }
    }
}

#pragma mark - Property Accessors
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setTitle:(NSString *)title
{
    _title = title;
    self.labelTitle.text = title;
}

- (void)setIcon:(UIImage *)icon
{
    _icon = icon;
    self.imageViewIcon.image = icon;
}

- (void)setStateIdentifier:(NSString *)stateIdentifier
{
    _stateIdentifier = stateIdentifier;
    
    BOOL state = [[NSUserDefaults standardUserDefaults] boolForKey:stateIdentifier];
    [self.switchObject setOn:state];
}

@end
