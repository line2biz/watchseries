//
//  Created by Nils Weidl on 18.10.13.
//  Copyright (c) 2013 ComputerVerse Corporation. All rights reserved.
//

#import "OptionViewController.h"
#import "KKPasscodeSettingsViewController.h"
#import "DLIDEKeyboardView.h"
#import "KeychainItemWrapper.h"

#import "SNAppPurchaseManager.h"


// REPLACE THIS VALUE WITH YOUR OWN IN-APP PURCHASE PRODUCT ID.

#define SUB_PRODUCT_ID @"com.netzsuche.iphonevideodownloader.pro"

// ALSO ADD THE RELATED PARENT APP BUNDLE IDENTIFIER TO THE INFO PLIST.


@interface OptionViewController () <SNAppPurchaseManagerDelegate>
{
    BOOL _productWasPurchased, _productWasRestored;
    
    // it becomes non nil after purchaseManagerProductsAvailable: delegate method
    SKProduct *_defaultProduct;
}
@end


NSInteger counter;

NSInteger counter2;

@implementation OptionViewController
@synthesize slider;


-(IBAction)sliderValueChanged:(UISlider*)sender {
    [NSString stringWithFormat:@"%.1f", [sender value]];
    
    [[UIScreen mainScreen] setBrightness: [sender value]];
    NSLog(@"brightness : %f",[[UIScreen mainScreen] brightness]);
    
    [[NSUserDefaults standardUserDefaults] setFloat:sender.value forKey:@"helligkeit"];
    [[NSUserDefaults standardUserDefaults] synchronize];
//    [YRDropdownView hideDropdownInView:_demoview animated:YES];
}

-(IBAction)reset:(id)sender {
    NSLog(@"reset!!!!!");
    
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:NSLocalizedString(@"timer", @"")];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"switch2"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"switch7"];
    [[NSUserDefaults standardUserDefaults] setBool:NO  forKey:@"UnlockedLevels"];
    [[NSUserDefaults standardUserDefaults] setBool:NO  forKey:@"gekauft"];
    [[NSUserDefaults standardUserDefaults] setBool:NO  forKey:@"adblocktrial"];
    [[NSUserDefaults standardUserDefaults] setBool:YES  forKey:@"FirstLaunch11"];
    [[NSUserDefaults standardUserDefaults] setBool:YES  forKey:@"FirstLaunch"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    NSLog(@"Cache gelöscht");
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:Nil forKey:@"thenewurl"];
    [defaults synchronize];
    
    KeychainItemWrapper *wrapper = [[KeychainItemWrapper alloc] initWithIdentifier:NSLocalizedString(@"iphonevideodownloader", @"") accessGroup:nil];
    
    NSString *key = [wrapper objectForKey:(__bridge id)(kSecValueData)];
    
    BOOL inapp = [[NSUserDefaults standardUserDefaults] boolForKey:@"adblocktrial"];
    if(inapp == NO || [key isEqualToString:@"NO"])
    {
        [wrapper setObject:@"NO" forKey:(__bridge id)(kSecValueData)];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    
    
    
    
}


-(IBAction)reset2:(id)sender {
    NSLog(@"reset2!!!!!");
    [[NSUserDefaults standardUserDefaults] setBool:NO  forKey:@"gekauft"];
    [[NSUserDefaults standardUserDefaults] setBool:YES  forKey:@"adblocktrial"];
    [[NSUserDefaults standardUserDefaults] synchronize];
     
    
    
    
}




-(IBAction)kauf:(id)sender {
      NSLog(@"gekauft!!!!!");
    
    [[NSUserDefaults standardUserDefaults] setBool:YES  forKey:@"gekauft"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    iap.hidden = YES;
    restore.hidden = YES;
    sperre.hidden = YES;
    sperre2.hidden = YES;

    
}

- (void)onRoff {
 if(mySwitch.on) {
     UIAlertView *alertclose2 = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"aenderung", @"")
                                                     message:Nil
                                                    delegate:self
                                           cancelButtonTitle:NSLocalizedString(@"Ok", @"")
                                           otherButtonTitles:nil];
     
     [alertclose2 show];
     
     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"PC_Hinweis", @"")
                                                     message:NSLocalizedString(@"PC_Hinweis_Text", @"")
                                                    delegate:self
                                           cancelButtonTitle:NSLocalizedString(@"Ok", @"")
                                           otherButtonTitles:nil];
     
     [alert show];
     
     
  //   label.text = NSLocalizedString(@"aenderung", @"");
        NSLog(@"switcher on");
     
     [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"switch"];
     [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"lableclose"];
     [[NSUserDefaults standardUserDefaults] synchronize];
    } else {
      //  label.text = NSLocalizedString(@"aenderung", @"");
        
        UIAlertView *alertclose2 = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"aenderung", @"")
                                                              message:Nil
                                                             delegate:self
                                                    cancelButtonTitle:NSLocalizedString(@"Ok", @"")
                                                    otherButtonTitles:nil];
        
        [alertclose2 show];
        
        
        NSLog(@"switcher off");

        [mySwitch setOn:NO animated:YES];
        
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"switch"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"lableclose"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        NSLog(@"lableclose YES");

//        [YRDropdownView hideDropdownInView:_demoview animated:YES];

        
    }
}







-(void)onRoff2 {
 if(mySwitch2.on) {
        NSLog(@"switcher 2 on");
     [[NSURLCache sharedURLCache] removeAllCachedResponses];
     NSLog(@"Cache gelöscht");
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"switch2"];
        [[NSUserDefaults standardUserDefaults] synchronize];
//     [YRDropdownView hideDropdownInView:_demoview animated:YES];

    }
    
    else {
        NSLog(@"switcher 2 off");
        [[NSURLCache sharedURLCache] removeAllCachedResponses];
        NSLog(@"Cache gelöscht");
        [mySwitch2 setOn:NO animated:YES];

        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"switch2"];
        
        
        [[NSUserDefaults standardUserDefaults] synchronize];
//        [YRDropdownView hideDropdownInView:_demoview animated:YES];

        
    }
}



-(void)onRoff6 {
    if(mySwitch6.on) {
        
        UIAlertView *alertclose2 = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"aenderung", @"")
                                                              message:Nil
                                                             delegate:self
                                                    cancelButtonTitle:NSLocalizedString(@"Ok", @"")
                                                    otherButtonTitles:nil];
        
        [alertclose2 show];
        NSLog(@"switche 6 on");
       // label.text = NSLocalizedString(@"aenderung", @"");
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"lableclose"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"switch6"];
        [[NSUserDefaults standardUserDefaults] synchronize];
//        [YRDropdownView hideDropdownInView:_demoview animated:YES];

    }
    
    else {
        NSLog(@"switcher 6 off");
        
        
        UIAlertView *alertclose2 = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"aenderung", @"")
                                                              message:Nil
                                                             delegate:self
                                                    cancelButtonTitle:NSLocalizedString(@"Ok", @"")
                                                    otherButtonTitles:nil];
        
        [alertclose2 show];
       // label.text = NSLocalizedString(@"aenderung", @"");
//        [YRDropdownView hideDropdownInView:_demoview animated:YES];
        
        [mySwitch6 setOn:NO animated:YES];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"lableclose"];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"switch6"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
 //       [YRDropdownView hideDropdownInView:_demoview animated:YES];

    }
}

- (IBAction)onRoff7:(id)sender {
    UISwitch *switch7 = (UISwitch *)sender;
    if(switch7.on) {
        NSLog(@"switche 7 on");
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"switch7"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    else {
        NSLog(@"switcher 7 off");
        
        [switch7 setOn:NO animated:YES];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"switch7"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
   
}


- (IBAction)sendtext:(id)sender {
    NSUserDefaults *giventext = [NSUserDefaults standardUserDefaults];
    [giventext setObject:mytextfield.text forKey:@"thenewurl"];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:mytextfield.text forKey:@"thenewurl"];
    [defaults synchronize];
    
//    [YRDropdownView hideDropdownInView:_demoview animated:YES];

}

//implementation
- (void)textFieldDidBeginEditing:(UITextField *)textField {
  
        [DLIDEKeyboardView attachToTextView:textField];
    svos = scrollview.contentOffset;
    CGPoint pt;
    CGRect rc = [textField bounds];
    rc = [textField convertRect:rc toView:scrollview];
    pt = rc.origin;
    pt.x = 0;
    pt.y -= 100;
    [scrollview setContentOffset:pt animated:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [scrollview setContentOffset:svos animated:YES];
    return YES;
}

-(IBAction)removekeyboard:(id)sender {
    
    [self resignFirstResponder];
}

-(IBAction)deletecache {
    
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"cacheloeschen", @"")
                            message:NSLocalizedString(@"cacheloeschentext", @"")
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"abbrechen", @"")
                                          otherButtonTitles:nil];
    [alert addButtonWithTitle:NSLocalizedString(@"jetztloeschen", @"")];
    
    [alert show];
    
    
}

-(IBAction)clearcookie
{
   
     //  [self dismissModalViewControllerAnimated:YES];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"cookiesloeschen", @"")
                                    message:NSLocalizedString(@"cookiesloeschentext", @"")
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"abbrechen", @"")
                                          otherButtonTitles:nil];
    [alert addButtonWithTitle:NSLocalizedString(@"ja", @"")];
   
    [alert show];


}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if([title isEqualToString:NSLocalizedString(@"ja", @"")])
    {
        //Clear All Cookies
        for(NSHTTPCookie *cookie in [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies]) {
            
            //if([[cookie domain] isEqualToString:someNSStringUrlDomain]) {
            
            [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:cookie];
            
        }
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"cookiesgeloeschen", @"")
                                                        message:@""
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"ok", @"")
                                              otherButtonTitles:nil];
        [alert show];
    }

    if([title isEqualToString:NSLocalizedString(@"jetztloeschen", @"")])
    {
        
        // Flush all cached data
        [[NSURLCache sharedURLCache] removeAllCachedResponses];
        NSLog(@"Cache gelöscht");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"cachegeloescht", @"")
                                                        message:@""
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"ok", @"")
                                              otherButtonTitles:nil];
        [alert show];
    }

    
 }
    
  // 


- (IBAction)facebook:(id)sender {
    NSURL *fanPageURL = [NSURL URLWithString:@"fb://profile/1414465062105060"];
    
    if (![[UIApplication sharedApplication] openURL: fanPageURL]) {
        //fanPageURL failed to open.  Open the website in Safari instead
        NSURL *webURL = [NSURL URLWithString:@"https://www.facebook.com/pages/iPhone-Video-Downloader-with-Adblock-for-iphone-and-ipad/1414465062105060"];
        [[UIApplication sharedApplication] openURL: webURL];
    }
}
- (IBAction)twitter:(id)sender {
    
    NSURL *twitterURL = [NSURL URLWithString:@"twitter:///user?screen_name=iOSTubeDownload"];
    
    if (![[UIApplication sharedApplication] openURL: twitterURL]) {
        //twitterURL failed to open.  Open the website in Safari instead
        NSURL *webURL = [NSURL URLWithString:@"http://twitter.com/iOSTubeDownload"];
        [[UIApplication sharedApplication] openURL: webURL];
    }
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"Settings";
    }
    return self;
}

- (void)sliderHandler:(UISlider *)sender {
    //NSLog(@"value:%f", sender.value);
    [[UIScreen mainScreen] setBrightness:sender.value];
}

- (void) viewWillAppear:(BOOL)animated {
    [self orientInferface:[[UIApplication sharedApplication] statusBarOrientation] duration:0.0];
    
    
    
    
    version.text =  [NSString stringWithFormat:NSLocalizedString(@"_version", nil), [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];
    // Version: 1.0
    
    count.text = [NSString stringWithFormat:NSLocalizedString(@"gesamt", @""),counter];
    count2.text = [NSString stringWithFormat:NSLocalizedString(@"aktuell", @""),counter2];
    
    NSLog(@"Autorefresh!!");
    NSLog(@"Geblockte Ads %zd",counter);
    NSLog(@"Aktuell %zd",counter2);
    
  
    
    buyButton.enabled = NO; // Only enable after populated with IAP price.
    
    
    // OLD CODE
    
//    // Request In-App Purchase product info and availability.
//    
//    if (![demoPurchase requestProduct:SUB_PRODUCT_ID])
//    {
//        // Returned NO, so notify user that In-App Purchase is Disabled in their Settings.
//        
//        [buyButton setTitle:NSLocalizedString(@"iapnichtaktiev", @"") forState:UIControlStateNormal];
//    }

    // NEW CODE
    BOOL applicationIsPurchased = NO;
    applicationIsPurchased = [[NSUserDefaults standardUserDefaults] boolForKey:@"gekauft"];
    
    if (!applicationIsPurchased) {
        [[SNAppPurchaseManager defaultManager] setDelegate:self];
        [[SNAppPurchaseManager defaultManager] setSandboxMode:YES]; // should be NO if it`s not the test
        [[SNAppPurchaseManager defaultManager] setServerAdress:@"http://data.iosappmedia.com/api.php?action=validate_receipt"];
        [[SNAppPurchaseManager defaultManager] setProductID:SUB_PRODUCT_ID];
    }
    
    BOOL inapp = [[NSUserDefaults standardUserDefaults] boolForKey:@"gekauft"];
    if(inapp == YES) {
        NSLog(@"Pro gekauft!");
        
        iap.hidden = YES;
        restore.hidden = YES;
        sperre.hidden = YES;
        sperre2.hidden = YES;
        
    }
    else {
        
        NSLog(@"gekauft nein");
        
        
        KeychainItemWrapper *wrapper = [[KeychainItemWrapper alloc] initWithIdentifier:NSLocalizedString(@"iphonevideodownloader", @"") accessGroup:nil];
        NSString *key = [wrapper objectForKey:(__bridge id)(kSecValueData)];
        
        BOOL adblocktrial = [[NSUserDefaults standardUserDefaults] boolForKey:@"adblocktrial"];
        if(adblocktrial == YES || [key isEqualToString:@"YES"]) {
            NSLog(@"TRIAL-WERT GESETZT");
            
            sperre.hidden = YES;
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"iap_Hinweis", @"")
                                                            message:NSLocalizedString(@"iap_Hinweis_Text", @"")
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"Ok", @"")
                                                  otherButtonTitles:nil];
            
            [alert show];
        }
        
        else {
            
            NSLog(@"TRIAL-WERT Nicht gesetzt");
            
            BOOL inapp = [[NSUserDefaults standardUserDefaults] boolForKey:@"UnlockedLevels"];
            if(inapp == YES) {
                NSLog(@"UnlockedLevels: jab");
                
               // iap.hidden = YES;
              //  restore.hidden = YES;
                sperre.hidden = YES;
                sperre2.hidden = YES;
                
                
            }
            
            else {  NSLog(@"UnlockedLevels: nop");
                sperre2.hidden = YES;
                
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"iap_Hinweis", @"")
                                                                message:NSLocalizedString(@"iap_Hinweis_Text", @"")
                                                               delegate:self
                                                      cancelButtonTitle:NSLocalizedString(@"Ok", @"")
                                                      otherButtonTitles:nil];
                
                [alert show];
                
            }
            
            
            
            
        }
    }
    [super viewWillAppear:animated];
}


-(IBAction)purchaseProduct {
    
    BOOL kk = [[NSUserDefaults standardUserDefaults] boolForKey:@"gekauft"];
    if(kk == YES) {
        
    } else {
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:@"/Library/MobileSubstrate/DynamicLibraries/iap.dylib"])
        {              NSLog(@"iAP Cracker GEFUNDEN!!!!!!!!!!!!!!!!!!!!!!");
            
            /* UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"cracker", @"")
             message:NSLocalizedString(@"crackertext", @"")
             delegate:self
             cancelButtonTitle:NSLocalizedString(@"ok", @"")
             otherButtonTitles:nil];
             
             
             [alert show];
             */
            
            
            
            sleep(2);
            
            UIAlertView *failedAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"purchasestopped2", @"")
                                                                  message:NSLocalizedString(@"purchasestoppedtext2", @"")
                                                                 delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", @"") otherButtonTitles:nil];
            [failedAlert show];
            
        }
        
        else if         ([[NSFileManager defaultManager] fileExistsAtPath:@"/Applications/IAPFree.app"])
            
        {
            
            NSLog(@"iAP Free GEFUNDEN!!!!!!!!!!!!!!!!!!!!!!");
            
            
            sleep(2);
            
            UIAlertView *failedAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"purchasestopped2", @"")
                                                                  message:NSLocalizedString(@"purchasestoppedtext2", @"")
                                                                 delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", @"") otherButtonTitles:nil];
            [failedAlert show];
        }
        
        else if         ([[NSFileManager defaultManager] fileExistsAtPath:@"/Library/MobileSubstrate/DynamicLibraries/LocalIAPStore.dylib"])
            
        {
            NSLog(@"LocallAPStore GEFUNDEN!!!!!!!!!!!!!!!!!!!!!!");
            
            
            sleep(2);
            
            UIAlertView *failedAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"purchasestopped2", @"")
                                                                  message:NSLocalizedString(@"purchasestoppedtext2", @"")
                                                                 delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", @"") otherButtonTitles:nil];
            [failedAlert show];
            
        }
        
        
        else {   // do something useful
            NSLog(@"keiner der drei iAP Cracker gefunden. guter junge! :)");
            
//            // First, ensure that the SKProduct that was requested by
//            // the EBPurchase requestProduct method in the viewWillAppear
//            // event is valid before trying to purchase it.
//            
//            if (demoPurchase.validProduct != nil)
//            {
//                // Then, call the purchase method.
//                
//                if (![demoPurchase purchaseProduct:demoPurchase.validProduct])
//                {
//                    // Returned NO, so notify user that In-App Purchase is Disabled in their Settings.
//                    UIAlertView *settingsAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"allowiap", @"") message:NSLocalizedString(@"allowiaptext", @"") delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", @"") otherButtonTitles:nil];
//                    [settingsAlert show];
//                }
//            }
            
            // new code
            if (_defaultProduct) {
                [[SNAppPurchaseManager defaultManager] purchaseProduct:_defaultProduct];
            } else {
                [self showAlertViewWithErrorMessage:NSLocalizedString(@"Error must have occur. Purchase is not available.", nil)];
            }
        }
    }
}

-(IBAction)restorePurchase
{
    
    
    BOOL kk = [[NSUserDefaults standardUserDefaults] boolForKey:@"gekauft"];
    if(kk == YES) {
        
    } else {
        
        
        
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:@"/Library/MobileSubstrate/DynamicLibraries/iap.dylib"])
        {
         
            
            NSLog(@"iAP Cracker GEFUNDEN!!!!!!!!!!!!!!!!!!!!!!");
            
            sleep(2);
            UIAlertView *restoreAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"restoreissue", @"") message:NSLocalizedString(@"restoreissuetext", @"") delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", @"") otherButtonTitles:nil];
            [restoreAlert show];
        }
        
        else if         ([[NSFileManager defaultManager] fileExistsAtPath:@"/Applications/IAPFree.app"])
            
        {
            NSLog(@"iAP Free GEFUNDEN!!!!!!!!!!!!!!!!!!!!!!");
            
            sleep(2);
            UIAlertView *restoreAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"restoreissue", @"") message:NSLocalizedString(@"restoreissuetext", @"") delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", @"") otherButtonTitles:nil];
            [restoreAlert show];
            
        }
        
        else if         ([[NSFileManager defaultManager] fileExistsAtPath:@"/Library/MobileSubstrate/DynamicLibraries/LocalIAPStore.dylib"])
            
        {
            NSLog(@"LocallAPStore GEFUNDEN!!!!!!!!!!!!!!!!!!!!!!");
            
            
            sleep(2);
            UIAlertView *restoreAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"restoreissue", @"") message:NSLocalizedString(@"restoreissuetext", @"") delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", @"") otherButtonTitles:nil];
            [restoreAlert show];
            
            
        }
        
        
        else {   // do something useful
            NSLog(@"keiner der drei iAP Cracker gefunden. guter junge! :)");
            
//            // Restore a customer's previous non-consumable or subscription In-App Purchase.
//            // Required if a user reinstalled app on same device or another device.
//            
//            // Call restore method.
//            if (![demoPurchase restorePurchase])
//            {
//                // Returned NO, so notify user that In-App Purchase is Disabled in their Settings.
//                UIAlertView *settingsAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"allowiap", @"") message:NSLocalizedString(@"allowiaptext", @"") delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", @"") otherButtonTitles:nil];
//                [settingsAlert show];   }
            
            // new code
            [[SNAppPurchaseManager defaultManager] restorePurchase];
        }
    }
}

#pragma mark - Alert View Methods
- (void)showRequestAlertViewWithTag:(NSInteger)iTag
                              title:(NSString *)sTitle
                            message:(NSString *)sMessage
                          textField:(BOOL)hasTextField
                       cancelButton:(NSString *)sCancel
                      confirmButton:(NSString *)sOk
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:sTitle
                                                        message:sMessage
                                                       delegate:self
                                              cancelButtonTitle:sCancel
                                              otherButtonTitles:sOk, nil];
    alertView.tag = iTag;
    if (hasTextField == YES) {
        alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    }
    [alertView show];
}

- (void)showAlertViewWithErrorMessage:(NSString *)sMessage
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil
                                                        message:sMessage
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                              otherButtonTitles:nil, nil];
    [alertView show];
}

#pragma mark - SNApp Purchase Manager Delegate
/// if there is an error, it must be processed
- (void)purchaseManagerDisplayErrorMessage:(NSString *)message
{
    [self showAlertViewWithErrorMessage:message];
}

/// reports about available products
- (void)purchaseManagerProductsAvailable:(NSArray *)productsAvailable
{
    // here we get the list of products available for purchase
    if ([productsAvailable count]) {
        _defaultProduct = (SKProduct *)[productsAvailable firstObject]; // we have only one product in this situation
        
        NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
        [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        [numberFormatter setLocale:_defaultProduct.priceLocale];
        NSString *formattedPrice = [numberFormatter stringFromNumber:_defaultProduct.price];
        [buyButton setTitle:[NSLocalizedString(@"upgradetopro", @"") stringByAppendingString:formattedPrice] forState:UIControlStateNormal];
        
        buyButton.enabled = YES;
    } else {
        // Product is NOT available in the App Store, so notify user.
        
        buyButton.enabled = NO; // Ensure buy button stays disabled.
        [buyButton setTitle:NSLocalizedString(@"nichtverfuegbar", @"") forState:UIControlStateNormal];
    }
}

// payment action results
- (void)purchaseManagerPurchasedProductWithID:(NSString *)productID
{
    _productWasPurchased = YES;
    _productWasRestored = NO;
    [[SNAppPurchaseManager defaultManager] checkReceiptByServerWithAdditionalFields:nil
                                                                 primaryResponseKey:@"valid"
                                                                 messageResponseKey:@"message"];
}

- (void)purchaseManagerRestoredProductWithID:(NSString *)productID
{
    _productWasPurchased = NO;
    _productWasRestored = YES;
    [[SNAppPurchaseManager defaultManager] checkReceiptByServerWithAdditionalFields:nil
                                                                 primaryResponseKey:@"valid"
                                                                 messageResponseKey:@"message"];
}

- (void)purchaseManagerFailedToPurchaseProductWithID:(NSString *)productID error:(NSError *)error
{
    UIAlertView *failedAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"purchasestopped", @"")
                                                          message:NSLocalizedString(@"purchasestoppedtext", @"")
                                                         delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", @"") otherButtonTitles:nil, nil];
    [failedAlert show];
}

- (void)purchaseManagerFailedToRestoreProductWithError:(NSError *)error
{
    if (!error) {
        UIAlertView *restoreAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"restoreissue", @"") message:NSLocalizedString(@"restoreissuetext", @"") delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", @"") otherButtonTitles:nil,nil];
        [restoreAlert show];
    } else {
        
        // if restoration canceled or an error has occur
        [self showAlertViewWithErrorMessage:[NSString stringWithFormat:@"%@\n%@", NSLocalizedString(@"Restoration Failed", nil), error.description]];
    }
}

// payment receipt`s check result
- (void)purchaseManagerReceiptIsOk:(BOOL)receiptIsOk
{
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"gekauft"]) {
        return;
    }
    
    if (receiptIsOk) {
        isPurchased = YES;
        NSString *alertMessage;
        [[NSUserDefaults standardUserDefaults] setBool:YES  forKey:@"gekauft"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSURLCache sharedURLCache] removeAllCachedResponses];
        NSLog(@"Cache gelöscht");
        
        iap.hidden = YES;
        restore.hidden = YES;
        sperre.hidden = YES;
        sperre2.hidden = YES;
        
        if (_productWasRestored) {
            // This was a Restore request.
            alertMessage = NSLocalizedString(@"textrestore", @"");
            
        } else if (_productWasPurchased) {
            // This was a Purchase request.
            alertMessage = NSLocalizedString(@"textpurchase", @"");
        }
        
        UIAlertView *updatedAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"vielendank", @"") message:alertMessage delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", @"") otherButtonTitles:nil];
        [updatedAlert show];
    }
}

#pragma mark - View lifecycle

- (void)orientInferface:(UIInterfaceOrientation)newOrientation duration:(NSTimeInterval)duration {
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        if (duration == 0.0) {
            self.automaticallyAdjustsScrollViewInsets = NO;
        }
        [self setNeedsStatusBarAppearanceUpdate];
        [UIView animateWithDuration:duration animations:^{
            if (UIInterfaceOrientationIsPortrait(newOrientation)) {
                scrollview.contentInset = UIEdgeInsetsMake(64, 0, 49, 0);
                scrollview.scrollIndicatorInsets = UIEdgeInsetsMake(64, 0, 49, 0);
            } else {
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                    scrollview.contentInset = UIEdgeInsetsMake(32, 0, 49, 0);
                    scrollview.scrollIndicatorInsets = UIEdgeInsetsMake(32, 0, 49, 0);
                } else {
                    scrollview.contentInset = UIEdgeInsetsMake(64, 0, 49, 0);
                    scrollview.scrollIndicatorInsets = UIEdgeInsetsMake(64, 0, 49, 0);
                }
            }
        }];
    }
}

- (BOOL)prefersStatusBarHidden {
    if (!UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation]) && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        return YES;
    } else {
        return NO;
    }
}

- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation {
    return UIStatusBarAnimationSlide;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration {
    [self orientInferface:interfaceOrientation duration:duration];
}




-(void)soundswitcher {
    if(soundschalter.on) {
        NSLog(@"soundswitch jab");
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"startsound"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    
    else {
        NSLog(@"soundswitch nop");
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"startsound"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithRed:0.176f green:0.176f blue:0.176f alpha:1.000f];
    
    isPurchased = NO; // default.
    
    float helligkeit = [[NSUserDefaults standardUserDefaults] floatForKey:@"helligkeit"];
    helligkeit = [[UIScreen mainScreen] brightness];
    [[NSUserDefaults standardUserDefaults] setFloat:helligkeit forKey:@"helligkeit"];
    slider.value = helligkeit;
     
    scrollview.contentSize = CGSizeMake(contentview.frame.size.width, contentview.frame.size.height);
    [scrollview addSubview:contentview];
    
    count.text = [NSString stringWithFormat:NSLocalizedString(@"gesamt", @""),counter];
    count2.text = [NSString stringWithFormat:NSLocalizedString(@"aktuell", @""),counter2];
    
    NSLog(@"Geblockte Ads %zd",counter);
    NSLog(@"Aktuell %zd",counter2);
    
    BOOL test= [[NSUserDefaults standardUserDefaults] boolForKey:@"switch"];
    NSLog(@"%@",test?@"switcher 1 YES":@"switcher 1 NO");
    if(test == YES)
        
        [mySwitch setOn:YES animated:YES];
    
    else
        [mySwitch setOn:NO animated:YES];
    [[NSUserDefaults standardUserDefaults] setBool:self->mySwitch.on forKey:@"switch"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    
    
    
    
    
    BOOL test2= [[NSUserDefaults standardUserDefaults] boolForKey:@"switch2"];
    NSLog(@"%@",test2?@"switcher 2 YES":@"switcher 2 NO");
    if(test2 == YES)
        [mySwitch2 setOn:YES animated:YES];
    else
        
        [mySwitch2 setOn:NO animated:YES];
    [[NSUserDefaults standardUserDefaults] setBool:self->mySwitch2.on forKey:@"switch2"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    BOOL sound= [[NSUserDefaults standardUserDefaults] boolForKey:@"startsound"];
    NSLog(@"%@",sound?@"startsound YES":@"startsound NO");
    if(sound == YES)
        [soundschalter setOn:YES animated:YES];
    else
        
        [soundschalter setOn:NO animated:YES];
    [[NSUserDefaults standardUserDefaults] setBool:self->soundschalter.on forKey:@"startsound"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    
    
    BOOL test6= [[NSUserDefaults standardUserDefaults] boolForKey:@"switch6"];
    NSLog(@"%@",test6?@"switcher 6 YES":@"switcher 6 NO");
    if(test6 == YES)
        
        [mySwitch6 setOn:YES animated:YES];
    
    else
        [mySwitch6 setOn:NO animated:YES];
    [[NSUserDefaults standardUserDefaults] setBool:self->mySwitch6.on forKey:@"switch6"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    BOOL test7= [[NSUserDefaults standardUserDefaults] boolForKey:@"switch7"];
    NSLog(@"%@",test7 ? @"switcher 7 YES":@"switcher 7 NO");
    if(test7 == YES)
        
        [mySwitch7 setOn:YES animated:YES];
    
    else
        [mySwitch7 setOn:NO animated:YES];
    [[NSUserDefaults standardUserDefaults] setBool:self->mySwitch7.on forKey:@"switch7"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    
    
    

    
    // UnlockedLevels
   /*
    BOOL inapp = [[NSUserDefaults standardUserDefaults] boolForKey:@"gekauft"];
    if(inapp == YES) {
        NSLog(@"Pro gekauft!");

        iap.hidden = YES;
        restore.hidden = YES;
        sperre.hidden = YES;
        sperre2.hidden = YES;

    }
    else {NSLog(@"gekauft nein");
        BOOL inapp = [[NSUserDefaults standardUserDefaults] boolForKey:@"UnlockedLevels"];
        if(inapp == YES) {
            NSLog(@"UnlockedLevels: jab");

            iap.hidden = YES;
            restore.hidden = YES;
            sperre.hidden = YES;
            sperre2.hidden = YES;

        }
        else {
            NSLog(@"UnlockedLevels: nope");
            sperre2.hidden = YES;

            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"iap_Hinweis", @"")
                                                               message:NSLocalizedString(@"iap_Hinweis_Text", @"")
                                                              delegate:self
                                                     cancelButtonTitle:NSLocalizedString(@"Ok", @"")
                                                     otherButtonTitles:nil];
            
            [alert show];
        
        }
       
 
    }
    
    
    */
        
    
    BOOL lableclose = [[NSUserDefaults standardUserDefaults] boolForKey:@"lableclose"];
    NSLog(@"%@",lableclose?@"bbb YES":@"NO");
    if(lableclose == YES) {
       // label.text = NSLocalizedString(@"aenderung", @"");
        
        UIAlertView *alertclose2 = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"aenderung", @"")
                                                              message:Nil
                                                             delegate:self
                                                    cancelButtonTitle:NSLocalizedString(@"Ok", @"")
                                                    otherButtonTitles:nil];
        
        [alertclose2 show];
        
        NSLog(@"%@",lableclose?@"lableclose YES":@"lableclose NO");}
    else {
        NSLog(@"%@",lableclose?@"lableclose YES":@"lableclose NO");
    }
    
    NSUserDefaults *giventext = [NSUserDefaults standardUserDefaults];
    
    NSString *savedtext = [giventext stringForKey:@"thenewurl"];
    [mytextfield setText:savedtext];
    
    //
    adsanzeige.text = NSLocalizedString(@"adsanzeige", @"");
    Cookies.text = NSLocalizedString(@"Cookies", @"");
    Soundtext.text = NSLocalizedString(@"Sound", @"");
    Downlaoder.text = NSLocalizedString(@"Downloader", @"");
    AdBlock.text = NSLocalizedString(@"AdBlock", @"");
    Homepage.text = NSLocalizedString(@"Homepage", @"");
    PressReturn.text = NSLocalizedString(@"PressReturn", @"");
    Passcode.text = NSLocalizedString(@"Passcode", @"");
    Useragent.text = NSLocalizedString(@"Useragent", @"");
    Cookies.text = NSLocalizedString(@"Cookies", @"");
    Brightness.text = NSLocalizedString(@"Brightness", @"");

    
    [mySwitch addTarget:self action:@selector(onRoff) forControlEvents:UIControlEventValueChanged];
    [mySwitch2 addTarget:self action:@selector(onRoff2) forControlEvents:UIControlEventValueChanged];
    
    [soundschalter addTarget:self action:@selector(soundswitcher) forControlEvents:UIControlEventValueChanged];
    
    [mySwitch6 addTarget:self action:@selector(onRoff6) forControlEvents:UIControlEventValueChanged];
}

- (IBAction)passwordSettings:(id)sender {
    KKPasscodeSettingsViewController* vc = [[KKPasscodeSettingsViewController alloc] initWithStyle:UITableViewStyleGrouped];
    
    UINavigationController* navController = [[UINavigationController alloc] initWithRootViewController:vc];
    
    [self presentViewController:navController animated:YES completion:nil];
}

@end
