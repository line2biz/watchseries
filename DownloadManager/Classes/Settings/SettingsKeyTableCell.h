//
//  SettingsKeyTableCell.h
//  DownloadManager
//
//  Created by Denis on 21.04.15.
//  Copyright (c) 2015 ComputerVerse Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SettingsKeyCellDelegate;

@interface SettingsKeyTableCell : UITableViewCell

@property (strong, nonatomic) UIImage *icon;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic, readonly) id <SettingsKeyCellDelegate> delegate;

- (void)configureWithIcon:(UIImage *)icon
                    title:(NSString *)title
                 delegate:(id <SettingsKeyCellDelegate>) delegate
                 selector:(SEL)keySelector;

@end


@protocol SettingsKeyCellDelegate <NSObject>
@required
- (void)didTapKeyButton:(UIButton *)sender;

@end