//
//  SettingsAdsTableCell.h
//  DownloadManager
//
//  Created by Denis on 21.04.15.
//  Copyright (c) 2015 ComputerVerse Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsAdsTableCell : UITableViewCell

@property (strong, nonatomic) NSString *sTitle;
@property (assign, nonatomic) NSInteger iTotalBlocked;
@property (assign, nonatomic) NSInteger iCurrentlyBlocked;

@end
