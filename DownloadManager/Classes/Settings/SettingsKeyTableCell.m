//
//  SettingsKeyTableCell.m
//  DownloadManager
//
//  Created by Denis on 21.04.15.
//  Copyright (c) 2015 ComputerVerse Corporation. All rights reserved.
//

#import "SettingsKeyTableCell.h"

@interface SettingsKeyTableCell ()

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewIcon;
@property (weak, nonatomic) IBOutlet UIButton *buttonKey;

@property (nonatomic) SEL keySelector;

@end

@implementation SettingsKeyTableCell

#pragma mark - Life Cycle
- (void)awakeFromNib
{
    // Initialization code
}

#pragma mark - Public Methods
- (void)configureWithIcon:(UIImage *)icon
                    title:(NSString *)title
                 delegate:(id <SettingsKeyCellDelegate>) delegate
                 selector:(SEL)keySelector
{
    self.icon = icon;
    self.title = title;
    _delegate = delegate;
    self.keySelector = keySelector;
}

#pragma mark - Property Accessors
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setTitle:(NSString *)title
{
    _title = title;
    self.labelTitle.text = title;
}

- (void)setIcon:(UIImage *)icon
{
    _icon = icon;
    self.imageViewIcon.image = icon;
}

- (void)setKeySelector:(SEL)keySelector
{
    _keySelector = keySelector;
    
    if ([self.delegate respondsToSelector:keySelector]) {
        [self.buttonKey addTarget:self.delegate action:keySelector forControlEvents:UIControlEventTouchUpInside];
    }
}


@end
