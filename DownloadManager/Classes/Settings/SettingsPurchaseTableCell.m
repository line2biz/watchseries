//
//  SettingsPurchaseTableCell.m
//  DownloadManager
//
//  Created by Denis on 21.04.15.
//  Copyright (c) 2015 ComputerVerse Corporation. All rights reserved.
//

#import "SettingsPurchaseTableCell.h"

@interface SettingsPurchaseTableCell ()

@property (weak, nonatomic) IBOutlet UIButton *buttonPurchase;
@property (weak, nonatomic) IBOutlet UIButton *buttonRestore;

@end

@implementation SettingsPurchaseTableCell

#pragma mark - Life Cycle
- (void)awakeFromNib {
    // Initialization code
}

#pragma mark - Property Accessors
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setPurchaseTitle:(NSString *)purchaseTitle
{
    _purchaseTitle = purchaseTitle;
    
    [self.buttonPurchase setTitle:purchaseTitle forState:UIControlStateNormal];
}

- (void)setRestoreTitle:(NSString *)restoreTitle
{
    _restoreTitle = restoreTitle;
    
    [self.buttonRestore setTitle:restoreTitle forState:UIControlStateNormal];
}

- (void)setDelegate:(id<NSObject>)delegate
{
    _delegate = delegate;
}

- (void)setPurchaseSelector:(SEL)purchaseSelector
{
    if ([self.delegate respondsToSelector:purchaseSelector]) {
        
        _purchaseSelector = purchaseSelector;
        
        [self.buttonPurchase addTarget:self.delegate action:_purchaseSelector forControlEvents:UIControlEventTouchUpInside];
    }
}

- (void)setRestoreSelector:(SEL)restoreSelector
{
    if ([self.delegate respondsToSelector:restoreSelector]) {
        
        _restoreSelector = restoreSelector;
        [self.buttonRestore addTarget:self.delegate action:_restoreSelector forControlEvents:UIControlEventTouchUpInside];
    }
}

@end
