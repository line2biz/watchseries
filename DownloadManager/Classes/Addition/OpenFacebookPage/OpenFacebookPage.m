//
//  OpenFacebookPage.m
//  PlayoffBeard
//
//  Created by Nils on 16.05.13.
//
//

#import "OpenFacebookPage.h"

@implementation OpenFacebookPage

- (NSString *)activityType {
    return @"openFacebookPage";
}

- (NSString *)activityTitle {
    if(self.nType == 0) {
        return NSLocalizedString(@"AddBookmark", @"");
        
    } else if(self.nType == 1) {
        return @"Magic Stick";
    }
    return @"Data Counter";
}

// Note: These images need to have a transparent background and I recommend these sizes:


// iPadShare@2x     126 px
// iPadShare        53  px
// iPhoneShare@2x   100 px
// iPhoneShare      50  px

// I found these sizes to work for what I was making.

- (UIImage *)activityImage {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        if(self.nType == 0)
        {
            return [UIImage imageNamed:@"iPadShare.png"];
        }
        else if(self.nType == 1)
        {
            return [UIImage imageNamed:@"iPadShare2.png"];
        }
        else
        {
            return [UIImage imageNamed:@"iPadShare3.png"];
        }
        
    }
    
    if(self.nType == 0) {
        return [UIImage imageNamed:@"iPhoneShare.png"];
    } else if(self.nType == 1) {
        return [UIImage imageNamed:@"iPhoneShare2.png"];
    }
    
    return [UIImage imageNamed:@"iPhoneShare3.png"];
}

- (BOOL)canPerformWithActivityItems:(NSArray *)activityItems {
    return YES;
}

- (void)prepareWithActivityItems:(NSArray *)activityItems {
    NSLog(@"prepareWithActivityItems");
}

- (void)performActivity {
    if(self.nType == 0) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ActivityLink1" object:nil];
    } else if(self.nType == 1) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ActivityLink2" object:nil];
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://itunes.apple.com/app/id666575009"]];
    }

    [self activityDidFinish:YES];
}

@end
