//
//  DLIDEKeyboardView.m
//  DLIDEKeyboard
//
//  Created by Denis Lebedev on 1/14/13.
//  Copyright (c) 2013 Denis Lebedev. All rights reserved.
//

#import "DLIDEKeyboardView.h"

#define IS_IPAD UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad

NSString *const kHideKeyBoardKey = @" ";
NSString *const kTabKey = @"\u279D";
NSString *const kFirstRowKey = @"\u25D0";
NSString *const kSecondRowKey = @"\u25D1";

@interface DLIDEKeyboardView () <UIInputViewAudioFeedback> {
    NSArray *_keys;
    NSInteger _kKeysInRow;
}

@property (nonatomic, strong) id<UITextInput> textView;
@property (nonatomic, strong) NSArray *keys;
@property (nonatomic, assign) NSInteger selectedRow;
@end

@implementation DLIDEKeyboardView

#pragma mark - Public

+ (void)attachToTextView:(UIResponder<UITextInput> *)textView {
    DLIDEKeyboardView *view = [[DLIDEKeyboardView alloc] init];
    if (![textView isKindOfClass:[UITextView class]] && ![textView isKindOfClass:[UITextField class]]) {
        [NSException raise:@"Keyboard can be attached only to text inputs" format:nil];
    }
    view.textView = textView;
    [(id)textView setInputAccessoryView:view];
}

#pragma mark - NSObject

- (id)init {
    if (self = [super init]) {
        CGFloat kKeyboardWidth = IS_IPAD ? 768.f : 320.f;
        CGFloat kKeyboardHeight = IS_IPAD ? 140.f : 45.f;
        self.frame = CGRectMake(0, 0, kKeyboardWidth, kKeyboardHeight);

        UIView *border1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kKeyboardWidth, 1)];
        border1.backgroundColor = [UIColor blackColor];
        border1.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        [self addSubview:border1];
        
        UIView *border2 = [[UIView alloc] initWithFrame:CGRectMake(0, 1, kKeyboardWidth, 1)];
        border2.backgroundColor = [UIColor blackColor];
        border2.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        [self addSubview:border2];
        
        self.backgroundColor = IS_IPAD ? [UIColor blackColor] :
        [UIColor blackColor];
        self.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        
        NSInteger kRows = IS_IPAD ? 2 : 1;
        _kKeysInRow = IS_IPAD ? 11 : 8;
        CGFloat kLeftPadding = 0.f;
        CGFloat kTopPadding = 4.f;
        CGFloat kSpacing =  IS_IPAD ? 5.f : 1.f;
        NSInteger kButtonWidth = (kKeyboardWidth - 2 * kLeftPadding - (_kKeysInRow - 1) * kSpacing) / _kKeysInRow;
        kLeftPadding = (kKeyboardWidth - kButtonWidth * _kKeysInRow - kSpacing * (_kKeysInRow - 1)) / 2;
                
        if (IS_IPAD) {
            _keys = @[@[@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"0", @"😃"],
            @[@".com",@".de", @".org", @".fr", @".co.uk", @".to", @".", @"/", @"_",@"-",  @"👍"]];
            
        } else {
            _keys = @[
                      //
                      @[@".com", @".de", @".org", @".to", @":", @"/",@"_",@"-"]];
            //    @[kHideKeyBoardKey, @"+", @"-", @"/", @"*",@"=", @"@", @"\"", @":", kFirstRowKey],
          // @[@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8",@"9"]];
        }
        
        for (int i = 0; i < kRows; i++) {
            for (int j = 0; j < _kKeysInRow; j++) {
                UIButton *b = [UIButton buttonWithType:UIButtonTypeCustom];
                b.tag = j + 100;
                b.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
                b.frame = CGRectMake(kLeftPadding + j*(kButtonWidth + kSpacing), kTopPadding + i*kButtonWidth, kButtonWidth, kButtonWidth);
                [b addTarget:self action:@selector(handleTap:) forControlEvents:UIControlEventTouchUpInside];
                [self addSubview:b];
                
                [b setTitle:_keys[i][j] forState:UIControlStateNormal];
                [b setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                [b setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                  if (IS_IPAD) {
                [b.titleLabel setFont:[UIFont boldSystemFontOfSize:15]];
                     
                      
                  } else {
                [b.titleLabel setFont:[UIFont boldSystemFontOfSize:12]];
                    }
                
                if (_keys[i][j] == kSecondRowKey || _keys[i][j] == kFirstRowKey) {
                    [b setBackgroundImage:[UIImage imageNamed:@"key-blue"] forState:UIControlStateNormal];
                } else {
                    [b setBackgroundImage:[UIImage imageNamed:@"key"] forState:UIControlStateNormal];
                }
                [b setImage:_keys[i][j] == kHideKeyBoardKey ? [UIImage imageNamed:@"keyboard"] : nil forState:UIControlStateNormal];
                [b setBackgroundImage:[UIImage imageNamed:@"key-pressed"] forState:UIControlStateHighlighted];
            }
        }
        [self refreshButtons];
    }
    return self;
}

#pragma mark - Private

- (void)handleTap:(UIButton *)button {
    [[UIDevice currentDevice] playInputClick];
    NSString *input = button.titleLabel.text;
    if ([input isEqualToString:kHideKeyBoardKey]) {
        [(id)self.textView resignFirstResponder];
    } else if ([input isEqualToString:kTabKey]) {
        [self.textView insertText:@"  "];
        return;
    } else if ([input isEqualToString:kFirstRowKey] || [input isEqualToString:kSecondRowKey]) {
        self.selectedRow = self.selectedRow == 1 ? 0 : 1;
        [self refreshButtons];
        return;
    } else {
        [self.textView insertText:input];
    }
}

- (void)refreshButtons {
    for (int j = 0; j < _kKeysInRow; j++) {
        UIButton *b = (UIButton *)[self viewWithTag:j + 100];
        [b setImage:_keys[self.selectedRow][j] == kHideKeyBoardKey ? [UIImage imageNamed:@"keyboard"] : nil forState:UIControlStateNormal];
        [b setTitle:_keys[self.selectedRow][j] forState:UIControlStateNormal];
    }
}

#pragma mark - UIInputViewAudioFeedback

- (BOOL)enableInputClicksWhenVisible {
    return YES;
}


@end
