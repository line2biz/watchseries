//
//  TabBarController.h
//  DownloadManager
//
//  Created by Nils Weidl on 18.10.13.
//  Copyright (c) 2013 ComputerVerse Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TabBarController : UITabBarController

@end
