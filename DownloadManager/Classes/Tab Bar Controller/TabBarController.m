//
//  TabBarController.m
//  DownloadManager
//
//  Created by Nils Weidl on 18.10.13.
//  Copyright (c) 2013 ComputerVerse Corporation. All rights reserved.
//

#import "TabBarController.h"
#import "BrowserViewController.h"
#import "OptionViewController.h"

#import "DownloadsViewController.h"
#import "DirectoryViewController.h"

#import "SettingTableController.h"

@interface TabBarController()

@end

@implementation TabBarController

#pragma mark - Life Cycle
- (id)init {
    self = [super init];
    if (self) {
//        NSMutableArray* array = [[NSMutableArray alloc] init];
//
//        BrowserViewController* browserController = [[BrowserViewController alloc] initWithNibName:@"BrowserViewController" bundle:[NSBundle mainBundle]];
//        browserController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Browser" image:[UIImage imageNamed:@"World_Times"] selectedImage:[UIImage imageNamed:@"BrowserSelected"]];
//        [array addObject:browserController];
//        
//        DownloadViewController* downloadController = [[DownloadViewController alloc] initWithNibName:@"DownloadViewController" bundle:[NSBundle mainBundle]];
//        UINavigationController* navController = [[UINavigationController alloc] initWithRootViewController:downloadController];
//        navController.tabBarItem = [[UITabBarItem alloc] initWithTabBarSystemItem:UITabBarSystemItemDownloads tag:0];
//        [array addObject:navController];
//        
//        StorageViewController* storageController = [[StorageViewController alloc] initWithPath:nil];
//        navController = [[UINavigationController alloc] initWithRootViewController:storageController];
//        navController.title = @"Media-Files";
//        navController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Media-Files" image:[UIImage imageNamed:@"Documents"] selectedImage:[UIImage imageNamed:@"DocumentsSelected"]];
//        [array addObject:navController];
//        
//        OptionViewController* optionController = [[OptionViewController alloc] initWithNibName:@"OptionViewController" bundle:[NSBundle mainBundle]];
//        navController = [[UINavigationController alloc] initWithRootViewController:optionController];
//        navController.title = @"Settings";
//        navController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Settings" image:[UIImage imageNamed:@"Settings"] selectedImage:[UIImage imageNamed:@"SettingsSelected"]];
//        [array addObject:navController];
//
//        [self setViewControllers:array];
//        
//        self.tabBar.barTintColor = [UIColor blackColor];
//        
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setDownloadBadge:) name:@"setDownloadBadge" object:nil];
    }
    return self;
}


- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        self.tabBar.barTintColor = [UIColor blackColor];
        self.tabBar.tintColor = [UIColor whiteColor];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setDownloadBadge:) name:@"setDownloadBadge" object:nil];
        
        NSMutableArray* array = [[NSMutableArray alloc] init];
        
        for (UIViewController *controller in self.viewControllers) {
            [array addObject:controller];
        }
        
//        DownloadViewController* downloadController = [[DownloadViewController alloc] initWithNibName:@"DownloadViewController" bundle:[NSBundle mainBundle]];
//        UINavigationController* navController = [[UINavigationController alloc] initWithRootViewController:downloadController];
//        navController.tabBarItem = [[UITabBarItem alloc] initWithTabBarSystemItem:UITabBarSystemItemDownloads tag:0];
//        [array addObject:navController];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        DownloadsViewController *downloadsViewController = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([DownloadsViewController class])];
        UINavigationController* navController = [[UINavigationController alloc] initWithRootViewController:downloadsViewController];
        navController.tabBarItem = [[UITabBarItem alloc] initWithTabBarSystemItem:UITabBarSystemItemDownloads tag:0];
        [array addObject:navController];
        
//        StorageViewController* storageController = [[StorageViewController alloc] initWithPath:nil];
//        navController = [[UINavigationController alloc] initWithRootViewController:storageController];
//        navController.title = @"Media-Files";
//        navController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Media-Files" image:[UIImage imageNamed:@"Documents"] selectedImage:[UIImage imageNamed:@"DocumentsSelected"]];
//        [array addObject:navController];
        
        DirectoryViewController *dirViewController = [DirectoryViewController DirectoryViewControllerWithPath:nil];
        navController = [[UINavigationController alloc] initWithRootViewController:dirViewController];
        navController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Media-Files" image:[UIImage imageNamed:@"Documents"] selectedImage:[UIImage imageNamed:@"DocumentsSelected"]];
        [array addObject:navController];

        
        
//        OptionViewController* optionController = [[OptionViewController alloc] initWithNibName:@"OptionViewController" bundle:[NSBundle mainBundle]];
        
        SettingTableController *optionController = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:NSStringFromClass([SettingTableController class])];
        
        navController = [[UINavigationController alloc] initWithRootViewController:optionController];
        navController.title = @"Settings";
        navController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Settings" image:[UIImage imageNamed:@"Settings"] selectedImage:[UIImage imageNamed:@"SettingsSelected"]];
        [array addObject:navController];
        
        [self setViewControllers:array];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSInteger iIdx = 0;
    for (__strong UITabBarItem *tabBarItem in self.tabBar.items) {
        
        if (iIdx == 1) {
            
            
        } else {
            
            NSString *sImageName;
            switch (iIdx) {
                case 0:
                    sImageName = @"BrowserSelected";
                    break;
                case 2:
                    sImageName = @"DocumentsSelected";
                    break;
                case 3:
                    sImageName = @"SettingsSelected";
                    break;
                    
                default:
                    break;
            }
            if (sImageName) {
                tabBarItem.selectedImage = [[UIImage imageNamed:sImageName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            }
            
        }
        
        iIdx++;
    }
    
}


#pragma mark - Private Methods
- (void)setDownloadBadge:(NSNotification*)aNot {
    NSDictionary* userInfo = aNot.userInfo;
    if ([userInfo[@"value"] isEqualToString:@"0"]) {
        [self.tabBar.items[1] setBadgeValue:nil];
    } else {
        [self.tabBar.items[1] setBadgeValue:userInfo[@"value"]];
    }
}

#pragma mark - State Preservation and Restoration
- (void)encodeRestorableStateWithCoder:(NSCoder *)coder
{
    [super encodeRestorableStateWithCoder:coder];
    
}

- (void)decodeRestorableStateWithCoder:(NSCoder *)coder
{
    [super decodeRestorableStateWithCoder:coder];
}


@end
