//
//  AppDelegate.m
//  DownloadManager
//
//  Created by Cai DaRong on 5/15/13.
//  Copyright (c) 2013 Cai DaRong. All rights reserved.
//  713622888
//  713723888


#import "AppDelegate.h"

#import "OptionViewController.h"
#import "iRate.h"
#import "Countly.h"
#import "TabBarController.h"
#import "DirectoryManager.h"

#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>

#import "KeychainItemWrapper.h"

#import "DMManager.h"

#import "SNRemoteServerManager.h"

#import "SNAdManager.h"




NSString * const kApplicationDidEnterBackground     = @"kApplicationDidEnterBackground";
NSString * const kApplicationWillEnterForeground    = @"kApplicationWillEnterForeground";
NSString * const kSpecialWindowTapNotification      = @"kSpecialWindowTapNotification";

NSString * const kLocalPlayerDefaultUsage           = @"kLocalPlayerDefaultUsage";
NSString * const kScreenModeAutomatic               = @"kScreenModeAutomatic";

NSString * const kBlockedAdsTotal                   = @"thecounter";
NSString * const kBlockedAdsCurrent                 = @"kBlockedAdsCurrent";

@interface AppDelegate ()

@end

@implementation AppDelegate


#pragma mark - Application Delegates
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // iniate alert queue
    [SNAlertQueueManager sharedManager];
        
    // if there were bookmarks on old version (without DataBase)
    // we should import them and clear memory
    NSArray *arrayOfBookmarks = [NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"bookmarks"]];
    
    if ([arrayOfBookmarks count] != 0) {
        for (id obj in arrayOfBookmarks) {
            if ([obj isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dictBookmark = (NSDictionary *)obj;
                
                NSString *sTitle = [dictBookmark objectForKey:@"title"];
                NSString *sUrl = [dictBookmark objectForKey:@"url"];
                
                if (sTitle.length && sUrl.length) {
                    
                    [DMBookmark addNewBookmarkWithTitle:sTitle url:sUrl];
                }
                if (obj == [arrayOfBookmarks lastObject]) {
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"bookmarks"];
                }
            }
        }
    }
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        UIUserNotificationType types = UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge;
        UIUserNotificationSettings *notificationSettings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];
    }
    
    // Web Cache
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *docDir = [paths objectAtIndex:0];
        NSString *path =  [NSString stringWithFormat:@"%@/diskCache", docDir];
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if ( ![fileManager isExecutableFileAtPath:path] ) {
            NSError *error;
            [[NSFileManager defaultManager] createDirectoryAtPath:path withIntermediateDirectories:NO attributes:nil error:&error];
            
            [[NSURL fileURLWithPath:path] setResourceValue:[NSNumber numberWithBool:YES] forKey:NSURLIsExcludedFromBackupKey error:nil];
        }
        
        // set url cache
        NSUInteger discCapacity = 500*1024*1024;
        NSUInteger memoryCapacity = 500*1024*1024;
        
        self.webCache = [[WatchseriesWebCache alloc] initWithMemoryCapacity: memoryCapacity
                                                    diskCapacity: discCapacity diskPath:path];
        [NSURLCache setSharedURLCache:self.webCache];
    }
    
    // Set the receiver application ID to initialise scanning.
    [ChromecastDeviceController sharedInstance].applicationID = kGCKMediaDefaultReceiverApplicationID;//@"4F8B3483";

    // keyboard bkg color
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        [[UIToolbar appearance] setBackgroundColor:[UIColor blackColor]];        
        
        [[UIButton appearanceWhenContainedIn:[UIAlertController class], nil] setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    }
    
    // start remote server
    [[SNRemoteServerManager sharedInstance] startServer];
    
    // nullify currently blocked ads
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:kBlockedAdsCurrent];
    
    // get new list for browser filter if needed
    [[SNAdManager sharedManager] needsToUpdateAdsListWithCompletion:^(BOOL updateNeeded) {
        if (updateNeeded == YES) {
            [[SNAdManager sharedManager] downloadAdsListWithCompletion:^(NSError *error, NSArray *aSites) {
                if (error) {
                    DBGLog(@"Error downloading Sites List: %@", error);
                }
                else {
                    [[SNAdManager sharedManager] saveRawDataToSecureContainerFromArray:aSites
                                                                        withCompletion:^(NSError *error) {
                                                                            if (error) {
                                                                                DBGLog(@"Error while saving Ad list: %@", error);                                                                                
                                                                            }
                                                                        }];
                }
            }];
        }
    }];
    
    
    
    // OLD CODE
    
    NSLog(NSLocalizedString(@"iphonevideodownloader", @""));
    
    BOOL inapp = [[NSUserDefaults standardUserDefaults] boolForKey:@"gekauft"];
    if(inapp == YES) {
        NSLog(@"Pro gekauft!");
        
    } else {
        
        NSLog(@"gekauft nein");
        
        KeychainItemWrapper *wrapper = [[KeychainItemWrapper alloc] initWithIdentifier:NSLocalizedString(@"iphonevideodownloader", @"") accessGroup:nil];
        NSString *key = [wrapper objectForKey:(__bridge id)(kSecValueData)];
        
        BOOL inapp = [[NSUserDefaults standardUserDefaults] boolForKey:@"adblocktrial"];
        if(inapp == YES || [key isEqualToString:@"YES"]) {
            NSLog(@"TRIAL-WERT GESETZT");
            
            NSLog(@"Startcounter geht zu 9!");
            
            [[NSUserDefaults standardUserDefaults]setInteger:9 forKey:NSLocalizedString(@"timer", @"")];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            [[NSUserDefaults standardUserDefaults] setBool:YES  forKey:@"adblocktrial"];
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"startsound"];
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"switch2"];
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"switch7"];
            
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kLocalPlayerDefaultUsage]; // don`t use local player by default at start
            
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            [[NSURLCache sharedURLCache] removeAllCachedResponses];
            NSLog(@"Cache gelöscht");
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:Nil forKey:@"thenewurl"];
            [defaults synchronize];
            
            [wrapper setObject:@"YES" forKey:(__bridge id)(kSecValueData)];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"_adblockout", @"")
                                                            message:NSLocalizedString(@"_adblockouttext", @"")
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"Ok", @"")
                                                  otherButtonTitles:nil];
            
            [alert show];
        } else {
            NSLog(@"TRIAL-WERT NICHT GESETZT");
        }
    }
    
    
    NSDictionary *defaultsDict = [[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithBool:YES], @"FirstLaunch11", nil];
    [[NSUserDefaults standardUserDefaults] registerDefaults:defaultsDict];
    
    NSUserDefaults *sharedDefaults = [NSUserDefaults standardUserDefaults];
    if ([sharedDefaults boolForKey:@"FirstLaunch11"]) {
        
        NSLog(@"FirstLaunch v11 YES");
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"startsound"];
        [[NSUserDefaults standardUserDefaults] setBool:NO  forKey:@"UnlockedLevels"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [sharedDefaults setBool:NO forKey:@"FirstLaunch11"];
        [sharedDefaults synchronize];
        /*
         
         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"neueversion", @"")
         message:NSLocalizedString(@"neueversiontext", @"")
         delegate:self
         cancelButtonTitle:NSLocalizedString(@"Ok", @"")
         otherButtonTitles:nil];
         
         [alert show];
         */
        
    } else {
        NSLog(@"FirstLaunch v11 NO");
    }
    
    
    NSDictionary *defaultsDict2 =
    [[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithBool:YES], @"FirstLaunch", nil];
    [[NSUserDefaults standardUserDefaults] registerDefaults:defaultsDict2];
    
    NSUserDefaults *sharedDefaults2 = [NSUserDefaults standardUserDefaults];
    if ([sharedDefaults2 boolForKey:@"FirstLaunch"]) {
        NSLog(@"FirstLaunch YES");
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"startsound"];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"switch2"];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"switch7"];
        [[NSUserDefaults standardUserDefaults] setBool:NO  forKey:@"switch6"];
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"thecounter"];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"FirstLaunch"];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"gutentag", @"")
                                                        message:NSLocalizedString(@"gutentagtext", @"")
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"Ok", @"")
                                              otherButtonTitles:nil];
        [alert show];
    } else {
        NSLog(@"FirstLaunch NO");
    }
    
    
    BOOL kk = [[NSUserDefaults standardUserDefaults] boolForKey:@"startsound"];
    if(kk == YES) {
        
        NSString *path1 = [[NSBundle mainBundle] pathForResource:@"sound" ofType:@"mp3"];
        AVAudioPlayer* theAudio =[[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path1] error:NULL];
        [theAudio play];
        
        sleep(8);
        
    } else {
        sleep(2);
    }
    
    
    NSError *sessionError = nil;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&sessionError];
    
    //    UInt32 doChangeDefaultRoute = 1;
    //    AudioSessionSetProperty(kAudioSessionProperty_OverrideCategoryDefaultToSpeaker, sizeof(doChangeDefaultRoute), &doChangeDefaultRoute);
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    
    NSError *error = nil;
    BOOL success = [audioSession overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:&error];
    if(!success) {
        NSLog(@"\n\n%s\nerror doing outputaudioportoverride - %@", __FUNCTION__, [error localizedDescription]);
    }
    
    application.applicationIconBadgeNumber = 0;
    
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *docDir = [paths objectAtIndex:0];
//    NSString *path =  [NSString stringWithFormat:@"%@/diskCache", docDir];
//    NSUInteger discCapacity = 60*1024*1024;
//    NSUInteger memoryCapacity = 50*1024*1024;
//    
//    NSFileManager *fileManager = [NSFileManager defaultManager];
//    if ( ![fileManager isExecutableFileAtPath:path] ) {
//        NSError *error;
//        [[NSFileManager defaultManager] createDirectoryAtPath:path withIntermediateDirectories:NO attributes:nil error:&error];
//        
//        [[NSURL fileURLWithPath:path] setResourceValue:[NSNumber numberWithBool:YES] forKey:NSURLIsExcludedFromBackupKey error:nil];
//    }
//    
//    self.cache = [[WebCache alloc] initWithMemoryCapacity: memoryCapacity
//                                             diskCapacity: discCapacity
//                                                 diskPath: path];
//    [NSURLCache setSharedURLCache:self.cache];
    
    [[Countly sharedInstance] start:@"ebe064cc2c9f42f70d58b3101025d4f7cc112b27" withHost:@"http://us12849119533871904.dynamiccloudserver.info"];
    
    [[KKPasscodeLock sharedLock] setDefaultSettings];
    [self showPasscodeLockIfNeeded];
    
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"lableclose"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    BOOL test6 = [[NSUserDefaults standardUserDefaults] boolForKey:@"switch6"];
    NSLog(@"%@",test6?@"switch6 YES":@"switch6 NO");
    if(test6 == YES) {
        
        //Clear All Cookies
        for(NSHTTPCookie *cookie in [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies]) {
            [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:cookie];
        }
    } else {
        NSLog(@"%@",test6?@"switcher 6 YES":@"switcher 6 NO");
    }
    
    counter2 = 0;
    counter = [[NSUserDefaults standardUserDefaults] integerForKey: @"thecounter"];
    
    BOOL test = [[NSUserDefaults standardUserDefaults] boolForKey:@"switch"];
    NSLog(@"%@",test?@"YES":@"NO");
    if(test == YES) {
        NSDictionary *dictionnary = [[NSDictionary alloc] initWithObjectsAndKeys:@"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/536.25 (KHTML, like Gecko) Version/6.0 Safari/536.25", @"UserAgent", nil];
        [[NSUserDefaults standardUserDefaults] registerDefaults:dictionnary];
        
        NSLog(@"%@",test?@"switcher YES":@"switcher NO");
    }
    
    
    NSString *deviceType = [UIDevice currentDevice].model;
    if([deviceType isEqualToString:@"iPad"]) {
        // it's an iPad
        NSLog(@"Es ist ein ipad");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"ipad", @"")
                                                        message:NSLocalizedString(@"ipadtext", @"")
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"_cancel", @"")
                                              otherButtonTitles:NSLocalizedString(@"Get_it", @""), nil];
        alert.tag = 1;
        [alert show];
        
    } else {
        // it's an iPhone
        NSLog(@"Es ist kein ipad");
    }
    
    
    NSInteger x = [[NSUserDefaults standardUserDefaults] integerForKey:NSLocalizedString(@"timer", @"")];
    
    if( x == 0 ) {
        NSLog(@"Startcounter ist bei 1. Start");
        
        [[NSUserDefaults standardUserDefaults]setInteger:2 forKey:NSLocalizedString(@"timer", @"")];
        [[NSUserDefaults standardUserDefaults]synchronize];
    } else if( x == 2 ) {
        
        NSLog(@"Startcounter ist bei 2");
        
        [[NSUserDefaults standardUserDefaults]setInteger:3 forKey:NSLocalizedString(@"timer", @"")];[[NSUserDefaults standardUserDefaults]synchronize];
        BOOL kk = [[NSUserDefaults standardUserDefaults] boolForKey:@"gekauft"];
        if(kk == NO) {
            
            BOOL kk = [[NSUserDefaults standardUserDefaults] boolForKey:@"adblocktrial"];
            if(kk == NO) {
                
                BOOL kk = [[NSUserDefaults standardUserDefaults] boolForKey:@"UnlockedLevels"];
                if(kk == NO) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"_ratetounlock", @"")
                                                                    message:NSLocalizedString(@"_ratetounlocktext", @"")
                                                                   delegate:self
                                                          cancelButtonTitle:NSLocalizedString(@"_cancel", @"")
                                                          otherButtonTitles:nil];
                    [alert addButtonWithTitle:NSLocalizedString(@"jetzt_bewerten", @"")];
                    alert.tag = 4;
                    
                    [alert show];
                }
            }
        }
    } else if ( x == 3 ) {
        
        NSLog(@"Startcounter ist bei 3");
        
        [[NSUserDefaults standardUserDefaults]setInteger:4 forKey:NSLocalizedString(@"timer", @"")];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        UIAlertView *alert2 = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"gutentag2", @"")
                                                         message:NSLocalizedString(@"gutentagtext2", @"")
                                                        delegate:self
                                               cancelButtonTitle:NSLocalizedString(@"Ok", @"")
                                               otherButtonTitles:nil];
        
        [alert2 show];
        
        
        /*
         
         [[NSUserDefaults standardUserDefaults]setInteger:4 forKey:NSLocalizedString(@"timer", @"")];
         [[NSUserDefaults standardUserDefaults]synchronize];
         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"_AdBlock", @"")
         message:NSLocalizedString(@"_AdBlocktext", @"")
         delegate:self
         cancelButtonTitle:NSLocalizedString(@"_cancel", @"")
         otherButtonTitles:NSLocalizedString(@"_Free_Download", @""), nil];
         
         
         alert.tag = 1;
         
         [alert show];
         
         */
        
    } else if( x == 4 ) {
        
        NSLog(@"Startcounter ist bei 4");
        
        [[NSUserDefaults standardUserDefaults]setInteger:5 forKey:NSLocalizedString(@"timer", @"")];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"facebook", @"")
                                                        message:NSLocalizedString(@"facebooktext", @"")
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"_cancel", @"")
                                              otherButtonTitles:nil];
        
        [alert addButtonWithTitle:NSLocalizedString(@"jaklar", @"")];
        alert.tag = 5;
        
        [alert show];
        
    } else if( x == 5 ) {
        
        NSLog(@"Startcounter ist bei 5");
        
        [[NSUserDefaults standardUserDefaults]setInteger:6 forKey:NSLocalizedString(@"timer", @"")];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"DataCounter", @"")
                                                        message:NSLocalizedString(@"DataCounterText", @"")
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"_cancel", @"")
                                              otherButtonTitles:nil];
        
        [alert addButtonWithTitle:NSLocalizedString(@"Install", @"")];
        alert.tag = 3;
        
        [alert show];
        
    } else if( x == 6 ) {
        
        NSLog(@"Startcounter ist bei 6");
        
        [[NSUserDefaults standardUserDefaults]setInteger:7 forKey:NSLocalizedString(@"timer", @"")];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"email", @"")
                                                        message:NSLocalizedString(@"emailtext", @"")
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"Ok", @"")
                                              otherButtonTitles:nil];
        
        [alert show];
        
    } else if( x == 7 ) {
        
        NSLog(@"Startcounter ist bei 7");
        
        [[NSUserDefaults standardUserDefaults]setInteger:8 forKey:NSLocalizedString(@"timer", @"")];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"_private_vault", @"")
                                                        message:NSLocalizedString(@"_private_vault_text", @"")
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"_cancel", @"")
                                              otherButtonTitles:NSLocalizedString(@"_Show_App", @""), nil];
        alert.tag = 2;
        
        [alert show];
        
    } else if( x == 8 ) {
        NSLog(@"Startcounter ist bei 8");
        NSLog(@"Startcounter geht zu 9!");
        
        [[NSUserDefaults standardUserDefaults]setInteger:9 forKey:NSLocalizedString(@"timer", @"")];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        BOOL inapp = [[NSUserDefaults standardUserDefaults] boolForKey:@"gekauft"];
        if(inapp == YES) {
            NSLog(@"Pro gekauft!");
        } else {
            
            NSLog(@"gekauft nein");
            
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"startsound"];
            [[NSUserDefaults standardUserDefaults] setBool:YES  forKey:@"adblocktrial"];
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"switch2"];
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"switch7"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            NSLog(@"AdBlocker & Downloader sind nun aus!");
            
            [[NSURLCache sharedURLCache] removeAllCachedResponses];
            NSLog(@"Cache gelöscht");
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:Nil forKey:@"thenewurl"];
            [defaults synchronize];
            NSLog(@"Startseite gelöscht");
            
            KeychainItemWrapper *wrapper = [[KeychainItemWrapper alloc] initWithIdentifier:NSLocalizedString(@"iphonevideodownloader", @"") accessGroup:nil];
            NSString *key = [wrapper objectForKey:(__bridge id)(kSecValueData)];
            
            BOOL inapp = [[NSUserDefaults standardUserDefaults] boolForKey:@"adblocktrial"];
            if(inapp == YES || [key isEqualToString:@"YES"]) {
                NSLog(@"TRIAL-WERT GESETZT");
                
                [wrapper setObject:@"YES" forKey:(__bridge id)(kSecValueData)];
            }
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"_adblockout", @"")
                                                            message:NSLocalizedString(@"_adblockouttext", @"")
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"Ok", @"")
                                                  otherButtonTitles:nil];
            [alert show];
        }
        
    } else if( x == 9 ) {
        
        NSLog(@"Startcounter ist und bleibt bei 9!");
        
        [[NSUserDefaults standardUserDefaults]setInteger:9 forKey:NSLocalizedString(@"timer", @"")];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
    
    float helligkeit = [[UIScreen mainScreen] brightness];
    [[NSUserDefaults standardUserDefaults] setFloat:helligkeit forKey:@"helligkeit"];
    
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    if ([UINavigationBar instancesRespondToSelector:@selector(setBarTintColor:)]) {
        [[UINavigationBar appearance] setBarTintColor:[UIColor blackColor]];
        [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
        [[UINavigationBar appearance] setBarStyle:UIBarStyleBlackTranslucent];
        
        [[UIToolbar appearance] setBarTintColor:[UIColor blackColor]];
        [[UIToolbar appearance] setTintColor:[UIColor whiteColor]];
        [[UIToolbar appearance] setBarStyle:UIBarStyleBlackTranslucent];
        
//        self.window.tintColor = [UIColor whiteColor];
        
    } else {
        [[UINavigationBar appearance] setTintColor:[UIColor blackColor]];
        [[UIToolbar appearance] setTintColor:[UIColor blackColor]];
    }
    
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    [self showPasscodeLockIfNeeded];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [[NSNotificationCenter defaultCenter] postNotificationName:kApplicationDidEnterBackground object:nil];
    
    [self showPasscodeLockIfNeeded];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [[NSNotificationCenter defaultCenter] postNotificationName:kApplicationWillEnterForeground object:nil];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [self showPasscodeLockIfNeeded];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [[NSNotificationCenter defaultCenter] postNotificationName:kApplicationDidEnterBackground object:nil];
}

- (void)application:(UIApplication *)app didReceiveLocalNotification:(UILocalNotification *)notif {
    NSLog(@"Recieved Notification %@",notif);
}

- (void)application:(UIApplication *)application handleEventsForBackgroundURLSession:(NSString *)identifier completionHandler:(void (^)())completionHandler
{
    self.backgroundTransferCompletionHandler = completionHandler;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    NSLog(@"\n\n%s\n\n",__PRETTY_FUNCTION__);
    return YES;
}

#pragma mark - Saving and Restoration
- (BOOL)application:(UIApplication*)application shouldSaveApplicationState:(NSCoder*)coder
{
    return YES;
}

- (BOOL)application:(UIApplication*)application shouldRestoreApplicationState:(NSCoder*)coder
{
    return YES;
}

#pragma mark - Private Methods
- (void)showPasscodeLockIfNeeded
{
    if ([[KKPasscodeLock sharedLock] isPasscodeRequired]) {
        KKPasscodeViewController *vc = [[KKPasscodeViewController alloc] initWithNibName:nil bundle:nil];
        vc.mode = KKPasscodeModeEnter;
        vc.delegate = self;
        
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
        
        [self.window.rootViewController presentViewController:nav animated:NO completion:nil];
    }
}

- (void)shouldEraseApplicationData:(KKPasscodeViewController*)viewController
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                    message:@"You have entered an incorrect passcode too many times. All account data in this app has been deleted."
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

- (SNSpecialWindow *)window
{
    static SNSpecialWindow *customWindow = nil;
    if (!customWindow) customWindow = [[SNSpecialWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    return customWindow;
}

#pragma mark - Public Methods
+ (void)initialize
{
    BOOL inapp = [[NSUserDefaults standardUserDefaults] boolForKey:@"gekauft"];
    if(inapp == YES) {
        NSLog(@"irate2 Pro ja!");
        
        [iRate sharedInstance].applicationBundleID = NSLocalizedString(@"full_id", @"");
        [iRate sharedInstance].onlyPromptIfLatestVersion = NO;
        [iRate sharedInstance].messageTitle = NSLocalizedString(@"RateMyApp", @"iRate message title");
        [iRate sharedInstance].message = NSLocalizedString(@"RateMyApptext", @"iRate message");
        [iRate sharedInstance].remindButtonLabel = NSLocalizedString(@"RemindMeLater", @"iRate remind button");
        [iRate sharedInstance].rateButtonLabel = NSLocalizedString(@"RateItNow", @"iRate accept button");
        [iRate sharedInstance].cancelButtonLabel = NSLocalizedString(@"NoRate", @"iRate decline button");
        
    } else {
        
        NSLog(@"irate2 Pro nein");
        
        [iRate sharedInstance].applicationBundleID = NSLocalizedString(@"full_id", @"");
        [iRate sharedInstance].onlyPromptIfLatestVersion = NO;
        [iRate sharedInstance].messageTitle = NSLocalizedString(@"_adblockout", @"iRate message title");
        [iRate sharedInstance].message = NSLocalizedString(@"_adblockouttext", @"iRate message");
        [iRate sharedInstance].cancelButtonLabel = NSLocalizedString(@"Ok", @"iRate decline button");
        
    }
}

+ (instancetype)sharedDelegate
{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

- (UIViewController*)topViewController {
    return [self topViewControllerWithRootViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

- (UIViewController*)topViewControllerWithRootViewController:(UIViewController*)rootViewController {
    if ([rootViewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController* tabBarController = (UITabBarController*)rootViewController;
        return [self topViewControllerWithRootViewController:tabBarController.selectedViewController];
    } else if ([rootViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController* navigationController = (UINavigationController*)rootViewController;
        return [self topViewControllerWithRootViewController:navigationController.visibleViewController];
    } else if (rootViewController.presentedViewController) {
        UIViewController* presentedViewController = rootViewController.presentedViewController;
        return [self topViewControllerWithRootViewController:presentedViewController];
    } else {
        return rootViewController;
    }
}

#pragma mark - AlertView Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 1 && buttonIndex == 1) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:NSLocalizedString(@"ipad_app_id", @"")]];
        // ipad version
        
    }
    if(alertView.tag == 2 && buttonIndex == 1) {
        
        NSString *deviceType = [UIDevice currentDevice].model;
        
        if([deviceType isEqualToString:@"iPad"]) {
            // it's an iPad
            NSLog(@"Es ist ein ipad");
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:NSLocalizedString(@"photovault_app_id_ipad", @"")]];
            // private vault
            
        } else {        // it's an iPhone
            NSLog(@"Es ist kein ipad");
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:NSLocalizedString(@"photovault_app_id", @"")]];
            // private vault
        }
    }
    
    if(alertView.tag == 3 && buttonIndex == 1) {
        
        NSString *deviceType = [UIDevice currentDevice].model;
        
        if([deviceType isEqualToString:@"iPad"]) {
            // it's an iPad
            NSLog(@"Es ist ein ipad");
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:NSLocalizedString(@"data_counter_app_id_ipad", @"")]];
            // private vault
        
        } else {        // it's an iPhone
            NSLog(@"Es ist kein ipad");
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:NSLocalizedString(@"data_counter_app_id", @"")]];
            // // Data Counter
        }
    }
    
    if(alertView.tag == 4 && buttonIndex == 1) {
        
        NSString *deviceType = [UIDevice currentDevice].model;
        
        if([deviceType isEqualToString:@"iPad"]) {
            // it's an iPad
            NSLog(@"Es ist ein ipad");
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:NSLocalizedString(@"ipad_app_id", @"")]];
            // private vault
        
        } else {        // it's an iPhone
            NSLog(@"Es ist kein ipad");
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:NSLocalizedString(@"eigene_app_id", @"")]];
            // // Data Counter
        }
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"UnlockedLevels"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        //rate to unlock - eigene app id
    }
    
    if(alertView.tag == 5 && buttonIndex == 1) {
        
        NSURL *fanPageURL = [NSURL URLWithString:@"fb://profile/1414465062105060"];
        
        if (![[UIApplication sharedApplication] openURL: fanPageURL]) {
            //fanPageURL failed to open.  Open the website in Safari instead
            NSURL *webURL = [NSURL URLWithString:@"https://www.facebook.com/pages/iPhone-Video-Downloader-with-Adblock-for-iphone-and-ipad/1414465062105060"];
            [[UIApplication sharedApplication] openURL: webURL];
        }
    }
}


@end
