//
//  DMManager.h
//  DownloadManager
//
//  Created by Denis on 29.12.14.
//  Copyright (c) 2014 ComputerVerse Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#import "DMBookmark+Category.h"
#import "DMCategories+Category.h"
#import "DMHistory+Category.h"

FOUNDATION_EXPORT NSString *const DMManagerModelNameValue;

@interface DMManager : NSObject

@property (readonly, strong, nonatomic) NSManagedObjectContext * managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel * managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator * persistentStoreCoordinator;

+ (DMManager *)sharedManager;
+ (NSManagedObjectContext *)managedObjectContext;
- (void)saveContext;

@end