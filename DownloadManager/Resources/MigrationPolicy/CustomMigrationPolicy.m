//
//  CustomMigrationPolicy.m
//  DownloadManager
//
//  Created by Denis on 10.02.15.
//  Copyright (c) 2015 Denis. All rights reserved.
//

#import "CustomMigrationPolicy.h"

@interface CustomMigrationPolicy ()
{
    NSInteger _idx;
}
@end

@implementation CustomMigrationPolicy

- (BOOL)beginEntityMapping:(NSEntityMapping *)mapping manager:(NSMigrationManager *)manager error:(NSError **)error
{
    _idx = 0;
    return YES;
}

- (BOOL)createDestinationInstancesForSourceInstance:(NSManagedObject*)source
                                      entityMapping:(NSEntityMapping*)map
                                            manager:(NSMigrationManager*)manager
                                              error:(NSError**)error
{   
    
    NSManagedObjectContext *destMOC = [manager destinationContext];
    NSString *destEntityName = [map destinationEntityName];
    
    NSManagedObject *dest = [NSEntityDescription insertNewObjectForEntityForName:destEntityName
                                                          inManagedObjectContext:destMOC];
    
    NSArray *keys = [[[source entity] attributesByName] allKeys];
    NSMutableArray *maValues = [NSMutableArray arrayWithCapacity:[keys count]];
    for (NSString *sKey in keys) {
        [maValues addObject:[source valueForKey:sKey]];
    }
    NSMutableDictionary *mDictValuesForKeys = [NSMutableDictionary dictionaryWithCapacity:[keys count]];
    for (NSInteger i=0; i<[keys count]; i++) {
        [mDictValuesForKeys setObject:[maValues objectAtIndex:i] forKey:[keys objectAtIndex:i]];
    }
    
    if ([destEntityName isEqualToString:@"DMBookmark"]) {
        _idx++;
        [mDictValuesForKeys setObject:[NSNumber numberWithInteger:_idx] forKey:@"bookmarkID"];
        [mDictValuesForKeys setObject:[NSNumber numberWithInteger:_idx] forKey:@"sortOrder"];
    }
    
    [dest setValuesForKeysWithDictionary:mDictValuesForKeys];
    
    [manager associateSourceInstance:source withDestinationInstance:dest forEntityMapping:map];
    
    return YES;
}

@end
